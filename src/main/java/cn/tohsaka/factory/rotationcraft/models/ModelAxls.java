package cn.tohsaka.factory.rotationcraft.models;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;

public class ModelAxls extends ModelBase {
    ModelRenderer mainshaft;
    public ModelAxls(){

        textureWidth = 128;
        textureHeight = 128;
        mainshaft = new ModelRenderer(this,0,0);
        mainshaft.addBox(-1,-1,-9,2,2,18);
        mainshaft.setRotationPoint(0F,0F,0);


        //new ModelResourceLocation("");
        //https://mcforge.readthedocs.io/en/1.13.x/models/advanced/introduction/
    }
    public ModelAxls(int width,int offset){

        textureWidth = 128;
        textureHeight = 128;
        mainshaft = new ModelRenderer(this,0,0);
        mainshaft.addBox(-1,-1,-9+offset,2,2,width);
        mainshaft.setRotationPoint(0F,0F,0);


        //new ModelResourceLocation("");
        //https://mcforge.readthedocs.io/en/1.13.x/models/advanced/introduction/
    }


    public void render(float size, float angle) {
        for(float i=0;i<=45;i+=45){//for(float i=-22.5F;i<=45;i+=22.5){
            setRotation(mainshaft, 0F, 0F, 0.0349066F + getRotation(getAbsoluteAngle(-angle+i)));
            mainshaft.render(size);
        }
        /*setRotation(mainshaft, 0F, 0F, 0.0349066F + getRotation(getAbsoluteAngle(-angle)));
        mainshaft.render(size);
        setRotation(mainshaft, 0F, 0F, 0.0349066F + getRotation(getAbsoluteAngle(-angle + 45)));
        mainshaft.render(size);*/
    }


    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    public float getRotation(double angle) {
        return ((float) angle / (float) 180) * (float) Math.PI;
    }

    public double getAbsoluteAngle(double angle) {
        return angle % 360;
    }

}
