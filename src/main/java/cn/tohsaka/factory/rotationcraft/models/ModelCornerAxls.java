package cn.tohsaka.factory.rotationcraft.models;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.renders.CornerRender;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.util.ResourceLocation;

public class ModelCornerAxls extends ModelBase {
    ModelRenderer mainshaft;
    ModelRenderer gear;
    ModelRenderer modelface;
    boolean drawface;
    public ModelCornerAxls(int width, int offset,boolean face){
        drawface = face;
        textureWidth = 128;
        textureHeight = 128;
        mainshaft = new ModelRenderer(this,0,0);
        mainshaft.addBox(-1,-1,-9+offset,2,2,width);
        mainshaft.setRotationPoint(0F,0F,0);

        gear = new ModelRenderer(this,0,0);
        gear.addBox(-2,-2,-3,4,4,1);


        modelface = new ModelRenderer(this,0,0);
        modelface.addBox(-7,-7,-8,14,14,1);
        modelface.setRotationPoint(0F,0F,0);
        //new ModelResourceLocation("");
        //https://mcforge.readthedocs.io/en/1.13.x/models/advanced/introduction/
    }

    public void render(float size, float angle, TextureManager textureManager) {
        for(float i=0;i<=45;i+=45){//for(float i=-22.5F;i<=45;i+=22.5){
            setRotation(mainshaft, 0F, 0F, 0.0349066F + getRotation(getAbsoluteAngle(-angle+i)));
            mainshaft.render(size);
            setRotation(gear, 0F, 0F, 0.0349066F + getRotation(getAbsoluteAngle(-angle+i)));
            gear.render(size);
        }
        if(drawface){
            if (textureManager != null)
            {
                textureManager.bindTexture(new ResourceLocation(RotationCraft.MOD_ID,"textures/render/corner.png"));
            }
            modelface.render(size);
        }
        /*setRotation(mainshaft, 0F, 0F, 0.0349066F + getRotation(getAbsoluteAngle(-angle)));
        mainshaft.render(size);
        setRotation(mainshaft, 0F, 0F, 0.0349066F + getRotation(getAbsoluteAngle(-angle + 45)));
        mainshaft.render(size);*/
    }


    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    public float getRotation(double angle) {
        return ((float) angle / (float) 180) * (float) Math.PI;
    }

    public double getAbsoluteAngle(double angle) {
        return angle % 360;
    }

}
