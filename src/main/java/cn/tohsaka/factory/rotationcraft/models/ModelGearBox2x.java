package cn.tohsaka.factory.rotationcraft.models;

import cn.tohsaka.factory.rotationcraft.prefab.render.GearboxModel;
import net.minecraft.client.model.ModelRenderer;
import cn.tohsaka.factory.rotationcraft.tiles.transmits.TileGearbox;

public class ModelGearBox2x extends GearboxModel {
    ModelRenderer mode0gear0;
    ModelRenderer mode0gear1;
    ModelRenderer mode1gear0;
    ModelRenderer mode1gear1;
    ModelRenderer subaxls;

    public ModelGearBox2x(){

        textureWidth = 128;
        textureHeight = 128;


        mode0gear0 = new ModelRenderer(this,0,0);
        mode0gear0.addBox(-2,-2,5,4,4,1);//big axis
        mode0gear0.setRotationPoint(0F,0F,0);

        mode0gear1 = new ModelRenderer(this,0,0);
        mode0gear1.addBox(-2,-2,-6,4,4,1);//big axis
        mode0gear1.setRotationPoint(-3,-3,0);

        mode1gear0 = new ModelRenderer(this,0,0);
        mode1gear0.addBox(-2,-2,-5,4,4,1);//big axis
        mode1gear0.setRotationPoint(0F,0F,0);

        mode1gear1 = new ModelRenderer(this,0,0);
        mode1gear1.addBox(-2,-2,5,4,4,1);//big axis
        mode1gear1.setRotationPoint(-3,-3,0);


        subaxls = new ModelRenderer(this,0,0);
        subaxls.addBox(-1,-1,-5,2,2,11);
        subaxls.setRotationPoint(-3,-3,0);


    }
    @Override
    public void render(float size, TileGearbox te) {
        super.render(size,te);
        for(float i=-22.5F;i<=45;i+=22.5){
            if(te.isDownshift()){
                setRotation(subaxls, 0F, 0F, 0.0349066F + getRotation(getAbsoluteAngle(((te.getInputAngle()/2)) + i)));
                setRotation(mode0gear0,0F,0F,outputshaft.rotateAngleZ+i);
                mode0gear0.render(size);
                setRotation(mode0gear1, 0F, 0F, subaxls.rotateAngleZ);
                mode0gear1.render(size);
            }else{
                //setRotation(subaxls, 0F, 0F, 0.0349066F + getRotation(getAbsoluteAngle((1-(te.getInputAngle()/2)) + i)));
                setRotation(subaxls, 0F, 0F, 0.0349066F + getRotation(getAbsoluteAngle((te.getInputAngle()) + i)));
                setRotation(mode1gear0,0F,0F,inputshaft.rotateAngleZ+i);
                mode1gear0.render(size);
                setRotation(mode1gear1, 0F, 0F, subaxls.rotateAngleZ);
                mode1gear1.render(size);
            }
            subaxls.render(size);
        }
    }


}
