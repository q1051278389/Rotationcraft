package cn.tohsaka.factory.rotationcraft.models;// Made with Blockbench 4.2.4
// Exported for Minecraft version 1.7 - 1.12
// Paste this class into your mod and generate all required imports


import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class turbine_new extends ModelBase {
	private final ModelRenderer bb_main;
	private final ModelRenderer face_r1;
	private final ModelRenderer face_r2;
	private final ModelRenderer face_r3;
	private final ModelRenderer right_r1;

	public turbine_new() {
		textureWidth = 128;
		textureHeight = 128;

		bb_main = new ModelRenderer(this);
		bb_main.setRotationPoint(0.0F, 24.0F, 0.0F);
		bb_main.cubeList.add(new ModelBox(bb_main, 40, 0, -8.0F, -1.0F, -8.0F, 16, 1, 16, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 40, 0, -8.0F, -16.0F, -8.0F, 16, 1, 16, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 15, 45, -8.0F, -15.0F, -7.0F, 1, 14, 14, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 54, 58, -7.0F, -15.0F, -7.99F, 14, 14, 1, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 21, 38, 2.0F, -10.0F, -8.0F, 4, 4, 1, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 14, 28, 7.0F, -15.0F, 7.0F, 1, 14, 1, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 14, 28, -8.0F, -15.0F, 7.0F, 1, 14, 1, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 14, 28, -8.0F, -15.0F, -8.0F, 1, 14, 1, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 14, 28, 7.0F, -15.0F, -8.0F, 1, 14, 1, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 21, 38, 2.0F, -10.0F, 7.0F, 4, 4, 1, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 54, 58, -7.0F, -15.0F, 7.99F, 14, 14, 1, 0.0F, false));

		face_r1 = new ModelRenderer(this);
		face_r1.setRotationPoint(0.0F, -8.0F, 15.0F);
		bb_main.addChild(face_r1);
		setRotationAngle(face_r1, 0.0F, 0.0F, -1.5708F);
		face_r1.cubeList.add(new ModelBox(face_r1, 21, 38, 2.0F, -2.0F, -8.0F, 4, 4, 1, 0.0F, false));
		face_r1.cubeList.add(new ModelBox(face_r1, 21, 38, 2.0F, -2.0F, -23.0F, 4, 4, 1, 0.0F, false));

		face_r2 = new ModelRenderer(this);
		face_r2.setRotationPoint(0.0F, -8.0F, 15.0F);
		bb_main.addChild(face_r2);
		setRotationAngle(face_r2, 0.0F, 0.0F, 1.5708F);
		face_r2.cubeList.add(new ModelBox(face_r2, 21, 38, 2.0F, -2.0F, -8.0F, 4, 4, 1, 0.0F, false));
		face_r2.cubeList.add(new ModelBox(face_r2, 21, 38, 2.0F, -2.0F, -23.0F, 4, 4, 1, 0.0F, false));

		face_r3 = new ModelRenderer(this);
		face_r3.setRotationPoint(0.0F, -8.0F, 15.0F);
		bb_main.addChild(face_r3);
		setRotationAngle(face_r3, 0.0F, 0.0F, -3.1416F);
		face_r3.cubeList.add(new ModelBox(face_r3, 21, 38, 2.0F, -2.0F, -8.0F, 4, 4, 1, 0.0F, false));
		face_r3.cubeList.add(new ModelBox(face_r3, 21, 38, 2.0F, -2.0F, -23.0F, 4, 4, 1, 0.0F, false));

		right_r1 = new ModelRenderer(this);
		right_r1.setRotationPoint(0.0F, 0.0F, 0.0F);
		bb_main.addChild(right_r1);
		setRotationAngle(right_r1, 0.0F, 3.1416F, 0.0F);
		right_r1.cubeList.add(new ModelBox(right_r1, 15, 45, -8.0F, -15.0F, -7.0F, 1, 14, 14, 0.0F, false));
	}

	@Override
	public void render(Entity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale) {
		render(scale);
	}
	public void render(float scale){
		bb_main.render(scale);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}