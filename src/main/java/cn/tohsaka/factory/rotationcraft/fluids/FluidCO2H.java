package cn.tohsaka.factory.rotationcraft.fluids;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import net.minecraft.block.Block;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;

import java.awt.*;

@GameInitializer
public class FluidCO2H extends Fluid {
    public FluidCO2H(String fluidName, ResourceLocation still, ResourceLocation flowing, Color color) {
        super(fluidName, still, flowing, color);
        setLuminosity(0);
        setTemperature(30);
        setBlock(null);
        FluidRegistry.registerFluid(this);
    }

    public static ResourceLocation still = new ResourceLocation(RotationCraft.MOD_ID,"fluid/co2h_still");
    public static ResourceLocation flow = new ResourceLocation(RotationCraft.MOD_ID,"fluid/co2h_still");

    public static void init(){
        FluidCO2H co2h = new FluidCO2H("co2h",still,flow,new Color(0xCCCCCC));
        FluidRegistry.addBucketForFluid(co2h);

        RotationCraft.fluids.put("co2h",co2h);
    }

    @Override
    public Fluid setBlock(Block block) {
        return this;
    }
}
