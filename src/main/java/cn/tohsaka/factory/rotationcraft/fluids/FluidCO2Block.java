package cn.tohsaka.factory.rotationcraft.fluids;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.MaterialLiquid;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.block.statemap.StateMapperBase;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fluids.BlockFluidClassic;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

import javax.annotation.Nonnull;

@GameInitializer(after = FluidCO2.class)
public class FluidCO2Block extends BlockFluidClassic implements IModelRegister {
    public static ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"co2");
    public FluidCO2Block() {
        super(RotationCraft.fluids.get("co2"), new MaterialLiquid(MapColor.GRAY_STAINED_HARDENED_CLAY));
        setRegistryName(NAME);
        setUnlocalizedName(getRegistryName().toString());
        ForgeRegistries.BLOCKS.register(this);

    }
    public static void init(){
        RotationCraft.blocks.put(NAME,new FluidCO2Block());
    }


    @Override
    public void registerModel() {
        ModelLoader.setCustomStateMapper(this, new StateMapperBase() {
            @Override
            protected ModelResourceLocation getModelResourceLocation(@Nonnull IBlockState state) {
                return new ModelResourceLocation(new ResourceLocation(RotationCraft.MOD_ID, "co2"),"normal");
            }
        });
    }
}
