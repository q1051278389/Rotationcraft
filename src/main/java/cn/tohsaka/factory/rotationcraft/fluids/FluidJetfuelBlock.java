package cn.tohsaka.factory.rotationcraft.fluids;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.MaterialLiquid;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.block.statemap.StateMapperBase;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fluids.BlockFluidClassic;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

import javax.annotation.Nonnull;

@GameInitializer(after = FluidLubricant.class)
public class FluidJetfuelBlock extends BlockFluidClassic implements IModelRegister {
    public static ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"jetfuel");
    public FluidJetfuelBlock() {
        super(RotationCraft.fluids.get("jetfuel"), new MaterialLiquid(MapColor.RED));
        setRegistryName(NAME);
        setUnlocalizedName(getRegistryName().toString());
        ForgeRegistries.BLOCKS.register(this);
    }
    public static void init(){
        RotationCraft.blocks.put(NAME,new FluidJetfuelBlock());
    }


    @Override
    public void registerModel() {
        ModelLoader.setCustomStateMapper(this, new StateMapperBase() {
            @Override
            protected ModelResourceLocation getModelResourceLocation(@Nonnull IBlockState state) {
                return new ModelResourceLocation(new ResourceLocation(RotationCraft.MOD_ID, "jetfuel"),"normal");
            }
        });
    }
}
