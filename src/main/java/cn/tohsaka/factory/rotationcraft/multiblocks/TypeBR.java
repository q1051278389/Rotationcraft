package cn.tohsaka.factory.rotationcraft.multiblocks;

import cn.tohsaka.factory.rotationcraft.multiblocks.tiles.TileReactorCore;
import cn.tohsaka.factory.rotationcraft.multiblocks.tiles.TileReactorIO;
import cn.tohsaka.factory.rotationcraft.multiblocks.tiles.TileReactorPart;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IStringSerializable;

import java.util.function.Supplier;

public enum TypeBR implements IStringSerializable {

        FRAME(TileReactorPart::new),
        CORE(TileReactorCore::new),
        PORT(TileReactorIO::new);

        private static final TypeBR[] VALUES = values();

        public static TypeBR getForMeta(int meta) {
            return VALUES[meta];
        }

        private final Supplier<TileEntity> tileFactory;

        TypeBR(Supplier<TileEntity> tileFactory) {
            this.tileFactory = tileFactory;
        }

        @Override
        public String getName() {
            return name().toLowerCase();
        }

        public int getMeta() {
            return ordinal();
        }

        public ItemStack newStack(int count) {
            return ItemStack.EMPTY;
            //return new ItemStack(, count, getMeta());
        }

        TileEntity createTileEntity() {
            return tileFactory.get();
        }



}
