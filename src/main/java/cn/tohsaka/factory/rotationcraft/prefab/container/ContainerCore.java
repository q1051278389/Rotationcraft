package cn.tohsaka.factory.rotationcraft.prefab.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerCore extends Container {
    public IInventory tile;
    public ContainerCore(IInventory te,InventoryPlayer playerInventory){
        this(te,playerInventory,8,84);
    }
    public ContainerCore(IInventory te,InventoryPlayer playerInventory,int x,int y){
        tile = te;
        xOffset = x;
        yOffset = y;
        bindPlayerInventory(playerInventory);
    }

    int xOffset = 8;
    int yOffset = 84;

    @Override
    public boolean canInteractWith(EntityPlayer playerIn) {
        return true;
    }

    protected void bindPlayerInventory(InventoryPlayer inventoryPlayer) {

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 9; j++) {
                addSlotToContainer(new Slot(inventoryPlayer, j + i * 9 + 9, xOffset + j * 18, yOffset + i * 18));
            }
        }
        for (int i = 0; i < 9; i++) {
            addSlotToContainer(new Slot(inventoryPlayer, i, xOffset + i * 18, yOffset + 58));
        }
    }
    public boolean hasInventory(){
        return false;
    }

    /*@Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slotIndex) {
        if (!supportsShiftClick(player, slotIndex)) {
            return ItemStack.EMPTY;
        }

        Slot slot = this.getSlot(slotIndex);
        if(slot!=null && slot.getHasStack()){
            ItemStack stack = slot.getStack();
            if(slotIndex >= player.inventory.mainInventory.size()){
                //transfer to player
                stack = transferStack(player.inventory,stack,0,player.inventory.mainInventory.size());
            }else{
                stack = transferStack(tile,stack,0,tile.getSizeInventory());
            }
        }
        return ItemStack.EMPTY;
    }*/

    @Override
    public ItemStack transferStackInSlot(EntityPlayer playerIn, int index)
    {
        if (!supportsShiftClick(playerIn, index)) {
            return ItemStack.EMPTY;
        }
        ItemStack itemstack = ItemStack.EMPTY;
        Slot slot = this.inventorySlots.get(index);

        if (slot != null && slot.getHasStack())
        {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();

            if (index < 36)
            {
                if (!this.mergeItemStack(itemstack1, 36, this.inventorySlots.size(), false))
                {
                    return ItemStack.EMPTY;
                }
            }
            else if (!this.mergeItemStack(itemstack1, 0, 36, false))
            {
                return ItemStack.EMPTY;
            }

            if (itemstack1.isEmpty())
            {
                slot.putStack(ItemStack.EMPTY);
            }
            else
            {
                slot.onSlotChanged();
            }
        }

        return itemstack;
    }


    protected boolean supportsShiftClick(EntityPlayer player, int slotIndex) {
        return true;
    }


    protected int getSizeInventory() {
        if (tile instanceof IInventory) {
            return ((IInventory) tile).getSizeInventory();
        }
        return 0;
    }




    public ItemStack transferStack(IInventory inventory, ItemStack output, int start, int end)
    {
        if (!output.isEmpty() && output.getCount() > 0)
        {
            for (int slot = start; slot < end; slot++)
            {
                if(output.isEmpty()){
                    break;
                }
                if(!inventory.isItemValidForSlot(slot,output)){
                    continue;
                }

                ItemStack s = inventory.getStackInSlot(slot);
                if (s.isEmpty())
                {
                    inventory.setInventorySlotContents(slot, output.copy());
                    output = ItemStack.EMPTY;
                    break;
                }
                else if (s.isItemEqual(output) && s.getCount() < s.getMaxStackSize())
                {
                    int count = Math.min(s.getMaxStackSize() - s.getCount(),output.getCount());
                    output.grow(-count);
                    s.grow(count);
                }
            }
        }
        return output;
    }
}
