package cn.tohsaka.factory.rotationcraft.prefab.tile;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IModelProvider;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.utils.FacingTool;
import cn.tohsaka.factory.rotationcraft.utils.RenderHelper2;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class TileModelMachine extends TileRotation implements IModelProvider {
    private RotaryModelBase model;
    private String modeltex;
    public TileModelMachine(){

    }
    public TileModelMachine(FacingTool.Position position){
        super(position);
    }
    @SideOnly(Side.CLIENT)
    public RotaryModelBase getModel(){
        return null;
    }
    @SideOnly(Side.CLIENT)
    public String getTexName(){
        return "";
    }

    @SideOnly(Side.CLIENT)
    public void renderTileEntityAt(double x, double y, double z){
        if(model==null){
            model = getModel();
        }
        if(modeltex==null || modeltex.equals("")){
            modeltex = getTexName();
        }


        {
            RenderHelper2.bindTexture(new ResourceLocation(RotationCraft.MOD_ID,"textures/render/"+getModelTex()+".png"));
            GlStateManager.pushMatrix();
            GlStateManager.enableRescaleNormal();
            GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
            GlStateManager.translate(x, y, z);
            GlStateManager.scale(1.0F, -1.0F, -1.0F);
            GlStateManager.translate(0.5F, 0.5F, 0.5F);
            GlStateManager.translate(0F,-2F,-1F);
            RenderUtils.rotate(getFacing(), 270, 90, 180, 0);
            beginRenderModelOnWorld();
        }
        if(this.getBlockType().getLightOpacity(world.getBlockState(pos),world,pos)>0){
            OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 240, 240);
        }
        model.renderAll(this,null,angle,0F);
        doFinalRender();
        GlStateManager.disableRescaleNormal();
        GlStateManager.popMatrix();
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
    }

    protected String getModelTex(){
        return modeltex;
    }

    public void beginRenderModelOnWorld(){

    }
    public void doFinalRender(){

    }
}
