package cn.tohsaka.factory.rotationcraft.prefab.tile;

import cn.tohsaka.factory.librotary.packet.PacketCustom;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.utils.FluidHelper;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemHandlerHelper;
import net.minecraftforge.items.wrapper.EmptyHandler;
import net.minecraftforge.items.wrapper.InvWrapper;
import net.minecraftforge.items.wrapper.SidedInvWrapper;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;

public class TileInvMachine extends TileMachineBase implements ISidedInventory {
    private int invsize = 0;
    protected ItemStack[] inventory = new ItemStack[]{};

    @Override
    public void getWailaBody(ItemStack itemStack,List<String> tooltip) {

    }

    @Override
    public int[] getSlotsForFace(EnumFacing enumFacing) {
        int[] slots = new int[invsize];
        for(int i=0;i<inventory.length;i++){
            slots[i] = i;
        }
        return slots;
    }

    @Override
    public boolean canInsertItem(int i, ItemStack itemStack, EnumFacing enumFacing) {
        SlotMode m = modeMap.get(i);
        if(m==null || modeMap.get(i)!=SlotMode.INPUT || modeMap.get(i)!=SlotMode.OMNI){
            return false;
        }
        if(!isItemValidForSlot(i,itemStack)){
            return false;
        }
        return true;
    }

    @Override
    public boolean canExtractItem(int i, ItemStack itemStack, EnumFacing enumFacing) {
        SlotMode m = modeMap.get(i);
        if(m==null || modeMap.get(i)!=SlotMode.OUTPUT || modeMap.get(i)!=SlotMode.OMNI){
            return false;
        }
        return true;
    }

    public enum SlotMode{
        NONE(-1),
        INPUT(0),
        OUTPUT(1),
        OMNI(2);

        int index;
        SlotMode(int i){
            index=i;
        }
    }
    private TileInvMachine INSTANCE;
    public TileInvMachine(){
        INSTANCE = this;
    }
    public void createSlots(int size){
        invsize = size;
        inventory = new ItemStack[invsize];
        Arrays.fill(inventory,ItemStack.EMPTY);
    }
    protected Map<Integer,SlotMode> modeMap = new HashMap<>();
    public TileInvMachine setSlotMode(SlotMode mode,int[] slots){
        for(int s:slots){
            setSlotMode(mode,s);
        }
        return this;
    }

    public TileInvMachine setSlotMode(SlotMode mode,int slot){
        modeMap.put(slot,mode);
        return this;
    }
    public TileInvMachine setSlotMode(SlotMode mode,int start,int end){
        for(int i=start;i<end;i++){
            modeMap.put(i,mode);
        }
        return this;
    }

    @Override
    public int getSizeInventory() {
        return invsize;
    }

    @Override
    public boolean isEmpty() {
        return Arrays.stream(inventory).allMatch(ItemStack::isEmpty);
    }

    @Override
    public ItemStack getStackInSlot(int index) {
        if(index>=invsize){
            return ItemStack.EMPTY;
        }
        return inventory[index];
    }

    /*@Override
    public ItemStack decrStackSize(int index, int count) {
        ItemStack item = inventory[index].copy();
        item.setCount(count);
        if(inventory[index].getCount()>count){
            inventory[index].setCount(inventory[index].getCount()-count);
        }else if(inventory[index].getCount()==count){
            inventory[index] = ItemStack.EMPTY;
        }else if(inventory[index].getCount()<count){
            item.setCount(inventory[index].getCount());
            inventory[index] = ItemStack.EMPTY;
        }
        setInventorySlotContents(index,inventory[index]);
        return item;
    }*/

    public ItemStack decrStackSize(int index, int count) {
        ItemStack itemstack = ItemStackHelper.getAndSplit(Arrays.asList(inventory), index, count);
        if (!itemstack.isEmpty()) {
            this.markDirty();
        }

        return itemstack;
    }

    @Override
    public ItemStack removeStackFromSlot(int slot) {
        if (inventory[slot].isEmpty()) {
            return ItemStack.EMPTY;
        }
        ItemStack stack = inventory[slot];
        inventory[slot] = ItemStack.EMPTY;
        return stack;
    }

    @Override
    public void setInventorySlotContents(int index, ItemStack stack) {
        inventory[index] = stack;
    }

    @Override
    public int getInventoryStackLimit() {
        return 64;
    }

    @Override
    public boolean isUsableByPlayer(EntityPlayer player) {
        return true;
    }

    @Override
    public void openInventory(EntityPlayer player) {

    }

    @Override
    public void closeInventory(EntityPlayer player) {

    }

    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        return true;
    }

    @Override
    public int getField(int id) {
        return 0;
    }

    @Override
    public void setField(int id, int value) {

    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public void clear() {
        Arrays.fill(inventory,ItemStack.EMPTY);
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public boolean hasCustomName() {
        return false;
    }

    public int process = 0;

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        if(compound.hasKey("inventory")){
            NBTTagList list = (NBTTagList) compound.getTag("inventory");
            for(int i=0;i<inventory.length;i++){
                inventory[i] = new ItemStack(list.getCompoundTagAt(i));
            }
        }
        if(compound.hasKey("process")){
            process = compound.getInteger("process");
        }
        if(compound.hasKey("isActive")){
            isActive = compound.getBoolean("isActive");
        }

        if(world != null && !world.isRemote){
            NetworkDispatcher.dispatchMachineUpdate(this);
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        NBTTagList list = new NBTTagList();
        for(ItemStack stack:inventory){
            list.appendTag(stack.serializeNBT());
        }
        compound.setTag("inventory",list);
        compound.setInteger("process",process);
        compound.setBoolean("isActive",isActive);
        return super.writeToNBT(compound);
    }

    @Override
    public void writeGuiData(PacketCustom packet, boolean isFullSync) {
        packet.writeNBTTagCompound(writeToNBT(new NBTTagCompound()));
        super.writeGuiData(packet, isFullSync);
    }

    @Override
    public void readGuiData(PacketCustom packet, boolean isFullSync) {
        super.readGuiData(packet, isFullSync);
        readFromNBT(packet.readNBTTagCompound());
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound tagCompound = super.getUpdateTag();
        tagCompound = writeToNBT(tagCompound);
        return tagCompound;
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tag) {
        super.handleUpdateTag(tag);
        readFromNBT(tag);
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY || super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        /*if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY){
            return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.cast(new ItemStackHandler(){
                @Nonnull
                @Override
                public ItemStack insertItem(int slot, @Nonnull ItemStack stack, boolean simulate) {
                    if(checkMode(slot,SlotMode.INPUT) && isItemValidForSlot(slot,stack)){
                        return InvUtils.insertItem(INSTANCE,slot,stack);
                    }
                    return stack;
                }

                @Nonnull
                @Override
                public ItemStack extractItem(int slot, int amount, boolean simulate) {
                    if(checkMode(slot,SlotMode.OUTPUT)){
                        return InvUtils.extractItem(INSTANCE,slot,amount);
                    }
                    return ItemStack.EMPTY;
                }
            });
        }*/
        if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
            return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.cast(new InvWrapper(this){
                @Nonnull
                @Override
                public ItemStack insertItem(int slot, @Nonnull ItemStack stack, boolean simulate) {
                    if(checkMode(slot,SlotMode.INPUT) && isItemValidForSlot(slot,stack)){
                        return super.insertItem(slot, stack, simulate);
                    }
                    return stack;
                }

                @Nonnull
                @Override
                public ItemStack extractItem(int slot, int amount, boolean simulate) {
                    if(checkMode(slot,SlotMode.OUTPUT)){
                        return super.extractItem(slot, amount, simulate);
                    }
                    return ItemStack.EMPTY;
                }

                @Override
                public boolean isItemValid(int slot, @Nonnull ItemStack stack) {
                    if(modeMap.containsKey(slot) && modeMap.get(slot) == SlotMode.OUTPUT){
                        return false;
                    }
                    return INSTANCE.isItemValidForSlot(slot,stack);
                }


            });
        }
        return super.getCapability(capability, facing);
    }



    public boolean checkMode(int slot,SlotMode need){
        if(modeMap.containsKey(slot)){
            SlotMode mode = modeMap.get(slot);
            if(mode!=SlotMode.NONE && mode == need || mode == SlotMode.OMNI){
                return true;
            }
        }
        return false;
    }

    @Override
    public void writeUpdateData(PacketCustom packet) {
        packet.writeNBTTagCompound(getUpdateTag());
        super.writeUpdateData(packet);
    }

    @Override
    public void readUpdatePacket(PacketCustom packet) {
        handleUpdateTag(packet.readNBTTagCompound());
        super.readUpdatePacket(packet);

    }

    @Override
    public void onBlockHarvested(EntityPlayer player) {
        if (!player.isCreative()){
            List<ItemStack> drops = new ArrayList<>();
            if(invsize>0){
                for(int i=0;i<invsize;i++){
                    if(canDropWithHarvested(i)){
                        ItemStack item = getStackInSlot(i).copy();
                        world.spawnEntity(new EntityItem(world,pos.getX()+0.5,pos.getY()+1,pos.getZ()+0.5,item));
                        setInventorySlotContents(i,ItemStack.EMPTY);
                    }
                }
            }
        }
    }

    public boolean canDropWithHarvested(int slot){
        return true;
    }


    public void transferOutputFluid(FluidTank fluidTank)
    {
        if (fluidTank.getFluidAmount() <= 0)
            return;

        int side;
        FluidStack output = new FluidStack(fluidTank.getFluid(), Math.min(fluidTank.getFluidAmount(), 16000));

        for(int i=2;i<6;i++){
            int toDrain = FluidHelper.insertFluidIntoAdjacentFluidHandler(this, EnumFacing.VALUES[i], output, true);
            if (toDrain > 0)
            {
                fluidTank.drain(toDrain, true);
                break;
            }
        }
        return;
    }

    public void transferOutputStack()
    {
        for(int slotid : modeMap.keySet()){
            if(modeMap.get(slotid).index > 0 && !getStackInSlot(slotid).isEmpty()){
                for(int i=0;i<6;i++){
                    transferItem(slotid, 64, EnumFacing.VALUES[i]);
                }
            }
        }


        return;
    }

    public boolean transferItem(int slot, int amount, EnumFacing side) {

        if (inventory[slot].isEmpty() || slot > inventory.length) {
            return false;
        }
        ItemStack initialStack = inventory[slot].copy();
        initialStack.setCount(Math.min(amount, initialStack.getCount()));
        TileEntity adjInv = getAdjacentTileEntity(this, side);

        if (isAccessibleOutput(adjInv, side)) {
            ItemStack inserted = addToInventory(adjInv, side, initialStack);

            if (inserted.getCount() >= initialStack.getCount()) {
                return false;
            }
            inventory[slot].shrink(initialStack.getCount() - inserted.getCount());
            if (inventory[slot].getCount() <= 0) {
                inventory[slot] = ItemStack.EMPTY;
            }
            return true;
        }
        return false;
    }


    public static TileEntity getAdjacentTileEntity(TileEntity refTile, EnumFacing dir) {

        return refTile == null ? null : getAdjacentTileEntity(refTile.getWorld(), refTile.getPos(), dir);
    }

    public static TileEntity getAdjacentTileEntity(World world, BlockPos pos, EnumFacing dir) {

        pos = pos.offset(dir);
        return world == null || !world.isBlockLoaded(pos) ? null : world.getTileEntity(pos);
    }

    public static ItemStack addToInventory(TileEntity tile, EnumFacing side, ItemStack stack) {

        if (stack.isEmpty()) {
            return ItemStack.EMPTY;
        }
        if (hasItemHandlerCap(tile, side.getOpposite())) {
            stack = insertStackIntoInventory(getItemHandlerCap(tile, side.getOpposite()), stack, false);
        }
        return stack;
    }

    public static boolean hasItemHandlerCap(TileEntity tileEntity, EnumFacing face) {
        return tileEntity != null && (tileEntity.hasCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, face) || tileEntity instanceof ISidedInventory || tileEntity instanceof IInventory);
    }

    public static ItemStack insertStackIntoInventory(IItemHandler handler, ItemStack stack, boolean simulate) {

        return insertStackIntoInventory(handler, stack, simulate, false);
    }

    public static ItemStack insertStackIntoInventory(IItemHandler handler, ItemStack stack, boolean simulate, boolean forceEmptySlot) {

        return forceEmptySlot ? ItemHandlerHelper.insertItem(handler, stack, simulate) : ItemHandlerHelper.insertItemStacked(handler, stack, simulate);
    }
    public static IItemHandler getItemHandlerCap(TileEntity tileEntity, EnumFacing face) {

        if (tileEntity.hasCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, face)) {
            return tileEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, face);
        } else if (tileEntity instanceof ISidedInventory && face != null) {
            return new SidedInvWrapper(((ISidedInventory) tileEntity), face);
        } else if (tileEntity instanceof IInventory) {
            return new InvWrapper(((IInventory) tileEntity));
        }
        return EmptyHandler.INSTANCE;
    }

    public static boolean isAccessibleOutput(TileEntity tile, EnumFacing side) {

        return hasItemHandlerCap(tile, side.getOpposite()) && getItemHandlerCap(tile, side.getOpposite()).getSlots() > 0;
    }
}
