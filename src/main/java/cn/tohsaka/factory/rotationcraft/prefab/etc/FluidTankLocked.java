package cn.tohsaka.factory.rotationcraft.prefab.etc;

import cn.tohsaka.factory.rotationcraft.utils.FluidHelper;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;

public class FluidTankLocked extends FluidTank {
    final Fluid lock;
    public FluidTankLocked(Fluid lockedFluid,int capacity) {
        super(capacity);
        lock = lockedFluid;
    }

    public Fluid getLock() {
        return lock;
    }

    @Override
    public boolean canFillFluidType(FluidStack fluid) {
        return FluidHelper.isFluidEqual(lock,fluid);
    }
}
