package cn.tohsaka.factory.rotationcraft.prefab.gui.elements;

import cn.tohsaka.factory.rotationcraft.prefab.etc.FluidTankLocked;
import cn.tohsaka.factory.rotationcraft.prefab.etc.FuelTank;
import cn.tohsaka.factory.rotationcraft.prefab.gui.ElementBase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.utils.FluidHelper;
import cn.tohsaka.factory.rotationcraft.utils.RenderHelper2;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;

import java.util.List;

public class ElementFuelTank extends ElementBase {

	public static final ResourceLocation TEXTURE = new ResourceLocation(ElementButtonBase.PATH_GUI+"rcelement/fueltank.png");

	protected FuelTank tank;
	protected FluidTank tank1;
	// If this is enabled, 1 pixel of fluid will always show in the tank as long as fluid is present.
	protected boolean alwaysShowMinimum = false;

	protected TextureAtlasSprite fluidTextureOverride;

	public ElementFuelTank(GuiMachinebase gui, int posX, int posY, FuelTank tank) {
		super(gui,posX,posY,7,56);
		this.tank =tank;
	}
	public ElementFuelTank(GuiMachinebase gui, int posX, int posY, FluidTank tank) {
		super(gui,posX,posY,7,56);
		this.tank1 =tank;
	}

	int type = 2;
	public ElementFuelTank setType(int c){
		type = c;
		return this;
	}
	int idletype = 0;
	public ElementFuelTank setIdleType(int t){
		idletype = t;
		return this;
	}

	@Override
	public void drawBackground(int mouseX, int mouseY, float gameTicks) {
		int sizeY = 0;
		if(tank==null){
			sizeY = Math.round((((float)tank1.getFluidAmount()) / ((float)tank1.getCapacity())) * 56);
		}else{
			sizeY = Math.round((((float)tank.getFuel()) / ((float)tank.getMaxCapacity())) * 56);
		}
		RenderHelper2.bindTexture(TEXTURE);
		drawTexturedModalRect(posX, posY, idletype * 7, 0, 7, 56);

		drawTexturedModalRect(posX, posY+(56-sizeY), type * 7, 56-sizeY, 7, sizeY);
	}

	@Override
	public void drawForeground(int mouseX, int mouseY) {

	}

	@Override
	public void addTooltip(List<String> list) {
		if(tank == null){
			if (tank1.getFluid() != null && tank1.getFluidAmount() > 0) {
				list.add(StringHelper.getFluidName(tank1.getFluid()));

				if (FluidHelper.isPotionFluid(tank1.getFluid())) {
					FluidHelper.addPotionTooltip(tank1.getFluid(), list, 1F);
				}
			}else if(tank1 instanceof FluidTankLocked){
				list.add(StringHelper.getFluidName(new FluidStack(((FluidTankLocked) tank1).getLock(),1)));
			}
			list.add(StringHelper.formatNumber(tank1.getFluidAmount()) + " / " + StringHelper.formatNumber(tank1.getCapacity()) + " mB");
		}else{
			if(tank.getCustomName().length()>0){
				list.add(StringHelper.localize(tank.getCustomName()));
				list.add(StringHelper.formatNumber(tank.getFuel()) + " / " + StringHelper.formatNumber(tank.getMaxCapacity()));
			}
		}
	}
}