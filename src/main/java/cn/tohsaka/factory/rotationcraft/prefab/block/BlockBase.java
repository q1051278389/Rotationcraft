package cn.tohsaka.factory.rotationcraft.prefab.block;


import cn.tohsaka.factory.librotary.api.power.interfaces.IPowerTranmitsAccelerator;
import cn.tohsaka.factory.rotationcraft.api.IUpgradable;
import cn.tohsaka.factory.rotationcraft.api.IUpgrade;
import cn.tohsaka.factory.rotationcraft.api.power.IBuildOptimizer;
import cn.tohsaka.factory.rotationcraft.config.GeneralConfig;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemScrewer;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileBase;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileMachineBase;
import cn.tohsaka.factory.rotationcraft.utils.FluidHelper;
import cn.tohsaka.factory.rotationcraft.utils.MathHelper;
import cn.tohsaka.factory.rotationcraft.utils.RotationUtils;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fml.common.eventhandler.Event;

@GameInitializer
public class BlockBase extends ModBlock {
    public static void init(){

    }
    public int getMaxmeta() {
        return 0;
    }

    public BlockBase(Material materialIn) {
        super(materialIn);
        setHardness(1F);
    }
    public static PropertyEnum FACING = PropertyEnum.create("facing",EnumFacing.class);
    public PropertyInteger VARIANT;
    @Override
    protected BlockStateContainer createBlockState() {
        if(getMaxmeta()>0){
            VARIANT = PropertyInteger.create("type",0,getMaxmeta());
            return new BlockStateContainer(this, new IProperty[] { FACING,VARIANT });
        }
        return new BlockStateContainer(this, new IProperty[] { FACING });
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        if(VARIANT!=null){
            return state.getValue(VARIANT);
        }
        return 0;
    }

    public IBlockState getStateForPlacement(World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer, EnumHand hand) {
        IBlockState state = this.getStateForPlacement(world, pos, facing, hitX, hitY, hitZ, meta, placer);
        if(this.VARIANT!=null){
            //int meta = placer.getHeldItem(hand).getMetadata();
            state = state.withProperty(VARIANT,meta);
        }
        return state;
    }


    @Override
    public IBlockState getActualState(IBlockState state, IBlockAccess worldIn, BlockPos pos) {
        TileEntity tileEntity = worldIn.getTileEntity(pos);
        if (tileEntity instanceof TileMachineBase) {
            TileMachineBase tile = (TileMachineBase) tileEntity;
            state = state.withProperty(FACING, tile.getFacing());
            if(VARIANT!=null){
                state = state.withProperty(VARIANT, tile.getMetadata());
            }
        }
        return super.getActualState(state, worldIn, pos);
    }


    @Override
    public void onBlockPlacedBy(World world, BlockPos pos, IBlockState state, EntityLivingBase player, ItemStack stack) {
        {
            if(this.VARIANT!=null){
                state = state.withProperty(VARIANT,stack.getMetadata());
            }
            {
                EnumFacing facing = RotationUtils.getPlacedRotationHorizontal(player);
                if(player.isSneaking() && facing.ordinal()>1) {
                    facing = facing.rotateY().rotateY();
                }
                state = state.withProperty(FACING,facing);
            }
            world.setBlockState(pos,state);
        }

        TileEntity tile = world.getTileEntity(pos);
        EnumFacing facing = null;
        if (tile instanceof TileMachineBase) {
            //EnumFacing facing = RotationUtils.getPlacedRotationHorizontal(player);
            if(((TileMachineBase) tile).allowVerticalFacing()){
                RayTraceResult rayTraceResult = rayTrace(world,(EntityPlayer) player,false);
                facing = rayTraceResult.sideHit;
            }else{
                facing = RotationUtils.getPlacedRotationHorizontal(player);
            }



            if(player.isSneaking()){
                if(facing.ordinal()>1){
                    facing = facing.rotateY().rotateY();
                }else{
                    /*if(facing == EnumFacing.UP){
                        facing = EnumFacing.DOWN;
                    }else{
                        facing = EnumFacing.UP;
                    }*/
                }
            }

            TileMachineBase machine = (TileMachineBase) tile;

            machine.setFacing(facing);
            machine.setMetadata(stack.getMetadata());
            machine.setPlacer((EntityPlayer) player);
        }

    }
    public void setDefaultState(){
        IBlockState ib = getBlockState().getBaseState().withProperty(FACING, EnumFacing.NORTH);
        if(VARIANT!=null){
            ib = ib.withProperty(VARIANT, 0);
        }
        setDefaultState(ib);
    }





    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if(hand == EnumHand.OFF_HAND){
            return true;
        }

        PlayerInteractEvent event = new PlayerInteractEvent.RightClickBlock(playerIn, hand, pos, facing, new Vec3d(hitX,hitY,hitZ));
        if (MinecraftForge.EVENT_BUS.post(event) || event.getResult() == Event.Result.DENY) {
            return false;
        }


        TileMachineBase tile = (TileMachineBase) worldIn.getTileEntity(pos);

        if (tile == null || !tile.canPlayerAccess(playerIn)) {
            return false;
        }

        /*if(worldIn.isRemote){
            return false;
        }
        if(playerIn.getHeldItem(hand).getItem() == Items.STICK && !playerIn.isSneaking()){
            TileMachineBase te = (TileMachineBase) worldIn.getTileEntity(pos);
            te.setFacing(RotationUtils.getPlacedRotationHorizontal(playerIn));
            return true;
        }*/

        ItemStack held = playerIn.getHeldItem(hand);


        if(held.getItem() instanceof ItemScrewer){
            return false;
        }

        if(held.getItem() instanceof IUpgrade && tile instanceof IUpgradable){
            if(((IUpgradable) tile).canUpgrade(held)){
                if(((IUpgradable) tile).installUpgrade(held,playerIn)){
                    held.shrink(1);
                    return true;
                }
            }
        }


        if(this instanceof IBuildOptimizer && GeneralConfig.buildoptimizer){

            if(held!=ItemStack.EMPTY && held.getItem() instanceof ItemBlock && ((ItemBlock)held.getItem()).getBlock() instanceof IBuildOptimizer){
                return true;
            }
        }


        if (FluidHelper.isFluidHandler(held) && tile.hasCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, facing))
        {
            IFluidHandler handler = tile.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY,facing);
            return FluidHelper.drainItemToHandler(held, handler, playerIn, hand);
        }

        TileMachineBase te = (TileMachineBase) worldIn.getTileEntity(pos);
        return te.openGui(playerIn);
    }



    /**
     * Called when a user uses the creative pick block button on this block
     *
     * @param state
     * @param target The full target the player is looking at
     * @param world
     * @param pos
     * @param player
     * @return A ItemStack to add to the player's inventory, empty itemstack if nothing should be added.
     */
    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
        if(VARIANT!=null){
            TileEntity tile = world.getTileEntity(pos);
            return new ItemStack(this,1,((TileMachineBase)tile).getMetadata());
        }
        return new ItemStack(this,1,0);
    }

    protected RayTraceResult rayTrace(World worldIn, EntityPlayer playerIn, boolean useLiquids)
    {
        float f = playerIn.rotationPitch;
        float f1 = playerIn.rotationYaw;
        double d0 = playerIn.posX;
        double d1 = playerIn.posY + (double)playerIn.getEyeHeight();
        double d2 = playerIn.posZ;
        Vec3d vec3d = new Vec3d(d0, d1, d2);
        double f2 = MathHelper.cos(-f1 * 0.017453292F - (float)Math.PI);
        double f3 = MathHelper.sin(-f1 * 0.017453292F - (float)Math.PI);
        double f4 = -MathHelper.cos(-f * 0.017453292F);
        double f5 = MathHelper.sin(-f * 0.017453292F);
        double f6 = f3 * f4;
        double f7 = f2 * f4;
        double d3 = playerIn.getEntityAttribute(EntityPlayer.REACH_DISTANCE).getAttributeValue();
        Vec3d vec3d1 = vec3d.addVector((double)f6 * d3, (double)f5 * d3, (double)f7 * d3);
        return worldIn.rayTraceBlocks(vec3d, vec3d1, useLiquids, !useLiquids, false);
        //return playerIn.rayTrace(4,0);
    }

    /**
     * This gets a complete list of items dropped from this block.
     *
     * @param drops   add all items this block drops to this drops list
     * @param world   The current world
     * @param pos     Block position in world
     * @param state   Current state
     * @param fortune Breakers fortune level
     */
    @Override
    public void getDrops(NonNullList<ItemStack> drops, IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {
        if(state.getBlock() instanceof BlockBase){
            int meta = 0;
            if(state.getBlock() instanceof BlockBase && ((BlockBase) state.getBlock()).VARIANT!=null){
                meta = state.getValue(((BlockBase) state.getBlock()).VARIANT);
            }
            drops.add(new ItemStack(state.getBlock(),1,meta));
            return ;
        }
        super.getDrops(drops, world, pos, state, fortune);
    }

    @Override
    public void onBlockHarvested(World world, BlockPos pos, IBlockState state, EntityPlayer player) {
        if(!world.isRemote){
            TileEntity te = world.getTileEntity(pos);
            if(te instanceof TileBase){
                ((TileBase)te).onBlockHarvested(player);
            }
        }
        super.onBlockHarvested(world,pos,state,player);
    }


    @Override
    public IBlockState getStateFromMeta(int meta) {
        IBlockState state = getDefaultState();
        if(VARIANT != null){
            state = state.withProperty(VARIANT,meta);
        }
        return state;
    }


    @Override
    public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos) {
        TileEntity te = worldIn.getTileEntity(pos);
        if(te!=null && te.hasWorld() && te instanceof IPowerTranmitsAccelerator){
            ((IPowerTranmitsAccelerator) te).onPowerChange();
        }
    }
}
