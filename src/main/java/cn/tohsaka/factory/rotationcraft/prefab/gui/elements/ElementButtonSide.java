package cn.tohsaka.factory.rotationcraft.prefab.gui.elements;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.prefab.gui.ElementBase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.utils.RenderHelper2;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;

import java.util.List;

public class ElementButtonSide extends ElementBase {

	private boolean tooltipLocalized = false;
	private boolean managedClicks;
	private String tooltip;
	private String text;
	private boolean isActive;
	private int id;
	private static ResourceLocation tex = new ResourceLocation(RotationCraft.MOD_ID,"textures/gui/elements/btntex.png");
	public ElementButtonSide(GuiMachinebase gui, int x, int y, String text) {
		super(gui,x,y);
		this.text = text;
		setSize(17,17);
	}
	public ElementButtonSide setActive(boolean isActive){
		this.isActive = isActive;
		return this;
	}
	public boolean toggle(){
		this.isActive = !isActive;
		return this.isActive;
	}

	public ElementButtonSide setText(String t){
		text=t;
		return this;
	}
	int offset = -1;
	public ElementButtonSide setText(String t, int o){
		text=t;
		offset = o;
		return this;
	}

	@Override
	public void drawBackground(int mouseX, int mouseY, float gameTicks) {

		GlStateManager.color(1, 1, 1, 1);
		RenderHelper2.bindTexture(tex);
		if (isEnabled()) {
			if (intersectsWith(mouseX, mouseY) || isActive) {
				drawTexturedModalRect(posX, posY, 0, 17, sizeX, sizeY);
			} else {
				drawTexturedModalRect(posX, posY, 0, 0, sizeX, sizeY);
			}
		} else {
			drawTexturedModalRect(posX, posY, 0, 34, 17, sizeY);
		}
		if(text!=null){
			FontRenderer fontRenderer = getFontRenderer();
			boolean u = fontRenderer.getUnicodeFlag();
			fontRenderer.setUnicodeFlag(false);
			fontRenderer.drawString(text,offset==-1?posX+6:offset,posY+5,0xFFFFFF,false);
			fontRenderer.setUnicodeFlag(u);
		}
	}

	@Override
	public void drawForeground(int mouseX, int mouseY) {

	}

	@Override
	public void addTooltip(List<String> list) {

		if (tooltip != null) {
			if (tooltipLocalized) {
				list.add(tooltip);
			} else {
				list.add(StringHelper.localize(tooltip));
			}
		}
	}


	@Override
	public boolean onMousePressed(int mouseX, int mouseY, int mouseButton) {
		if(isEnabled()){
			playSound();
			if(id>-1){
				gui.handleIDButtonClick(id,mouseButton);
			}else{
				gui.handleElementButtonClick(getName(), mouseButton);
			}
		}
		return true;
	}

	protected void playSound() {
		GuiMachinebase.playClickSound(1.0F);
	}



	public ElementButtonSide setid(int d){
		id = d;
		return this;
	}
	public int getid(){
		return id;
	}
}