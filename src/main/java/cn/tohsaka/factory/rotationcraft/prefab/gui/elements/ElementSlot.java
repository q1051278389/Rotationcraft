package cn.tohsaka.factory.rotationcraft.prefab.gui.elements;

import cn.tohsaka.factory.rotationcraft.prefab.gui.ElementBase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.utils.RenderHelper2;
import net.minecraft.util.ResourceLocation;

public class ElementSlot extends ElementBase {
	public static final ResourceLocation TEXTURE = new ResourceLocation(ElementButtonBase.PATH_ELEMENTS+"slot.png");
	public ElementSlot(GuiMachinebase gui, int posX, int posY) {
		super(gui, posX, posY);
		setTexture(TEXTURE.toString(),32,32);
	}

	public ElementSlot(GuiMachinebase gui, int posX, int posY,ResourceLocation tex) {
		super(gui, posX, posY);
		setTexture(tex.toString(),32,32);
	}

	@Override
	public void drawBackground(int mouseX, int mouseY, float gameTicks) {
		RenderHelper2.bindTexture(this.texture);
		drawTexturedModalRect(posX-1,posY-1,0,0,18,18);
	}

	@Override
	public void drawForeground(int mouseX, int mouseY) {

	}

}