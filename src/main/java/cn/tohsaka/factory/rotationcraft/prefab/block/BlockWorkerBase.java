package cn.tohsaka.factory.rotationcraft.prefab.block;

import cn.tohsaka.factory.rotationcraft.blocks.transfer.pipe.IPipeConnectable;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileMachineBase;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockWorkerBase extends Block implements IPipeConnectable {
    public BlockWorkerBase() {
        super(Material.ROCK);
    }



    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if(hand == EnumHand.OFF_HAND){
            return true;
        }


        TileMachineBase te = (TileMachineBase) worldIn.getTileEntity(pos);
        if(te!=null){
            return te.openGui(playerIn);
        }
        return false;
    }
}
