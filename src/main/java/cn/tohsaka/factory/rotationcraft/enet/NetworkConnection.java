package cn.tohsaka.factory.rotationcraft.enet;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;

import javax.annotation.Nullable;
import java.util.UUID;

public class NetworkConnection {
    public UUID nodeid;
    public BlockPos pos;
    public World world;
    public EnumFacing side;
    public String TargetName;
    public Node connectedBy;
    public NetworkConnection(Node node,World world,BlockPos pos,EnumFacing side,Node con,String name){
        nodeid = node.getUuid();
        this.world = world;
        this.pos = pos;
        this.side = side;
        this.TargetName = name;
        this.connectedBy = con;
    }
    public World getWorld(){
        return world;
    }
    public UUID getNodeid(){
        return nodeid;
    }
    public BlockPos getPos(){
        return pos;
    }
    public EnumFacing getSide(){
        return side;
    }
    public BlockPos getCapabilityPos(){
        return pos.offset(side,1);
    }
    @Nullable
    public <T> T getCapability(Capability<T> capability)
    {
        if(world==null || pos==null || side==null){
            return null;
        }
        TileEntity tileEntity = world.getTileEntity(pos.offset(side,1));
        if(tileEntity!=null){
            return tileEntity.getCapability(capability,getSide().getOpposite());
        }
        return null;
    }

    @Nullable
    public boolean hasCapability(Capability<?> capability)
    {
        if(world==null || pos==null || side==null){
            return false;
        }
        TileEntity tileEntity = world.getTileEntity(pos.offset(side,1));
        if(tileEntity!=null){
            return tileEntity.hasCapability(capability,getSide().getOpposite());
        }
        return false;
    }
}
