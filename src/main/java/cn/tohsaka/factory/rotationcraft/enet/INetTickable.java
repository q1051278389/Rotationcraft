package cn.tohsaka.factory.rotationcraft.enet;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Collection;

public interface INetTickable {
    public void tickNetwork();
    public boolean isNodeInvalid();
    public Collection<NetworkConnection> getConnections();
    public BlockPos getBlockPos();
    public World getWorld();
}
