package cn.tohsaka.factory.rotationcraft.enet;

import cn.tohsaka.factory.rotationcraft.utils.FacingTool;
import net.minecraft.util.ITickable;
import net.minecraftforge.common.capabilities.Capability;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class Network implements ITickable {
    private UUID uuid;
    private ConcurrentHashMap<UUID,Node> nodes = new ConcurrentHashMap<UUID,Node>();
    private static ConcurrentHashMap<UUID,UUID> netmap = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<UUID,Network> networks = new ConcurrentHashMap<>();
    public Network(){
        this.uuid = UUID.randomUUID();
        networks.put(uuid,this);
        NetworkTicker.instance.addElement(this);
    }

    public boolean isSameNet(Network network){
        return uuid.equals(network.uuid);
    }
    public static void createByNode(Node node){
        new Network().addNode(node);
    }

    public Network addNode(Node node){
        if(Network.netmap.containsKey(node.getUuid())){
            Network network = Network.getNetwork(Network.netmap.get(node.getUuid()));
            if(network!=null){
                network.removeNode(node);
            }
        }
        nodes.put(node.getUuid(),node);
        netmap.put(node.getUuid(),this.uuid);
        return this;
    }

    public static Network getNetwork(UUID uuid){
        return Network.networks.get(uuid);
    }
    public static Network getNetwork(Node node){
        if(node==null){
            return null;
        }
        if(netmap.containsKey(node.getUuid())){
            return getNetwork(netmap.get(node.getUuid()));
        }
        return null;
    }

    public static void mergeNetwork(Network... nets){
        Network newly = new Network();
        for (Network network : nets) {
            for (Node node : network.nodes.values()) {
                newly.addNode(node);
            }
        }
    }


    public Network removeNode(Node node){
        if(nodes.containsKey(node.getUuid())){
            netmap.remove(node.getUuid());
            nodes.remove(node.getUuid());
        }
        if(nodes.size()==0){
            networks.remove(this.uuid);
        }
        return this;
    }
    public UUID getUuid(){
        return uuid;
    }

    @Override
    public void update() {
        if(nodes.size()==0){
            networks.remove(this.uuid);
            NetworkTicker.instance.removeElement(this);
            return;
        }
        for (Node node : nodes.values()) {
            if(node.getTickable().isNodeInvalid()){
                removeNode(node);
                continue;
            }
            node.getTickable().tickNetwork();
        }
        return;
    }


    public Collection<NetworkConnection> getConnectionsNeared(Node base){
        if(nodes.values().size()==0){
            return Collections.EMPTY_LIST;
        }
        LinkedHashSet<NetworkConnection> connections = new LinkedHashSet<>();
        List<Node> sorted = nodes.values().size()>1 ? nodes.values().stream().sorted(new Comparator<Node>() {
            @Override
            public int compare(Node n1, Node n2) {
                int o1 = FacingTool.posOffset(n1.getTickable().getBlockPos(), base.getTickable().getBlockPos());
                int o2 = FacingTool.posOffset(n2.getTickable().getBlockPos(), base.getTickable().getBlockPos());
                return o1 - o2;
            }
        }).collect(Collectors.toList()):nodes.values().stream().collect(Collectors.toList());

        for (Node node : sorted) {
            INetTickable tickable = node.getTickable();
            if(!tickable.isNodeInvalid()) {
                connections.addAll(tickable.getConnections());
            }
        }
        return connections;
    }


    public Set<NetworkConnection> getConnections(){
        LinkedHashSet<NetworkConnection> connections = new LinkedHashSet<>();
        for (Node node : nodes.values()) {
            INetTickable tickable = node.getTickable();
            if(!tickable.isNodeInvalid()) {
                connections.addAll(tickable.getConnections());
            }
        }
        return connections;
    }


    public static Set<NetworkConnection> getConnections(Node node){
        if(node==null){
            return Collections.EMPTY_SET;
        }
        if(node.getNetwork().isPresent()){
            return node.getNetwork().get().getConnections();
        }
        return Collections.EMPTY_SET;
    }
    public <T> Map<NetworkConnection,T> getCapability(Capability<T> cap,Node node){
        LinkedHashSet<NetworkConnection> connections = ((LinkedHashSet<NetworkConnection>) ((node==null)?getConnections():getConnectionsNeared(node)));
        if(connections==null || connections.size()==0){
            return Collections.EMPTY_MAP;
        }
        LinkedHashMap<NetworkConnection,T> map = new LinkedHashMap<>();
        for (NetworkConnection connection : connections) {
            if(connection.hasCapability(cap)){
                T cap2 = connection.getCapability(cap);
                if(cap2!=null && connection.connectedBy.getUuid()!=node.getUuid()){
                    map.put(connection,cap2);
                }
            }
        }
        return map;
    }


    public void invalidate(){
        for (Node node : nodes.values()) {
            removeNode(node);
        }
        networks.remove(this.uuid);
    }
}
