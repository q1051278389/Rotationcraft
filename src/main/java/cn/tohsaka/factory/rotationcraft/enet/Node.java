package cn.tohsaka.factory.rotationcraft.enet;

import java.util.Optional;
import java.util.UUID;

public class Node{
    private INetTickable tickable;
    private UUID uuid;
    public Node(INetTickable tile,UUID uuid){
        this.tickable = tile;
        this.uuid = uuid;
    }

    public INetTickable getTickable() {
        return tickable;
    }
    public UUID getUuid(){
        return uuid;
    }
    public Optional<Network> getNetwork(){
        Network network = Network.getNetwork(this);
        if(network==null){
            return Optional.empty();
        }
        return Optional.of(Network.getNetwork(this));
    }
}
