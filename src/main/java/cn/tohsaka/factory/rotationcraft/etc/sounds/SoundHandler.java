package cn.tohsaka.factory.rotationcraft.etc.sounds;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.ISound;
import net.minecraft.client.audio.ITickableSound;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.audio.Sound;
import net.minecraft.client.audio.SoundEventAccessor;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.client.ForgeHooksClient;
import net.minecraftforge.client.event.sound.PlaySoundEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class SoundHandler {
    public static SoundHandler INSTANCE = new SoundHandler();

    private static Set<UUID> jetpackSounds = new HashSet<>();
    private static Set<UUID> gasmaskSounds = new HashSet<>();
    private static Set<UUID> flamethrowerSounds = new HashSet<>();

    private static Map<Long, ISound> soundMap = new HashMap<>();
    private static boolean IN_MUFFLED_CHECK = false;

    public static void clearPlayerSounds() {
        jetpackSounds.clear();
        gasmaskSounds.clear();
        flamethrowerSounds.clear();
    }

    public static void clearPlayerSounds(UUID uuid) {
        jetpackSounds.remove(uuid);
        gasmaskSounds.remove(uuid);
        flamethrowerSounds.remove(uuid);
    }


    public static void playSound(SoundEvent sound) {
        playSound(PositionedSoundRecord.getRecord(sound, 1.0F, 1.0F));
    }

    public static void playSound(ISound sound) {
        Minecraft.getMinecraft().getSoundHandler().playSound(sound);
    }

    public static ISound startTileSound(SoundEvent sound, float volume, BlockPos pos) {
        return startTileSound(sound.getRegistryName(),volume,pos,0);
    }
    public static ISound startTileSound(SoundEvent sound, float volume, BlockPos pos,int delay) {
        return startTileSound(sound.getRegistryName(),volume,pos,delay);
    }
    public static ISound startTileSound(ResourceLocation soundLoc, float volume, BlockPos pos,int delay) {
        // First, check to see if there's already a sound playing at the desired location
        ISound s = soundMap.get(pos.toLong());
        if(s != null){
            ResourceLocation rl = s.getSound().getSoundLocation();
            String[] p = rl.getResourcePath().split("/");

            if(!p[p.length-1].equals(soundLoc.getResourcePath())){
                stopTileSound(pos);
                s = null;
            }
        }
        if (s == null || !Minecraft.getMinecraft().getSoundHandler().isSoundPlaying(s)) {
            // No sound playing, start one up - we assume that tile sounds will play until explicitly stopped
            s = new PositionedSoundRecord(soundLoc, SoundCategory.BLOCKS, (float) (volume * 1.0F), 1.0f,
                  true, delay, ISound.AttenuationType.LINEAR, pos.getX() + 0.5f, pos.getY() + 0.5f, pos.getZ() + 0.5f) {
                @Override
                public float getVolume() {
                    if (this.sound == null) {
                        this.createAccessor(Minecraft.getMinecraft().getSoundHandler());
                    }
                    return super.getVolume();
                }
            };

            // Start the sound
            playSound(s);

            // N.B. By the time playSound returns, our expectation is that our wrapping-detector handler has fired
            // and dealt with any muting interceptions and, CRITICALLY, updated the soundMap with the final ISound.
            s = soundMap.get(pos.toLong());
        }

        return s;
    }

    public static void stopTileSound(BlockPos pos) {
        long posKey = pos.toLong();
        ISound s = soundMap.get(posKey);
        if (s != null) {
            // TODO: Saw some code that suggests there is a soundmap MC already tracks; investigate further
            // and maybe we can avoid this dedicated soundmap
            Minecraft.getMinecraft().getSoundHandler().stopSound(s);
            soundMap.remove(posKey);
        }
    }

    @SubscribeEvent(priority = EventPriority.LOWEST)
    public void onTilePlaySound(PlaySoundEvent event) {
        // Ignore any sound event which is null or is happening in a muffled check
        ISound resultSound = event.getResultSound();
        if (resultSound == null || IN_MUFFLED_CHECK) {
            return;
        }

        // Ignore any sound event outside this mod namespace
        ResourceLocation soundLoc = event.getSound().getSoundLocation();
        if (!soundLoc.getResourceDomain().equals(RotationCraft.MOD_ID)) {
            return;
        }

        resultSound = new TileSound(event.getSound(), resultSound.getVolume());
        event.setResultSound(resultSound);

        BlockPos pos = new BlockPos(resultSound.getXPosF() - 0.5f, resultSound.getYPosF() - 0.5f, resultSound.getZPosF() - 0.5);
        soundMap.put(pos.toLong(), resultSound);
    }

    private static class TileSound implements ITickableSound {

        private ISound original;
        private float volume;
        private boolean donePlaying = false;

        // Choose an interval between 60-80 ticks (3-4 seconds) to check for muffling changes. We do this
        // to ensure that not every tile sound tries to run on the same tick and thus create
        // uneven spikes of CPU usage
        private int checkInterval = 60 + ThreadLocalRandom.current().nextInt(20);

        private Minecraft mc = Minecraft.getMinecraft();

        TileSound(ISound original, float volume) {
            this.original = original;
            this.volume = volume * getMufflingFactor();
        }

        @Override
        public void update() {
            // Every configured interval, see if we need to adjust muffling
            if (mc.world.getTotalWorldTime() % checkInterval == 0) {

                // Run the event bus with the original sound. Note that we must making sure to set the GLOBAL/STATIC
                // flag that ensures we don't wrap already muffled sounds. This is...NOT ideal and makes some
                // significant (hopefully well-informed) assumptions about locking/ordering of all these calls.
                IN_MUFFLED_CHECK = true;
                ISound s = ForgeHooksClient.playSound(mc.getSoundHandler().sndManager, original);
                IN_MUFFLED_CHECK = false;

                if (s == this) {
                    // No filtering done, use the original sound's volume
                    volume = original.getVolume() * getMufflingFactor();
                } else if (s == null) {
                    // Full on mute; go ahead and shutdown
                    donePlaying = true;
                } else {
                    // Altered sound returned; adjust volume
                    volume = s.getVolume() * getMufflingFactor();
                }
            }
        }

        private float getMufflingFactor() {
            // Pull the TE from the sound position and see if supports muffling upgrades. If it does, calculate what
            // percentage of the original volume should be muted
            TileEntity te = mc.world.getTileEntity(new BlockPos(original.getXPosF(), original.getYPosF(), original.getZPosF()));
            return 1.0f;
        }

        @Override
        public boolean isDonePlaying() {
            return donePlaying;
        }

        @Override
        public float getVolume() {
            return volume;
        }

        @Nonnull
        @Override
        public ResourceLocation getSoundLocation() {
            return original.getSoundLocation();
        }

        @Nullable
        @Override
        public SoundEventAccessor createAccessor(@Nonnull net.minecraft.client.audio.SoundHandler handler) {
            return original.createAccessor(handler);
        }

        @Nonnull
        @Override
        public Sound getSound() {
            return original.getSound();
        }

        @Nonnull
        @Override
        public SoundCategory getCategory() {
            return original.getCategory();
        }

        @Override
        public boolean canRepeat() {
            return original.canRepeat();
        }

        @Override
        public int getRepeatDelay() {
            return original.getRepeatDelay();
        }

        @Override
        public float getPitch() {
            return original.getPitch();
        }

        @Override
        public float getXPosF() {
            return original.getXPosF();
        }

        @Override
        public float getYPosF() {
            return original.getYPosF();
        }

        @Override
        public float getZPosF() {
            return original.getZPosF();
        }

        @Nonnull
        @Override
        public AttenuationType getAttenuationType() {
            return original.getAttenuationType();
        }
    }
}