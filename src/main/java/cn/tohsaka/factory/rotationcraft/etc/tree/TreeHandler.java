package cn.tohsaka.factory.rotationcraft.etc.tree;

import cn.tohsaka.factory.rotationcraft.utils.Utils;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatList;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Collectors;

import static cn.tohsaka.factory.rotationcraft.etc.tree.FallingTreeUtils.isLeafBlock;
import static cn.tohsaka.factory.rotationcraft.etc.tree.FallingTreeUtils.isTreeBlock;

public class TreeHandler{
	@Nonnull
	public static Optional<Tree> getTree(@Nonnull World world, @Nonnull BlockPos blockPos){
		Block logBlock = world.getBlockState(blockPos).getBlock();
		if(!isTreeBlock(logBlock)){
			return Optional.empty();
		}
		Queue<BlockPos> toAnalyzePos = new LinkedList<>();
		Set<BlockPos> analyzedPos = new HashSet<>();
		Tree tree = new Tree(world, blockPos);
		toAnalyzePos.add(blockPos);
		while(!toAnalyzePos.isEmpty()){
			BlockPos analyzingPos = toAnalyzePos.remove();
			tree.addLog(analyzingPos);
			analyzedPos.add(analyzingPos);
			Collection<BlockPos> nearbyPos = neighborLogs(world, logBlock, analyzingPos, analyzedPos);
			nearbyPos.removeAll(analyzedPos);
			toAnalyzePos.addAll(nearbyPos.stream().filter(pos -> !toAnalyzePos.contains(pos)).collect(Collectors.toList()));
		}
		



		
		return Optional.of(tree);
	}

	private static long getLeavesAround(@Nonnull World world, @Nonnull BlockPos blockPos){
		return Arrays.stream(EnumFacing.values())
				.map(blockPos::offset)
				.filter(testPos -> isLeafBlock(world.getBlockState(testPos).getBlock()))
				.count();
	}

	@Nonnull
	private static Collection<BlockPos> neighborLogs(@Nonnull World world, @Nonnull Block logBlock, @Nonnull BlockPos blockPos, @Nonnull Collection<BlockPos> analyzedPos){
		List<BlockPos> neighborLogs = new LinkedList<>();
		final BlockPos.MutableBlockPos checkPos = new BlockPos.MutableBlockPos();
		for(int x = -1; x <= 1; x++){
			for(int z = -1; z <= 1; z++){
				for(int y = -1; y <= 1; y++){
					checkPos.setPos(blockPos.getX() + x, blockPos.getY() + y, blockPos.getZ() + z);
					if(!analyzedPos.contains(checkPos) && isSameTree(world, checkPos, logBlock)){
						neighborLogs.add(checkPos.toImmutable());
					}
				}
			}
		}
		neighborLogs.addAll(analyzedPos);
		return neighborLogs;
	}
	
	private static boolean isSameTree(@Nonnull World world, BlockPos checkBlockPos, Block parentLogBlock){
		Block checkBlock = world.getBlockState(checkBlockPos).getBlock();

		//return isTreeBlock(checkBlock); mixlog
		return checkBlock.equals(parentLogBlock);
	}
	
	public static boolean destroyInstant(@Nonnull Tree tree, @Nullable EntityPlayer player, @Nullable NonNullList<ItemStack> drops){
		final World world = tree.getWorld();
		double rawWeightedUsesLeft = tree.getLogCount();
		ItemStack tool = new ItemStack(Items.DIAMOND_AXE);
		NonNullList result = NonNullList.create();
		tree.getLogs().stream().limit((int) rawWeightedUsesLeft).forEachOrdered(logBlock -> {
			final IBlockState logState = world.getBlockState(logBlock);
			if(drops==null){
				player.addStat(StatList.getObjectBreakStats(Item.getItemFromBlock(logState.getBlock())));
				logState.getBlock().harvestBlock(world, player, logBlock, logState, world.getTileEntity(logBlock), tool);
				world.destroyBlock(logBlock, false);
			}else{
				logState.getBlock().getDrops(result,world,logBlock,logState,1);
				world.setBlockToAir(logBlock);

			}
		});
		final int radius = 6;
		if(radius > 0){
			tree.getLogs().stream().max(Comparator.comparingInt(BlockPos::getY)).ifPresent(topLog -> {
				BlockPos.MutableBlockPos checkPos = new BlockPos.MutableBlockPos();
				for(int dx = -radius; dx < radius; dx++){
					for(int dy = -radius; dy < radius; dy++){
						for(int dz = -radius; dz < radius; dz++){
							checkPos.setPos(topLog.getX() + dx, topLog.getY() + dy, topLog.getZ() + dz);
							final IBlockState checkState = world.getBlockState(checkPos);
							final Block checkBlock = checkState.getBlock();
							if(isLeafBlock(checkBlock)){
								if(drops==null){
									checkBlock.dropBlockAsItem(world, checkPos, checkState, 0);
									world.destroyBlock(checkPos, false);
								}else{
									checkBlock.getDrops(result,world,checkPos,checkState,1);
									world.setBlockToAir(checkPos);
								}
							}
						}
					}
				}
			});
		}
		Utils.sortStacks(result,drops);
		return true;
	}


}