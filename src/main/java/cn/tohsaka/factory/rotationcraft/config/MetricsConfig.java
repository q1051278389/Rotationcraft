package cn.tohsaka.factory.rotationcraft.config;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import net.minecraftforge.common.config.Config;

import java.util.UUID;

@Config(modid = RotationCraft.MOD_ID, type = Config.Type.INSTANCE, name = "Rotationcraft",category = "metrics")
public class MetricsConfig {
    public static boolean enabled = true;
    public static String serverUuid = UUID.randomUUID().toString();
    public static boolean logFailedRequests = true;
    public static boolean logSentData = true;
    public static boolean logResponseStatusText = true;
}
