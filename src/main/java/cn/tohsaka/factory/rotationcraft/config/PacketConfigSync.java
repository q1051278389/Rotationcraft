package cn.tohsaka.factory.rotationcraft.config;

import cn.tohsaka.factory.librotary.packet.PacketCustom;
import net.minecraft.nbt.NBTTagCompound;

import java.lang.reflect.Field;

import static cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher.NET_CHANNEL;

public class PacketConfigSync{
    public static PacketCustom getPacket(){
        PacketCustom packet = new PacketCustom(NET_CHANNEL, 127);
        NBTTagCompound tag = new NBTTagCompound();
        Field[] fields = GeneralConfig.class.getFields();
        try {
            for(Field field:fields){
                if(field.getType() == Boolean.class){
                    tag.setBoolean(field.getName(),field.getBoolean(GeneralConfig.class));
                }

                if(field.getType() == Integer.class){
                    tag.setInteger(field.getName(),field.getInt(GeneralConfig.class));
                }

                if(field.getType() == Float.class){
                    tag.setFloat(field.getName(),field.getFloat(GeneralConfig.class));
                }

                if(field.getType() == Long.class){
                    tag.setLong(field.getName(),field.getLong(GeneralConfig.class));
                }

                if(field.getType() == String.class){
                    tag.setString(field.getName(),(String) field.get(GeneralConfig.class));
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        packet.writeNBTTagCompound(tag);
        return packet;
    }

    public static void readPacket(PacketCustom packet){
        NBTTagCompound tag = packet.readNBTTagCompound();

        try {
            for (String name:tag.getKeySet()){
                Field field = GeneralConfig.class.getField(name);
                if(field!=null){
                    if(field.getType() == Boolean.class){
                        field.set(GeneralConfig.class,tag.getBoolean(name));
                    }

                    if(field.getType() == Integer.class){
                        field.set(GeneralConfig.class,tag.getInteger(name));
                    }

                    if(field.getType() == Float.class){
                        field.set(GeneralConfig.class,tag.getFloat(name));
                    }

                    if(field.getType() == Long.class){
                        field.set(GeneralConfig.class,tag.getLong(name));
                    }

                    if(field.getType() == String.class){
                        field.set(GeneralConfig.class,tag.getString(name));
                    }
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }
}
