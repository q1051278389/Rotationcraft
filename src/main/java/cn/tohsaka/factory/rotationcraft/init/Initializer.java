package cn.tohsaka.factory.rotationcraft.init;

import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.utils.ClassUtil;

import java.lang.reflect.Method;
import java.util.Collection;

public class Initializer {
    private static Collection<String> clazzs;
    public static void init(){
        fire("cn.tohsaka.factory.rotationcraft","init");
        /*init("cn.tohsaka.factory.rotationcraft.blocks","init");
        init("cn.tohsaka.factory.rotationcraft.items","init");
        init("cn.tohsaka.factory.rotationcraft.fluids","init");
        init("cn.tohsaka.factory.rotationcraft.fluids","init");*/
    }


    private static void fire(String pkg,String method){
        clazzs = ClassUtil.scan(pkg, GameInitializer.class);
        fire(method);
    }
    public static Class<?> firingClass = null;

    private static void fire(String method){
        for (String cn : clazzs) {
            try {
                Class<?> cls = Class.forName(cn);
                if(method.equals("postinit")){

                    if(!INeedPostInit.class.isAssignableFrom(cls)){
                        continue;
                    }
                }else if(method.equals("loadcomplete")){
                    if(!INeedLoadComplete.class.isAssignableFrom(cls)){
                        continue;
                    }
                }

                if(cls!=null){
                    Method method1 = cls.getMethod(method);
                    if(method1!=null){
                        firingClass = cls;
                        cls.getMethod(method).invoke(null);
                    }
                }

            }catch (Exception e){
                if(!(e instanceof NoSuchMethodException)){
                    e.printStackTrace();
                }
            }
        }
    }
    public static void init2(){
        fire("init2");
    }
    public static void posinit(){
        fire("postinit");
    }
    public static void loadcomplete(){
        fire("loadcomplete");
    }
}
