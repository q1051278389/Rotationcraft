package cn.tohsaka.factory.rotationcraft.crafting.scripts;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.blocks.base.BlockBlast;
import cn.tohsaka.factory.rotationcraft.blocks.base.BlockWorktable;
import cn.tohsaka.factory.rotationcraft.blocks.reactor.BlockConcrete;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.*;
import cn.tohsaka.factory.rotationcraft.utils.RecipeHelper;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;

@GameInitializer
public class VanillaRecipes{
    public static void init2(){
        //
        RecipeHelper.addShapelessRecipe(ItemMaterial.getStackById(49,2), "dustGold","dustRedstone");



        RecipeHelper.addShapedRecipe(
                new ItemStack(RotationCraft.blocks.get(BlockWorktable.NAME)),
                "AAA","BCB","BDB",'A', ItemMaterial.getStackById(1),'B',Blocks.BRICK_BLOCK,'C',Blocks.CRAFTING_TABLE,'D',Blocks.CHEST);

        RecipeHelper.addShapedRecipe(
                new ItemStack(RotationCraft.blocks.get(BlockBlast.NAME)),
                "AAA","ACA","BBB",'A', Blocks.STONEBRICK,'B',Blocks.BRICK_BLOCK,'C',Blocks.CHEST);

        RecipeHelper.addShapedRecipe(
                new ItemStack(RotationCraft.items.get(ItemScrewer.NAME)),
                " A ","B  ","   ",'A', Items.IRON_INGOT,'B',Blocks.PLANKS);

        RecipeHelper.addShapelessRecipe(
                new ItemStack(RotationCraft.items.get(ItemGuide.NAME)),Items.BOOK,Items.IRON_INGOT);


        FurnaceRecipes.instance().addSmelting(Items.COAL,new ItemStack(RotationCraft.items.get(ItemReactorMaterial.graphite)),0.1f);

        RecipeHelper.addShapedRecipe(new ItemStack(RotationCraft.items.get(ItemReactorMaterial.triso))," A ","ABA"," A ",'A',new ItemStack(RotationCraft.items.get(ItemReactorMaterial.graphite)),'B',new ItemStack(RotationCraft.items.get(ItemReactorMaterial.dustUranium)));
        RecipeHelper.addShapedRecipe(new ItemStack(RotationCraft.items.get(ItemReactorMaterial.triso),4),"CAC","ABA","CAC",'A',new ItemStack(RotationCraft.items.get(ItemReactorMaterial.graphite)),'B',new ItemStack(RotationCraft.items.get(ItemReactorMaterial.dustUranium)),'C',new ItemStack(RotationCraft.items.get(ItemReactorMaterial.dustEmerald)));


        RecipeHelper.addShapedRecipe(new ItemStack(BlockConcrete.itemBlock),"ABA","BAB","ABA",'A', Blocks.SAND,'B', Items.CLAY_BALL);


        RecipeHelper.addShapelessRecipe(
                new ItemStack(RotationCraft.items.get(ItemPortalGem.NAME)), Items.ENDER_EYE,Items.ENDER_PEARL);

        RecipeHelper.addShapelessRecipe(ItemMaterial.getStackById(85,1), ItemMaterial.getStackById(54),"dustCoal");
    }

}
