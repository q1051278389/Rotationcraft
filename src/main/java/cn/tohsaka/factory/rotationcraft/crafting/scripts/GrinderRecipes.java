package cn.tohsaka.factory.rotationcraft.crafting.scripts;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.blocks.base.BlockCanola;
import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.GrinderRecipe;
import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.init.INeedPostInit;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemCrushedOre;
import cn.tohsaka.factory.rotationcraft.items.ItemMaterial;
import cn.tohsaka.factory.rotationcraft.items.ItemReactorMaterial;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileGrinder;
import cn.tohsaka.factory.rotationcraft.utils.inventory.OreValidator;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.NonNullList;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.oredict.OreDictionary;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@GameInitializer
public class GrinderRecipes implements INeedPostInit {
    private static OreValidator oreValidator = new OreValidator();
    public static boolean initialized = false;
    public static List<ItemStack> stacksToRemove = new ArrayList<>();
    public static List<FluidStack> fluidToRemove = new ArrayList<>();
    public static void init(){
        oreValidator.addPrefix("ore");
        oreValidator.addPrefix("ingot");
        oreValidator.addPrefix("log");
        oreValidator.addPrefix("plank");
        oreValidator.addExact("sand");
        oreValidator.addExact("treeSapling");
        oreValidator.addExact("treeLeaves");
    }
    public static void postinit(){
        NonNullList<CraftingPattern> patternList = NonNullList.create();
        String[] oreNames = OreDictionary.getOreNames();
        for(String oreName:oreNames){
            if(oreValidator.validate(oreName)){
                addRecipe(oreName,patternList);
            }
        }

        plainRecipes(patternList);

        initialized = true;


        List<CraftingPattern> c =  patternList.stream().filter( craftingPattern -> {
            ItemStack output = craftingPattern.getOutput();
            for(ItemStack stack:stacksToRemove){
                return output.getItem().equals(stack.getItem()) && output.getMetadata() == stack.getMetadata();
            }
            return false;
        }).collect(Collectors.toList());

        for(CraftingPattern cc : c){
            patternList.remove(cc);
        }
        stacksToRemove.clear();

        A:for(FluidStack stack:fluidToRemove){
            B:for (CraftingPattern recipe:patternList){
                if(CraftingPattern.hasFluids(recipe.getData())>0){
                    FluidStack[] fs = CraftingPattern.getFluid(recipe.getData());
                    if(fs[0].isFluidEqual(stack)){
                        patternList.remove(recipe);
                    }
                }
            }
        }
        fluidToRemove.clear();


        Recipes.INSTANCE.addPattern(TileGrinder.class,patternList);
    }

    public static void plainRecipes(NonNullList<CraftingPattern> patternList){
        patternList.add(new GrinderRecipe(Ingredient.fromStacks(new ItemStack(BlockCanola.seed)),ItemStack.EMPTY,1,CraftingPattern.setFluid(new NBTTagCompound(),"fluid",new FluidStack(RotationCraft.fluids.get("lubricant"),1000))));

        patternList.add(new GrinderRecipe(new ItemStack(Blocks.NETHERRACK),ItemMaterial.getStackById(43)));
        patternList.add(new GrinderRecipe(new ItemStack(Blocks.SOUL_SAND),ItemMaterial.getStackById(44)));

        patternList.add(new GrinderRecipe(new ItemStack(Items.BONE),new ItemStack(Items.DYE,7,15)));
        patternList.add(new GrinderRecipe(new ItemStack(Items.REEDS),new ItemStack(Items.SUGAR,4)));
        patternList.add(new GrinderRecipe(new ItemStack(Items.BLAZE_ROD),new ItemStack(Items.BLAZE_POWDER,6)));
        patternList.add(new GrinderRecipe(new ItemStack(Blocks.MAGMA),new ItemStack(Items.MAGMA_CREAM,4)));


        patternList.add(new GrinderRecipe(new ItemStack(Items.EMERALD),new ItemStack(RotationCraft.items.get(ItemReactorMaterial.dustEmerald))));
        patternList.add(new GrinderRecipe(Ingredient.fromStacks(OreDictionary.getOres("ingotUranium").toArray(new ItemStack[]{})),new ItemStack(RotationCraft.items.get(ItemReactorMaterial.dustUranium))));
    }


    public static void addRecipe(String name, NonNullList<CraftingPattern> recipes){
        if(name.startsWith("log")){
            recipes.add(new GrinderRecipe(Ingredient.fromStacks(OreDictionary.getOres(name).toArray(new ItemStack[]{})), new ItemStack(Blocks.PLANKS)));
        }
        if(name.startsWith("plank")){
            recipes.add(new GrinderRecipe(Ingredient.fromStacks(OreDictionary.getOres(name).toArray(new ItemStack[]{})), ItemMaterial.getStackById(50,4)));
        }
        if(name.equalsIgnoreCase("treeLeaves")){
            recipes.add(new GrinderRecipe(Ingredient.fromStacks(OreDictionary.getOres(name).toArray(new ItemStack[]{})), ItemMaterial.getStackById(50,2)));
        }
        if(name.equalsIgnoreCase("treeSapling")){
            recipes.add(new GrinderRecipe(Ingredient.fromStacks(OreDictionary.getOres(name).toArray(new ItemStack[]{})), ItemMaterial.getStackById(50,2)));
        }

        if(name.startsWith("ore")){
            ItemStack[] stacks = OreDictionary.getOres(name).toArray(new ItemStack[]{});
            if(stacks.length>0 && !stacks[0].isEmpty()){
                recipes.add(new GrinderRecipe(Ingredient.fromStacks(OreDictionary.getOres(name).toArray(new ItemStack[]{})), ItemCrushedOre.createByOre(stacks[0],2,3)));
            }
        }
        if(name.startsWith("ingot")){
            ItemStack[] stacks = OreDictionary.getOres("dust"+name.substring(5)).toArray(new ItemStack[]{});
            if(stacks.length>0 && !stacks[0].isEmpty()){
                ItemStack[] stacks1 = OreDictionary.getOres(name).toArray(new ItemStack[]{});
                if(stacks1.length>0) {
                    recipes.add(new GrinderRecipe(Ingredient.fromStacks(stacks1), stacks[0]));
                }
            }
        }
    }
}
