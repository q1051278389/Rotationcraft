package cn.tohsaka.factory.rotationcraft.crafting;

import java.util.*;

public class Recipes {
    public static Recipes INSTANCE;
    public Recipes(){

    }
    static {
        INSTANCE = new Recipes();
    }
    public static void init(){

    }


    private Map<Class, List> patternMap = new HashMap();

    public List<CraftingPattern> getList(Class type){
        return Collections.unmodifiableList(patternMap.get(type));
    }
    public void addPattern(Class type,CraftingPattern pattern){
        if(!patternMap.containsKey(type)){
            patternMap.put(type,new ArrayList());
        }
        patternMap.get(type).add(pattern);
    }
    public void addPattern(Class type,List<CraftingPattern> pattern){
        if(!patternMap.containsKey(type)){
            patternMap.put(type,new ArrayList());
        }
        patternMap.get(type).addAll(pattern);
    }
}
