package cn.tohsaka.factory.rotationcraft.crafting;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.nbt.NBTTagCompound;

public class GrinderRecipe extends CraftingPattern{
    public Ingredient igd;
    public GrinderRecipe(Ingredient input, ItemStack output, int input_count, NBTTagCompound data) {
        super(null, output, null, data);
        data.setInteger("input_count",input_count);
        igd = input;
    }


    public GrinderRecipe(ItemStack input, ItemStack output) {
        this(Ingredient.fromStacks(input), output,input.getCount(),new NBTTagCompound());
    }

    public GrinderRecipe(Ingredient input, ItemStack output) {
        this(input, output,1,new NBTTagCompound());
    }


}
