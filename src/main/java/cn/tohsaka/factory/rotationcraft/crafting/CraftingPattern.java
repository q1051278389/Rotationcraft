package cn.tohsaka.factory.rotationcraft.crafting;

import cn.tohsaka.factory.rotationcraft.utils.ItemStackUtils;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.oredict.OreDictionary;

import java.util.*;


public class CraftingPattern{
    NBTTagCompound data;
    String[] input;
    ItemStack[] omni;
    ItemStack output;
    public CraftingPattern(){
        this.input = new String[]{};
        this.omni = new ItemStack[]{};
        this.output = ItemStack.EMPTY;
        this.data = null;
    }
    public CraftingPattern(String[] input, ItemStack output, ItemStack[] omni, NBTTagCompound data){
        this.input = input;
        this.omni = omni;
        this.output = output;
        this.data = data;
    }


    public CraftingPattern(String[] input,ItemStack output){
        this(input,output,new ItemStack[]{},new NBTTagCompound());
    }
    public CraftingPattern(String[] input,ItemStack output,NBTTagCompound tagCompound){
        this(input,output,new ItemStack[]{},tagCompound);
    }

    public CraftingPattern copy(){
        return new CraftingPattern(input.clone(),output.copy(),omni.clone(),data.copy());
    }

    public static boolean isStackEquals(String s, ItemStack stack) {
        String[] ss = s.split(",");
        if(ss[0].startsWith("ore:")){
            int[] oreid = OreDictionary.getOreIDs(stack);
            int oreid_i = OreDictionary.getOreID(ss[0].substring(4));
            for(int i:oreid){
                if(i == oreid_i){
                    return true;
                }
            }
            return false;
        }
        ItemStack sss = ItemStackUtils.parseItem(s);
        return sss.isItemEqual(stack);
    }

    public static boolean isStackEquals2(String s, ItemStack stack) {
        if(s.startsWith("ore")){
            int[] oreid = OreDictionary.getOreIDs(stack);
            int oreid_i = OreDictionary.getOreID(s);
            for(int i:oreid){
                if(OreDictionary.getOreName(i).equalsIgnoreCase(s)){
                    return true;
                }
            }
            return false;
        }
        ItemStack ss = ItemStackUtils.parseItem(s);
        return ss.isItemEqual(stack);
    }

    public String[] getInput() {
        return input;
    }

    public ItemStack getOutput() {
        return output;
    }

    public ItemStack[] getOmni() {
        return omni;
    }

    public NBTTagCompound getData() {
        return data;
    }
    public CraftingPattern setShapedless(){
        data.setBoolean("shapedless",true);
        return this;
    }
    public boolean isShapedless(){
        return data.hasKey("shapedless");
    }

    private ItemStack[] mergedinput = null;


    public ItemStack[] getMergedInput(){
        Map<String,ItemStack> item = new HashMap<>();
        for(String ii:input){
            ItemStack[] j = CraftingPattern.parseOreDict(ii);
            ItemStack i = j[0];
            if(!i.isEmpty() && i.getItem() != Items.AIR){
                String id = i.getUnlocalizedName()+":"+i.getMetadata();
                if(item.containsKey(id)){
                    item.get(id).setCount(item.get(id).getCount()+i.getCount());
                }else{
                    item.put(id,i.copy());
                }
            }
        }
        mergedinput = item.values().toArray(new ItemStack[]{});
        return mergedinput;
    }






    public static ItemStack[] parseOreDict(String str){
        if(str.equalsIgnoreCase("NULL")){
            return new ItemStack[]{ItemStack.EMPTY};
        }
        if(str.startsWith("ore:")){
            String[] ss = str.split(",");
            ItemStack[] s = OreDictionary.getOres(ss[0].substring(4)).toArray(new ItemStack[]{});
            if(ss.length>1){
                for(int i=0;i<s.length;i++){
                    s[i].setCount(Integer.valueOf(ss[1]));
                }
            }
            return s;
        }
        return new ItemStack[]{ItemStackUtils.parseItem(str)};
    }

    public static NBTTagCompound setFluid(NBTTagCompound tag,String name,FluidStack fluidStack){
        NBTTagList list;
        if(tag.hasKey("Fluids")){
            list = (NBTTagList) tag.getTag("Fluids");
        }else{
            list = new NBTTagList();
        }
        NBTTagCompound tt = new NBTTagCompound();
        tt.setString("NAME",name);
        fluidStack.writeToNBT(tt);
        list.appendTag(tt);
        tag.setTag("Fluids",list);
        return tag;
    }
    public static FluidStack[] getFluid(NBTTagCompound tag){
        List<FluidStack> fluids = new ArrayList<>();
        if(tag.hasKey("Fluids")){
            NBTTagList list = (NBTTagList) tag.getTag("Fluids");
            for(NBTBase tt : list){
                fluids.add(FluidStack.loadFluidStackFromNBT((NBTTagCompound) tt));
            }
            return fluids.toArray(new FluidStack[]{});
        }
        return null;
    }
    public static int hasFluids(NBTTagCompound tag){
        List<FluidStack> fluids = new ArrayList<>();
        if(tag.hasKey("Fluids")){
            NBTTagList list = (NBTTagList) tag.getTag("Fluids");
            return list.tagCount();
        }
        return 0;
    }

    public static NBTTagCompound setRedstone(NBTTagCompound tagCompound){
        tagCompound.setBoolean("redstone",true);
        return tagCompound;
    }










    public ItemStack[] isMatch(ItemStack[] itemStacks){
        List<ItemStack> out = new LinkedList<>();
        if(itemStacks.length<getInput().length){
            return null;
        }
        for(int i=0;i<getInput().length;i++){
            if(!CraftingPattern.isStackEquals(getInput()[i], itemStacks[i])){
                return null;
            }
            ItemStack s = itemStacks[i].copy();
            s.setCount(CraftingPattern.parseOreDict(getInput()[i])[0].getCount());
            out.add(s);
        }
        if(getInput().length < itemStacks.length && !Arrays.stream(itemStacks).skip(getInput().length).allMatch(ItemStack::isEmpty)){
            return null;
        }
        return out.toArray(itemStacks);
    }





}
