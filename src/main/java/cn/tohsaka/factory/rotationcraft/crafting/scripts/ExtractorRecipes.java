package cn.tohsaka.factory.rotationcraft.crafting.scripts;

import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.ExtractorRecipe;
import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.init.INeedPostInit;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileExtractor;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileFurnace;
import com.google.common.collect.LinkedHashMultimap;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraftforge.oredict.OreDictionary;

import java.util.Iterator;
import java.util.List;
import java.util.Random;

@GameInitializer
public class ExtractorRecipes implements INeedPostInit {
    private static final Random r = new Random();
    public static void postinit(){
        LinkedHashMultimap<String,ItemStack> map = LinkedHashMultimap.create();
        String[] oreNames = OreDictionary.getOreNames();
        int length = oreNames.length;

        for(int i = 0; i < length; ++i) {
            String name = oreNames[i];
            if (name != null && shouldCare(name)) {
                Iterator iter = OreDictionary.getOres(name).iterator();

                while(iter.hasNext()) {
                    ItemStack item = (ItemStack)iter.next();
                    if (!item.isEmpty()) {
                        map.put(name,item);
                    }
                }
            }
        }
        for(String name : map.keySet()){
            NonNullList<ItemStack> items = OreDictionary.getOres(name);
            if(items.size()==0){
                continue;
            }
            ItemStack smeltingResult = TileFurnace.getSmeltingResult(items.get(0),r).copy();

            if(name.startsWith("ore")){
                String fixedName = name.substring(3);
                if(map.containsKey("gem"+fixedName)){
                    NonNullList<ItemStack> out = OreDictionary.getOres("gem"+fixedName);
                    if(out.size()>0){
                        Recipes.INSTANCE.addPattern(TileExtractor.class,new ExtractorRecipe(OreDictionary.getOres(name),out.get(0)));
                    }
                }else if(map.containsKey("ingot"+fixedName)){
                    NonNullList<ItemStack> out = OreDictionary.getOres("ingot"+fixedName);
                    if(out.size()>0){
                        Recipes.INSTANCE.addPattern(TileExtractor.class,new ExtractorRecipe(OreDictionary.getOres(name),out.get(0)));
                    }
                }else if(!smeltingResult.isEmpty()){

                    Recipes.INSTANCE.addPattern(TileExtractor.class,new ExtractorRecipe(OreDictionary.getOres(name),smeltingResult));


                }else if(map.containsKey("crystal"+fixedName)){
                    NonNullList<ItemStack> out = OreDictionary.getOres("crystal"+fixedName);
                    if(out.size()>0){
                        Recipes.INSTANCE.addPattern(TileExtractor.class,new ExtractorRecipe(OreDictionary.getOres(name),out.get(0)));
                    }
                }else if(map.containsKey("dust"+fixedName)){
                    NonNullList<ItemStack> out = OreDictionary.getOres("dust"+fixedName);
                    if(out.size()>0){
                        Recipes.INSTANCE.addPattern(TileExtractor.class,new ExtractorRecipe(OreDictionary.getOres(name),out.get(0)));
                    }
                }
            }
        }
        List<CraftingPattern> patternList = Recipes.INSTANCE.getList(TileExtractor.class);
    }
    public static boolean shouldCare(String name){
        return (name.startsWith("ore") || name.startsWith("crystal") || name.startsWith("gem") || name.startsWith("ingot") || name.startsWith("dust"));
    }
}
