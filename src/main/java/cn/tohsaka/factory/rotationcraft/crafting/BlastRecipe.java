package cn.tohsaka.factory.rotationcraft.crafting;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.nbt.NBTTagCompound;

import java.util.LinkedList;
import java.util.List;

public class BlastRecipe extends CraftingPattern{
    public List<Ingredient> inputList = new LinkedList<>();
    public List<ItemStack[]>inputItem = new LinkedList<>();
    public List<Ingredient> cata = new LinkedList<>();
    public List<ItemStack[]> cataItem = new LinkedList<>();
    public BlastRecipe(String[] cata,String input[],ItemStack output,boolean shapeless) {
        this.output = output;
        for(String i:input){
            ItemStack[] s = parseOreDict(i);
            inputList.add(Ingredient.fromStacks(s));
            inputItem.add(s);
        }
        this.data = new NBTTagCompound();
        data.setBoolean("shapeless",shapeless);
        for(String c:cata){
            ItemStack[] stacks = parseOreDict(c);
            this.cata.add(Ingredient.fromStacks(stacks));
            this.cataItem.add(stacks);
        }
    }


    public BlastRecipe(ItemStack[] cata,ItemStack[] input,ItemStack output,int temp) {
        this.output = output;
        for(ItemStack i:input){
            inputList.add(Ingredient.fromStacks(i));
            inputItem.add(new ItemStack[]{i});
        }
        this.data = new NBTTagCompound();
        data.setBoolean("shapeless",false);
        data.setFloat("temp",temp);
        data.setFloat("bound",0);
        for(ItemStack c:cata){
            this.cata.add(Ingredient.fromStacks(c));
            this.cataItem.add(new ItemStack[]{c});
        }
    }


    public BlastRecipe(ItemStack[] cata,ItemStack input,ItemStack output,float bound,int temp) {
        this(cata,new ItemStack[]{input},output,temp);
        data.setFloat("bound",bound);
        data.setBoolean("shapeless",true);
        data.setFloat("bound",bound);
    }


    public BlastRecipe(String[] cata,String input,ItemStack output,float bound,float temp) {
        this(cata,new String[]{input},output,true);
        data.setFloat("bound",bound);
        data.setFloat("temp",temp);
    }
    public BlastRecipe(String[] cata,String input[],ItemStack output,float bound,float temp) {
        this(cata,input,output,false);
        data.setFloat("bound",bound);
        data.setFloat("temp",temp);
    }
    public float getBound(){
        if(data==null || !data.hasKey("bound")){
            return 0;
        }
        return data.getFloat("bound");
    }
    public float getTemp(){
        if(data==null || !data.hasKey("temp")){
            return 0F;
        }
        return data.getFloat("temp");
    }
    public boolean test(ItemStack[] stacks,ItemStack[] catalist){
        for(int i=0;i<cata.size();i++){
            if(!cata.get(i).test(catalist[i])){
                return false;
            }
        }
        if(isShapeless()){
            for(int i=0;i< stacks.length;i++){
                if(!stacks[i].isEmpty() && !inputList.get(0).test(stacks[i])){
                    return false;
                }
            }
            return true;
        }else{
            for(int i=0;i<9;i++){
                if(i>=stacks.length || !inputList.get(i).test(stacks[i])){
                    return false;
                }
            }
            return true;
        }
    }
    public boolean isShapeless(){
        return data.getBoolean("shapeless");
    }
}
