package cn.tohsaka.factory.rotationcraft.api;

public interface IControlableEngine {
    abstract float getEffective();
    abstract void setEffective(float effective);
}
