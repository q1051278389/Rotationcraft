package cn.tohsaka.factory.rotationcraft.api.power;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public interface IModeProvider {
    abstract boolean onChangingMode(World world, EntityPlayer player, BlockPos pos);
}
