package cn.tohsaka.factory.rotationcraft.api;

import cn.tohsaka.factory.librotary.packet.PacketCustom;

public interface IGuiPacketHandler {
    abstract void writeGuiData(PacketCustom packet, boolean isFullSync);
    abstract void readGuiData(PacketCustom packet, boolean isFullSync);
}
