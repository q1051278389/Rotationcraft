package cn.tohsaka.factory.rotationcraft.api;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.EnumFacing;

public interface IScrewable {
    abstract boolean screw(EnumFacing facing, EntityLivingBase player);
}
