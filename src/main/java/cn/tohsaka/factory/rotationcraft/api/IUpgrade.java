package cn.tohsaka.factory.rotationcraft.api;

import net.minecraft.item.ItemStack;

public interface IUpgrade{
    public boolean readyForUse(ItemStack stack);
}
