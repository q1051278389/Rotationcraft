package cn.tohsaka.factory.rotationcraft.api.power;

public interface IGearboxRenderProvider {
    public abstract float getInputAngle();
    public abstract float getOutputAngle();
    public abstract boolean isDownshift();//0 = downshift  1=upshift
}
