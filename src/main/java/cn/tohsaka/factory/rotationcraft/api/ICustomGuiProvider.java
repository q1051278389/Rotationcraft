package cn.tohsaka.factory.rotationcraft.api;

import net.minecraft.entity.player.EntityPlayer;

public interface ICustomGuiProvider {
    abstract Object getGuiClient(EntityPlayer player,int id);

    abstract Object getGuiServer(EntityPlayer player,int id);
}
