package cn.tohsaka.factory.rotationcraft.api;

import net.minecraft.util.EnumFacing;

public interface IFacing {
    EnumFacing getFacing();

    void setFacing(EnumFacing facing);
}
