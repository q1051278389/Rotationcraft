package cn.tohsaka.factory.rotationcraft.worldgen;

import net.minecraft.block.BlockOre;

public class OreGenDefinition {
    public BlockOre ore;
    public int minY;
    public int maxY;
    public int spawnChance;
    public int veinSize;
    public OreGenDefinition(BlockOre ore, int minY, int maxY, int spawnChance, int veinSize){
        this.ore = ore;
        this.minY = minY;
        this.maxY = maxY;
        this.spawnChance = spawnChance;
        this.veinSize = veinSize+3;

    }

}