package cn.tohsaka.factory.rotationcraft.tiles.power;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.blocks.engine.BlockDc;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.engine.ModelDC;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileEngine;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@GameInitializer(after = BlockDc.class)
public class TileDc extends TileEngine{
    public static void init(){
        GameRegistry.registerTileEntity(TileDc.class,new ResourceLocation(RotationCraft.MOD_ID,"tiledc"));
    }
    public TileDc(){
        super(new RotaryPower(4,256));
    }
    boolean isPowered = false;
    @Override
    public void tick() {
        boolean p = world.isBlockPowered(pos);
        if(p){
           isPowered = true;
           powerSource.setPower(new RotaryPower(4,256));
        }else{
            isPowered = false;
            powerSource.setPower(RotaryPower.ZERO);
        }
    }
    @SideOnly(Side.CLIENT)
    @Override
    public RotaryModelBase getModel() {
        return new ModelDC();
    }
    @SideOnly(Side.CLIENT)
    @Override
    public String getTexName() {
        return "dc";
    }
}
