package cn.tohsaka.factory.rotationcraft.tiles.power;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.power.IModeProvider;
import cn.tohsaka.factory.rotationcraft.blocks.engine.BlockCreativeEngine;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileEngine;
import cn.tohsaka.factory.rotationcraft.utils.AxlsUtils;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ITickable;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;

import java.util.List;

@GameInitializer(after = BlockCreativeEngine.class)
public class TileCreativeEngine extends TileEngine implements ITickable, IModeProvider {
    public TileCreativeEngine() {
        super(new RotaryPower(16384,65536));
    }

    public static void init(){
        GameRegistry.registerTileEntity(TileCreativeEngine.class,new ResourceLocation(RotationCraft.MOD_ID,"tilecreativeengine"));
    }

    @Override
    public void update() {

        if(world.isRemote){
            angle = AxlsUtils.calcAngle(angle,powerSource.getPower().getSpeed());
        }else {
            powerSource.setPower(new RotaryPower((32*level),(int) (128*level),angle,1));
        }
    }



    @Override
    public boolean isActive() {
        return true;
    }



    private int level = 1;

    @Override
    public boolean onChangingMode(World world, EntityPlayer player, BlockPos pos) {
        if(!world.isRemote){
            level*=2;//1 2 4 8 16 32,64,128,256,512,1024
            if(level>65536){
                level=1;
            }

            player.sendMessage(new TextComponentString(String.format("Mode change to: %s / %s",(32*level),(128*level))));
            markDirty();
        }
        return true;
    }

    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {

    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        level = Math.max(1,compound.getInteger("level"));
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        compound.setInteger("level",level);
        return compound;
    }
}
