package cn.tohsaka.factory.rotationcraft.tiles.reactor;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fml.common.registry.GameRegistry;

import javax.annotation.Nullable;

@GameInitializer
public class TileBedIO extends TileBedFrame {
    public static void init(){
        GameRegistry.registerTileEntity(TileBedIO.class,new ResourceLocation(RotationCraft.MOD_ID,"tilebedio"));
    }
    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        return isFramed() && capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY && facing.ordinal() < 2;
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        if(isFramed() && capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY && facing.ordinal() < 2){
            return getCore().getCapability(capability,facing);
        }
        return super.getCapability(capability, facing);
    }
}
