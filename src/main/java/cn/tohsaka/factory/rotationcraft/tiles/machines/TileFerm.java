package cn.tohsaka.factory.rotationcraft.tiles.machines;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.blocks.machine.BlockFerm;
import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerFerm;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.GuiFerm;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileRotation;
import cn.tohsaka.factory.rotationcraft.props.DamageSources;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import cn.tohsaka.factory.rotationcraft.utils.Utils;
import cn.tohsaka.factory.rotationcraft.utils.WorldUtils;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.FluidTankInfo;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.FluidTankProperties;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.common.registry.GameRegistry;

import javax.annotation.Nullable;
import java.util.List;

@GameInitializer(after = BlockFerm.class)
public class TileFerm extends TileRotation implements IGuiProvider {
    public TileFerm(){
        createSlots(3);
        setSlotMode(SlotMode.INPUT,new int[]{0,1});
        setSlotMode(SlotMode.OUTPUT,2);
    }

    public FluidTank fluidTank = new FluidTank(16000);

    public static void init(){
        GameRegistry.registerTileEntity(TileFerm.class,new ResourceLocation(RotationCraft.MOD_ID,"tileferm"));
    }

    @Override
    public Object getGuiClient(EntityPlayer player) {
        return new GuiFerm(this,player.inventory);
    }

    @Override
    public Object getGuiServer(EntityPlayer player) {
        return new ContainerFerm(this,player.inventory);
    }

    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        if(index == 2){
            return false;
        }
        if(getStackInSlot(index).isItemEqual(stack)){
            return true;
        }
        List<CraftingPattern> recipes = Recipes.INSTANCE.getList(TileFerm.class);
        boolean isPowered = isRedStonePowered();
        for(CraftingPattern recipe:recipes){
            boolean reqrs = false;
            NBTTagCompound tag = recipe.getData();
            if(tag.hasKey("redstone")){
                reqrs = true;
            }

            if(reqrs!=isPowered){
                continue;
            }

            if(CraftingPattern.isStackEquals(recipe.getInput()[index],stack)){
                return true;
            }
        }
        return false;
    }

    @Override
    public void update() {
        if(this.world.getBiome(pos) == net.minecraft.init.Biomes.HELL){
            WorldUtils.makeExplode(pos,16,world,99999, null, DamageSources.FERM_EXPLODE);
            world.removeTileEntity(pos);
            world.setBlockToAir(pos);
            return;
        }
        super.update();
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        return capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY || super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY){
            return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(new IFluidHandler()
            {
                @Override
                public IFluidTankProperties[] getTankProperties()
                {
                    FluidTankInfo info = fluidTank.getInfo();
                    return new IFluidTankProperties[] { new FluidTankProperties(info.fluid, info.capacity, true, false) };
                }

                @Override
                public int fill(FluidStack resource, boolean doFill)
                {
                    if(!resource.getFluid().equals(fluidTank.getFluid())){
                        List<CraftingPattern> recipes = Recipes.INSTANCE.getList(TileFerm.class);
                        boolean pass = false;
                        for(CraftingPattern recipe:recipes){
                            if(CraftingPattern.hasFluids(recipe.getData())>0 && resource.isFluidEqual(CraftingPattern.getFluid(recipe.getData())[0])){

                                pass = true;
                            }
                        }
                        if(pass == false){
                            return 0;
                        }
                    }
                    int filled = fluidTank.fill(resource, doFill);
                    if(filled>0){
                        setSomthingchanged();
                    }
                    return filled;
                }

                @Nullable
                @Override
                public FluidStack drain(FluidStack resource, boolean doDrain)
                {
                    return null;
                }

                @Nullable
                @Override
                public FluidStack drain(int maxDrain, boolean doDrain)
                {

                    return null;
                }
            });
        }
        return super.getCapability(capability, facing);
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        if(compound.hasKey("tank1")){
            fluidTank.readFromNBT(compound.getCompoundTag("tank1"));
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        NBTTagCompound tank1 = fluidTank.writeToNBT(new NBTTagCompound());
        compound.setTag("tank1",tank1);
        return super.writeToNBT(compound);
    }


    @Override
    public RotaryPower getMinimumPower() {
        return new RotaryPower(32,128,0,0);
    }

    @Override
    public boolean canStart() {
        return powerMachine.getPower().getTorque()>=32 && powerMachine.getPower().getSpeed()>=128;
    }

    CraftingPattern pattern;
    @Override
    public boolean processStart() {
        if(inventory[0].isEmpty() || inventory[1].isEmpty()){
            return false;
        }

        if(!inventory[2].isEmpty() && inventory[2].getCount()>= inventory[2].getMaxStackSize()){
            return false;
        }

        pattern = findPattern();
        if(pattern == null){
            return false;
        }
        ItemStack input0 = CraftingPattern.parseOreDict(pattern.getInput()[0])[0];
        ItemStack input1 = CraftingPattern.parseOreDict(pattern.getInput()[1])[0];
        FluidStack fluid = CraftingPattern.hasFluids(pattern.getData())>0?CraftingPattern.getFluid(pattern.getData())[0]:null;
        if(input0.getCount()> inventory[0].getCount() || input1.getCount() > inventory[1].getCount() || (fluid!=null && fluid.amount > fluidTank.getFluid().amount)){
            return false;
        }
        this.decrStackSize(0,input0.getCount());
        this.decrStackSize(1,input1.getCount());
        if(fluid!=null){
            fluidTank.drain(fluid.amount,true);
        }
        processMax = processRem = getTempBasedProcessTime(basetime);
        return true;
    }

    int basetime = 1600;

    @Override
    public int getProcessTickMinus() {
        return 40;
    }

    @Override
    public void processTick() {
        processRem -= basetime / getProcessTickMinus();
    }

    @Override
    public void processEnd() {
        if(pattern!=null){
            Utils.distributeOutput(this,pattern.getOutput().copy(),2,3,true);
        }
    }

    public CraftingPattern findPattern(){
        List<CraftingPattern> recipes = Recipes.INSTANCE.getList(TileFerm.class);
        boolean isPowered = isRedStonePowered();
        for(CraftingPattern recipe:recipes){
            NBTTagCompound tag = recipe.getData();
            String[] stack = recipe.getInput();
            if(isPowered != tag.hasKey("redstone")){
                continue;
            }
            if(CraftingPattern.isStackEquals(stack[0],getStackInSlot(0)) && CraftingPattern.isStackEquals(stack[1],getStackInSlot(1))){
                if(CraftingPattern.hasFluids(tag)>0){
                    if(fluidTank.getFluid()!=null && fluidTank.getFluid().isFluidEqual(CraftingPattern.getFluid(recipe.getData())[0])){
                        return recipe.copy();
                    }else {
                        continue;
                    }
                }else{
                    return recipe.copy();
                }
            }
        }
        return null;
    }
    public static int[] besttemp = new int[]{40,50};

    public int getTempBasedProcessTime(int basetime){
        float t = this.getTemp();//90
        if(t >= besttemp[0] && t <= besttemp[1]){
            return calcProcessTime(basetime);
        }
        float s = Math.abs(t - ((besttemp[0]+besttemp[1])/2));//90 - 45 = 45

        return calcProcessTime(basetime * (s/10));
    }

    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {
        super.getWailaBody(itemStack, tooltip);
        tooltip.add(StringHelper.localize("info.rc.waila.names.mode")+(isRedStonePowered()?0:1));
    }
}
