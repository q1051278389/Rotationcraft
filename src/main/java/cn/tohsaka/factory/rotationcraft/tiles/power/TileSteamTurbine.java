package cn.tohsaka.factory.rotationcraft.tiles.power;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IScrewable;
import cn.tohsaka.factory.rotationcraft.api.IUpgradable;
import cn.tohsaka.factory.rotationcraft.api.IUpgrade;
import cn.tohsaka.factory.rotationcraft.blocks.engine.BlockSteamEngine;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.engine.ModelSteamTurbine;
import cn.tohsaka.factory.rotationcraft.etc.sounds.SoundHandler;
import cn.tohsaka.factory.rotationcraft.init.Sounds;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemUpgrade;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileEngine;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.List;

@GameInitializer(after = BlockSteamEngine.class)
public class TileSteamTurbine extends TileEngine implements IUpgradable, IScrewable {
    public float temp;
    public FluidTank tank = new FluidTank(64000);
    public static void init(){
        GameRegistry.registerTileEntity(TileSteamTurbine.class,new ResourceLocation(RotationCraft.MOD_ID,"tilesteamturbine"));
    }
    public TileSteamTurbine(){
        super(new RotaryPower(1024,1024));
    }
    boolean isPowered = false;
    boolean somthingchanged = false;

    @Override
    public void tick() {
        isActive = powerSource.getPower().isPowered();
        if(world.getTotalWorldTime() % 40 == 0) {
            if (somthingchanged) {
                NetworkDispatcher.dispatchMachineUpdate(this);
                markDirty();
            }

            int usage = getSteamUsage();

            if(tank.getFluidAmount()>=usage){
                isPowered = true;
                RotaryPower power = new RotaryPower(2 << (upgrades.size() * 2),1024,0,2);
                tank.drain(usage,true);
                powerSource.setPower(power);
                somthingchanged = true;
            }else{
                powerSource.setPower(RotaryPower.ZERO.copy());
                isPowered = false;
                somthingchanged = true;
            }
        }

    }
    public int getSteamUsage(){
        return 40 + (2 << (upgrades.size() * 2 + 5));
    }
    @Override
    public void update() {
        super.update();
        if(world.isRemote){
            if(isActive){
                playsound();
            }else{
                stopsound();
            }
        }
    }

    @Override
    public void invalidate() {
        if(world.isRemote){
            stopsound();
        }
        super.invalidate();
    }

    @SideOnly(Side.CLIENT)
    public void playsound(){
        SoundHandler.startTileSound(Sounds.STEAMENGINE.getEvent(),0.5F,pos);
    }

    @SideOnly(Side.CLIENT)
    public void stopsound(){
        SoundHandler.stopTileSound(pos);
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound tag = super.getUpdateTag();
        tag.setBoolean("isActive",isActive);
        tank.writeToNBT(tag);
        return tag;
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tag) {
        super.handleUpdateTag(tag);
        tank.readFromNBT(tag);
        isActive = tag.getBoolean("isActive");
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        tank.readFromNBT(compound);
        isActive = compound.getBoolean("isActive");
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound = super.writeToNBT(compound);
        compound.setBoolean("isActive",isActive);
        tank.writeToNBT(compound);
        return compound;
    }



    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        return capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY || super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY){
            return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(new IFluidHandler(){

                @Override
                public IFluidTankProperties[] getTankProperties() {
                    return tank.getTankProperties();
                }

                @Override
                public int fill(FluidStack resource, boolean doFill) {
                    if(!resource.getFluid().equals(FluidRegistry.getFluid("steam"))){
                        return 0;
                    }
                    return tank.fill(resource,doFill);
                }

                @Nullable
                @Override
                public FluidStack drain(FluidStack resource, boolean doDrain) {
                    return null;
                }

                @Nullable
                @Override
                public FluidStack drain(int maxDrain, boolean doDrain) {
                    return null;
                }
            });
        }
        return super.getCapability(capability, facing);
    }


    @SideOnly(Side.CLIENT)
    @Override
    public RotaryModelBase getModel() {
        return new ModelSteamTurbine();
    }
    @SideOnly(Side.CLIENT)
    @Override
    public String getTexName() {
        return "steamturbtex";
    }

    @Override
    public void onBlockHarvested(EntityPlayer player) {
        super.onBlockHarvested(player);
        dropUpgrades(world,pos,upgrades);
    }

    @Override
    public boolean canUpgrade(ItemStack stack) {
        if(stack.getItem() instanceof IUpgrade && ((IUpgrade) stack.getItem()).readyForUse(stack) && stack.getItem() instanceof ItemUpgrade && stack.getMetadata()>0 && stack.getMetadata()<6){
            return stack.getMetadata() - upgrades.size() == 1;
        }
        return false;
    }

    @Override
    public boolean installUpgrade(ItemStack stack,EntityPlayer player) {
        this.upgrades.add(stack.copy());
        if(!world.isRemote){
            NetworkDispatcher.dispatchMachineUpdate(this);
            player.sendMessage(new TextComponentTranslation("info.rc.etc.upgrade_installed"));
        }else {
            SoundHandler.playSound(SoundEvents.BLOCK_ANVIL_USE);
        }
        return true;
    }


    @Override
    public boolean screw(EnumFacing facing, EntityLivingBase player) {
        if(upgrades.size()>0){
            if(world.isRemote){
                return true;
            }
            dropUpgrades(world,pos,upgrades.get(upgrades.size()-1).copy());
            NetworkDispatcher.dispatchMachineUpdate(this);
            return true;
        }
        return false;
    }

    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {
        super.getWailaBody(itemStack, tooltip);
        tooltip.add("steam usage: "+String.valueOf(getSteamUsage())+" mB/t");
    }
}
