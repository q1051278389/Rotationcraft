package cn.tohsaka.factory.rotationcraft.tiles.machines;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IModelProvider;
import cn.tohsaka.factory.rotationcraft.blocks.base.BlockRockPlate;
import cn.tohsaka.factory.rotationcraft.config.GeneralConfig;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.machine.ModelBedrockBreaker;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemMaterial;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileModelMachine;
import cn.tohsaka.factory.rotationcraft.utils.FacingTool;
import cn.tohsaka.factory.rotationcraft.utils.RenderHelper2;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;
import cn.tohsaka.factory.rotationcraft.utils.Utils;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.ArrayList;

@GameInitializer(after = BlockRockPlate.class)
public class TileBEMiner extends TileModelMachine implements IModelProvider{
    public TileBEMiner(){
        createSlots(1);
        setSlotMode(SlotMode.OUTPUT,0);
    }
    public static void init(){
        GameRegistry.registerTileEntity(TileBEMiner.class,new ResourceLocation(RotationCraft.MOD_ID,"tilebeminer"));
    }

    @Override
    public RotaryModelBase getModel() {
        return new ModelBedrockBreaker();
        //return ((getFacing().ordinal()<2)?bbv:bb);
    }

    @Override
    public String getTexName() {
        return "bedrocktex";//+((getFacing().ordinal()<2)?"v":"");
    }

    @Override
    public void updateInformationForTEISR(ItemStack stack, TileEntity tileEntity, float partialTicks) {

    }

    @Override
    public void beginItemRender() {
        GlStateManager.rotate(90,0,1,0);
    }


    int harvestoffset = 0;


    @Override
    public void setFacing(EnumFacing facing) {
        harvestoffset = 0;
        super.setFacing(facing);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void renderTileEntityAt(double x, double y, double z){
        {
            RenderHelper2.bindTexture(new ResourceLocation(RotationCraft.MOD_ID,"textures/render/"+getTexName()+".png"));

            GlStateManager.pushMatrix();
            GlStateManager.enableRescaleNormal();
            GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
            GlStateManager.translate(x, y, z);
            GlStateManager.scale(1.0F, -1.0F, -1.0F);
            GlStateManager.translate(0.5F, 0.5F, 0.5F);
            GlStateManager.translate(0F,-2F,-1F);
            if(getFacing().ordinal()>1){
                RenderUtils.rotate(getFacing(), 270, 90, 180, 0);
            }else{
                if(getFacing().ordinal()==1){
                    GlStateManager.rotate(-90,0,0,1);
                    GlStateManager.translate(-1,-1,0);
                }else{
                    GlStateManager.rotate(90,0,0,1);
                    GlStateManager.translate(1,-1,0);
                }
            }
        }
        if(this.getBlockType().getLightOpacity(world.getBlockState(pos),world,pos)>0){
            OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 240, 240);
        }
        ArrayList p = new ArrayList<Object>();
        p.add(harvestoffset+1);
        getModel().renderAll(this,p,angle,0F);

        GlStateManager.disableRescaleNormal();
        GlStateManager.popMatrix();
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
    }

    int basetime = 600;

    @Override
    public boolean canStart() {
        if(GeneralConfig.BEMINER_MAXOFFSET>0 && harvestoffset>GeneralConfig.BEMINER_MAXOFFSET){
            return false;
        }
        return powerMachine.getPower().getWatt()>=getMinimumPower().getWatt() && getStackInSlot(0).getCount()<64;
    }
    BlockPos workpos = null;
    @Override
    public boolean processStart() {
        transferOutputStack();
        if(workpos == null){
            workpos = FacingTool.getRelativePos(pos,facing, FacingTool.Position.FRONT,harvestoffset+1);
        }
        if(harvestoffset>0){
            for(int i=0;i<harvestoffset;i++){
                BlockPos p = FacingTool.getRelativePos(pos,facing, FacingTool.Position.FRONT,i+1);
                IBlockState state = world.getBlockState(p);
                if(!state.getBlock().isAir(state,world,p) && !(state.getBlock() instanceof BlockLiquid)){
                    harvestoffset = i;
                    workpos = p;
                    markDirty();
                    break;
                }
            }
        }
        processRem = processMax = calcProcessTime();
        return true;
    }

    public int calcProcessTime(){
        RotaryPower minimumpower = getMinimumPower();
        long ratio = Math.abs(powerMachine.getPower().getSpeed() - minimumpower.getSpeed()) / minimumpower.getSpeed();
        //float time = basetime - ((basetime/40f)*ratio);
        float time = basetime - ((basetime/100f)*ratio);
        return Math.max(0,Math.round(time));
    }

    @Override
    public void processTick() {
        processRem--;
    }

    @Override
    public void processEnd() {
        if(world.isAirBlock(workpos)){
            harvestoffset++;
            markDirty();
            return;
        }
        IBlockState state = world.getBlockState(workpos);
        if(state.getBlock().equals(Blocks.BEDROCK)){
            world.setBlockState(workpos,BlockRockPlate.INSTANCE.getStateFromMeta(15));
            return;
        }

        if(state.getBlock() instanceof BlockRockPlate){
            int layers = BlockRockPlate.INSTANCE.getMetaFromState(state);
            if(layers>1){
                world.setBlockState(workpos,BlockRockPlate.INSTANCE.getStateFromMeta(layers-1));
                return;
            }else if(layers==1){
                world.setBlockToAir(workpos);
                Utils.distributeOutput(this, ItemMaterial.getStackById(45,2),0,1,true);
                harvestoffset++;
                markDirty();
                return;
            }
            return;
        }
        if(state.getBlock().getBlockHardness(state,world,workpos)>0){
            world.setBlockToAir(workpos);
            harvestoffset++;
            markDirty();
            return;
        }
    }

    @Override
    public RotaryPower getMinimumPower() {
        return new RotaryPower(16384,256,0,0);
    }


    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound tag = super.getUpdateTag();
        tag = super.writeGuiTagCompound(tag);
        tag.setInteger("offset",harvestoffset);
        return tag;
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tag) {
        super.handleUpdateTag(tag);
        super.readFromNBT(tag);
        harvestoffset = tag.getInteger("offset");
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        harvestoffset = compound.getInteger("offset");
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound = super.writeToNBT(compound);
        compound.setInteger("offset",harvestoffset);
        return compound;
    }

    @Override
    public boolean allowVerticalFacing() {
        return true;
    }

    public void dropItem(){
        if(!world.isRemote){
            if(!getStackInSlot(0).isEmpty()){
                world.spawnEntity(new EntityItem(world,pos.getX(),pos.getY(),pos.getZ(),getStackInSlot(0)));
                setInventorySlotContents(0,ItemStack.EMPTY);
            }
        }
    }
}
