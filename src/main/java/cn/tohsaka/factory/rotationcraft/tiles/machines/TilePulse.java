package cn.tohsaka.factory.rotationcraft.tiles.machines;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.api.IModelProvider;
import cn.tohsaka.factory.rotationcraft.blocks.machine.BlockExtractor;
import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.PulseRecipe;
import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.machine.ModelPulseFurnace;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerPulse;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.GuiPulse;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemMaterial;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileModelMachine;
import cn.tohsaka.factory.rotationcraft.utils.FacingTool;
import cn.tohsaka.factory.rotationcraft.utils.Utils;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.FluidTankInfo;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.FluidTankProperties;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.common.registry.GameRegistry;
import org.lwjgl.opengl.GL11;

import javax.annotation.Nullable;
import java.util.List;

@GameInitializer(after = BlockExtractor.class)
public class TilePulse extends TileModelMachine implements IModelProvider, IGuiProvider {
    public TilePulse(){
        super(FacingTool.Position.DOWN);
        createSlots(2);
        setSlotMode(SlotMode.INPUT,0);
        setSlotMode(SlotMode.OUTPUT,1);
    }

    @Override
    public void update() {
        super.update();
        if(!world.isRemote){
            if (temp>getDamagedTemp()){
                destroy();
            }
            if(world.getTotalWorldTime() % 20 == 0){
                if(waterTank.getFluidAmount()>=250 && temp>1800){
                    waterTank.drain(250,true);
                    temp -= 350;
                }
            }
        }
    }
    PulseRecipe cur = null;
    @Override
    public boolean canStart() {
        if(powerMachine.getPower().getWatt() < getMinimumPower().getWatt() && powerMachine.getPower().getSpeed() < getMinimumPower().getSpeed()){
            return false;
        }
        if(isActive && cur!=null){
            return true;
        }
        if(!getStackInSlot(0).isEmpty()){
            for (CraftingPattern recipe : Recipes.INSTANCE.getList(TilePulse.class)){
                if(((PulseRecipe)recipe).iinput.test(getStackInSlot(0))){
                    cur = (PulseRecipe) recipe;
                    break;
                }
            }
        }

        if(cur!=null){
            int needcap = cur.getData().getInteger("fluid");
            if(needcap>cataTank.getFluidAmount()){
                return false;
            }

            if(!isActive() && !getStackInSlot(1).isEmpty() && !getStackInSlot(1).isItemEqual(cur.getOutput())){
                return false;
            }
        }
        return cur!=null;
    }

    @Override
    public boolean processStart() {
        if(cur != null && !getStackInSlot(0).isEmpty()){
            int needcap = cur.getData().getInteger("fluid");
            if(needcap>cataTank.getFluidAmount()){
                return false;
            }
            cataTank.drain(needcap,true);
            ItemStack input0 = CraftingPattern.parseOreDict(cur.getInput()[0])[0];
            decrStackSize(0,input0.getCount());
            float ratio = powerMachine.getPower().getSpeed() / getMinimumPower().getSpeed();
            processRem = processMax = Math.max(Math.round(cur.getData().getInteger("worktime") / Math.max(ratio,1F)),1);
            return true;
        }
        return false;
    }

    @Override
    public void processTick() {
        processRem--;
        temp+=15;
    }

    @Override
    public void processEnd() {
        Utils.distributeOutput(this,cur.getOutput().copy(),1,2,true);
    }

    @Override
    public boolean canDamage() {
        return true;
    }

    @Override
    public float getDamagedTemp() {
        return 2400;
    }

    public FluidTank cataTank = new FluidTank(8000);
    public FluidTank waterTank = new FluidTank(8000);

    public static void init(){
        GameRegistry.registerTileEntity(TilePulse.class,new ResourceLocation(RotationCraft.MOD_ID,"tilepulse"));
    }

    @Override
    public RotaryModelBase getModel() {
        return new ModelPulseFurnace();
    }

    @Override
    public String getTexName() {
        if(temp>100 && temp<=500){
            return "pulse/p0";
        }
        if(temp>500 && temp<=1000){
            return "pulse/p1";
        }
        if(temp>1000 && temp<=1500){
            return "pulse/p2";
        }
        if(temp>1800){
            return "pulse/p3";
        }
        return "pulse/p0";
    }

    @Override
    public void updateInformationForTEISR(ItemStack stack, TileEntity tileEntity, float partialTicks) {

    }

    @Override
    public void beginItemRender() {
        GL11.glRotatef(90,0,1,0);
    }

    @Override
    public RotaryPower getMinimumPower() {
        return new RotaryPower(1,131072,0,0);
    }


    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound tag = super.getUpdateTag();
        tag.setTag("catatank",cataTank.writeToNBT(new NBTTagCompound()));
        tag.setTag("watertank",waterTank.writeToNBT(new NBTTagCompound()));
        return tag;
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tag) {
        super.handleUpdateTag(tag);
        if(tag.hasKey("catatank")){
            cataTank.readFromNBT(tag.getCompoundTag("catatank"));
        }
        if(tag.hasKey("watertank")){
            waterTank.readFromNBT(tag.getCompoundTag("watertank"));
        }
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        if(compound.hasKey("catatank")){
            cataTank.readFromNBT(compound.getCompoundTag("catatank"));
        }
        if(compound.hasKey("watertank")){
            waterTank.readFromNBT(compound.getCompoundTag("watertank"));
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound = super.writeToNBT(compound);
        compound.setTag("catatank",cataTank.writeToNBT(new NBTTagCompound()));
        compound.setTag("watertank",waterTank.writeToNBT(new NBTTagCompound()));
        return compound;
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        return capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY || capability == CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY || super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY){
            return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(new IFluidHandler() {

                @Override
                public IFluidTankProperties[] getTankProperties() {
                    FluidTankInfo info = cataTank.getInfo();
                    FluidTankInfo info2 = waterTank.getInfo();
                    return new IFluidTankProperties[] {
                            new FluidTankProperties(info.fluid, info.capacity, true, false),
                            new FluidTankProperties(info2.fluid, info2.capacity, true, false)
                    };
                }

                @Override
                public int fill(FluidStack resource, boolean doFill) {
                    int i = 0;
                    if(resource.getFluid().equals(FluidRegistry.WATER)){
                        i = waterTank.fill(resource,doFill);
                    }
                    if(resource.getFluid().equals(RotationCraft.fluids.get("jetfuel"))){
                        i = cataTank.fill(resource,doFill);
                    }
                    if(i>0){
                        markDirty();
                    }
                    return i;
                }


                @Nullable
                @Override
                public FluidStack drain(FluidStack resource, boolean doDrain) {
                    return resource;
                }

                @Nullable
                @Override
                public FluidStack drain(int maxDrain, boolean doDrain) {
                    return null;
                }
            });
        }
        return super.getCapability(capability, facing);
    }


    private void destroy() {
        world.setBlockToAir(pos);
        world.createExplosion(null, pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, 20,false);
        world.spawnEntity(new EntityItem(world,pos.getX(),pos.getY()+1,pos.getZ(), ItemMaterial.getStackById(3,8)));
    }

    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {
        tooltip.add(String.valueOf(temp)+"'C");
    }


    @Override
    public Object getGuiClient(EntityPlayer player) {
        return new GuiPulse(this,player.inventory);
    }

    @Override
    public Object getGuiServer(EntityPlayer player) {
        return new ContainerPulse(this,player.inventory);
    }
}
