package cn.tohsaka.factory.rotationcraft.tiles.base;

import cn.tohsaka.factory.librotary.packet.PacketCustom;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.ICrafterWorktable;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.blocks.base.BlockWorktable;
import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.crafting.WorktableRecipe;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerWorktable;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.GuiWorktable;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileInvMachine;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@GameInitializer(after = BlockWorktable.class)
public class TileWorktable extends TileInvMachine implements IGuiProvider, ICrafterWorktable {
    private TileWorktable instance;
    public TileWorktable(){
        createSlots(10);
        setSlotMode(SlotMode.NONE,0,10);
        instance = this;
    }
    public static void init(){
        GameRegistry.registerTileEntity(TileWorktable.class,new ResourceLocation(RotationCraft.MOD_ID,"tileworktable"));
    }

    @Override
    public Object getGuiClient(EntityPlayer player) {
        return new GuiWorktable(this,player.inventory);
    }

    @Override
    public Object getGuiServer(EntityPlayer player) {
        return new ContainerWorktable(this,player.inventory);
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tag) {
        super.handleUpdateTag(tag);
    }
    @Override
    public NBTTagCompound getUpdateTag() {

        return super.getUpdateTag();
    }

    @Override
    public void writeGuiData(PacketCustom packet, boolean isFullSync) {

        super.writeGuiData(packet, isFullSync);
    }

    @Override
    public void readGuiData(PacketCustom packet, boolean isFullSync) {
        super.readGuiData(packet, isFullSync);
    }



    public InventoryCrafting inventoryCrafting = new InventoryCrafting(new Container() {
        @Override
        public boolean canInteractWith(EntityPlayer entityPlayer) {
            return true;
        }
    },3,3){
        @Override
        public ItemStack getStackInSlot(int index) {
            return instance.getStackInSlot(index);
        }
    };




    @Override
    public ItemStack getResult() {
        WorktableRecipe recipe = getMatchedRecipe();
        return recipe==null?ItemStack.EMPTY:recipe.getOutput().copy();
    }

    @Override
    public boolean canTakeStack(int slotIndex) {
        return true;
    }

    @Override
    public boolean onCraftingStart(EntityPlayer player) {
        return doCraft(player,1).isEmpty();
    }

    @Override
    public void onCraftingComplete(EntityPlayer player) {

    }

    @Nullable
    public WorktableRecipe getMatchedRecipe(){
        for(CraftingPattern pattern: Recipes.INSTANCE.getList(TileWorktable.class)){
            if(((WorktableRecipe)pattern).test(this)){
                return ((WorktableRecipe)pattern);
            }
        }
        return null;
    }

    public ItemStack doCraft(EntityPlayer player,int count){
        WorktableRecipe curRecipe = getMatchedRecipe();
        ItemStack output = ItemStack.EMPTY;
        if(curRecipe != null && count > 0){
            for(int i=0;i<count;i++){
                int output_count = curRecipe.getOutput().getCount();
                if((output.getCount()+output_count)>output.getMaxStackSize()){
                    break;
                }
                if(!output.isItemEqual(curRecipe.getOutput().copy())){
                    output = curRecipe.getOutput().copy();
                }else {
                    output.grow(output_count);
                }
                for(int j=0;j<9;j++){
                    decrStackSize(j,1);
                }
            }
            return output;
        }
        return ItemStack.EMPTY;
    }
    public int getMaxCraft(){
        List<ItemStack> stacks = Arrays.stream(inventory).limit(9).filter(stack1 -> {
            return !stack1.isEmpty();
        }).sorted(Comparator.comparingInt(ItemStack::getCount)).collect(Collectors.toList());
        if(stacks.size()>0){
            return stacks.get(0).getCount();
        }
        return 0;
    }
}
