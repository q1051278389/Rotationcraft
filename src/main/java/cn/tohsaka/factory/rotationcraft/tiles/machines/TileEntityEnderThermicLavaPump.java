package cn.tohsaka.factory.rotationcraft.tiles.machines;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.blocks.base.BlockPlaceHolder;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileModelMachine;
import net.minecraft.block.Block;
import net.minecraft.block.BlockDynamicLiquid;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraftforge.common.ForgeChunkManager;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;

import static cn.tohsaka.factory.rotationcraft.utils.FluidHelper.lookupFluidForBlock;

public class TileEntityEnderThermicLavaPump extends TileModelMachine {
    public boolean finished = false;
    private FluidTank tank = new FluidTank(16000);
    private int pump_y = -1;
    private int chunk_x = 0;
    private int chunk_z = 0;
    private int b = 0;
    private boolean find_new_block = false;
    private boolean init = false;
    private int chunk_no = -1;
    private float p = 0.95F;
    private ForgeChunkManager.Ticket chunkTicket;

    public TileEntityEnderThermicLavaPump() {

    }
    IBlockState placeholder = BlockPlaceHolder.INSTANCE.getDefaultState();
    public void update2() {
      if (this.world.isRemote) {
        return;
      }
      if (this.finished) {
        if (this.chunkTicket != null) {
          ForgeChunkManager.releaseTicket(this.chunkTicket);
          this.chunkTicket = null;
        }
        return;
      }
      if (this.chunkTicket == null) {
        boolean valid = false;

        this.chunkTicket = ForgeChunkManager.requestTicket(RotationCraft.MOD_ID, this.world, ForgeChunkManager.Type.NORMAL);
        if (this.chunkTicket == null) {
          this.finished = true;
          markDirty();
          return;
        }

        this.chunkTicket.getModData().setString("id", "pump");
        this.chunkTicket.getModData().setInteger("pumpX", this.pos.getX());
        this.chunkTicket.getModData().setInteger("pumpY", this.pos.getY());
        this.chunkTicket.getModData().setInteger("pumpZ", this.pos.getZ());
        ForgeChunkManager.forceChunk(this.chunkTicket, new ChunkPos(this.pos.getX() >> 4, this.pos.getZ() >> 4));
      }
      boolean goAgain = true;
      for (int t = 0; t < 16 && goAgain; t++) {
        goAgain = false;
        int bx = this.b >> 4, bz = this.b & 0xF;
        int pump_x = (this.chunk_x << 4) + bx;
        int pump_z = (this.chunk_z << 4) + bz;
        BlockPos cur = new BlockPos(pump_x, this.pump_y, pump_z);
        if (this.pump_y >= 0 && getFluidOnPos(cur) != null) {
          if (((this.tank.getInfo()).fluid == null || (this.tank.getInfo()).fluid.amount <= 0)) {
            FluidStack fluidStack = new FluidStack(getFluidOnPos(cur),1000);
            this.tank.fill(fluidStack, true);
            world.setBlockState(cur,placeholder);
            this.pump_y--;
            markDirty();
          }
        } else {
          goAgain = true;
          if (!this.init)
            this.b = 256;
          this.b++;
          if (this.b >= 256) {
            this.b = 0;
            goAgain = false;
            if (this.init && this.chunk_no > 0)
              for (int i = -2; i <= 2; i++) {
                for (int dz = -2; dz <= 2; dz++)
                  ForgeChunkManager.unforceChunk(this.chunkTicket, new ChunkPos(this.chunk_x + i, this.chunk_z + dz));
              }
            this.chunk_no++;
            setChunk(this.chunk_no);
            for (int dx = -2; dx <= 2; dx++) {
              for (int dz = -2; dz <= 2; dz++) {
                ForgeChunkManager.forceChunk(this.chunkTicket, new ChunkPos(this.chunk_x + dx, this.chunk_z + dz));
                this.world.getChunkFromChunkCoords(this.chunk_x + dx, this.chunk_z + dz);
              }
            }
            ForgeChunkManager.forceChunk(this.chunkTicket, new ChunkPos(this.pos.getX() >> 4, this.pos.getZ() >> 4));
          }
          this.pump_y = this.pos.getY() - 1;
          this.init = true;
          markDirty();
        }
      }


    }



  public void setChunk(int chunk_no) {
    int base_chunk_x = this.pos.getX() >> 4;
    int base_chunk_z = this.pos.getZ() >> 4;
    int j = chunk_no;
    if (j == 0) {
      this.chunk_x = base_chunk_x;
      this.chunk_z = base_chunk_z;
      return;
    }
    j--;
    for (int k = 1; k <= 5; k++) {
      if (j >= 4 * k) {
        j -= 4 * k;
      } else {
        if (j < k) {
          this.chunk_x = base_chunk_x + j;
          this.chunk_z = base_chunk_z + k - j;
        } else if (j < 2 * k) {
          j -= k;
          this.chunk_x = base_chunk_x + k - j;
          this.chunk_z = base_chunk_z - j;
        } else if (j < 3 * k) {
          j -= 2 * k;
          this.chunk_x = base_chunk_x - j;
          this.chunk_z = base_chunk_z - k - j;
        } else {
          j -= 3 * k;
          this.chunk_x = base_chunk_x - k - j;
          this.chunk_z = base_chunk_z + j;
        }
        return;
      }
    }
    this.finished = true;
    markDirty();
    chunk_no = 255;
  }

  @Override
  public RotaryPower getMinimumPower() {
    return null;
  }

  private Fluid getFluidOnPos(BlockPos target){
      IBlockState state = world.getBlockState(target);
      Block block = state.getBlock();
      if(block instanceof BlockDynamicLiquid){
        return null;
      }
      Fluid fluid = lookupFluidForBlock(block);
      return fluid;
    }

  @Override
  public void updateInformationForTEISR(ItemStack stack, TileEntity tileEntity, float partialTicks) {

  }

  @Override
  public void beginItemRender() {

  }
}