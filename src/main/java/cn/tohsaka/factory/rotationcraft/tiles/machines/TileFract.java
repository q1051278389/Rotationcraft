package cn.tohsaka.factory.rotationcraft.tiles.machines;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.api.IModelProvider;
import cn.tohsaka.factory.rotationcraft.blocks.machine.BlockExtractor;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.machine.ModelFraction;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerFract;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.GuiFract;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemMaterial;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileModelMachine;
import cn.tohsaka.factory.rotationcraft.utils.FacingTool;
import cn.tohsaka.factory.rotationcraft.utils.FluidHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.FluidUtil;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.common.registry.GameRegistry;

import javax.annotation.Nullable;
import java.util.LinkedList;

@GameInitializer(after = BlockExtractor.class)
public class TileFract extends TileModelMachine implements IGuiProvider, IModelProvider {
    private static LinkedList<Ingredient> ingredients = new LinkedList();
    private static int[] weight = new int[]{5,5,3,3,7,7};
    public TileFract(){
        super(FacingTool.Position.DOWN);
        createSlots(8);
        setSlotMode(SlotMode.INPUT,new int[]{0,1,2,3,4,5});
        setSlotMode(SlotMode.OMNI,new int[]{7});
        if(ingredients.isEmpty()){
            ingredients.add(Ingredient.fromItem(ItemMaterial.getItemById(43)));
            ingredients.add(Ingredient.fromItem(ItemMaterial.getItemById(44)));
            ingredients.add(Ingredient.fromItem(Items.BLAZE_POWDER));
            ingredients.add(Ingredient.fromItem(Items.MAGMA_CREAM));
            ingredients.add(Ingredient.fromItem(ItemMaterial.getItemById(60)));
            ingredients.add(Ingredient.fromItem(Items.COAL));
        }
    }

    public static void init(){
        GameRegistry.registerTileEntity(TileFract.class,new ResourceLocation(RotationCraft.MOD_ID,"tilefract"));
    }

    @Override
    public boolean canStart() {
        return allSlotValid();
    }

    @Override
    public boolean processStart() {
        processMax = processRem = procTime;
        return true;
    }

    @Override
    public void processTick() {
        processRem--;
    }

    @Override
    public void update() {
        if(!getStackInSlot(7).isEmpty() && fuelTank.getFluidAmount()>=1000 && FluidHelper.isFluidHandler(getStackInSlot(7))){
            if(getStackInSlot(7).getItem().equals(Items.BUCKET)){
                setInventorySlotContents(7,FluidUtil.getFilledBucket(new FluidStack(fuelTank.getFluid(),1000)));
                fuelTank.drain(1000,true);
            }else{
                IFluidHandler handler = getStackInSlot(7).getCapability(CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY,null);
                if (handler==null){
                    handler = getStackInSlot(7).getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY,null);
                }
                if (handler!=null){
                    int filled = handler.fill(fuelTank.getFluid(),true);
                    fuelTank.drain(filled,true);
                }
            }
        }
        super.update();
    }

    @Override
    public void processEnd() {
        int count = procTime==1?getNumberConsecutiveOperations():1;
        for(int i=0;i<count;i++){
            if(canStart()){
                make();
            }
        }
        recalcProcessTime();
    }

    public FluidTank fuelTank = new FluidTank(Fluid.BUCKET_VOLUME * 64);
    private void make() {
        if (!world.isRemote) {
            for(int i=0;i<6;i++){
                if(world.rand.nextInt(10)<weight[i]){
                    decrStackSize(i,1);
                }
            }
        }
        fuelTank.fill(new FluidStack(RotationCraft.fluids.get("jetfuel"),Math.max(1000,world.rand.nextInt(1500))),true);
        markDirty();
    }



    private boolean allSlotValid() {
        if(getStackInSlot(6).isEmpty()){
            return false;
        }
        if(fuelTank.getFluidAmount()>Fluid.BUCKET_VOLUME*63){
            return false;
        }
        for (int i = 0; i < ingredients.size(); i++) {
            if (getStackInSlot(i) == null) {
                return false;
            }

            if(!ingredients.get(i).test(getStackInSlot(i))){
                return false;
            }
        }
        return true;
    }

    private static int procTime = 128;//1049000 / 8192 = 128
    private int getNumberConsecutiveOperations(){
        float ratio = (float) powerMachine.getPower().getSpeed() / (float)getMinimumPower().getSpeed();
        if(ratio>128){
            return Math.max(1,Math.round((ratio-128) / 4));
        }
        return 1;
    }

    private void recalcProcessTime(){
        float ratio = (float) powerMachine.getPower().getSpeed() / (float)getMinimumPower().getSpeed();
        procTime = Math.max(1,Math.round(128-ratio));
    }



    @Override
    public RotaryPower getMinimumPower() {
        return new RotaryPower(8,8192,0,0);
    }


    @Override
    public NBTTagCompound writeGuiTagCompound(NBTTagCompound tag) {
        tag.setTag("fuel",fuelTank.writeToNBT(new NBTTagCompound()));
        tag.setInteger("proctime",procTime);
        return super.writeGuiTagCompound(tag);
    }

    @Override
    public void readGuiTagCompound(NBTTagCompound tag) {
        if(tag.hasKey("fuel")){
            fuelTank.readFromNBT(tag.getCompoundTag("fuel"));
        }
        tag.getInteger("proctime");
        super.readGuiTagCompound(tag);
    }


    @Override
    public void readFromNBT(NBTTagCompound compound) {
        compound.setTag("fuel",fuelTank.writeToNBT(new NBTTagCompound()));
        super.readFromNBT(compound);
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        if(compound.hasKey("fuel")){
            fuelTank.readFromNBT(compound.getCompoundTag("fuel"));
        }
        return super.writeToNBT(compound);
    }

    @Override
    public RotaryModelBase getModel() {
        return new ModelFraction();
    }

    @Override
    public String getTexName() {
        return "fractex";
    }

    @Override
    public void updateInformationForTEISR(ItemStack stack, TileEntity tileEntity, float partialTicks) {

    }

    @Override
    public void beginItemRender() {

    }

    @Override
    public Object getGuiClient(EntityPlayer player) {
        return new GuiFract(this,player.inventory);
    }

    @Override
    public Object getGuiServer(EntityPlayer player) {
        return new ContainerFract(this,player.inventory);
    }


    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        if(stack.isEmpty()){
            return false;
        }
        if(index<6){
            return ingredients.get(index).test(stack);
        }
        if(index==6){
            return Ingredient.fromItem(Items.GHAST_TEAR).test(stack);
        }
        if(index==7){
            return FluidHelper.isFluidHandler(stack);
        }
        return false;
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing side) {
        if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY && side == EnumFacing.UP){
            return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(new IFluidHandler() {
                @Override
                public IFluidTankProperties[] getTankProperties() {
                    return fuelTank.getTankProperties();
                }

                @Override
                public int fill(FluidStack resource, boolean doFill) {
                    return 0;
                }

                @Nullable
                @Override
                public FluidStack drain(FluidStack resource, boolean doDrain) {
                    return fuelTank.drain(resource,doDrain);
                }

                @Nullable
                @Override
                public FluidStack drain(int maxDrain, boolean doDrain) {
                    return fuelTank.drain(maxDrain,doDrain);
                }
            });
        }
        return super.getCapability(capability, side);
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing side) {
        return (capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY && side == EnumFacing.UP) || super.hasCapability(capability, side);
    }
}
