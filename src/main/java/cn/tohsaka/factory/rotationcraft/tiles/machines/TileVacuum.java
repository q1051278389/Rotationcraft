package cn.tohsaka.factory.rotationcraft.tiles.machines;

import cn.tohsaka.factory.librotary.api.power.CapabilityRotaryPower;
import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.blocks.machine.BlockVacuum;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileRotation;
import cn.tohsaka.factory.rotationcraft.utils.FacingTool;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;
import cn.tohsaka.factory.rotationcraft.utils.Utils;
import net.minecraft.client.gui.inventory.GuiChest;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ContainerChest;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fml.common.registry.GameRegistry;

import javax.annotation.Nullable;
import java.util.List;

@GameInitializer(after = BlockVacuum.class)
public class TileVacuum extends TileRotation implements IGuiProvider {
    public static void init() {
        GameRegistry.registerTileEntity(TileVacuum.class, new ResourceLocation(RotationCraft.MOD_ID, "tilevacuum"));
    }

    public TileVacuum() {
        createSlots(27);
        setSlotMode(SlotMode.OUTPUT,0,27);
    }

    @Override
    public Object getGuiClient(EntityPlayer player) {
        return new GuiChest(player.inventory,this);
    }

    @Override
    public Object getGuiServer(EntityPlayer player) {
        return new ContainerChest(player.inventory,this,player);
    }

    @Override
    public RotaryPower getMinimumPower() {
        return new RotaryPower(1,1024);
    }

    @Override
    public void update() {

        if(world.getTotalWorldTime() % 20 == 0){
            powerMachine.setPower(RotaryPower.ZERO);
            for(int i=0;i< 6;i++){
                updatePowerInput(FacingTool.Position.getbyIndex(i));
                if(powerMachine.getPower().isPowered()){
                    break;
                }
            }
            if(powerMachine.getPower().isPowered()) {
                transferOutputStack();
            }
        }

        if(!world.isRemote){
            doHoover(getRange());
        }
    }

    @Override
    public void getOutBoxList(NonNullList<RenderUtils.RenderItem> drawables) {
        long time = world.getTotalWorldTime();
        if((time - renderboxtime)>getOutBoxLivingTicks()){
            RenderUtils.removeRender(this);
            return;
        }
        int range = getRange();
        drawables.add(new RenderUtils.RenderItemBigBox(0x00FF00,new AxisAlignedBB(pos.add(range+1,range+1,range+1),pos.add(-range,-range,-range)),2));
    }
    public int getRange(){
        int range = 2;
        if(powerMachine.getPower().isPowered() && powerMachine.getPower().getWatt()>getMinimumPower().getWatt()){
            range += Math.min(16,Math.round(powerMachine.getPower().getTorque() / getMinimumPower().getTorque()));
        }
        return range;
    }

    @Override
    public int getOutBoxLivingTicks() {
        return 200;
    }

    private void doHoover(int range) {
        int removed = 0;
        List<EntityItem> entities = world.getEntitiesWithinAABB(EntityItem.class,new AxisAlignedBB(pos.add(range+1,range+1,range+1),pos.add(-range,-range,-range)));
        for (EntityItem entity : entities) {

            double x = (pos.getX() + 0.5D - entity.posX);
            double y = (pos.getY() + 0.5D - entity.posY);
            double z = (pos.getZ() + 0.5D - entity.posZ);
            double distance = Math.sqrt(x * x + y * y + z * z);
            if (distance < 1) {
                if(!world.isRemote){
                    if(entity instanceof EntityItem && !entity.isDead){
                        ItemStack itemStack = ((EntityItem) entity).getItem();
                        if(Utils.checkInvCanInsertItem(this,itemStack,0,27)){
                            Utils.distributeOutput(this,itemStack,0,27,true);
                            world.removeEntity(entity);
                            removed++;
                        }
                    }
                }
            } else {
                double speed = 0.1;
                double distScale = 1.0 - Math.min(0.9, (distance - 1) / 32);
                distScale *= distScale;
                entity.motionX += x / distance * distScale * speed;
                entity.motionY += y / distance * distScale * 0.4;
                entity.motionZ += z / distance * distScale * speed;
                if (!world.isRemote)
                    entity.velocityChanged = true;
            }

        }
        if(removed>0){
            world.markChunkDirty(pos,null);
        }
    }

    @Nullable
    @Override
    public ITextComponent getDisplayName() {
        return new TextComponentString(RotationCraft.blocks.get(BlockVacuum.NAME).getLocalizedName());
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing side) {
        return capability == CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY || super.hasCapability(capability, side);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing side) {
        if(capability == CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY){
            return CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY.cast(powerMachine);
        }
        return super.getCapability(capability, side);
    }

    @Override
    public String getMeterInfo() {
        return super.getMeterInfo()+"\n" + "Range: "+getRange()+"\n";
    }
}
