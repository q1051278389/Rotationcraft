package cn.tohsaka.factory.rotationcraft.tiles.machines;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IModelProvider;
import cn.tohsaka.factory.rotationcraft.blocks.machine.BlockSprinkler;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.machine.ModelSprinkler;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileModelMachine;
import cn.tohsaka.factory.rotationcraft.utils.FacingTool;
import cn.tohsaka.factory.rotationcraft.utils.IteratorAABB;
import cn.tohsaka.factory.rotationcraft.utils.ReikaParticleHelper;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;
import net.minecraft.block.BlockCrops;
import net.minecraft.block.BlockFarmland;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.*;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.FluidTankProperties;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.Random;

@GameInitializer(after = BlockSprinkler.class)
public class TileSprinkler extends TileModelMachine implements IModelProvider {
    public FluidTank waterTank = new FluidTank(Fluid.BUCKET_VOLUME * 16);
    public static void init(){
        GameRegistry.registerTileEntity(TileSprinkler.class,new ResourceLocation(RotationCraft.MOD_ID,"tilesprinkler"));
    }

    public TileSprinkler(){
        super(FacingTool.Position.UP);
    }

    @Override
    public void updateInformationForTEISR(ItemStack stack, TileEntity tileEntity, float partialTicks) {

    }

    @Override
    public void beginItemRender() {

    }

    @Override
    public RotaryPower getMinimumPower() {
        return RotaryPower.ZERO;
    }

    @Override
    public RotaryModelBase getModel() {
        return new ModelSprinkler();
    }

    @Override
    public String getTexName() {
        return "sprinklertex";
    }

    IteratorAABB workIter;
    int workcount = 0;
    @Override
    public void update() {
        if(workIter==null || !workIter.hasNext()){
            workIter = new IteratorAABB(pos.add(-getRange(),-getRange(),-getRange()),(getRange()*2)+1,1,(getRange()*2)+1);
            workcount = Math.round(workIter.getCount()/2);
        }
        if(waterTank.getFluidAmount()>=100){
            if(world.isRemote){
                spawnParticles(world,pos.getX(),pos.getY(),pos.getZ());
            }else{
                if(world.getTotalWorldTime() % 30 == 0){
                    work(workcount);
                }
                waterTank.drain(100,true);
            }
        }
    }
    Runnable task;
    public void work(int count){
        for(int i=0;i<=count;i++){
            int ii = world.rand.nextInt(workIter.getCount());

            IBlockState stateDirt = world.getBlockState(workIter.get(ii).add(0,-1,0));
            if(stateDirt.getBlock() instanceof BlockFarmland){
               world.setBlockState(workIter.get(ii).add(0,-1,0), stateDirt.withProperty(BlockFarmland.MOISTURE, 7), 2);
            }

            IBlockState state = world.getBlockState(workIter.get(ii));
            if(state!=null && state.getBlock()!=null && (state.getBlock() instanceof BlockCrops || state.getBlock() instanceof IPlantable)){
                state.getBlock().updateTick(world, workIter.get(ii), state,world.rand);
            }

        }
    }

    @Override
    public void getOutBoxList(NonNullList<RenderUtils.RenderItem> drawables) {
        super.getOutBoxList(drawables);
        if(workIter!=null){
            drawables.add(new RenderUtils.RenderItemBigBox(0x00FF00,workIter.getAabb(),2));
        }
    }

    @Override
    public int getOutBoxLivingTicks() {
        return 100;
    }

    public int getRange(){
        return 4;
    }

    @SideOnly(Side.CLIENT)
    public void spawnParticles(World world, int x, int y, int z) {

        int d = 0;
        double ypos = y+0.125;
        double vel;
        double r = getRange()/10D;
        Random rand = world.rand;


        double py = y-0.1875D+0.5;
        int n = (rand.nextInt(2) == 0 ? 1 : 0)+rand.nextInt(1+d);
        for (int i = 0; i < n; i++) {
            double px = x-1+2*rand.nextFloat();
            double pz = z-1+2*rand.nextFloat();
            ReikaParticleHelper.spawnAt(EnumParticleTypes.WATER_SPLASH,world, px+0.5, py, pz+0.5, 0, 0, 0);
        }

        for (vel = 0; vel < r; vel += 0.1) {
            py = y-0.1875D+0.5;
            n = (rand.nextInt(2) == 0 ? 1 : 0)+rand.nextInt(1+d*4);
            for (int i = 0; i < n; i++) {
                double vx = vel*(-1+rand.nextFloat()*2);
                vx *= 1.05;
                double vz = vel*(-1+rand.nextFloat()*2);
                vz *= 1.05;
                double px = x-1+2*rand.nextFloat();
                double pz = z-1+2*rand.nextFloat();
                ReikaParticleHelper.spawnAt(EnumParticleTypes.WATER_SPLASH,world, px+0.5, py, pz+0.5, vx, 0, vz);
            }
        }
    }






    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        return (capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY && (facing==null || facing == EnumFacing.UP)) || super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY){
            return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(new IFluidHandler()
            {
                @Override
                public IFluidTankProperties[] getTankProperties()
                {
                    FluidTankInfo info = waterTank.getInfo();
                    return new IFluidTankProperties[] { new FluidTankProperties(info.fluid, info.capacity, true, false) };
                }

                @Override
                public int fill(FluidStack resource, boolean doFill)
                {
                    if(resource.getFluid()!= FluidRegistry.WATER){
                        return 0;
                    }
                    int filled = waterTank.fill(resource, doFill);
                    if(filled>0){
                        setSomthingchanged();
                    }
                    return filled;
                }

                @Nullable
                @Override
                public FluidStack drain(FluidStack resource, boolean doDrain)
                {
                    return null;
                }

                @Nullable
                @Override
                public FluidStack drain(int maxDrain, boolean doDrain)
                {

                    return null;
                }
            });
        }
        return super.getCapability(capability, facing);
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        if(compound.hasKey("tank1")){
            waterTank.readFromNBT(compound.getCompoundTag("tank1"));
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        NBTTagCompound tank1 = waterTank.writeToNBT(new NBTTagCompound());
        compound.setTag("tank1",tank1);
        return super.writeToNBT(compound);
    }
}
