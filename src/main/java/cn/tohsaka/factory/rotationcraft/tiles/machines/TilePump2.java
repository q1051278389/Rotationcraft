package cn.tohsaka.factory.rotationcraft.tiles.machines;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IModelProvider;
import cn.tohsaka.factory.rotationcraft.blocks.base.BlockPlaceHolder;
import cn.tohsaka.factory.rotationcraft.blocks.machine.BlockExtractor;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.machine.ModelPump;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileModelMachine;
import cn.tohsaka.factory.rotationcraft.utils.AxlsUtils;
import cn.tohsaka.factory.rotationcraft.utils.FluidHelper;
import cn.tohsaka.factory.rotationcraft.utils.IteratorAABB;
import net.minecraft.block.Block;
import net.minecraft.block.BlockDynamicLiquid;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.*;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fml.common.registry.GameRegistry;
import org.lwjgl.opengl.GL11;

import javax.annotation.Nullable;

@GameInitializer(after = BlockExtractor.class)
public class TilePump2 extends TileModelMachine implements IModelProvider {
    public TilePump2(){

    }

    public FluidTank fluidTank = new FluidTank(32000);

    public static void init(){
        GameRegistry.registerTileEntity(TilePump2.class,new ResourceLocation(RotationCraft.MOD_ID,"tilepump2"));
    }

    @Override
    public RotaryModelBase getModel() {
        return new ModelPump();
    }

    @Override
    public String getTexName() {
        return "pumptex";
    }

    @Override
    public void updateInformationForTEISR(ItemStack stack, TileEntity tileEntity, float partialTicks) {

    }

    @Override
    public void beginItemRender() {
        GL11.glRotatef(90,0,1,0);
    }



    @Override
    public boolean isActive() {
        return powerMachine.getPower().isPowered() &&
                powerMachine.getPower().getWatt()>getMinimumPower().getWatt() &&
                powerMachine.getPower().getTorque()>=getMinimumPower().getTorque();
    }

    public IteratorAABB workIterator;

    @Override
    public void update() {
        if(world.isRemote){
            if(isActive()) {
                angle = AxlsUtils.calcAngle(angle, powerMachine.getPower().getSpeed());
            }
            /*if(isActive() && !RenderUtils.containsRender(this)){
                addIO();
            }*/
        }else{
            updatePowerInput();
            if(!isActive()){
                return;
            }
            transferOutputFluid();
            if((fluidTank.getCapacity() - fluidTank.getFluidAmount())<1000){
                return;
            }
            update2(1);

        }
    }
    IBlockState placeholder = BlockPlaceHolder.INSTANCE.getDefaultState();

    public boolean finished = false;
    public PumpWorking pumpWorking;
    public void update2(int range) {
        if (this.world.isRemote) {
            return;
        }
        if(workIterator==null || !workIterator.hasNext()){
            int rr = (int) Math.floor(range/2d);
            workIterator = new IteratorAABB(new BlockPos(0,0,0).add(-rr,0,-rr),range,1,range);
            pumpWorking = null;
        }
        if(pumpWorking == null || pumpWorking.isfinished){
            BlockPos next = workIterator.next();
            ChunkPos chunkPos = world.getChunkFromBlockCoords(pos).getPos();
            pumpWorking = new PumpWorking(new ChunkPos(chunkPos.x+next.getX(),chunkPos.z+next.getZ()),this.pos.getY()-1);
        }else {

        }

    }

    private class PumpWorking{
        ChunkPos chunkPos;
        int startY;
        boolean isfinished = false;
        public PumpWorking(ChunkPos chunkPos, int startY) {
            this.chunkPos = chunkPos;
            this.startY = startY;
        }

        public ChunkPos getChunkPos() {
            return chunkPos;
        }

        public void setChunkPos(ChunkPos chunkPos) {
            this.chunkPos = chunkPos;
        }

        public int getStartY() {
            return startY;
        }

        public void setStartY(int startY) {
            this.startY = startY;
        }

        public boolean isIsfinished() {
            return isfinished;
        }

        public void setIsfinished(boolean isfinished) {
            this.isfinished = isfinished;
        }
    }


    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        return compound;
    }

    public void transferOutputFluid()
    {
        if (fluidTank.getFluidAmount() <= 0)
            return;

        int side;
        FluidStack output = new FluidStack(fluidTank.getFluid(), Math.min(fluidTank.getFluidAmount(), 16000));

        for(int i=2;i<6;i++){
            int toDrain = FluidHelper.insertFluidIntoAdjacentFluidHandler(this, EnumFacing.VALUES[i], output, true);
            if (toDrain > 0)
            {
                fluidTank.drain(toDrain, true);
                break;
            }
        }
        return;
    }

    private Fluid getFluidOnBottom(){
        IBlockState state = world.getBlockState(pos.add(0,-1,0));
        Block block = state.getBlock();
        Fluid fluid = lookupFluidForBlock(block);
        return fluid;
    }
    private Fluid getFluidOnPos(BlockPos target){
        IBlockState state = world.getBlockState(target);
        Block block = state.getBlock();
        if(block instanceof BlockDynamicLiquid){
            return null;
        }
        Fluid fluid = lookupFluidForBlock(block);
        return fluid;
    }
    final RotaryPower minimumpower = new RotaryPower(16,256,0,0);
    @Override
    public RotaryPower getMinimumPower() {
        return minimumpower;
    }

    public static Fluid lookupFluidForBlock(Block b) {
        if (b == Blocks.LAVA){
            return FluidRegistry.LAVA;
        }
        if(b == Blocks.FLOWING_LAVA){
            return null;
        }
        if (b == Blocks.WATER || b == Blocks.FLOWING_WATER) {
            return FluidRegistry.WATER;
        }
        Fluid f = FluidRegistry.lookupFluidForBlock(b);
        if (f == null && b instanceof IFluidBlock) {
            f = ((IFluidBlock) b).getFluid();
        }
        return f;
    }



    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        return capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY || capability == CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY || super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY) {
            return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(fluidTank);
        }
        return super.getCapability(capability, facing);
    }

}
