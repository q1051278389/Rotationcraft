package cn.tohsaka.factory.rotationcraft.tiles.transmits;

import cn.tohsaka.factory.librotary.api.power.CapabilityRotaryPower;
import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.librotary.api.power.base.RotaryPowerForwarder;
import cn.tohsaka.factory.librotary.api.power.interfaces.IMeterable;
import cn.tohsaka.factory.librotary.api.power.interfaces.IPowerForwarder;
import cn.tohsaka.factory.librotary.api.power.interfaces.IPowerMachine;
import cn.tohsaka.factory.librotary.api.power.interfaces.IPowerTranmitsAccelerator;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.api.power.IModeProvider;
import cn.tohsaka.factory.rotationcraft.blocks.transmits.BlockCorner;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerCorner;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.GuiCorner;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileMachineBase;
import cn.tohsaka.factory.rotationcraft.utils.AxlsUtils;
import cn.tohsaka.factory.rotationcraft.utils.FacingTool;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;
import cn.tohsaka.factory.rotationcraft.utils.WorldUtils;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fml.common.registry.GameRegistry;

import javax.annotation.Nullable;
import java.util.List;

@GameInitializer(after = BlockCorner.class)
public class TileCorner extends TileMachineBase implements RenderUtils.IOutBoxDrawable, ITickable, IModeProvider, IGuiProvider, IPowerTranmitsAccelerator, IMeterable {
    public IPowerForwarder powerForwarder;
    public TileCorner(){
        powerForwarder = new RotaryPowerForwarder(this) {

            @Nullable
            @Override
            public World getWorld() {
                return TileCorner.this.getWorld();
            }

            @Nullable
            @Override
            public BlockPos getSrc() {
                return TileCorner.this.getSrc();
            }

            @Nullable
            @Override
            public BlockPos getDest() {
                return TileCorner.this.getDst();
            }

            @Nullable
            @Override
            public IPowerMachine getParent() {
                return WorldUtils.getCapability(CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY,pos,world,facingin);
            }

            @Nullable
            @Override
            public IPowerMachine getChild() {
                return WorldUtils.getCapability(CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY,pos,world,facing);
            }
        };
    }
    public double powercounter = 0L;
    public long ticks = 0L;
    public static void init(){
        GameRegistry.registerTileEntity(TileCorner.class,new ResourceLocation(RotationCraft.MOD_ID,"tilecorner"));
    }
    @Override
    public void update() {
        if(world.isRemote){
            angle = AxlsUtils.calcAngle(angle,powerForwarder.getPower().getSpeed());
        }else {
            updatePowerInput();
        }
    }






    protected void updatePowerInput(){
        BlockPos inPos;
        if(facingin.ordinal()>1){
            inPos = FacingTool.getRelativePos(pos, facingin, FacingTool.Position.FRONT,1);
        }else{
            if(facingin == EnumFacing.UP){
                inPos = FacingTool.getRelativePos(pos, facingin, FacingTool.Position.UP,1);
            }else{
                inPos = FacingTool.getRelativePos(pos, facingin, FacingTool.Position.DOWN,1);
            }
        }

        IPowerMachine cap = CapabilityRotaryPower.getCapability(world, inPos,EnumFacing.VALUES[facingin.ordinal()^1]);
        if(cap!=null){
            RotaryPower powerIn = cap.getPower();
            powerForwarder.setPower(powerIn);
        }else{
            powerForwarder.setPower(RotaryPower.ZERO);
        }
    }


    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        if(capability == CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY){
            if(facing == null){
                return true;
            }
            if(facing == this.facing){
                return true;
            }
            return false;
        }
        return super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        if(capability == CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY){
            if(facing == null){
                return CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY.cast(powerForwarder);
            }
            if(facing == this.facing){
                return CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY.cast(powerForwarder);
            }
        }
        return super.getCapability(capability, facing);
    }





    @Override
    public void getOutBoxList(NonNullList<RenderUtils.RenderItem> drawables) {
        if(facingin.ordinal()>1){
            drawables.add(new RenderUtils.RenderItem(0x0000FF, FacingTool.getRelativePos(pos, facingin, FacingTool.Position.FRONT,1),1F));
        }else{
            if(facingin == EnumFacing.UP){
                drawables.add(new RenderUtils.RenderItem(0x0000FF, FacingTool.getRelativePos(pos, facingin, FacingTool.Position.UP,1),1F));
            }else{
                drawables.add(new RenderUtils.RenderItem(0x0000FF, FacingTool.getRelativePos(pos, facingin, FacingTool.Position.DOWN,1),1F));
            }
        }
        if(facing.ordinal()>1){
            drawables.add(new RenderUtils.RenderItem(0xFF0000,FacingTool.getRelativePos(pos,facing, FacingTool.Position.FRONT,1),1F));
        }else{
            if(facing == EnumFacing.UP){
                drawables.add(new RenderUtils.RenderItem(0xFF0000,FacingTool.getRelativePos(pos,facing, FacingTool.Position.UP,1),1F));
            }else{
                drawables.add(new RenderUtils.RenderItem(0xFF0000,FacingTool.getRelativePos(pos,facing, FacingTool.Position.DOWN,1),1F));
            }
        }

        super.getOutBoxList(drawables);
    }



    public BlockPos getSrc() {
        return FacingTool.getRelativePos(pos, facingin, FacingTool.Position.FRONT,1);
    }

    @Override
    public BlockPos getDst() {
        return FacingTool.getRelativePos(pos,facing, FacingTool.Position.FRONT,1);
    }

    public EnumFacing facingin;
    public void setFacingIn(EnumFacing f){
        facingin = f;
    }
    public EnumFacing getFacingIn(){
        return facingin;
    }




    @Override
    public boolean isActive() {
        return powerForwarder.getPower().isPowered();
    }

    RotaryPower inputpower = RotaryPower.ZERO;



    @Override
    public boolean onChangingMode(World world, EntityPlayer player, BlockPos pos) {
        return false;
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound tag = super.getUpdateTag();

        return tag;
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tag) {
        super.handleUpdateTag(tag);
        writeToNBT(tag);
    }

    @Override
    public Object getGuiClient(EntityPlayer player) {
        return new GuiCorner(this,player.inventory);
    }

    @Override
    public Object getGuiServer(EntityPlayer player) {
        return new ContainerCorner(player.inventory);
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        facingin = EnumFacing.VALUES[compound.getInteger("facingin")];
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound.setInteger("facingin",getFacingIn().ordinal());
        return super.writeToNBT(compound);
    }

    @Override
    public NBTTagCompound getModePacket() {
        NBTTagCompound tag = super.getModePacket();
        tag.setInteger("facing",facing.ordinal());
        tag.setInteger("facingin",facingin.ordinal());
        return tag;
    }

    @Override
    public void handleModePacket(NBTTagCompound compound) {
        super.handleModePacket(compound);
        setFacingIn(EnumFacing.VALUES[compound.getInteger("facingin")]);
        setFacing(EnumFacing.VALUES[compound.getInteger("facing")]);
    }

    @Override
    public void setFacing(EnumFacing facing) {
        this.facing = facing;
        IBlockState ib = this.world.getBlockState(this.pos);
        ib = ib.withProperty(BlockCorner.FACING,facing);
        this.world.setBlockState(this.pos,ib);


        if(world.isRemote){
            if(!RenderUtils.containsRender(this)){
                renderboxtime = world.getTotalWorldTime();
                RenderUtils.addRender(this);
            }
        }

        markDirty();
    }

    @Override
    public void rotateBlock(EntityPlayer player) {

    }

    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {

    }

    @Override
    public void onPowerChange() {
        updatePowerInput();
    }

    @Override
    public String getMeterInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("TileEntity["+world.getBlockState(pos).getBlock().getLocalizedName()+"]\n");
        sb.append(this.pos.toString()+"  FacingIn: "+facingin.getName()+"\n");
        sb.append(powerForwarder.getPower()+"  FacingOut: "+facing.getName()+"\n");
        return sb.toString();
    }

    @Override
    public void onMeter() {
        addIO();
    }

}
