package cn.tohsaka.factory.rotationcraft.tiles.machines;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IModelProvider;
import cn.tohsaka.factory.rotationcraft.api.thermal.IThermalMachine;
import cn.tohsaka.factory.rotationcraft.blocks.machine.BlockExtractor;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.machine.ModelFriction;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileModelMachine;
import cn.tohsaka.factory.rotationcraft.utils.FacingTool;
import cn.tohsaka.factory.rotationcraft.utils.ReikaMathLibrary;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import cn.tohsaka.factory.rotationcraft.utils.TempetureUtils;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.registry.GameRegistry;
import org.lwjgl.opengl.GL11;

import java.util.List;

@GameInitializer(after = BlockExtractor.class)
public class TileFriction extends TileModelMachine implements IModelProvider, IThermalMachine {
    public TileFriction(){

    }

    public static void init(){
        GameRegistry.registerTileEntity(TileFriction.class,new ResourceLocation(RotationCraft.MOD_ID,"tilefriction"));
    }

    @Override
    public RotaryModelBase getModel() {
        return new ModelFriction();
    }

    @Override
    public String getTexName() {
        return "frictiontex";
    }

    @Override
    public void updateInformationForTEISR(ItemStack stack, TileEntity tileEntity, float partialTicks) {

    }

    @Override
    public void beginItemRender() {
        GL11.glRotatef(180,0,1,0);
    }


    @Override
    public void update() {
        super.update();
        if(!world.isRemote){
            updatePowerInput();
            if(world.getTotalWorldTime() % 60 == 0){
                updateTemperature();
                somthingchanged = true;
            }

            if(somthingchanged){
                NetworkDispatcher.dispatchMachineUpdate(this);
                somthingchanged = false;
            }
        }
    }

    private static final int MINTORQUE = 64;
    private static final int MINPOWER = 4096;
    private static final int MAXTEMP = 1950;
    public void updateTemperature() {
        if(!checkTarget() || world==null || pos==null){
            return;
        }
        RotaryPower power = powerMachine.getPower();
        if (power.getTorque() >= MINTORQUE && power.getWatt() >= MINPOWER && power.getSpeed() > 0) {
            temp += 3* ReikaMathLibrary.logbase(power.getSpeed(), 2)*ReikaMathLibrary.logbase(power.getTorque(), 2);
        }
        int Tamb = power.getWatt() > MINPOWER && power.getTorque() > MINTORQUE ? 30 : Math.round(TempetureUtils.getBiomeTemp(world,pos));
        if (temp > Tamb) {
            temp -= (temp-Tamb)/5;
        }
        else {
            temp += (temp-Tamb)/5;
        }
        if (temp - Tamb <= 4 && temp > Tamb)
            temp--;
        if (temp > MAXTEMP) {
            temp = MAXTEMP;
        }
        if (temp < Tamb){
            temp = Tamb;
        }
    }

    private boolean checkTarget() {
        if(world!=null){
            BlockPos target = FacingTool.getRelativePos(getPos(),facing, FacingTool.Position.FRONT,1);
            TileEntity tileEntity = world.getTileEntity(target);
            if(tileEntity!=null && tileEntity instanceof IThermalMachine){
                if(!((IThermalMachine) tileEntity).isTempSource()){
                    return true;
                }
            }
        }
        return false;
    }


    @Override
    public boolean canDamage() {
        return false;
    }

    @Override
    public float getDamagedTemp() {
        return 0;
    }

    @Override
    public void onDamage() {

    }

    @Override
    public float getTemp() {
        return temp;
    }
    @Override
    public void setTemp(Float thermal) {

    }

    boolean somthingchanged = false;
    @Override
    public void addTemp(Float thermal) {

    }

    @Override
    public boolean isTempSource() {
        return true;
    }

    @Override
    public RotaryPower getMinimumPower() {
        return new RotaryPower(1,1);
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound tag = super.getUpdateTag();
        tag.setFloat("temp",temp);
        return tag;
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tag) {
        super.handleUpdateTag(tag);
        if(tag.hasKey("temp")){
            temp = tag.getFloat("temp");
        }
    }

    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {
        tooltip.add(String.format("%.1f",getTemp())+" "+ StringHelper.localize("info.rc.tempchar"));
    }
}
