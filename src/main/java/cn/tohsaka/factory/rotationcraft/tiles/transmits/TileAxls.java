package cn.tohsaka.factory.rotationcraft.tiles.transmits;

import cn.tohsaka.factory.librotary.api.power.CapabilityRotaryPower;
import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.librotary.api.power.base.RotaryPowerForwarder;
import cn.tohsaka.factory.librotary.api.power.interfaces.IMeterable;
import cn.tohsaka.factory.librotary.api.power.interfaces.IPowerForwarder;
import cn.tohsaka.factory.librotary.api.power.interfaces.IPowerMachine;
import cn.tohsaka.factory.librotary.api.power.interfaces.IPowerTranmitsAccelerator;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.power.IModeProvider;
import cn.tohsaka.factory.rotationcraft.blocks.transmits.BlockAxls;
import cn.tohsaka.factory.rotationcraft.blocks.transmits.BlockMeter;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileMachineBase;
import cn.tohsaka.factory.rotationcraft.props.DamageSources;
import cn.tohsaka.factory.rotationcraft.utils.AxlsUtils;
import cn.tohsaka.factory.rotationcraft.utils.FacingTool;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;
import cn.tohsaka.factory.rotationcraft.utils.WorldUtils;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fml.common.registry.GameRegistry;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

@GameInitializer(after = BlockAxls.class)
public class TileAxls extends TileMachineBase implements RenderUtils.IOutBoxDrawable,ITickable, IModeProvider, IPowerTranmitsAccelerator, IMeterable {
    public IPowerForwarder powerForwarder;
    public TileAxls(){
        powerForwarder = new RotaryPowerForwarder(this) {


            @Nullable
            @Override
            public World getWorld() {
                return TileAxls.this.getWorld();
            }

            @Nullable
            @Override
            public BlockPos getSrc() {
                return TileAxls.this.getSrc();
            }

            @Nullable
            @Override
            public BlockPos getDest() {
                return TileAxls.this.getDst();
            }

            @Nullable
            @Override
            public IPowerMachine getParent() {
                return WorldUtils.getCapability(CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY,pos,world,facing);
            }

            @Nullable
            @Override
            public IPowerMachine getChild() {
                return WorldUtils.getCapability(CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY,pos,world,facing.getOpposite());
            }
        };
    }

    public double powercounter = 0L;
    public long ticks = 0L;
    public static void init(){
        GameRegistry.registerTileEntity(TileAxls.class,new ResourceLocation(RotationCraft.MOD_ID,"tileaxls"));
    }
    @Override
    public void update() {
        if(world.isRemote){
            if(isActive()) {
                angle = AxlsUtils.calcAngle(angle, powerForwarder.getPower().getSpeed());

                if (metercounter) {
                    powercounter += powerForwarder.getPower().getWatt();
                    ticks++;
                }
            }
        }else{
            updatePowerInput();
            if(isActive()){
                if(getType()==0){
                    RotaryPower maxium = RotaryPower.MAXPOWER_BEDROCK;
                    switch (getMetadata()){
                        case 0:
                            maxium = RotaryPower.MAXPOWER_HSLA;
                            break;
                        case 1:
                            maxium = RotaryPower.MAXPOWER_DIAMOND;
                            break;
                    }

                    if(powerForwarder.getPower().getWatt() > maxium.getWatt() &&
                            (powerForwarder.getPower().getSpeed() > maxium.getSpeed() || powerForwarder.getPower().getTorque() > maxium.getTorque())){
                        this.invalidatebyerror();
                    }
                }
            }
        }
    }


    @Override
    public void getOutBoxList(NonNullList<RenderUtils.RenderItem> drawables) {
        if(facing.ordinal()>1){
            drawables.add(new RenderUtils.RenderItem(0x0000FF, FacingTool.getRelativePos(pos, facing, FacingTool.Position.FRONT,1),1F));
            drawables.add(new RenderUtils.RenderItem(0xFF0000,FacingTool.getRelativePos(pos,facing, FacingTool.Position.BACK,1),1F));

        }else{
            if(facing == EnumFacing.UP){
                drawables.add(new RenderUtils.RenderItem(0x0000FF, pos.add(0,1,0),1F));
                drawables.add(new RenderUtils.RenderItem(0xFF0000,pos.add(0,-1,0),1F));
            }else{
                drawables.add(new RenderUtils.RenderItem(0x0000FF, pos.add(0,-1,0),1F));
                drawables.add(new RenderUtils.RenderItem(0xFF0000,pos.add(0,1,0),1F));
            }
        }
        super.getOutBoxList(drawables);
    }


    public BlockPos getSrc() {
        if(facing.ordinal()<2){
            if(facing==EnumFacing.UP){
                return pos.add(0,-1,0);
            }else{
                return pos.add(0,1,0);
            }
        }
        return FacingTool.getRelativePos(pos, facing, FacingTool.Position.FRONT,1);
    }

    @Override
    public BlockPos getDst() {
        if(facing.ordinal()<2){
            if(facing==EnumFacing.UP){
                return pos.add(0,1,0);
            }else{
                return pos.add(0,-1,0);
            }
        }
        return FacingTool.getRelativePos(pos, facing, FacingTool.Position.BACK,1);
    }



    @Override
    public boolean isActive() {
        return powerForwarder.getPower().isPowered();
    }


    public void invalidatebyerror(){
        //WorldUtils.makeDamaged(pos.add(0,2,0),world,getDamagedDrops());
        WorldUtils.makeExplode(pos,16,world,(int) Math.round(powerForwarder.getPower().getWatt()/1000L), getDamagedDrops(), DamageSources.GEAR_EXPLODE);
        world.removeTileEntity(pos);
        this.world.setBlockToAir(pos);
        super.invalidate();
        world.setBlockToAir(pos);
    }

    public List<ItemStack> getDamagedDrops(){
        List<ItemStack> list = new ArrayList<>();
        switch (getMetadata()){
            case 0:
                list.add(new ItemStack(Items.IRON_INGOT,1));
                break;
            case 1:
                list.add(new ItemStack(Items.DIAMOND,1));
                break;
        }
        return list;
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound tag = super.getUpdateTag();
        return tag;
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tag) {
        super.handleUpdateTag(tag);
    }
    public boolean metercounter = false;
    @Override
    public boolean onChangingMode(World world, EntityPlayer player, BlockPos pos) {
        if(getType()>0){
            metercounter = !metercounter;
            powercounter=0;
            ticks=0;
            return true;
        }
        return false;
    }
    public int getType(){
        return getBlockType().equals(RotationCraft.blocks.get(BlockMeter.NAME))?1:0;
    }

    @Override
    public boolean allowVerticalFacing() {
        if(this.getBlockType().getRegistryName().getResourcePath().toLowerCase().contains("meter")){
            return false;
        }
        return true;
    }

    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {

    }






    protected void updatePowerInput(){
        if(facing.ordinal()<2){
            if(facing==EnumFacing.UP){
                updatePowerInput(FacingTool.Position.UP);
            }else{
                updatePowerInput(FacingTool.Position.DOWN);
            }
        }else{
            updatePowerInput(FacingTool.Position.FRONT);
        }
    }
    protected void updatePowerInput(FacingTool.Position position){
        EnumFacing side = FacingTool.revert(facing);
        IPowerMachine cap = null;
        if(facing.ordinal()>1){
            cap = CapabilityRotaryPower.getCapability(world, FacingTool.getRelativePos(pos,facing, position,1),side);
        }else{
            cap = CapabilityRotaryPower.getCapability(world, pos.add(0,(position== FacingTool.Position.UP)?1:-1,0),side);
        }
        if(cap!=null){
            RotaryPower powerIn = cap.getPower();
            powerForwarder.setPower(powerIn);
        }else{
            powerForwarder.setPower(RotaryPower.ZERO);
        }
    }


    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        if(capability == CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY){
            if(facing == null){
                return true;
            }
            if(facing == FacingTool.revert(this.facing)){
                return true;
            }
            return false;
        }
        return super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        if(capability == CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY){
            if(facing == null){
                return CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY.cast(powerForwarder);
            }
            if(facing == FacingTool.revert(this.facing)){
                return CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY.cast(powerForwarder);
            }
        }
        return super.getCapability(capability, facing);
    }

    @Override
    public void onPowerChange() {
        updatePowerInput();
    }

    @Override
    public String getMeterInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("TileEntity["+world.getBlockState(pos).getBlock().getLocalizedName()+"]\n");
        sb.append(this.pos.toString()+"  Facing: "+facing.getName()+"\n");
        sb.append(powerForwarder.getPower()+"\n");
        return sb.toString();
    }

    @Override
    public void onMeter() {
        addIO();
    }
}
