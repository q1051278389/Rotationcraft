package cn.tohsaka.factory.rotationcraft.tiles.machines;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.librotary.packet.PacketCustom;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IModelProvider;
import cn.tohsaka.factory.rotationcraft.blocks.machine.BlockDynamo;
import cn.tohsaka.factory.rotationcraft.blocks.reactor.BlockBoiler;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.machine.ModelExchanger;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.etc.FluidTankLocked;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileModelMachine;
import cn.tohsaka.factory.rotationcraft.tiles.reactor.TileBoiler;
import cn.tohsaka.factory.rotationcraft.utils.FacingTool;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.FluidTankPropertiesWrapper;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.common.registry.GameRegistry;

import javax.annotation.Nullable;
import java.util.List;

@GameInitializer(after = BlockDynamo.class)
public class TileExchanger extends TileModelMachine implements IModelProvider {

    public TileExchanger(){
        super(FacingTool.Position.DOWN);
    }

    public static void init(){
        GameRegistry.registerTileEntity(TileExchanger.class,new ResourceLocation(RotationCraft.MOD_ID,"tileexchanger"));
    }

    @Override
    public RotaryModelBase getModel() {
        return new ModelExchanger();
    }

    @Override
    public String getTexName() {
        return "exchanger";
    }

    @Override
    protected String getModelTex() {
        return "exchanger";
    }

    @Override
    public void updateInformationForTEISR(ItemStack stack, TileEntity tileEntity, float partialTicks) {

    }

    @Override
    public void beginItemRender() {

    }

    @Override
    public void update() {

            if(!world.isRemote) {
                updatePowerInput();
                if (world.getTotalWorldTime() % 40 == 0) {
                    for (int i = 0; i < 4; i++) {
                        IBlockState state = world.getBlockState(pos.offset(EnumFacing.VALUES[i + 2]));
                        validSide[i] = (state.getBlock() instanceof BlockBoiler) ? true : false;
                    }
                }
                if (powerMachine.getPower().getTorque() >= powerRequired.getTorque() && powerMachine.getPower().getWatt() > powerRequired.getWatt()) {
                    int count = Math.min(20, Math.max(1, Math.round(powerMachine.getPower().getSpeed() / powerRequired.getSpeed())));
                    if((world.getTotalWorldTime() % Math.max(1,(20-count))) == 0){
                        work();
                    }
                }
                if(world.getTotalWorldTime() % 200 ==0){
                    markDirty();
                }
            }

    }
    public void work(){
        if(tankco2h.getFluidAmount()>=20 && (tankco2.getCapacity() - tankco2.getFluidAmount())>=20){
            tankco2h.drain(20,true);
            tankco2.fill(new FluidStack(RotationCraft.fluids.get("co2"),20),true);
            temp+=20;
        }
        if(temp>200){
            for(int i=0;i<4;i++){
                TileEntity tileEntity = world.getTileEntity(pos.offset(EnumFacing.VALUES[i+2]));
                if(tileEntity instanceof TileBoiler){
                    temp -=((TileBoiler) tileEntity).insertHeat(temp);
                }
            }
        }
    }
    public boolean[] validSide = {false,false,false,false};
    public FluidTank tankco2h = new FluidTankLocked(RotationCraft.fluids.get("co2h"),4000);
    public FluidTank tankco2 = new FluidTankLocked(RotationCraft.fluids.get("co2"),4000);

    private static final RotaryPower powerRequired = new RotaryPower(16,512);
    @Override
    public RotaryPower getMinimumPower() {
        return powerRequired;
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound tag = super.getUpdateTag();
        tag.setTag("co2h",tankco2h.writeToNBT(new NBTTagCompound()));
        tag.setTag("co2",tankco2.writeToNBT(new NBTTagCompound()));
        return tag;
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tag) {
        super.handleUpdateTag(tag);
        if (tag.hasKey("co2h")) {
            tankco2h.readFromNBT(tag.getCompoundTag("co2h"));
        }
        if (tag.hasKey("co2")) {
            tankco2.readFromNBT(tag.getCompoundTag("co2"));
        }
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        if (compound.hasKey("co2h")) {
            tankco2h.readFromNBT(compound.getCompoundTag("co2h"));
        }
        if (compound.hasKey("co2")) {
            tankco2.readFromNBT(compound.getCompoundTag("co2"));
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound.setTag("co2h",tankco2h.writeToNBT(new NBTTagCompound()));
        compound.setTag("co2",tankco2.writeToNBT(new NBTTagCompound()));
        return super.writeToNBT(compound);
    }

    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {
        super.getWailaBody(itemStack, tooltip);
        tooltip.add("Temperture:"+String.format("%.2f",temp));
    }


    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing side) {
        return (capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY && side!=EnumFacing.DOWN) || super.hasCapability(capability, side);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing side) {
        if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY){
            if(side==null){
                return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(new IFluidHandler() {
                    @Override
                    public IFluidTankProperties[] getTankProperties() {
                        return new IFluidTankProperties[]{new FluidTankPropertiesWrapper(tankco2),new FluidTankPropertiesWrapper(tankco2h)};
                    }

                    @Override
                    public int fill(FluidStack fluidStack, boolean b) {
                        return 0;
                    }

                    @Nullable
                    @Override
                    public FluidStack drain(FluidStack fluidStack, boolean b) {
                        return null;
                    }

                    @Nullable
                    @Override
                    public FluidStack drain(int i, boolean b) {
                        return null;
                    }
                });
            }else {
                if(side==EnumFacing.UP){
                    return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(tankco2);
                }else{
                    return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(tankco2h);
                }
            }
        }
        return super.getCapability(capability, side);
    }

    @Override
    public void writeGuiData(PacketCustom packet, boolean isFullSync) {
        packet.writeNBTTagCompound(getUpdateTag());
    }

    @Override
    public void readGuiData(PacketCustom packet, boolean isFullSync) {
        handleUpdateTag(packet.readNBTTagCompound());
    }
}
