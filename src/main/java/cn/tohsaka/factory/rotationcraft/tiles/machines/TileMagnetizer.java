package cn.tohsaka.factory.rotationcraft.tiles.machines;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IModelProvider;
import cn.tohsaka.factory.rotationcraft.blocks.machine.BlockMagnetizer;
import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.MagnetizerRecipe;
import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.animated.ModelMagnetizer;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileModelMachine;
import cn.tohsaka.factory.rotationcraft.utils.AxlsUtils;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;

import java.text.DecimalFormat;
import java.util.List;

@GameInitializer(after = BlockMagnetizer.class)
public class TileMagnetizer extends TileModelMachine implements IModelProvider {
    public static void init(){
        GameRegistry.registerTileEntity(TileMagnetizer.class,new ResourceLocation(RotationCraft.MOD_ID,"tilemagnetizer"));
    }
    public TileMagnetizer(){
        createSlots(1);
        setSlotMode(SlotMode.OMNI,0);
    }

    @Override
    public void update() {
        if(!world.isRemote){
            updatePowerInput();
            tick();

        }else{
            angle = AxlsUtils.calcAngle(angle,powerMachine.getPower().getSpeed());
        }
    }
    private boolean lastrs = false;
    public void tick(){

        boolean rs = world.isBlockPowered(pos);
        if(rs != lastrs && hasItem() && powerMachine.getPower().getTorque()>=1 && powerMachine.getPower().getSpeed() >= getMinimumPower().getSpeed()){
            int ratio = Math.round(powerMachine.getPower().getSpeed() / getMinimumPower().getSpeed());
            magnetize(Math.max(1,ratio));
        }

        lastrs = rs;
    }

    public void magnetize(int ratio){
        ItemStack stack = getStackInSlot(0);
        for(CraftingPattern pattern: Recipes.INSTANCE.getList(TileMagnetizer.class)){
            if(((MagnetizerRecipe)pattern).igd.test(stack)){
                MagnetizerRecipe recipe = ((MagnetizerRecipe)pattern);
                if(stack.hasTagCompound()){
                    NBTTagCompound tag = stack.getTagCompound();
                    double value = tag.getDouble("magnet");
                    value = Math.min(recipe.maxCharge,value + (ratio * recipe.uTvia2K));
                    tag.setDouble("magnet",value);
                    stack.setTagCompound(tag);
                }else{
                    NBTTagCompound tag = new NBTTagCompound();
                    double value = Math.min(recipe.maxCharge,(ratio * recipe.uTvia2K));
                    tag.setDouble("magnet",value);
                    stack.setTagCompound(tag);
                }
            }
        }
    }

    public boolean playerInteract(EntityPlayer playerIn, EnumHand hand, EnumFacing facing) {
        if(world.isRemote){
            return true;
        }
        ItemStack stack = playerIn.getHeldItem(hand).copy();
        stack.setCount(1);
        if(hasItem()){
            world.spawnEntity(new EntityItem(world,pos.getX(),pos.getY()+1,pos.getZ(),getStackInSlot(0)));
            setInventorySlotContents(0,ItemStack.EMPTY);
        }else{
            setInventorySlotContents(0,stack.copy());
            playerIn.getHeldItem(hand).grow(-1);
        }
        return true;
    }

    public boolean hasItem(){
        return !inventory[0].isEmpty();
    }



    @Override
    public RotaryModelBase getModel() {
        return new ModelMagnetizer();
    }

    @Override
    public String getTexName() {
        return "magnettex";
    }
    @Override
    public void beginRenderModelOnWorld() {
        GlStateManager.rotate(-90,0,1,0);
    }

    @Override
    public RotaryPower getMinimumPower() {
        return new RotaryPower(1,2048);
    }

    @Override
    public void updateInformationForTEISR(ItemStack stack, TileEntity tileEntity, float partialTicks) {

    }

    @Override
    public void beginItemRender() {

    }

    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {
        super.getWailaBody(itemStack, tooltip);
        if(hasItem() && getStackInSlot(0).hasTagCompound()){
            ItemStack stack = getStackInSlot(0);
            DecimalFormat df = new DecimalFormat("#0.00000");
            tooltip.add(stack.getDisplayName()+" : "+df.format(stack.getTagCompound().getDouble("magnet"))+" Tesla");
        }
    }
}
