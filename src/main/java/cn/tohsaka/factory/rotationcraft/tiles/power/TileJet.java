package cn.tohsaka.factory.rotationcraft.tiles.power;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IControlableEngine;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.api.IUpgradable;
import cn.tohsaka.factory.rotationcraft.api.IUpgrade;
import cn.tohsaka.factory.rotationcraft.api.thermal.IThermalMachine;
import cn.tohsaka.factory.rotationcraft.blocks.engine.BlockDc;
import cn.tohsaka.factory.rotationcraft.blocks.engine.BlockJet;
import cn.tohsaka.factory.rotationcraft.config.GeneralConfig;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.engine.ModelJet;
import cn.tohsaka.factory.rotationcraft.etc.sounds.SoundHandler;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerJet;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.GuiJet;
import cn.tohsaka.factory.rotationcraft.init.Sounds;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemMaterial;
import cn.tohsaka.factory.rotationcraft.items.ItemUpgrade;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileEngine;
import cn.tohsaka.factory.rotationcraft.props.DamageSources;
import cn.tohsaka.factory.rotationcraft.utils.FacingTool;
import cn.tohsaka.factory.rotationcraft.utils.FluidHelper;
import cn.tohsaka.factory.rotationcraft.utils.ReikaParticleHelper;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.FluidTankInfo;
import net.minecraftforge.fluids.UniversalBucket;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.FluidTankProperties;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Random;

@GameInitializer(after = BlockDc.class)
public class TileJet extends TileEngine implements IGuiProvider, IThermalMachine, IControlableEngine, IUpgradable {
    public static void init(){
        GameRegistry.registerTileEntity(TileJet.class,new ResourceLocation(RotationCraft.MOD_ID,"tilejet"));
    }
    public FluidTank fuelTank;
    public TileJet(){
        super(new RotaryPower(2048,65536));
        createSlots(1);
        setSlotMode(SlotMode.OMNI,new int[]{0});
        fuelTank = new FluidTank(1000 * 64);
    }
    @SideOnly(Side.CLIENT)
    @Override
    public RotaryModelBase getModel() {
        return new ModelJet();
    }
    @SideOnly(Side.CLIENT)
    @Override
    public String getTexName() {
        return "jettex";
    }

    @SideOnly(Side.CLIENT)
    public void playsound(){
        if(startTime<10){
            SoundHandler.startTileSound(Sounds.JETSTART.getEvent(),0.5F,pos);
        }
        if(startTime>=300){
            SoundHandler.startTileSound(afterBurning?Sounds.JETAFTERBURNER.getEvent():Sounds.JETRUNNING.getEvent(),0.5F,pos);
        }
    }

    @SideOnly(Side.CLIENT)
    public void stopsound(){
        SoundHandler.stopTileSound(pos);
    }


    @Override
    public void update() {
        if(!getStackInSlot(0).isEmpty() && (fuelTank.getCapacity()-fuelTank.getFluidAmount())>0){
            if(getStackInSlot(0).getItem() instanceof UniversalBucket){
                if((fuelTank.getCapacity()-fuelTank.getFluidAmount())>=1000){
                    FluidStack fluid = FluidHelper.getFluidForFilledItem(getStackInSlot(0));
                    if(fluid.getFluid().equals(RotationCraft.fluids.get("jetfuel"))){
                        fuelTank.fill(fluid,true);
                        setInventorySlotContents(0,new ItemStack(Items.BUCKET));
                    }
                }
            }else{
                IFluidHandler handler = getStackInSlot(0).getCapability(CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY,null);
                if (handler==null){
                    handler = getStackInSlot(0).getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY,null);
                }
                if (handler!=null){
                    FluidStack stack = handler.drain(new FluidStack(RotationCraft.fluids.get("jetfuel"),1000),true);
                    fuelTank.fill(stack,true);
                }
            }
        }
        super.update();
        if(isActive){
            if(isFailing){
                failingtime++;
            }
            if(world.isRemote){
                doAfterburning(world,pos.getX(),pos.getY(),pos.getZ());
                if(isFailing){
                    if(placer!=null && world.getPlayerEntityByName(placer)!=null){
                        world.getPlayerEntityByName(placer).sendStatusMessage(new TextComponentString(String.format(StringHelper.localize("info.rc.jetfail.warning"),300-Math.round(failingtime/20))),true);
                    }
                    doSmoke();
                }
            }else{
                if(world.getTotalWorldTime() % 4 == 0 && failingtime>(20*300)){
                    doFail();
                }
            }
            doHoover();
        }


        if(world.isRemote){
            if(isActive){
                playsound();
            }else{
                stopsound();
            }
        }
    }

    private void doFail() {
        world.createExplosion(null, pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, 30*world.rand.nextFloat(), GeneralConfig.jetfail_terrainbreak);
    }


    public void doSmoke(){
        for(int i=0;i<8;i++){
            ReikaParticleHelper.spawnAt(EnumParticleTypes.SMOKE_NORMAL,world, pos.getX()+world.rand.nextDouble(), pos.getY()+1*world.rand.nextDouble(), pos.getZ()+world.rand.nextDouble());
        }
    }




    @Override
    public void tick() {
        if(fuelTank.getFluidAmount() > 0){
            if(isActive){
                if(startTime<=2000){
                    startTime++;
                    float per = startTime / 2000F;
                    powerSource.setPower(new RotaryPower(getTorque(),Math.min(65536,Math.round(65536*per)),0,0));
                    if(world.getTotalWorldTime() % 20 == 0){
                        fuelTank.drain(afterBurning?160:80,true);
                    }
                }else{
                    powerSource.setPower(new RotaryPower(getTorque(),Math.round(65536 * effective),0,0));
                    if((world.getTotalWorldTime() % (Math.round(20f / effective))) == 0){
                        fuelTank.drain(afterBurning?160:80,true);
                    }
                }

            }else{
                startTime = 0;
                isActive = true;
                fuelTank.drain(100,true);
                NetworkDispatcher.dispatchMachineUpdate(this);
            }
        }else{
            isActive = false;
            startTime = 0;
            powerSource.setPower(RotaryPower.ZERO);
        }
        if(world.getTotalWorldTime() % 100 == 0){
            markDirty();
        }
    }
    public boolean afterBurning = false;
    float startTime = 0;
    public int getTorque(){
        return afterBurning?2048:1024;
    }
    public void setAfterBurning(boolean afterBurning) {
        this.afterBurning = afterBurning;
        if(powerSource.getPower().isPowered()){
            if(!world.isRemote){
                powerSource.setPower(new RotaryPower(getTorque(),Math.round(powerSource.getPower().getSpeed() * effective),0,0));
            }else{
                playsound();
            }
        }
    }
    public boolean canAfterBurn(){
        return hasUpgrade(ItemUpgrade.getByMeta(6));
    }

    @Override
    public boolean canUpgrade(ItemStack stack) {
        return stack.getItem() instanceof IUpgrade && ((IUpgrade) stack.getItem()).readyForUse(stack) && stack.isItemEqual(ItemUpgrade.getByMeta(6)) && !hasUpgrade(stack);
    }

    @Override
    public boolean installUpgrade(ItemStack stack,EntityPlayer player) {
        this.upgrades.add(stack.copy());
        if(!world.isRemote){
            NetworkDispatcher.dispatchMachineUpdate(this);
            player.sendMessage(new TextComponentTranslation("info.rc.etc.upgrade_installed"));
        }else {
            SoundHandler.playSound(SoundEvents.BLOCK_ANVIL_USE);
        }
        return true;
    }


    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        fuelTank.readFromNBT(compound);
        startTime = compound.getFloat("startTime");
        isActive = compound.getBoolean("isActive");
        afterBurning = compound.getBoolean("afterBurning");
        isFailing = compound.getBoolean("isFailing");
        if(compound.hasKey("effective")){
            effective = compound.getFloat("effective");
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound = super.writeToNBT(compound);
        compound.setFloat("startTime",startTime);
        compound.setBoolean("isActive",isActive);
        compound.setBoolean("afterBurning",afterBurning);
        compound.setBoolean("isFailing",isFailing);
        fuelTank.writeToNBT(compound);
        compound.setFloat("effective",effective);
        return compound;
    }

    @Override
    public NBTTagCompound getModePacket() {
        NBTTagCompound tag = super.getModePacket();
        tag.setBoolean("afterBurning",afterBurning);
        return tag;
    }

    @Override
    public void handleModePacket(NBTTagCompound compound) {
        super.handleModePacket(compound);
        setAfterBurning(compound.getBoolean("afterBurning"));
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        return capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY || super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY){
            return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(new IFluidHandler(){

                @Override
                public IFluidTankProperties[] getTankProperties() {
                    FluidTankInfo info = fuelTank.getInfo();
                    return new IFluidTankProperties[] { new FluidTankProperties(info.fluid, info.capacity, true, false) };
                }

                @Override
                public int fill(FluidStack resource, boolean doFill) {
                    if(!resource.getFluid().equals(RotationCraft.fluids.get("jetfuel"))){
                        return 0;
                    }
                    return fuelTank.fill(resource,doFill);
                }

                @Nullable
                @Override
                public FluidStack drain(FluidStack resource, boolean doDrain) {
                    return null;
                }

                @Nullable
                @Override
                public FluidStack drain(int maxDrain, boolean doDrain) {
                    return null;
                }
            });
        }
        return super.getCapability(capability, facing);
    }

    @Override
    public void invalidate() {
        if(world.isRemote){
            stopsound();
        }
        super.invalidate();
    }

    @Override
    public Object getGuiClient(EntityPlayer player) {
        return new GuiJet(this,player.inventory);
    }

    @Override
    public Object getGuiServer(EntityPlayer player) {
        return new ContainerJet(this,player.inventory);
    }




    private void doAfterburning(World world, int x, int y, int z) {
        if (this.afterBurning) {
            this.afterBurnParticles(world, x,y,z);
            /*if (world.getTotalWorldTime()%200 == 0) {
                addTemp(1F);
                if (getTemp() > getDamagedTemp()) {
                    doFail();
                }
                else if (getTemp() >= 600) {
                    ReikaParticleHelper.spawnAroundBlock(EnumParticleTypes.SMOKE_LARGE,world, x, y, z, 8);
                }
            }*/
        }
    }

    private void afterBurnParticles(World world, int x, int y, int z) {
        BlockPos back = FacingTool.getRelativePos(pos,facing, FacingTool.Position.FRONT,1);
        double dx = x- back.getX();
        double dz = z- back.getZ();
        dx /= 2;
        dz /= 2;
        double vx = (x-back.getX())*6D;
        double vz = (z-back.getZ())*6D;
        Random rand = world.rand;
        for (int i = 0; i < 16; i++) {
            int r = 255;
            int g = 0;
            int b = 0;
            double px = dx+x+0.25+0.5*rand.nextDouble()+vx*rand.nextDouble();
            double pz = dz+z+0.25+0.5*rand.nextDouble()+vz*rand.nextDouble();
            double dd = Math.abs(px-x)+Math.abs(pz-z);
            if (dd < 1.5+rand.nextDouble()) {
                r = 0;
                g = 127;
                b = 255;
            }
            else if (dd < 2.5+rand.nextDouble()) {
                r = 255;
                g = 255;
                b = 255;
            }
            else if (dd < 3+rand.nextDouble()*2) {
                g = 255;
            }
            else if (dd < 5+rand.nextDouble()*3 && rand.nextBoolean()) {
                g = 10;
            }
            ReikaParticleHelper.spawnColoredParticleAt(world, px, y+0.75*rand.nextDouble(), pz, r, g, b);
        }
    }


    private boolean isFailing = false;
    private int failingtime = 0;
    public boolean isFailing(){
        return isFailing;
    }
    private void doHoover() {

        List<Entity> entities = world.getEntitiesWithinAABB(Entity.class,getSuckAABB());
        BlockPos dst = FacingTool.getRelativePos(pos,facing, FacingTool.Position.BACK,4);
        for (Entity entity : entities) {
            if(entity instanceof EntityPlayer && ((EntityPlayer) entity).isCreative()){
                continue;
            }

            double x = (pos.getX() + 0.5D - entity.posX);
            double y = (pos.getY() + 0.5D - entity.posY);
            double z = (pos.getZ() + 0.5D - entity.posZ);
            double distance = Math.sqrt(x * x + y * y + z * z);
            if (distance < 1) {
                if(!world.isRemote){
                    if(entity instanceof EntityItem && !entity.isDead){
                        ItemStack stack = ((EntityItem) entity).getItem();
                        entity.setAir(0);
                        world.removeEntity(entity);
                        world.spawnEntity(new EntityItem(world,dst.getX(),dst.getY(),dst.getZ(),stack));
                    }
                    if(entity instanceof EntityLivingBase){
                        entity.setFire(20);
                        entity.attackEntityFrom(DamageSources.JET_SUCK, Integer.MAX_VALUE);
                        isFailing = true;
                        markDirty();
                    }
                }
            } else {
                double speed = 0.1;
                if(entity instanceof EntityLivingBase){
                    speed = 0.03;
                }
                double distScale = 1.0 - Math.min(0.9, (distance - 1) / 32);
                distScale *= distScale;

                entity.motionX += x / distance * distScale * speed;
                entity.motionY += y / distance * distScale * 0.4;
                entity.motionZ += z / distance * distScale * speed;
                if (!world.isRemote)
                    entity.velocityChanged = true;
            }
            world.markChunkDirty(pos,null);
        }

    }



    private AxisAlignedBB getSuckAABB(){
        BlockPos pos2 = FacingTool.getRelativePos(pos,facing, FacingTool.Position.FRONT,3);
        return new AxisAlignedBB(pos2).grow(2,2,2);
    }

    @Override
    public void onDamage() {

    }


    @Override
    public boolean canDamage() {
        return true;
    }

    @Override
    public float getDamagedTemp() {
        return 2000;
    }


    float temp=0;
    @Override
    public float getTemp() {
        return temp;
    }

    @Override
    public void setTemp(Float thermal) {
        temp = thermal;
    }

    @Override
    public void addTemp(Float thermal) {
        temp += thermal;
    }

    @Override
    public boolean isTempSource() {
        return false;
    }

    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {
        super.getWailaBody(itemStack, tooltip);
        tooltip.add(StringHelper.localize("info.rc.waila.names.fuel")+":"+String.valueOf(fuelTank.getFluidAmount()));
    }


    @Override
    public void onBlockHarvested(EntityPlayer player) {
        if(player.isCreative()){
            return;
        }
        if(isInvalid() || isFailing()){
            world.spawnEntity(new EntityItem(world,pos.getX(),pos.getY(),pos.getZ(), ItemMaterial.getStackById(20)));
            return;
        }else {
            world.spawnEntity(new EntityItem(world,pos.getX(),pos.getY(),pos.getZ(), new ItemStack(RotationCraft.blocks.get(BlockJet.NAME),1)));
            dropUpgrades(world,pos,upgrades);
            return;
        }
    }
    float effective = 1f;
    @Override
    public float getEffective() {
        return effective;
    }

    @Override
    public void setEffective(float e) {
        effective = e;
        if(isActive && startTime>1900){
            powerSource.setPower(new RotaryPower(16,Math.round(131072 * effective),0,0));
        }
        markDirty();
    }
}