package cn.tohsaka.factory.rotationcraft.tiles.transmits;

import cn.tohsaka.factory.librotary.api.power.CapabilityRotaryPower;
import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.librotary.api.power.interfaces.IPowerMachine;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.blocks.transmits.BlockWorm;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.animated.ModelWorm;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.utils.AxlsUtils;
import cn.tohsaka.factory.rotationcraft.utils.FacingTool;
import cn.tohsaka.factory.rotationcraft.utils.ReikaMathLibrary;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fml.common.registry.GameRegistry;

import javax.annotation.Nullable;
import java.util.List;

@GameInitializer(after = BlockWorm.class)
public class TileWorm  extends TileGearbox {
    public static void init(){
        GameRegistry.registerTileEntity(TileWorm.class,new ResourceLocation(RotationCraft.MOD_ID,"tileworm"));
    }
    public TileWorm(){

    }

    public BlockPos getSrc() {
        return FacingTool.getRelativePos(pos, facing, FacingTool.Position.FRONT,1);
    }

    public BlockPos getDest() {
        return FacingTool.getRelativePos(pos,facing, FacingTool.Position.BACK,1);
    }


    @Override
    public String getMeterInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("TileEntity["+world.getBlockState(pos).getBlock().getLocalizedName()+"]\n");
        sb.append(this.pos.toString()+"  Facing: "+facing.getName()+"\n");
        sb.append("PowerIn: " + powerTransformer.getPowerIn()+"\n");
        sb.append(String.format("PowerOut[%s]: ", powerTransformer.getPower()+"\n"));
        return sb.toString();
    }

    @Override
    public void onMeter() {
        addIO();
    }

    @Override
    public RotaryModelBase getModel() {
        return new ModelWorm();
    }



    @Override
    public String getTexName() {
        return "transmission/shaft/shafttex";
    }

    @Override
    public void updateInformationForTEISR(ItemStack stack, TileEntity tileEntity, float partialTicks) {

    }
    @Override
    public void beginItemRender() {

    }

    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {

    }

    @Override
    public void getOutBoxList(NonNullList<RenderUtils.RenderItem> drawables) {
        drawables.add(new RenderUtils.RenderItem(0x0000FF, getSrc() ,1F));
        drawables.add(new RenderUtils.RenderItem(0xFF0000, getDest(),1F));

        super.getOutBoxList(drawables);
    }

    protected void updatePowerInput(){

        IPowerMachine cap = CapabilityRotaryPower.getCapability(world, FacingTool.getRelativePos(pos,facing, FacingTool.Position.FRONT,1),FacingTool.revert(facing));
        if(cap!=null){
            powerTransformer.setPowerIn(cap.getPower());
        }else{
            powerTransformer.setPowerIn(RotaryPower.ZERO);
        }
    }

    private double getPowerLossFraction(long speed) {
        return (128-4* ReikaMathLibrary.logbase(speed, 2))/100;
    }
    public double getCurrentLoss() {
        return this.getPowerLossFraction(powerTransformer.getPowerIn().getSpeed());
    }

    public static final int torquelimit = (Integer.MAX_VALUE-1)/2;
    private static final int WORMRATIO = 64;
    @Override
    public void update() {
        if(world.isRemote){
            if(world.isRemote && powerTransformer.getPower().isPowered()){
                if(angleInput == 0){
                    angleInput = powerTransformer.getPowerIn().getAngle();
                }
                angleInput = AxlsUtils.calcAngle(angleInput,powerTransformer.getPowerIn().getSpeed());
                angleOutput = AxlsUtils.calcAngle(angleOutput,powerTransformer.getPower().getSpeed());
            }else {
                angleInput = 0;
            }
        }else{
            updatePowerInput();
            if(powerTransformer.getPowerIn().isPowered()){
                RotaryPower powerIn = powerTransformer.getPowerIn();
                double speed = (int)((powerIn.getSpeed() / WORMRATIO)*this.getPowerLossFraction(powerIn.getSpeed()));
                long torque = powerIn.getTorque();
                if (torque <= torquelimit/WORMRATIO)
                    torque = torque * WORMRATIO;
                else {
                    torque = torquelimit;
                    world.spawnParticle(EnumParticleTypes.CRIT, pos.getX()+world.rand.nextFloat(), pos.getY()+world.rand.nextFloat(), pos.getZ()+world.rand.nextFloat(), -0.5+world.rand.nextFloat(), world.rand.nextFloat(), -0.5+world.rand.nextFloat());
                    world.playSound(pos.getX()+0.5,pos.getY()+0.5,pos.getZ()+0.5,SoundEvents.ENTITY_BLAZE_HURT, SoundCategory.AMBIENT,0.1F,1F,true);
                }
                powerTransformer.setPower(new RotaryPower(torque,(long) speed,powerIn.getAngle(),4));
            }else {
                powerTransformer.setPower(RotaryPower.ZERO);
            }

        }
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing side) {
        return capability == CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY;
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing side) {
        if(capability == CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY){
            return CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY.cast(powerTransformer);
        }
        return null;
    }

    @Override
    public void onBlockHarvested(EntityPlayer player) {

    }

    @Override
    public boolean onChangingMode(World world, EntityPlayer player, BlockPos pos) {

        return false;
    }
}
