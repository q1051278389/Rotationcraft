package cn.tohsaka.factory.rotationcraft.tiles.power;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.api.IScrewable;
import cn.tohsaka.factory.rotationcraft.api.IUpgradable;
import cn.tohsaka.factory.rotationcraft.blocks.engine.BlockPerfEngine;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.engine.ModelPerformance;
import cn.tohsaka.factory.rotationcraft.etc.sounds.SoundHandler;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerPfe;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.GuiPfe;
import cn.tohsaka.factory.rotationcraft.init.Sounds;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemMaterial;
import cn.tohsaka.factory.rotationcraft.items.ItemUpgrade;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.etc.FuelTank;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileEngine;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileInvMachine;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.List;

@GameInitializer(after = BlockPerfEngine.class)
public class TilePfe extends TileEngine implements IGuiProvider, IUpgradable, IScrewable {
    public static void init(){
        GameRegistry.registerTileEntity(TilePfe.class,new ResourceLocation(RotationCraft.MOD_ID,"tilepfe"));
    }
    public FuelTank fuelTank = new FuelTank(16*1000,"fuel");
    public FuelTank fuelTankrs = new FuelTank(16*1000,"fuelrs");
    public FuelTank fuelTankcata = new FuelTank(16*1000,"fuelcata");
    @SideOnly(Side.CLIENT)
    @Override
    public RotaryModelBase getModel() {
        return new ModelPerformance();
    }
    @SideOnly(Side.CLIENT)
    @Override
    public String getTexName() {
        return "perftex";
    }

    public TilePfe(){
        super(new RotaryPower(256,1024));
        createSlots(3);
        setSlotMode(TileInvMachine.SlotMode.INPUT,new int[]{0,1,2});
    }

    @SideOnly(Side.CLIENT)
    public void playsound(){
        SoundHandler.startTileSound(Sounds.HYDROENGINE.getEvent(),0.5F,pos);
    }

    @SideOnly(Side.CLIENT)
    public void stopsound(){
        SoundHandler.stopTileSound(pos);
    }

    @Override
    public void update() {
        super.update();
        if(world.isRemote){
            if(isActive){
                playsound();
            }else{
                stopsound();
            }
        }
    }

    @Override
    public void invalidate() {
        if(world.isRemote){
            stopsound();
        }
        super.invalidate();
    }

    @Override
    public void tick() {
        if(fuelTank.getFreeSpace()>1000 && !getStackInSlot(0).isEmpty()){
            fuelTank.inc(800);
            decrStackSize(0,1);
        }
        if(fuelTankrs.getFreeSpace()>1000 && !getStackInSlot(1).isEmpty()){
            fuelTankrs.inc(2000);
            decrStackSize(1,1);
        }
        if(fuelTankcata.getFreeSpace()>1000 && !getStackInSlot(2).isEmpty()){
            if(getStackInSlot(2).isItemEqual(new ItemStack(Items.BLAZE_POWDER))){
                fuelTankcata.inc(4000);
            }else{
                fuelTankcata.inc(2000);
            }
            decrStackSize(2,1);
        }

        if(!fuelTank.isEmpty() && !fuelTankrs.isEmpty() && !fuelTankcata.isEmpty()){
            fuelTank.dec(1);
            fuelTankrs.dec(1);
            fuelTankcata.dec(1);
            isActive = true;
            powerSource.setPower(new RotaryPower((upgrades.size()>0)?512:256,1024,0,0));
        }else{
            isActive = false;
            powerSource.setPower(RotaryPower.ZERO);
        }

        if(world.getTotalWorldTime() % 100 == 0){
            markDirty();
        }
    }

    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        if(index == 0){
            return stack.isItemEqual(new ItemStack(ItemMaterial.materialMap.get(60)));
        }
        if(index == 1){
            return stack.isItemEqual(new ItemStack(Items.REDSTONE));
        }

        if(index == 2){
            return stack.isItemEqual(new ItemStack(Items.BLAZE_POWDER)) || stack.isItemEqual(new ItemStack(Items.GLOWSTONE_DUST));
        }

        return false;
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        fuelTank.readFromNBT(compound);
        fuelTankrs.readFromNBT(compound);
        fuelTankcata.readFromNBT(compound);
        isActive = compound.getBoolean("isActive");
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        NBTTagCompound tag = super.writeToNBT(compound);
        tag = fuelTank.writeToNBT(compound);
        tag = fuelTankrs.writeToNBT(compound);
        tag = fuelTankcata.writeToNBT(compound);
        compound.setBoolean("isActive",isActive);
        return tag;
    }

    @Override
    public Object getGuiClient(EntityPlayer player) {
        return new GuiPfe(this,player.inventory);
    }

    @Override
    public Object getGuiServer(EntityPlayer player) {
        return new ContainerPfe(this,player.inventory);
    }

    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {
        super.getWailaBody(itemStack, tooltip);
        tooltip.add(StringHelper.localize("info.rc.waila.names.fuel")+":"+String.valueOf(fuelTank.getFuel()));
        tooltip.add(StringHelper.localize("info.rc.waila.names.rs")+":"+String.valueOf(fuelTankrs.getFuel()));
        tooltip.add(StringHelper.localize("info.rc.waila.names.cata")+":"+String.valueOf(fuelTankcata.getFuel()));
        if(Math.min(Math.min(fuelTank.getFuel(),fuelTankrs.getFuel()),fuelTankcata.getFuel()) <= 0){
            return;
        }
        tooltip.add(StringHelper.localize("info.rc.waila.names.rtime")+":"+Math.round(Math.min(Math.min(fuelTank.getFuel(),fuelTankrs.getFuel()),fuelTankcata.getFuel()) /20));
    }

    @Override
    public void onBlockHarvested(EntityPlayer player) {
        super.onBlockHarvested(player);
        dropUpgrades(world,pos,upgrades);
    }

    @Override
    public boolean canUpgrade(ItemStack stack) {
        if(upgrades.size()==0 && stack.getItem() instanceof ItemUpgrade && stack.getMetadata()==0){
            return true;
        }
        return false;
    }

    @Override
    public boolean installUpgrade(ItemStack stack,EntityPlayer player) {
        this.upgrades.add(stack.copy());
        if(!world.isRemote){
            NetworkDispatcher.dispatchMachineUpdate(this);
            player.sendMessage(new TextComponentTranslation("info.rc.etc.upgrade_installed"));
        }else {
            SoundHandler.playSound(SoundEvents.BLOCK_ANVIL_USE);
        }
        return true;
    }


    @Override
    public boolean screw(EnumFacing facing, EntityLivingBase player) {
        if(upgrades.size()>0){
            if(world.isRemote){
                return true;
            }
            dropUpgrades(world,pos,upgrades.get(upgrades.size()-1).copy());
            NetworkDispatcher.dispatchMachineUpdate(this);
            return true;
        }
        return false;
    }
}
