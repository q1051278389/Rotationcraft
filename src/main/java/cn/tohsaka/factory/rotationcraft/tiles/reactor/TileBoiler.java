package cn.tohsaka.factory.rotationcraft.tiles.reactor;

import cn.tohsaka.factory.librotary.packet.PacketCustom;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.etc.FluidTankLocked;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileMachineBase;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.FluidTankPropertiesWrapper;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.common.registry.GameRegistry;

import javax.annotation.Nullable;
import java.util.List;

@GameInitializer
public class TileBoiler extends TileMachineBase implements ITickable {
    public static void init(){
        GameRegistry.registerTileEntity(TileBoiler.class,new ResourceLocation(RotationCraft.MOD_ID,"tileboiler"));
    }

    @Override
    public void update() {
        if(!world.isRemote){
            if(water.getFluidAmount()>100 && temperture>200 && (steam.getCapacity() - steam.getFluidAmount())>100){
                temperture -= 10;
                water.drain(300,true);
                steam.fill(new FluidStack(RotationCraft.fluids.get("steamlp"),300),true);
            }



            if(world.getTotalWorldTime() % 200 == 0){
                markDirty();
            }
        }
    }


    public float temperture = 24.0F;
    public FluidTank water = new FluidTankLocked(FluidRegistry.WATER,64000);
    public FluidTank steam = new FluidTankLocked(RotationCraft.fluids.get("steamlp"),64000);
    public float insertHeat(float tempMax){
        if(tempMax>200){
            float temp = Math.min(50,tempMax-200);
            temperture+=temp;
            return temp;
        }
        return 0f;
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        if(compound.hasKey("water")){
            water.readFromNBT(compound.getCompoundTag("water"));
        }
        if(compound.hasKey("steam")){
            steam.readFromNBT(compound.getCompoundTag("steam"));
        }
        if(compound.hasKey("temp")){
            temperture = compound.getFloat("temp");
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound.setTag("water",water.writeToNBT(new NBTTagCompound()));
        compound.setTag("steam",steam.writeToNBT(new NBTTagCompound()));
        compound.setFloat("temp",temperture);
        return super.writeToNBT(compound);
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound compound = super.getUpdateTag();
        compound.setTag("water",water.writeToNBT(new NBTTagCompound()));
        compound.setTag("steam",steam.writeToNBT(new NBTTagCompound()));
        compound.setFloat("temp",temperture);
        return compound;
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tag) {
        super.handleUpdateTag(tag);
        if(tag.hasKey("water")){
            water.readFromNBT(tag.getCompoundTag("water"));
        }
        if(tag.hasKey("steam")){
            steam.readFromNBT(tag.getCompoundTag("steam"));
        }
        if(tag.hasKey("temp")){
            temperture = tag.getFloat("temp");
        }
    }

    @Override
    public void writeGuiData(PacketCustom packet, boolean isFullSync) {
        packet.writeNBTTagCompound(getUpdateTag());
    }

    @Override
    public void readGuiData(PacketCustom packet, boolean isFullSync) {
        handleUpdateTag(packet.readNBTTagCompound());
    }

    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {
        tooltip.add("Temperture:"+String.format("%.2f",temperture));
    }


    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing side) {
        return (capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY) || super.hasCapability(capability, side);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing side) {
        if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY){
            return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(new IFluidHandler() {
                @Override
                public IFluidTankProperties[] getTankProperties() {
                    return new IFluidTankProperties[]{new FluidTankPropertiesWrapper(water),new FluidTankPropertiesWrapper(steam)};
                }

                @Override
                public int fill(FluidStack fluidStack, boolean b) {
                    return water.fill(fluidStack,b);
                }

                @Nullable
                @Override
                public FluidStack drain(FluidStack fluidStack, boolean b) {
                    return steam.drain(fluidStack,b);
                }

                @Nullable
                @Override
                public FluidStack drain(int i, boolean b) {
                    return steam.drain(i,b);
                }
            });
        }
        return super.getCapability(capability, side);
    }
}
