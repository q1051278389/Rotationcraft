package cn.tohsaka.factory.rotationcraft.tiles.machines;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IModelProvider;
import cn.tohsaka.factory.rotationcraft.blocks.machine.BlockExtractor;
import cn.tohsaka.factory.rotationcraft.config.GeneralConfig;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.machine.ModelBoiler;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.models.FluidCube;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileModelMachine;
import cn.tohsaka.factory.rotationcraft.utils.*;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.FluidTankPropertiesWrapper;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.List;

import static cn.tohsaka.factory.rotationcraft.config.GeneralConfig.STEAM_CONVERT_RATIO;

@GameInitializer(after = BlockExtractor.class)
public class TileSteamBoiler extends TileModelMachine implements IModelProvider {
    public TileSteamBoiler(){
        super(FacingTool.Position.DOWN);
        fluidhandler = new IFluidHandler() {
            @Override
            public IFluidTankProperties[] getTankProperties() {
                return new IFluidTankProperties[]{new FluidTankPropertiesWrapper(waterTank),new FluidTankPropertiesWrapper(steamTank)};
            }

            @Override
            public int fill(FluidStack fluidStack, boolean b) {
                return waterTank.fill(fluidStack,b);
            }

            @Nullable
            @Override
            public FluidStack drain(FluidStack fluidStack, boolean b) {
                return steamTank.drain(fluidStack,b);
            }

            @Nullable
            @Override
            public FluidStack drain(int i, boolean b) {
                return steamTank.drain(i,b);
            }
        };
    }
    private IFluidHandler fluidhandler;
    private static final RotaryPower minimum = new RotaryPower(1,1024);
    @Override
    public RotaryPower getMinimumPower() {
        return minimum;
    }

    public FluidTank waterTank = new FluidTank(32000);
    public FluidTank steamTank = new FluidTank(32000);

    public static void init(){
        GameRegistry.registerTileEntity(TileSteamBoiler.class,new ResourceLocation(RotationCraft.MOD_ID,"tilesteamboiler"));
    }

    public void transferOutputFluid()
    {
        if (steamTank.getFluidAmount() <= 0)
            return;

        int side;
        FluidStack output = new FluidStack(steamTank.getFluid(), Math.min(steamTank.getFluidAmount(), 16000));

        for(int i=1;i<6;i++){
            int toDrain = FluidHelper.insertFluidIntoAdjacentFluidHandler(this, EnumFacing.VALUES[i], output, true);
            if (toDrain > 0)
            {
                steamTank.drain(toDrain, true);
                break;
            }
        }
        return;
    }

    private static final int MAXTEMP = 650;

    public void updateTemperature() {
        float Tamb = TempetureUtils.getBlockRelativeTemp(world,pos);
        RotaryPower power = powerMachine.getPower();
        if (power.getWatt() > 0) {
            this.temp += 0.5* ReikaMathLibrary.logbase(power.getWatt(), 2);
        }
        if (this.temp > Tamb) {
            this.temp -= (this.temp-Tamb)/40;
        }
        else {
            this.temp += (this.temp-Tamb)/40;
        }
        if (this.temp - Tamb <= 40 && this.temp > Tamb)
            this.temp--;
        if (this.temp > MAXTEMP) {
            this.temp = MAXTEMP;
            this.overheat();
        }
    }

    public void overheat() {
        if(GeneralConfig.boiler_explode_level==0){
            return;
        }
        world.setBlockToAir(pos);
        world.createExplosion(null, pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, GeneralConfig.boiler_explode_level, true);
    }

    @Override
    public void update() {
        if (!world.isRemote) {
            updatePowerInput();
            if(world.getTotalWorldTime() % 20 == 0){
                updateTemperature();
            }
            if(temp>=100){
                int space = steamTank.getCapacity() - steamTank.getFluidAmount();
                if (space > 0) {
                    int ss = (int) ((powerMachine.getPower().getTorque() / STEAM_CONVERT_RATIO) * Math.log(powerMachine.getPower().getSpeed()));
                    int mB = Math.min(space, Math.min(waterTank.getFluidAmount(), ss));
                    if (mB > 0){
                        steamTank.fill(new FluidStack(FluidRegistry.getFluid("steam"),mB),true);
                        double t = Math.log(mB);
                        this.temp = (float) Math.max(0,this.temp-t);
                    }
                }
            }
            transferOutputFluid();
        }else{
            angle = AxlsUtils.calcAngle(angle,powerMachine.getPower().getSpeed());
        }
    }


    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        if(compound.hasKey("water")){
            waterTank.readFromNBT(compound.getCompoundTag("water"));
        }
        if(compound.hasKey("steam")){
            steamTank.readFromNBT(compound.getCompoundTag("steam"));
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound.setTag("water",waterTank.writeToNBT(new NBTTagCompound()));
        compound.setTag("steam",steamTank.writeToNBT(new NBTTagCompound()));
        return super.writeToNBT(compound);
    }


    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing side) {
        return capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY || super.hasCapability(capability, side);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing side) {
        if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY){
            return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(fluidhandler);
        }
        return super.getCapability(capability, side);
    }

    @Override
    public RotaryModelBase getModel() {
        return new ModelBoiler();
    }

    @Override
    public String getTexName() {
        return "boilertex";
    }

    @Override
    public void updateInformationForTEISR(ItemStack stack, TileEntity tileEntity, float partialTicks) {

    }
    @Override
    public void beginItemRender() {

    }


    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {
        tooltip.add("Temperature:"+String.format("%.2f",this.temp));
    }


}
