package cn.tohsaka.factory.rotationcraft.tiles.machines;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IModelProvider;
import cn.tohsaka.factory.rotationcraft.blocks.machine.BlockFan;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.animated.ModelFan;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileModelMachine;
import cn.tohsaka.factory.rotationcraft.utils.AxlsUtils;
import cn.tohsaka.factory.rotationcraft.utils.IteratorAABB;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;
import net.minecraft.block.BlockCrops;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import org.lwjgl.opengl.GL11;

import java.util.List;

@GameInitializer(after = BlockFan.class)
public class TileFan extends TileModelMachine implements IModelProvider {
    public static void init(){
        GameRegistry.registerTileEntity(TileFan.class,new ResourceLocation(RotationCraft.MOD_ID,"tilefan"));
    }

    @Override
    public void update() {
        if(!world.isBlockPowered(pos)){
            updatePowerInput();
            if(world.isRemote){
                angle = AxlsUtils.calcAngle(angle,powerMachine.getPower().getSpeed());
            }else{
                if(world.getTotalWorldTime() % 10 == 0) {
                    if (powerMachine.getPower().getWatt() >= getMinimumPower().getWatt() && powerMachine.getPower().getTorque() >= getMinimumPower().getTorque()) {
                        this.makeBeam();
                    }
                }
                pushEntities();
            }
        }
    }

    public void makeBeam(){
        doHarvest();
    }

    IteratorAABB workIterator;

    private void doHarvest(){
        if(workIterator==null || !workIterator.hasNext()){
            workIterator = getWorkArea();
        }
        while (workIterator.hasNext()){
            BlockPos workpos = workIterator.next();
            harvest(world,workpos);
        }
    }

    private void harvest(World world, BlockPos pos) {
        IBlockState blockState = world.getBlockState(pos);
        if(blockState.getBlock() instanceof BlockCrops){
            BlockCrops blockCrops = (BlockCrops) blockState.getBlock();
            if(blockCrops.isMaxAge(blockState)){
                blockCrops.dropBlockAsItemWithChance(world,pos,blockState,1,1);
                world.setBlockState(pos,blockCrops.withAge(0));
            }
        }
    }

    private void pushEntity(){
        if(workIterator!=null){
            List<Entity> entities = world.getEntitiesWithinAABB(Entity.class,workIterator.getAabb().expand(0,1,0));
            float blowPower = getBlowPower();
            for(Entity entity : entities){
                if(entity instanceof EntityItem){
                    blowEntity(entity,blowPower);
                }else if(entity instanceof EntityLivingBase && !((EntityLivingBase)entity).isSneaking() && ((EntityLivingBase)entity).onGround){
                    blowEntity(entity,blowPower);
                }
            }
        }
    }

    private void blowEntity(Entity entity,float blowPower){
        if(facing == EnumFacing.NORTH){
            //z--;
            entity.motionZ-=blowPower;
        }
        if(facing == EnumFacing.SOUTH){
            //z++;
            entity.motionZ+=blowPower;
        }

        if(facing == EnumFacing.WEST){
            //x--;
            entity.motionX-=blowPower;
        }

        if(facing == EnumFacing.EAST){
            //x++;
            entity.motionX+=blowPower;
        }
        entity.motionY+=0.02;
        entity.velocityChanged = true;
    }

    private float getBlowPower(){
        if(powerMachine.getPower().getSpeed() > getMinimumPower().getSpeed()){
            float r = powerMachine.getPower().getSpeed() / getMinimumPower().getSpeed();
            return Math.min(100,Math.max(1,r));
        }
        return 1;
    }

    public static final long MAXPOWER = 2097152;

    public int getRange() {
        if (powerMachine.getPower().getWatt() < getMinimumPower().getWatt())
            return 0;
        int power2 = (int)Math.min(powerMachine.getPower().getWatt() - getMinimumPower().getWatt(), MAXPOWER);
        int range = 8+power2/2048;
        if (range > 32)
            range = 32;
        return range;
    }

    private IteratorAABB getWorkArea(){
        IteratorAABB aabb = null;
        int rangef = getRange();
        if(facing == EnumFacing.NORTH){
            //z--;
            aabb = new IteratorAABB(pos.add(-2,0,-rangef),5,1,rangef);
        }
        if(facing == EnumFacing.SOUTH){
            //z++;
            aabb = new IteratorAABB(pos.add(-2,0,1),5,1,rangef);
        }

        if(facing == EnumFacing.WEST){
            //x--;
            aabb = new IteratorAABB(pos.add(-rangef,0,-2),rangef,1,5);
        }

        if(facing == EnumFacing.EAST){
            //x++;
            aabb = new IteratorAABB(pos.add(1,0,-2),rangef,1,5);
        }
        return aabb;
    }

    @Override
    public void getOutBoxList(NonNullList<RenderUtils.RenderItem> drawables) {
        super.getOutBoxList(drawables);
        drawables.add(new RenderUtils.RenderItemBigBox(0x00FF00,getWorkArea().getAabb(),2));
    }

    @Override
    public void beginRenderModelOnWorld() {
        GL11.glRotatef(-90,0,1,0);
    }

    @Override
    public void updateInformationForTEISR(ItemStack stack, TileEntity tileEntity, float partialTicks) {

    }

    @Override
    public void beginItemRender() {

    }

    @Override
    public RotaryPower getMinimumPower() {
        return new RotaryPower(4,256);
    }

    @Override
    public RotaryModelBase getModel() {
        return new ModelFan();
    }

    @Override
    public String getTexName() {
        return "fantex";
    }

















    private int pushEntities() {
        if(workIterator==null){
            return 0;
        }
        List<Entity> entitiesFound = world.getEntitiesWithinAABB(Entity.class, workIterator.getAabb());
        int moved = 0;
        final boolean doPush = true;
        int direction = 1;
        float speed = 0.05f * getBlowPower();
        for (Entity entity : entitiesFound) {
            if (entity instanceof EntityPlayer && ((EntityPlayer) entity).isSneaking()) {
                continue; //sneak avoid feature
            }
            moved++;
            double newx = entity.motionX;
            double newy = entity.motionY;
            double newz = entity.motionZ;
            switch (facing) {
                case NORTH:
                    direction = !doPush ? 1 : -1;
                    newz += direction * speed;
                    break;
                case SOUTH:
                    direction = doPush ? 1 : -1;
                    newz += direction * speed;
                    break;
                case EAST:
                    direction = doPush ? 1 : -1;
                    newx += direction * speed;
                    break;
                case WEST:
                    direction = !doPush ? 1 : -1;
                    newx += direction * speed;
                    break;
                case DOWN:
                    direction = !doPush ? 1 : -1;
                    newy += direction * speed;
                    break;
                case UP:
                    direction = doPush ? 1 : -1;
                    newy += direction * speed;
                    break;
            }
            entity.motionX = newx;
            entity.motionY = newy;
            entity.motionZ = newz;
            entity.velocityChanged = true;
        }
        return moved;
    }

}
