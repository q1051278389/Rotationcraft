package cn.tohsaka.factory.rotationcraft.tiles.reactor;

import cn.tohsaka.factory.librotary.packet.PacketCustom;
import cn.tohsaka.factory.rotationcraft.api.IGuiPacketHandler;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.blocks.reactor.BlockMB;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerBedReactor;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.GuiBedReactor;
import cn.tohsaka.factory.rotationcraft.items.ItemReactorMaterial;
import cn.tohsaka.factory.rotationcraft.utils.IteratorAABB;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.wrapper.InvWrapper;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Arrays;

public abstract class TileBedCoreBase extends TileBedFrame implements IInventory, IGuiProvider, IGuiPacketHandler {
    private boolean isFramed = false;

    public ItemStack[] inventory = new ItemStack[2];
    public TileBedCoreBase(){
        Arrays.fill(inventory,ItemStack.EMPTY);
    }


    public boolean isFramed() {
        return isFramed;
    }
    public void destoryFrame(){
        isFramed = false;
        markDirty();
    }

    @Override
    public void checkingStructure(EntityPlayer playerIn) {
        IteratorAABB iter = new IteratorAABB(this.pos.add(-1,-1,-1),3,3,3);
        int i=0;
        isFramed = false;
        while (iter.hasNext()){
            BlockPos pp = iter.next();
            IBlockState state = world.getBlockState(pp);
            if(state.getBlock() != RotationCraft.blocks.get(BlockMB.NAME)){
                isFramed = false;
                break;
            }
            int type = state.getValue(BlockMB.TYPE);
            if((i==4 || i==22) && type!=1){
                isFramed = false;
                break;
            }

            if(i==13 && type!=2){
                isFramed = false;
                break;
            }

            if(state.getBlock() instanceof BlockMB && type<2){
                TileEntity tile = world.getTileEntity(pp);
                if(tile!=null && tile instanceof TileBedFrame){
                    ((TileBedFrame) tile).setCore(this.pos);
                }
            }
        }
        isFramed = true;
        playerIn.sendMessage(new TextComponentString("Structure completed."));
    }

    @Override
    public void onBlockHarvested(EntityPlayer player) {
        IteratorAABB iter = new IteratorAABB(this.pos.add(-1,-1,-1),3,3,3);
        while (iter.hasNext()){
            BlockPos pp = iter.next();
            IBlockState state = world.getBlockState(pp);
            if(state.getBlock() instanceof BlockMB && state.getValue(BlockMB.TYPE)<2){
                TileEntity tile = world.getTileEntity(pp);
                if(tile!=null && tile instanceof TileBedFrame){
                    ((TileBedFrame) tile).core = null;
                }
            }
        }
    }

    @Override
    public Object getGuiClient(EntityPlayer player) {
        return new GuiBedReactor((TileBedCore) this,player.inventory);
    }

    @Override
    public Object getGuiServer(EntityPlayer player) {
        return new ContainerBedReactor((TileBedCore) this,player.inventory);
    }



    @Override
    public void readFromNBT(NBTTagCompound compound) {
        if(world!=null) {
            super.readFromNBT(compound);
            if (compound.hasKey("inventory")) {
                NBTTagList list = (NBTTagList) compound.getTag("inventory");
                for (int i = 0; i < inventory.length; i++) {
                    inventory[i] = new ItemStack(list.getCompoundTagAt(i));
                }
            }
            if (compound.hasKey("isframed")) {
                isFramed = compound.getBoolean("isframed");
            }
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        NBTTagList list = new NBTTagList();
        for(ItemStack stack:inventory){
            list.appendTag(stack.serializeNBT());
        }
        compound.setTag("inventory",list);
        compound.setBoolean("isframed",isFramed());
        return super.writeToNBT(compound);
    }


    @Override
    public int getSizeInventory() {
        return 2;
    }

    @Override
    public boolean isEmpty() {
        return Arrays.stream(inventory).allMatch(ItemStack::isEmpty);
    }

    @Override
    public ItemStack getStackInSlot(int i) {
        return inventory[i];
    }

    @Override
    public ItemStack decrStackSize(int i, int i1) {
        ItemStack itemstack = ItemStackHelper.getAndSplit(Arrays.asList(inventory), i, i1);
        if (!itemstack.isEmpty()) {
            this.markDirty();
        }
        return itemstack;
    }

    @Override
    public ItemStack removeStackFromSlot(int i) {
        if (inventory[i].isEmpty()) {
            return ItemStack.EMPTY;
        }
        ItemStack stack = inventory[i];
        inventory[i] = ItemStack.EMPTY;
        return stack;
    }

    @Override
    public void setInventorySlotContents(int i, ItemStack itemStack) {
        inventory[i] = itemStack;
    }

    @Override
    public int getInventoryStackLimit() {
        return 64;
    }

    @Override
    public boolean isUsableByPlayer(EntityPlayer entityPlayer) {
        return true;
    }

    @Override
    public void openInventory(EntityPlayer entityPlayer) {

    }

    @Override
    public void closeInventory(EntityPlayer entityPlayer) {

    }

    @Override
    public boolean isItemValidForSlot(int i, ItemStack itemStack) {
        return itemStack.getItem() instanceof ItemReactorMaterial && itemStack.getItem() == RotationCraft.items.get(ItemReactorMaterial.triso);
    }

    @Override
    public int getField(int i) {
        return 0;
    }

    @Override
    public void setField(int i, int i1) {

    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public void clear() {
        Arrays.fill(inventory,ItemStack.EMPTY);
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public boolean hasCustomName() {
        return false;
    }

    @Override
    public void writeGuiData(PacketCustom packet, boolean isFullSync) {
        packet.writeNBTTagCompound(writeGuiTagCompound(new NBTTagCompound()));
    }

    abstract NBTTagCompound writeGuiTagCompound(NBTTagCompound tagCompound);

    @Override
    public void readGuiData(PacketCustom packet, boolean isFullSync) {
        readGuiTagCompound(packet.readNBTTagCompound());
    }

    abstract void readGuiTagCompound(NBTTagCompound readNBTTagCompound);






    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY || capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY || super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY){
            return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.cast(new InvWrapper(this){
                @Nonnull
                @Override
                public ItemStack insertItem(int slot, @Nonnull ItemStack stack, boolean simulate) {
                    if(slot == 0 && isItemValidForSlot(slot,stack)){
                        return super.insertItem(slot, stack, simulate);
                    }
                    return stack;
                }

                @Nonnull
                @Override
                public ItemStack extractItem(int slot, int amount, boolean simulate) {
                    if(slot==1){
                        return super.extractItem(slot, amount, simulate);
                    }
                    return ItemStack.EMPTY;
                }

                @Override
                public boolean isItemValid(int slot, @Nonnull ItemStack stack) {
                    if(slot == 0 && isItemValidForSlot(slot,stack)){
                        return true;
                    }
                    return false;
                }


            });
        }

        return super.getCapability(capability, facing);
    }
}
