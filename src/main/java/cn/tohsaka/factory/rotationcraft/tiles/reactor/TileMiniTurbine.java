package cn.tohsaka.factory.rotationcraft.tiles.reactor;

import cn.tohsaka.factory.librotary.api.power.CapabilityRotaryPower;
import cn.tohsaka.factory.librotary.packet.PacketCustom;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IModelProvider;
import cn.tohsaka.factory.rotationcraft.api.power.IModeProvider;
import cn.tohsaka.factory.rotationcraft.blocks.reactor.BlockMiniTurbine;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.turbine.ModelMiniTurbine;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileMachineBase;
import cn.tohsaka.factory.rotationcraft.utils.FacingTool;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Optional;

@GameInitializer(after = BlockMiniTurbine.class)
public class TileMiniTurbine extends TileMachineBase implements ITickable, IModeProvider, IModelProvider {

    public static void init(){
        GameRegistry.registerTileEntity(TileMiniTurbine.class,new ResourceLocation(RotationCraft.MOD_ID,"tileminiturbine"));
    }
    @Override
    public RotaryModelBase getModel() {
        return new ModelMiniTurbine(1);
    }

    @Override
    public String getTexName() {
        return "turbine";
    }

    @Override
    public void updateInformationForTEISR(ItemStack stack, TileEntity tileEntity, float partialTicks) {

    }

    @Override
    public void beginItemRender() {
        GlStateManager.scale(0.5,0.5,0.5);
        GlStateManager.translate(0,1,0);
        GlStateManager.rotate(90,0,1,0);
    }

    @Override
    public boolean onChangingMode(World world, EntityPlayer player, BlockPos pos) {
        return false;
    }




    public BlockPos getSrc() {
        return FacingTool.getRelativePos(pos,facing,FacingTool.Position.FRONT,1);
    }


    public BlockPos getDest() {
        return FacingTool.getRelativePos(pos,facing,FacingTool.Position.BACK,1);
    }



    @Override
    public void update() {
        if(controlunit==null && cupos!=null){
            TileEntity te = world.getTileEntity(cupos);
            if(te instanceof TileTCU && !te.isInvalid()){
                setCU(((TileTCU) te));
            }
        }else if(controlunit!=null && controlunit.isInvalid()){
            controlunit = null;
            cupos = null;
            markDirty();
        }
    }



    @Override
    public void getOutBoxList(NonNullList<RenderUtils.RenderItem> drawables) {
        drawables.add(new RenderUtils.RenderItem(0xFF0000, getDest(),1F));
        drawables.add(new RenderUtils.RenderItem(0x0000FF, getSrc(),1F));
        super.getOutBoxList(drawables);
    }
    TileTCU controlunit;
    protected void setCU(TileTCU tileTCU){
        if(tileTCU !=null && tileTCU.equals(controlunit)){
            return;
        }
        controlunit = tileTCU;
        if(tileTCU == null){
            cupos = null;
        }
        markDirty();
    }
    public Optional<TileTCU> cu(){
        return Optional.ofNullable(controlunit);
    }


    @Override
    public void setFacing(EnumFacing facing) {
        if(cu().isPresent() && cu().get().isValidatedTurbine()){
            return;
        }
        cu().ifPresent(cu ->{
            cu.invalidateTurbine();
        });
        super.setFacing(facing);
    }

    public void setFacing2(EnumFacing facing) {
        super.setFacing(facing);
        if(FMLCommonHandler.instance().getSide() == Side.CLIENT){
            removeIO();
        }
    }

    @Override
    public void writeGuiData(PacketCustom packet, boolean isFullSync) {

    }

    @Override
    public void readGuiData(PacketCustom packet, boolean isFullSync) {

    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound compound = super.getUpdateTag();
        if(controlunit!=null){
            BlockPos p = cu().get().getPos();
            compound.setBoolean("cu",true);
            compound.setInteger("cu_x",p.getX());
            compound.setInteger("cu_y",p.getY());
            compound.setInteger("cu_z",p.getZ());
        }else{
            compound.setBoolean("cu",false);
        }
        return compound;
    }

    @Override
    public void handleUpdateTag(NBTTagCompound compound) {
        super.handleUpdateTag(compound);
        if(compound.getBoolean("cu")){
            cupos = new BlockPos(compound.getInteger("cu_x"),compound.getInteger("cu_y"),compound.getInteger("cu_z"));
            if(controlunit!=null && !controlunit.getPos().equals(cu())){
                TileEntity te = world.getTileEntity(cupos);
                if(te instanceof TileTCU && !te.isInvalid()){
                    setCU(((TileTCU) te));
                }else{
                    controlunit = null;
                    cupos = null;
                }
            }
        }else{
            controlunit = null;
            cupos = null;
        }
    }

    @Override
    public void onBlockHarvested(EntityPlayer player) {
        cu().ifPresent(tcu ->{
            tcu.invalidateTurbine();
        });
    }

    private BlockPos cupos;
    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        if(compound.getBoolean("cu")){
            cupos = new BlockPos(compound.getInteger("cu_x"),compound.getInteger("cu_y"),compound.getInteger("cu_z"));
        }else{
            controlunit = null;
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound = super.writeToNBT(compound);
        if(controlunit!=null){
            BlockPos p = cu().get().getPos();
            compound.setBoolean("cu",true);
            compound.setInteger("cu_x",p.getX());
            compound.setInteger("cu_y",p.getY());
            compound.setInteger("cu_z",p.getZ());
        }else{
            compound.setBoolean("cu",false);
        }
        return compound;
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing side) {
        if(cu().isPresent() && cu().get().level>0 && cu().get().getLevelByPos(this.pos)== cu().get().level && (side==facing.getOpposite() || side ==null)){
            return cu().get().getCapability(capability,facing);
        }
        return null;
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing side) {
        if(capability == CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY && (side==facing.getOpposite() || side ==null)){
            return (cu().isPresent() && cu().get().level>0 && cu().get().getLevelByPos(this.pos)== cu().get().level && (side==facing.getOpposite() || side ==null));
        }
        return super.hasCapability(capability, facing);
    }

    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {

    }
}
