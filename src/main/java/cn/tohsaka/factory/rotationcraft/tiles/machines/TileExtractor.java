package cn.tohsaka.factory.rotationcraft.tiles.machines;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.api.IModelProvider;
import cn.tohsaka.factory.rotationcraft.blocks.machine.BlockExtractor;
import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.ExtractorRecipe;
import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.machine.ModelExtractor;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerExtractor;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.GuiExtractor;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemCrushedOre;
import cn.tohsaka.factory.rotationcraft.items.ItemMaterial;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileModelMachine;
import cn.tohsaka.factory.rotationcraft.utils.FacingTool;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.common.registry.GameRegistry;
import org.lwjgl.opengl.GL11;

import javax.annotation.Nullable;
import java.util.List;

@GameInitializer(after = BlockExtractor.class)
public class TileExtractor extends TileModelMachine implements IGuiProvider, IModelProvider {
    public TileExtractor(){
        super(FacingTool.Position.DOWN);
        createSlots(9);
        setSlotMode(SlotMode.INPUT,new int[]{0,1,2,3,4,5,6});
        setSlotMode(SlotMode.OUTPUT,new int[]{7,8});
    }

    public FluidTank fluidTank = new FluidTank(32000);

    public static void init(){
        GameRegistry.registerTileEntity(TileExtractor.class,new ResourceLocation(RotationCraft.MOD_ID,"tileextractor"));
    }

    @Override
    public RotaryModelBase getModel() {
        return new ModelExtractor();
    }

    @Override
    public String getTexName() {
        return "extractortex";
    }

    @Override
    public void updateInformationForTEISR(ItemStack stack, TileEntity tileEntity, float partialTicks) {

    }

    @Override
    public void beginItemRender() {
        GL11.glRotatef(90,0,1,0);
    }

    @Override
    public Object getGuiClient(EntityPlayer player) {
        return new GuiExtractor(this,player.inventory);
    }

    @Override
    public Object getGuiServer(EntityPlayer player) {
        return new ContainerExtractor(this,player.inventory);
    }

    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        if(index==0){
            List<CraftingPattern> list = Recipes.INSTANCE.getList(TileExtractor.class);
            for(CraftingPattern pattern:list){
                if(((ExtractorRecipe)pattern).test(stack)){
                    return true;
                }
            }

            /*if(stack.getItem() instanceof ItemBlock){
                int[] ids = OreDictionary.getOreIDs(stack);
                for(int id:ids){
                    if(OreDictionary.getOreName(id).toLowerCase().contains("ore")){
                        return true;
                    }
                }
            }*/
            return false;
        }

        if(index>0 && index < 4 && stack.getItem() instanceof ItemCrushedOre && stack.getMetadata() == (index-1) && stack.hasTagCompound()){
            return true;
        }
        return false;
    }

    public static final int tickmax = 300;
    public int drill = 0;

    public int step1 = 0;
    public int step2 = 0;
    public int step3 = 0;
    public int step4 = 0;
    public static final RotaryPower[] steps = new RotaryPower[]{
            new RotaryPower(512,128,0,0),
            new RotaryPower(1,16384,0,0),
            new RotaryPower(1,32768,0,0),
            new RotaryPower(256,256,0,0)
    };
    @Override
    public void update() {
        if(!world.isRemote){
            for(int i=1;i<4;i++){
                if(!getStackInSlot(i+3).isEmpty()){
                    if(getStackInSlot(i).isEmpty()){
                        setInventorySlotContents(i,getStackInSlot(i+3).copy());
                        setInventorySlotContents(i+3,ItemStack.EMPTY);
                        continue;
                    }
                    if(getStackInSlot(i).getCount()<64 && getStackInSlot(i).isItemEqual(getStackInSlot(i+3)) && ItemCrushedOre.isOreEqual(getStackInSlot(i),getStackInSlot(i+3))){
                        int size = Math.min(getStackInSlot(i+3).getCount(),64 - getStackInSlot(i).getCount());
                        getStackInSlot(i).setCount(getStackInSlot(i).getCount()+size);
                        decrStackSize(i+3,size);
                    }
                }
            }
            updatePowerInput();
            RotaryPower power = powerMachine.getPower();
            if(power.getTorque()>=steps[0].getTorque() && power.getWatt()>=steps[0].getWatt()){
                if(step1>calcProcessTime(tickmax,steps[0])){
                    step1 = 0;
                    doStep1();
                }
                step1++;
            }
            if(power.getSpeed()>=steps[1].getSpeed() && power.getWatt()>=steps[1].getWatt()){
                if(step2>calcProcessTime(tickmax,steps[1])){
                    step2 = 0;
                    doStep2();
                }
                step2++;
            }

            if(power.getSpeed()>=steps[2].getSpeed() && power.getWatt()>=steps[2].getWatt()){
                if(step3>calcProcessTime(tickmax,steps[2])){
                    step3 = 0;
                    doStep3();
                }
                step3++;
            }

            if(power.getTorque()>=steps[3].getTorque() && power.getWatt()>=steps[3].getWatt()){
                if(step4>calcProcessTime(tickmax,steps[3])){
                    step4 = 0;
                    doStep4();
                }
                step4++;
            }
        }
    }

    @Override
    public RotaryPower getMinimumPower() {
        return RotaryPower.ZERO;
    }


    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        fluidTank.setFluid(new FluidStack(FluidRegistry.WATER,compound.getInteger("water")));
        drill = compound.getInteger("drill");
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound = super.writeToNBT(compound);
        compound.setInteger("water",fluidTank.getFluidAmount());
        compound.setInteger("drill",drill);
        return compound;
    }

    @Override
    public NBTTagCompound writeGuiTagCompound(NBTTagCompound tag) {
        tag.setInteger("step1",step1);
        tag.setInteger("step2",step2);
        tag.setInteger("step3",step3);
        tag.setInteger("step4",step4);
        tag.setInteger("drill",drill);
        tag.setInteger("water",fluidTank.getFluidAmount());
        return super.writeGuiTagCompound(tag);
    }

    @Override
    public void readGuiTagCompound(NBTTagCompound tag) {
        super.readGuiTagCompound(tag);
        step1 = tag.getInteger("step1");
        step2 = tag.getInteger("step2");
        step3 = tag.getInteger("step3");
        step4 = tag.getInteger("step4");
        fluidTank.setFluid(new FluidStack(FluidRegistry.WATER,tag.getInteger("water")));
        drill = tag.getInteger("drill");
    }



    public void doStep1(){
        if(getStackInSlot(0).isEmpty()){
            return;
        }
        if(getStackInSlot(4).isEmpty()
                || (getStackInSlot(4).hasTagCompound()
                    && new ItemStack(getStackInSlot(4).getTagCompound().getCompoundTag("ore")).isItemEqual(getStackInSlot(0))))
        {
            int bound = 1;
            if(drill == 1){
                float b = world.rand.nextFloat();
                if(b > 0.7){
                    bound +=2;
                }
            }
            if(drill == 2){
                bound+=3;
            }

            if(getStackInSlot(4).isEmpty()){
                setInventorySlotContents(4,ItemCrushedOre.createByOre(getStackInSlot(0),bound,0));
                decrStackSize(0,1);
            }else {
                if(getStackInSlot(4).getCount()<=(64-bound)){
                    getStackInSlot(4).setCount(getStackInSlot(4).getCount()+bound);
                    decrStackSize(0,1);
                }
            }
        }
    }
    public void doStep2(){
        int inslot = 1;
        int outslot = 5;
        if(getStackInSlot(inslot).isEmpty()){
            return;
        }
        if(fluidTank.getFluidAmount()<100){
            return;
        }
        if(getStackInSlot(outslot).isEmpty()){
            setInventorySlotContents(outslot,ItemCrushedOre.createBySelf(getStackInSlot(inslot),1,1));
            decrStackSize(inslot,1);
            fluidTank.drain(100,true);
        }else{
            if(ItemCrushedOre.isOreEqual(getStackInSlot(inslot),getStackInSlot(outslot)))
            {
                if(getStackInSlot(outslot).getCount()<64){
                    getStackInSlot(outslot).setCount(getStackInSlot(outslot).getCount()+1);
                    decrStackSize(inslot,1);
                    fluidTank.drain(100,true);
                }
            }
        }
    }
    public void doStep3(){
        int inslot = 2;
        int outslot = 6;
        if(getStackInSlot(inslot).isEmpty()){
            return;
        }
        if(fluidTank.getFluidAmount()<100){
            return;
        }
        if(getStackInSlot(outslot).isEmpty()){
            setInventorySlotContents(outslot,ItemCrushedOre.createBySelf(getStackInSlot(inslot),1,2));
            decrStackSize(inslot,1);
            fluidTank.drain(100,true);
        }else{
            if(ItemCrushedOre.isOreEqual(getStackInSlot(inslot),getStackInSlot(outslot)))
            {
                if(getStackInSlot(outslot).getCount()<64){
                    getStackInSlot(outslot).setCount(getStackInSlot(outslot).getCount()+1);
                    decrStackSize(inslot,1);
                    fluidTank.drain(100,true);
                }
            }
        }
    }
    public void doStep4(){
        int inslot = 3;
        int outslot = 7;
        if(getStackInSlot(inslot).isEmpty()){
            return;
        }
        int bound = Math.max(1,world.rand.nextInt(5));
        if(getStackInSlot(outslot).isEmpty()){
            doExtra();
            setInventorySlotContents(outslot,ItemCrushedOre.createBySelf(getStackInSlot(inslot),bound,3));
            decrStackSize(inslot,1);
        }else{

            if(ItemCrushedOre.isOreEqual(getStackInSlot(inslot),getStackInSlot(outslot))){
                if(getStackInSlot(outslot).getCount()<(64-bound)){
                    doExtra();
                    getStackInSlot(outslot).setCount(getStackInSlot(outslot).getCount()+bound);
                    decrStackSize(inslot,1);
                }
            }
        }
    }

    private void doExtra() {
        String name = ItemCrushedOre.getOreName(getStackInSlot(3));
        if(name==null){
            return;
        }
        int chance = world.rand.nextInt(100);
        if(name.equals("oreIron")){
            if(chance>95){
                outputExtra(ItemMaterial.getStackById(54),1);
                return;
            }
        }
        if(name.equals("oreNickel")){
            if(chance>93){
                outputExtra(ItemCrushedOre.createByOre(CraftingPattern.parseOreDict("ore:orePlatinum")[0],1,3),1);
                return;
            }
        }
        if(name.equals("oreGold")){
            if(chance>90){
                outputExtra(ItemCrushedOre.createByOre(CraftingPattern.parseOreDict("ore:oreSilver")[0],1,3),1);
                return;
            }
        }
        if(name.equals("oreSilver")){
            if(chance>98){
                outputExtra(ItemCrushedOre.createByOre(CraftingPattern.parseOreDict("ore:orePlatinum")[0],1,3),1);
                return;
            }
        }
        if(name.equals("oreTin")){
            if(chance>90){
                outputExtra(ItemCrushedOre.createByOre(CraftingPattern.parseOreDict("ore:oreSilver")[0],1,3),1);
                return;
            }
        }
    }

    private void outputExtra(ItemStack stack,int count){
        ItemStack stackExtra = getStackInSlot(8);
        stack.setCount(count);
        if(stackExtra.isEmpty()){
            setInventorySlotContents(8,stack);
        }else if(stackExtra.getCount()<=(64-count) && stackExtra.isItemEqual(stack)){
            stackExtra.setCount(stackExtra.getCount()+count);
        }
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY){
            return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(new IFluidHandler() {
                @Override
                public IFluidTankProperties[] getTankProperties() {
                    return fluidTank.getTankProperties();
                }

                @Override
                public int fill(FluidStack fluidStack, boolean b) {
                    if(fluidStack.getFluid().equals(FluidRegistry.WATER)){
                        return fluidTank.fill(fluidStack,b);
                    }
                    return 0;
                }

                @Nullable
                @Override
                public FluidStack drain(FluidStack fluidStack, boolean b) {
                    return null;
                }

                @Nullable
                @Override
                public FluidStack drain(int i, boolean b) {
                    return null;
                }
            });
        }
        return super.getCapability(capability, facing);
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        return capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY || super.hasCapability(capability, facing);
    }

    @Override
    public void onBlockHarvested(EntityPlayer player) {
        super.onBlockHarvested(player);
        if(!player.isCreative()){
            if(drill > 0){
                EntityItem item = new EntityItem(world,pos.getX(),pos.getY()+1,pos.getZ());
                item.setItem(ItemMaterial.getStackById((drill==1)?23:22));
                world.spawnEntity(item);
            }
        }
    }
}
