package cn.tohsaka.factory.rotationcraft.tiles.transmits;

import cn.tohsaka.factory.librotary.api.power.CapabilityRotaryPower;
import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.librotary.api.power.base.RotaryPowerTransformer;
import cn.tohsaka.factory.librotary.api.power.interfaces.IMeterable;
import cn.tohsaka.factory.librotary.api.power.interfaces.IPowerMachine;
import cn.tohsaka.factory.librotary.api.power.interfaces.IPowerTransformer;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.api.IModelProvider;
import cn.tohsaka.factory.rotationcraft.api.power.IGearboxRenderProvider;
import cn.tohsaka.factory.rotationcraft.blocks.transmits.BlockCVT;
import cn.tohsaka.factory.rotationcraft.blocks.transmits.BlockGearbox2x;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.animated.gearbox.ModelCVT;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerCVT;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.GuiCVT;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileModelMachine;
import cn.tohsaka.factory.rotationcraft.utils.AxlsUtils;
import cn.tohsaka.factory.rotationcraft.utils.FacingTool;
import cn.tohsaka.factory.rotationcraft.utils.FluidHelper;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.FluidTankInfo;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.FluidTankProperties;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.common.registry.GameRegistry;

import javax.annotation.Nullable;
import java.util.List;

@GameInitializer(after = BlockGearbox2x.class)
public class TileCVT  extends TileModelMachine implements ITickable, IGearboxRenderProvider, IModelProvider, IGuiProvider, IMeterable {
    IPowerTransformer powerTransformer;
    public static void init(){
        GameRegistry.registerTileEntity(TileCVT.class,new ResourceLocation(RotationCraft.MOD_ID,"tilecvt"));
    }
    public TileCVT(){
        createSlots(1);
        setSlotMode(SlotMode.NONE,0);
        powerTransformer = new RotaryPowerTransformer(this) {



            @Nullable
            @Override
            public World getWorld() {
                return TileCVT.this.getWorld();
            }

            @Nullable
            @Override
            public BlockPos getSrc() {
                return TileCVT.this.getSrc();
            }

            @Nullable
            @Override
            public BlockPos getDest() {
                return FacingTool.getRelativePos(pos, facing, FacingTool.Position.BACK, 1);
            }

            @Nullable
            @Override
            public IPowerMachine getParent() {
                return CapabilityRotaryPower.getCapability(world, FacingTool.getRelativePos(pos,facing, FacingTool.Position.FRONT,1),FacingTool.revert(facing));
            }

            @Nullable
            @Override
            public IPowerMachine getChild() {
                return CapabilityRotaryPower.getCapability(world, FacingTool.getRelativePos(pos,facing, FacingTool.Position.BACK,1),facing);
            }
        };
    }

    public FluidTank lubricantTank = new FluidTank(1000);
    public float angleInput = 0;
    public float angleOutput = 0;
    private boolean somethingchanged = false;
    @Override
    public void update() {
        if(world.isRemote){
            angle = AxlsUtils.calcAngle(angle,powerTransformer.getPower().getSpeed());
        }else{
            updatePowerInput();
            if(powerTransformer.getPowerIn().getWatt()==0 || lubricantTank.getFluidAmount()==0){
                powerTransformer.setPower(RotaryPower.ZERO);
            }else{
                ItemStack item = getStackInSlot(0);
                if(item.isEmpty()){
                    powerTransformer.setPower(RotaryPower.ZERO);
                }else{
                    RotaryPower powerIn = powerTransformer.getPowerIn();
                    if(isDownshift()){
                        powerTransformer.setPower(new RotaryPower(powerIn.getTorque()*(long)getRatio(),powerIn.getSpeed()/(long)getRatio(),angleOutput,powerIn.getBlend()));
                    }else{
                        powerTransformer.setPower(new RotaryPower(powerIn.getTorque()/(long)getRatio(),powerIn.getSpeed()*(long)getRatio(),angleOutput,powerIn.getBlend()));
                    }
                }
            }
            if(somethingchanged){
                markDirty();
                somethingchanged = false;
            }
        }
    }

    @Override
    public void setInventorySlotContents(int index, ItemStack stack) {
        super.setInventorySlotContents(index, stack);
        if(stack.isEmpty()){
            setRsRatio(new int[]{1,1});
        }else{
            if(rsRatio[0]>(stack.getCount()+1)){
                rsRatio[0] = stack.getCount()+1;
            }
            if(rsRatio[1]>(stack.getCount()+1)){
                rsRatio[1] = stack.getCount()+1;
            }
            setRsRatio(rsRatio);
        }
    }



    @Override
    public boolean isActive() {
        return powerTransformer.getPower().isPowered();
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound tag = super.getUpdateTag();
        tag.setTag("lubricant",lubricantTank.writeToNBT(new NBTTagCompound()));
        tag.setIntArray("rsratio",rsRatio);
        tag.setIntArray("mode",mode);
        return tag;
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tag) {
        super.handleUpdateTag(tag);
        rsRatio = tag.getIntArray("rsratio");
        mode = tag.getIntArray("mode");
        lubricantTank.readFromNBT(tag.getCompoundTag("lubricant"));
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        if(compound.hasKey("rsratio") && compound.hasKey("mode")){
            rsRatio = compound.getIntArray("rsratio");
            mode = compound.getIntArray("mode");
        }
        lubricantTank.readFromNBT(compound.getCompoundTag("lubricant"));
    }

    public void readFromNBTSelf(NBTTagCompound compound) {
        lubricantTank.readFromNBT(compound.getCompoundTag("lubricant"));
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        NBTTagCompound tag = super.writeToNBT(compound);
        tag.setTag("lubricant",lubricantTank.writeToNBT(new NBTTagCompound()));
        tag.setIntArray("rsratio",rsRatio);
        tag.setIntArray("mode",mode);
        return tag;
    }



    @Override
    public float getInputAngle() {
        return (float) angleInput;
    }

    @Override
    public float getOutputAngle() {
        return (float) angleOutput;
    }

    @Override
    public boolean isDownshift() {
        int s = world.isBlockPowered(pos)?1:0;
        return mode[s]==0?true:false;
    }

    public int getRatio() {
        int s = world.isBlockPowered(pos)?1:0;
        return rsRatio[s];
    }

    public void setMode(int i,int v){
        mode[i]=v;
        NetworkDispatcher.uploadingClientSetting(null,this);
    }
    public void setRsRatio(int[] r){
        if(getStackInSlot(0).isEmpty()){
            return;
        }
        int count = getStackInSlot(0).getCount()+1;
        if(r[0]>count){
            r[0]=count;
        }
        if(r[1]>count){
            r[1]=count;
        }
        rsRatio = r;
        NetworkDispatcher.uploadingClientSetting(null,this);
    }
    public int getMode(int i){
        return mode[i];
    }
    public int getRatio(int i){
        return rsRatio[i];
    }
    private int[] rsRatio = {1,1};
    private int[] mode = {0,0};//0 is down


    @Override
    public NBTTagCompound getModePacket() {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setIntArray("mode",mode);
        tag.setIntArray("rsratio",rsRatio);
        return tag;
    }

    @Override
    public void handleModePacket(NBTTagCompound compound) {
        mode = compound.getIntArray("mode");
        rsRatio = compound.getIntArray("rsratio");
    }



    public BlockPos getSrc() {
        return FacingTool.getRelativePos(pos, facing, FacingTool.Position.BACK,1);
    }


    public BlockPos getDest() {
        return FacingTool.getRelativePos(pos,facing, FacingTool.Position.FRONT,1);
    }

    @Override
    public void getOutBoxList(NonNullList<RenderUtils.RenderItem> drawables) {
        drawables.add(new RenderUtils.RenderItem(0xFF0000, getDest(),1F));
        super.getOutBoxList(drawables);
    }


    @Override
    public void onBlockHarvested(EntityPlayer player) {
        if(!player.isCreative()){
            ItemStack itemStack = itemStack = new ItemStack(RotationCraft.blocks.get(BlockCVT.NAME),1);
            if(!itemStack.isEmpty()){
                itemStack.setTagCompound(writeToNBT(new NBTTagCompound()));
                world.spawnEntity(new EntityItem(world,pos.getX(),pos.getY(),pos.getZ(),itemStack));
            }
        }
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing side) {
        if(capability == CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY){
            if(side == null || side==this.facing){
                return true;
            }
            return false;
        }
        return capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY || capability == CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY || super.hasCapability(capability, side);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing side) {
        if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY){
            return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(new IFluidHandler() {

                @Override
                public IFluidTankProperties[] getTankProperties() {
                    FluidTankInfo info = lubricantTank.getInfo();
                    return new IFluidTankProperties[] { new FluidTankProperties(info.fluid, info.capacity, true, false) };
                }

                @Override
                public int fill(FluidStack resource, boolean doFill) {
                    if (resource == null || !FluidHelper.isFluidEqual(RotationCraft.fluids.get("lubricant"),resource.getFluid()) || lubricantTank.getFluidAmount()>0) {
                        return 0;
                    }
                    int amount = lubricantTank.fill(resource, doFill);
                    markDirty();
                    return amount;
                }



                @Nullable
                @Override
                public FluidStack drain(FluidStack resource, boolean doDrain) {
                    return null;
                }

                @Nullable
                @Override
                public FluidStack drain(int maxDrain, boolean doDrain) {
                    return null;
                }
            });
        }

        if(capability == CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY){
            if(side == null || side == this.facing){
                return CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY.cast(powerTransformer);
            }
        }



        return super.getCapability(capability, side);
    }

    @Override
    public RotaryModelBase getModel() {
        return new ModelCVT();
    }
    @Override
    public String getTexName() {
        return "transmission/gear/cvttex";
    }

    @Override
    public void updateInformationForTEISR(ItemStack stack, TileEntity tileEntity, float partialTicks) {
        if(stack.hasTagCompound()){
            lubricantTank.readFromNBT(stack.getTagCompound().getCompoundTag("lubricant"));
        }
    }
    @Override
    public void beginItemRender() {

    }

    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {

    }

    @Override
    public Object getGuiClient(EntityPlayer player) {
        return new GuiCVT(this,player.inventory);
    }

    @Override
    public Object getGuiServer(EntityPlayer player) {
        return new ContainerCVT(this,player.inventory);
    }

    @Override
    public int getInventoryStackLimit() {
        return 31;
    }



    protected void updatePowerInput(){
        updatePowerInput(FacingTool.Position.BACK);
    }
    protected void updatePowerInput(FacingTool.Position position){
        IPowerMachine cap = CapabilityRotaryPower.getCapability(world, FacingTool.getRelativePos(pos,facing, position,1),facing);
        if(cap!=null){
            powerTransformer.setPowerIn(cap.getPower());
        }else{
            powerTransformer.setPowerIn(RotaryPower.ZERO);
        }
    }

    @Override
    public RotaryPower getMinimumPower() {
        return RotaryPower.ZERO;
    }

    public void onWorldChanged(boolean blockPowered) {
    }

    @Override
    public String getMeterInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("TileEntity["+world.getBlockState(pos).getBlock().getLocalizedName()+"]\n");
        sb.append(this.pos.toString()+"  Facing: "+facing.getName()+"\n");
        sb.append("PowerIn: " + powerTransformer.getPowerIn()+"\n");
        sb.append(String.format("Ratio[RS ON/OFF]: %s / %s \n",rsRatio[1]+""+(mode[1]==1?"U":"D"),rsRatio[0]+""+(mode[0]==1?"U":"D")));
        sb.append("PowerOut: " + powerTransformer.getPower()+"\n");
        return sb.toString();
    }

}
