package cn.tohsaka.factory.rotationcraft.tiles.machines;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.blocks.machine.BlockTeleporter;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerTeleporter;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.GuiTeleporter;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemPortalGem;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileRotation;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.ClickType;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.util.ITeleporter;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.registry.GameRegistry;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;

@GameInitializer(after = BlockTeleporter.class)
public class TileTeleporter extends TileRotation implements IGuiProvider {
    public static void init(){
        GameRegistry.registerTileEntity(TileTeleporter.class,new ResourceLocation(RotationCraft.MOD_ID,"tileteleporter"));
    }
    public TileTeleporter(){
        createSlots(27);
    }
    @Override
    public RotaryPower getMinimumPower() {
        int count = (int)Arrays.stream(inventory).filter(ItemStack::hasTagCompound).count();
        return new RotaryPower(4,16384 * Math.max(count,1));
    }

    @Override
    public void update() {
        updatePowerInput();
    }

    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        return (stack.getItem() instanceof ItemPortalGem) && stack.hasTagCompound();
    }

    @Override
    public Object getGuiClient(EntityPlayer player) {
        return new GuiTeleporter(player.inventory,this){
            @Override
            public List<String> getItemToolTip(ItemStack p_191927_1_) {
                List<String> list = super.getItemToolTip(p_191927_1_);
                list.add(StringHelper.localize("info.rc.teleporter.slotinfo"));
                return list;
            }
        };
    }

    @Override
    public Object getGuiServer(EntityPlayer player) {
        return new ContainerTeleporter(player.inventory,this){
            @Override
            public ItemStack slotClick(int slotId, int dragType, ClickType clickTypeIn, EntityPlayer player) {
                if(dragType==1 && clickTypeIn == ClickType.PICKUP && slotId<getSizeInventory()){
                    Teleport(getStackInSlot(slotId),player);
                    return ItemStack.EMPTY;
                }
                return super.slotClick(slotId, dragType, clickTypeIn, player);
            }
        };
    }

    public void Teleport(ItemStack stack,EntityPlayer player){
        if(powerMachine.getPower().getTorque()<getMinimumPower().getTorque() || powerMachine.getPower().getSpeed() < getMinimumPower().getSpeed()){
            player.sendMessage(new TextComponentTranslation("info.rc.teleporter.nopower"));
            return;
        }
        if(stack.hasTagCompound()){
            player.closeScreen();
            BlockPos teleportpos = new BlockPos(stack.getTagCompound().getDouble("x"),stack.getTagCompound().getDouble("y"),stack.getTagCompound().getDouble("z"));
            int dimid = stack.getTagCompound().getInteger("world");
            DimensionManager.init();
            DimensionManager.initDimension(dimid);
            FMLCommonHandler.instance().getMinecraftServerInstance().addScheduledTask(new Runnable() {
                public void run() {
                    initUnloadedDimension(dimid);
                    WorldServer remoteWorld = DimensionManager.getWorld(dimid);
                    if (remoteWorld != null) {
                        ITeleporter teleport = new Wolrd_Teleporter(remoteWorld, teleportpos, (byte)EnumFacing.getDirectionFromEntityLiving(pos,player).getIndex());
                        FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().transferPlayerToDimension((EntityPlayerMP) player, dimid, teleport);
                        remoteWorld.updateEntityWithOptionalForce(player, true);

                        SoundEvent portalSound = (SoundEvent)SoundEvent.REGISTRY.getObject(new ResourceLocation("block.note.bass"));
                        remoteWorld.playSound((EntityPlayer)null, player.getPosition(), portalSound, SoundCategory.MASTER, 1.0F, 1F);
                    }

                }
            });
        }
    }

    public static boolean initUnloadedDimension(int dimension_id) {
        DimensionManager.init();
        Integer[] loadedDimensions = DimensionManager.getIDs();
        Arrays.sort(loadedDimensions);
        if (Arrays.binarySearch(loadedDimensions, dimension_id) < 0) {
            DimensionManager.initDimension(dimension_id);
            return true;
        } else {
            return false;
        }
    }

    @Nullable
    @Override
    public ITextComponent getDisplayName() {
        return new TextComponentString(RotationCraft.blocks.get(BlockTeleporter.NAME).getLocalizedName());
    }



    private class Wolrd_Teleporter implements ITeleporter {
        private final WorldServer world;
        private final BlockPos pos;
        private final float yaw;
        public Wolrd_Teleporter(WorldServer world, BlockPos pos, float yaw) {
            this.world = world;
            this.pos = pos;
            this.yaw = yaw;
        }

        @Override
        public void placeEntity(World world, Entity entity, float yaw) {
            entity.setLocationAndAngles((double)this.pos.getX()+0.5, (double)this.pos.getY(), (double)this.pos.getZ()+0.5, this.yaw, 0.0F);
            entity.motionX = entity.motionY = entity.motionZ = 0.0D;
        }
    }
}
