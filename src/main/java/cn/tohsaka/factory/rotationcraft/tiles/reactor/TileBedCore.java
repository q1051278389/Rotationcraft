package cn.tohsaka.factory.rotationcraft.tiles.reactor;

import cn.tohsaka.factory.rotationcraft.api.IGuiPacketHandler;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.blocks.reactor.BlockConcrete;
import cn.tohsaka.factory.rotationcraft.blocks.reactor.BlockMB;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemReactorMaterial;
import cn.tohsaka.factory.rotationcraft.prefab.etc.FluidTankLocked;
import cn.tohsaka.factory.rotationcraft.prefab.etc.FuelTank;
import cn.tohsaka.factory.rotationcraft.utils.IteratorAABB;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.FluidTankPropertiesWrapper;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.common.registry.GameRegistry;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

@GameInitializer
public class TileBedCore extends TileBedCoreBase implements ITickable, IGuiPacketHandler {

    public TileBedCore(){

    }
    public static void init(){
        GameRegistry.registerTileEntity(TileBedCore.class,new ResourceLocation(RotationCraft.MOD_ID,"tilebedcore"));
    }

    public FuelTank fuelTank = new FuelTank(38400,"bedfuel").setCustomName("info.rc.bedreactor.fuel.name");
    public FuelTank garbage = new FuelTank(76800,"garbage").setCustomName("info.rc.bedreactor.garbage.name");

    public FluidTankLocked co2tank = new FluidTankLocked(RotationCraft.fluids.get("co2"),48000);
    public FluidTankLocked hotco2tank = new FluidTankLocked(RotationCraft.fluids.get("co2h"),48000);

    public double temperture = 24.0D;
    public double coretemp = 24.0D;
    public float effective = 0f;
    public int radsize = 0;
    @Override
    public void update() {
        if(!world.isRemote && world.getTotalWorldTime() % 10 == 0){
            diffTemp();
            if (fuelTank.getFreeSpace()>100 && !getStackInSlot(0).isEmpty()){
                decrStackSize(0,1);
                fuelTank.inc(100);
            }

            if(isActive() && effective<50){
                effective = Math.max(50,effective+=0.1);
            }
            if(effective>50){
                effective = 50;
            }

            if(isActive()){
                temperture += (world.rand.nextInt(Math.round(effective * 10f)) / 10f);
                if(world.rand.nextFloat()>=0.7){
                    fuelTank.dec(2);
                }
                garbage.inc(2);
                markDirty();
            }

            if(garbage.getFuel()>100 && (getStackInSlot(1).isEmpty() || getStackInSlot(1).getCount()<64)){
                garbage.dec(100);
                if(getStackInSlot(1).isEmpty()){
                    setInventorySlotContents(1,new ItemStack(RotationCraft.items.get(ItemReactorMaterial.triso_used)));
                }else {
                    getStackInSlot(1).grow(1);
                }
            }

            if(temperture>200 && co2tank.getFluidAmount()>0 && hotco2tank.getFluidAmount()<hotco2tank.getCapacity() && world.getTotalWorldTime() % 20 == 0){
                //int t = Math.min(Math.round(effective),co2tank.getFluidAmount());
                double t = Math.max(Math.round(effective),(temperture-200)*0.25);
                int tt = (int) Math.round(Math.min(t,co2tank.getFluidAmount()));
                co2tank.drain(tt,true);
                hotco2tank.fill(new FluidStack(hotco2tank.getLock(),tt),true);
                temperture-=tt;
            }



            if(temperture>800){
                this.world.createExplosion(null, this.pos.getX(), this.pos.getY(), this.pos.getZ(), 10f, true);
            }
            
            
            
            if(world.getTotalWorldTime() % 100 == 0){
                lookupAround();
                if(radsize>0){
                    temperture = Math.max(24,temperture - (radsize * 1.5f));
                }
            }
        }
    }

    private void explode() {

    }

    public void diffTemp(){
        if(temperture==coretemp){
            return;
        }
        if(Math.abs(temperture-coretemp) > 50){
            coretemp+=(temperture-coretemp)/50;
        }else{
            coretemp+= (temperture-coretemp)/10;
        }
    }

    public boolean isActive(){
        if(fuelTank.getFuel()>0){
            return true;
        }
        return false;
    }
    int[] corner = new int[]{1, 5, 21, 25, 26, 30, 46, 50, 51, 55, 71, 75};
    int lookupAround(){
        List<BlockPos> radlist = new ArrayList<>();
        IteratorAABB iter = new IteratorAABB(this.pos.add(-2,-1,-2),5,3,5);
        while (iter.hasNext()){
            BlockPos pp = iter.next();
            IBlockState state = world.getBlockState(pp);
            if(!(state.getBlock() instanceof BlockMB) && !(state.getBlock() instanceof BlockConcrete)){
                radlist.add(pp);
            }
        }
        radsize = radlist.size();
        return radsize;
    }



    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {

        if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY){
            return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(new IFluidHandler() {
                @Override
                public IFluidTankProperties[] getTankProperties() {
                    return new IFluidTankProperties[]{new FluidTankPropertiesWrapper(co2tank),new FluidTankPropertiesWrapper(hotco2tank)};
                }

                @Override
                public int fill(FluidStack fluidStack, boolean b) {
                    return co2tank.fill(fluidStack,b);
                }

                @Nullable
                @Override
                public FluidStack drain(FluidStack fluidStack, boolean b) {
                    return hotco2tank.drain(fluidStack,b);
                }

                @Nullable
                @Override
                public FluidStack drain(int i, boolean b) {
                    return hotco2tank.drain(i,b);
                }
            });
        }

        return super.getCapability(capability, facing);
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        fuelTank.readFromNBT(compound);
        garbage.readFromNBT(compound);
        co2tank.readFromNBT(compound.getCompoundTag("co2"));
        hotco2tank.readFromNBT(compound.getCompoundTag("hotco2"));

        temperture = compound.getDouble("temp");
        coretemp = compound.getDouble("coretemp");
        effective = compound.getFloat("effective");
    }
    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        fuelTank.writeToNBT(compound);
        garbage.writeToNBT(compound);
        compound.setTag("co2",co2tank.writeToNBT(new NBTTagCompound()));
        compound.setTag("hotco2",hotco2tank.writeToNBT(new NBTTagCompound()));

        compound.setDouble("temp",temperture);
        compound.setDouble("coretemp",coretemp);
        compound.setFloat("effective",effective);

        return super.writeToNBT(compound);
    }

    @Override
    NBTTagCompound writeGuiTagCompound(NBTTagCompound compound) {
        fuelTank.writeToNBT(compound);
        garbage.writeToNBT(compound);
        compound.setTag("co2",co2tank.writeToNBT(new NBTTagCompound()));
        compound.setTag("hotco2",hotco2tank.writeToNBT(new NBTTagCompound()));
        compound.setDouble("temp",temperture);
        compound.setDouble("coretemp",coretemp);
        compound.setFloat("effective",effective);
        compound.setInteger("radsize",radsize);
        return compound;
    }

    @Override
    void readGuiTagCompound(NBTTagCompound compound) {
        fuelTank.readFromNBT(compound);
        garbage.readFromNBT(compound);
        co2tank.readFromNBT(compound.getCompoundTag("co2"));
        hotco2tank.readFromNBT(compound.getCompoundTag("hotco2"));
        temperture = compound.getDouble("temp");
        coretemp = compound.getDouble("coretemp");
        effective = compound.getFloat("effective");
        radsize = compound.getInteger("radsize");
    }

}
