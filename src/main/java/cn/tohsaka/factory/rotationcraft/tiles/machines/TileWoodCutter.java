package cn.tohsaka.factory.rotationcraft.tiles.machines;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IModelProvider;
import cn.tohsaka.factory.rotationcraft.blocks.machine.BlockExtractor;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.machine.ModelWoodcutter;
import cn.tohsaka.factory.rotationcraft.etc.tree.TreeHandler;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileModelMachine;
import cn.tohsaka.factory.rotationcraft.utils.FacingTool;
import cn.tohsaka.factory.rotationcraft.utils.IteratorAABB;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;
import cn.tohsaka.factory.rotationcraft.utils.Utils;
import net.minecraft.block.BlockSapling;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.registry.GameRegistry;
import org.lwjgl.opengl.GL11;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@GameInitializer(after = BlockExtractor.class)
public class TileWoodCutter extends TileModelMachine implements IModelProvider {
    List<ItemStack> stackOnCache = new LinkedList<>();
    public TileWoodCutter(){
        createSlots(8);
        setSlotMode(SlotMode.OUTPUT,0,8);
    }

    public static void init(){
        GameRegistry.registerTileEntity(TileWoodCutter.class,new ResourceLocation(RotationCraft.MOD_ID,"tilewoodcutter"));
    }

    @Override
    public RotaryModelBase getModel() {
        return new ModelWoodcutter();
    }

    @Override
    public String getTexName() {
        return "woodcuttertex";
    }

    @Override
    public void updateInformationForTEISR(ItemStack stack, TileEntity tileEntity, float partialTicks) {

    }

    @Override
    public void beginItemRender() {
        GL11.glRotatef(90,0,1,0);
    }



    @Override
    public boolean isActive() {
        return powerMachine.getPower().isPowered() && powerMachine.getPower().getWatt()>getMinimumPower().getWatt() && powerMachine.getPower().getTorque()>=getMinimumPower().getTorque();
    }

    public IteratorAABB workIterator;

    @Override
    public RotaryPower getMinimumPower() {
        return new RotaryPower(32,256,0,0);
    }

    @Override
    public void update() {
        super.update();
    }


    @Override
    public void getOutBoxList(NonNullList<RenderUtils.RenderItem> drawables) {
        super.getOutBoxList(drawables);
        drawables.add(new RenderUtils.RenderItemBigBox(0x00FF00,new IteratorAABB(pos.add(-3,0,-3),7,1,7).getAabb(facing, FacingTool.Position.FRONT,4),2));
    }

    @Override
    public boolean canStart() {
        transferOutputStack();
        return Arrays.stream(inventory).allMatch(ItemStack::isEmpty);
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound tag = super.getUpdateTag();
        if(workIterator!=null){
            tag.setTag("iter",workIterator.toNBTTagCompound());
        }
        return tag;
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tag) {
        super.handleUpdateTag(tag);
        if(tag.hasKey("iter")){
            workIterator = IteratorAABB.readFromNBT(tag.getCompoundTag("iter"));
        }
    }

    @Override
    public boolean processStart() {
        if(workIterator==null){
                    workIterator = new IteratorAABB(pos.add(-3,0,-3),7,1,7);
                    markDirty();
        }
        if(!workIterator.hasNext()){
            processStop();
            workIterator = null;
        }
        processRem = processMax = 10;
        return true;
    }

    @Override
    public void processTick() {
        processRem--;
    }

    @Override
    public void processEnd() {
        if(workIterator!=null && workIterator.hasNext()){
            BlockPos pos = FacingTool.getRelativePos(workIterator.next(),facing, FacingTool.Position.FRONT,4);
            NonNullList<ItemStack> stacks = NonNullList.create();
            TreeHandler.getTree(world,pos).ifPresent(tree -> {
                TreeHandler.destroyInstant(tree, null,stacks);
            });
            for(int i=0;i<stacks.size();i++){
                if(stacks.get(i).getItem() instanceof ItemBlock && ((ItemBlock)stacks.get(i).getItem()).getBlock() instanceof BlockSapling){
                    IBlockState state = ((ItemBlock)stacks.get(i).getItem()).getBlock().getStateFromMeta(stacks.get(i).getMetadata());
                    int count = stacks.get(i).getCount();
                    for(int z=0;z<workIterator.getCount();z++){
                        BlockPos pp = FacingTool.getRelativePos(workIterator.get(z),facing, FacingTool.Position.FRONT,4);
                        if(world.isAirBlock(pp)){
                            world.setBlockState(pp,state);
                            count--;
                        }
                        if(count==0){
                            break;
                        }
                    }
                    if(count<=0){
                        stacks.remove(i);
                    }else{
                        stacks.get(i).setCount(count);
                    }
                }
            }
            for(ItemStack s:stacks){
                Utils.distributeOutput(this,s,0,8,true);
            }
        }
    }

    @Override
    public void beginRenderModelOnWorld() {
        GlStateManager.rotate(-90,0,1,0);
    }
}
