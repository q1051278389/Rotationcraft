package cn.tohsaka.factory.rotationcraft.tiles.machines;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.blocks.machine.BlockVoidMiner;
import cn.tohsaka.factory.rotationcraft.crafting.scripts.GlobalOre;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerVoidMiner;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.GuiVoidMiner;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileRotation;
import cn.tohsaka.factory.rotationcraft.utils.FacingTool;
import cn.tohsaka.factory.rotationcraft.utils.Utils;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.registry.GameRegistry;
@GameInitializer(after = BlockVoidMiner.class)
public class TileMiner extends TileRotation implements IGuiProvider {
    public static void init(){
        GameRegistry.registerTileEntity(TileMiner.class,new ResourceLocation(RotationCraft.MOD_ID,"tileminer"));
    }
    public TileMiner(){
        super(FacingTool.Position.UP);
        createSlots(27);
        setSlotMode(SlotMode.OUTPUT,0,27);
    }

    boolean validate = false;

    @Override
    public void update() {
        if(!world.isRemote){
            if(world.getTotalWorldTime() % 100 == 0){
                validate = true;
                for(int i=0;i<pos.getY();i++){
                    if(!world.isAirBlock(new BlockPos(pos.getX(),i,pos.getZ()))){
                        validate =  false;
                    }
                }
            }
            transferOutputStack();
        }
        super.update();
    }

    @Override
    public boolean canStart() {
        return validate;
    }

    @Override
    public RotaryPower getMinimumPower() {
        return new RotaryPower(2048,512,0,0);
    }
    int bound = 0;
    @Override
    public boolean processStart() {
        int ratio = Math.round((powerMachine.getPower().getSpeed() - getMinimumPower().getSpeed()) / 512F);
        if(ratio>40){
            bound = Math.round((ratio - 40) / 20F);
            if(bound>16){
                bound = 16;
            }
        }else {
            bound = 1;
        }
        float time = 400 - Math.min(ratio*10,400);
        processMax = processRem = Math.max(0,Math.round(time));
        return true;
    }

    @Override
    public void processTick() {
        processRem--;
    }

    @Override
    public void processEnd() {
        for(int i=0;i<bound;i++){
            Utils.distributeOutput(this, GlobalOre.next(world.rand),0,27,true);
        }
    }

    @Override
    public Object getGuiClient(EntityPlayer player) {
        return new GuiVoidMiner(this,player.inventory);
    }

    @Override
    public Object getGuiServer(EntityPlayer player) {
        return new ContainerVoidMiner(this,player.inventory);
    }
}
