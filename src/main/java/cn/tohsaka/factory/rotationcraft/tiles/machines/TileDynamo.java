package cn.tohsaka.factory.rotationcraft.tiles.machines;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IModelProvider;
import cn.tohsaka.factory.rotationcraft.api.IScrewable;
import cn.tohsaka.factory.rotationcraft.blocks.machine.BlockDynamo;
import cn.tohsaka.factory.rotationcraft.config.GeneralConfig;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.animated.ModelDynamo2;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileModelMachine;
import cn.tohsaka.factory.rotationcraft.utils.FacingTool;
import cn.tohsaka.factory.rotationcraft.utils.ModUtils;
import cn.tohsaka.factory.rotationcraft.utils.ReikaMathLibrary;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import cofh.redstoneflux.api.IEnergyHandler;
import cofh.redstoneflux.api.IEnergyProvider;
import cofh.redstoneflux.api.IEnergyReceiver;
import ic2.api.energy.event.EnergyTileLoadEvent;
import ic2.api.energy.event.EnergyTileUnloadEvent;
import ic2.api.energy.tile.IEnergyAcceptor;
import ic2.api.energy.tile.IEnergySource;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fml.common.registry.GameRegistry;

import javax.annotation.Nullable;
import java.util.List;

@GameInitializer(after = BlockDynamo.class)
public class TileDynamo extends TileModelMachine implements IModelProvider, IEnergyHandler, IEnergyProvider, IEnergySource, IScrewable {

    public TileDynamo(){

    }

    public static void init(){
        GameRegistry.registerTileEntity(TileDynamo.class,new ResourceLocation(RotationCraft.MOD_ID,"tiledynamo"));
    }

    @Override
    public RotaryModelBase getModel() {
        return new ModelDynamo2();
    }

    @Override
    public String getTexName() {
        return "dynamotex";
    }

    @Override
    protected String getModelTex() {
        if(powerMachine.getPower().getWatt()>0){
            return "dynamotex2";
        }
        return "dynamotex";
    }

    @Override
    public void updateInformationForTEISR(ItemStack stack, TileEntity tileEntity, float partialTicks) {

    }

    @Override
    public void beginItemRender() {

    }

    @Override
    public void update() {
        if(!world.isRemote){
            updatePowerInput();
            if(FEReceiver!=null || RFReceiver!=null){
                transferEnergy();
            }
            if(world.getTotalWorldTime() % 40 == 0){
                updateAdjacentHandlers();
                markDirty();
            }
        }else if(world!=null){
            angle += ReikaMathLibrary.doubpow(ReikaMathLibrary.logbase(powerMachine.getPower().getSpeed()+1, 2), 1.05);
        }
    }

    @Override
    public RotaryPower getMinimumPower() {
        return new RotaryPower(1,1);
    }


    @Override
    public boolean canConnectEnergy(EnumFacing from) {
        return from==facing;
    }

    @Override
    public int getEnergyStored(EnumFacing from) {
        return getGenRF();
    }

    @Override
    public int getMaxEnergyStored(EnumFacing from) {
        return getGenRF();
    }

    @Override
    public int extractEnergy(EnumFacing from, int maxExtract, boolean simulate) {
        if(from!=facing){
            return 0;
        }
        return getGenRF();
    }

    public int getGenRF() {
        long rf = Math.round(powerMachine.getPower().getWatt() / GeneralConfig.FE_converter_ratio);
        if(rf > Integer.MAX_VALUE){
            return Integer.MAX_VALUE;
        }
        return (int) rf;
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing side) {
        return (capability == CapabilityEnergy.ENERGY && (side == facing || side == null)) || super.hasCapability(capability,side);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing side) {
        if (capability == CapabilityEnergy.ENERGY && (side == facing || side == null)) {
            return CapabilityEnergy.ENERGY.cast(new net.minecraftforge.energy.IEnergyStorage() {

                @Override
                public int receiveEnergy(int maxReceive, boolean simulate) {

                    return 0;
                }

                @Override
                public int extractEnergy(int maxExtract, boolean simulate) {
                    return getGenRF();
                }

                @Override
                public int getEnergyStored() {

                    return getGenRF();
                }

                @Override
                public int getMaxEnergyStored() {

                    return getEnergyStored();
                }

                @Override
                public boolean canExtract() {
                    return true;
                }

                @Override
                public boolean canReceive() {
                    return false;
                }

            });
        }

        return super.getCapability(capability,side);
    }

    protected IEnergyReceiver RFReceiver = null;
    protected IEnergyStorage FEReceiver = null;

    protected void updateAdjacentHandlers() {
        if (world.isRemote) {
            return;
        }
        BlockPos target = FacingTool.getRelativePos(pos,facing, FacingTool.Position.FRONT,1);
        if (world.isAirBlock(target)){
            return;
        }
        RFReceiver = null;
        FEReceiver = null;
        TileEntity tile = world.getTileEntity(target);
        if(tile !=null){
            EnumFacing f = facing.getOpposite();
            if(tile.hasCapability(CapabilityEnergy.ENERGY,f)) {
                FEReceiver = tile.getCapability(CapabilityEnergy.ENERGY, f);
            }else if(tile instanceof IEnergyReceiver){
                if(((IEnergyReceiver) tile).canConnectEnergy(f)) {
                    RFReceiver = (IEnergyReceiver) tile;
                }
            }
        }
    }

    protected void transferEnergy() {
        if(FEReceiver!=null){
            FEReceiver.receiveEnergy(getGenRF(),false);
        }else if(RFReceiver!=null){
            EnumFacing f = facing.getOpposite();
            RFReceiver.receiveEnergy(f,getGenRF(),false);
        }
    }



    @Override
    public void onLoad() {
        super.onLoad();
        if(ModUtils.isModLoaded("ic2")){
            MinecraftForge.EVENT_BUS.post(new EnergyTileLoadEvent(this));
        }
    }

    @Override
    public void onChunkUnload() {
        super.onChunkUnload();
        if(ModUtils.isModLoaded("ic2")) {
            MinecraftForge.EVENT_BUS.post(new EnergyTileUnloadEvent(this));
        }
    }

    @Override
    public void invalidate() {
        super.invalidate();
        if(ModUtils.isModLoaded("ic2")) {
            MinecraftForge.EVENT_BUS.post(new EnergyTileUnloadEvent(this));
        }
    }

    @Override
    public boolean emitsEnergyTo(IEnergyAcceptor receiver, EnumFacing side) {
        return side==facing;
    }


    @Override
    public double getOfferedEnergy() {
        return getGenRF() / 4;
    }

    @Override
    public void drawEnergy(double amount) {

    }
    @Override
    public int getSourceTier() {
        return tier;
    }
    int tier = 0;
    public int loopIC2Tier(){
        tier++;
        if(tier>9){
            tier = 0;
        }
        markDirty();
        return tier;
    }
    int[] tier_volt = new int[]{8,32,128,512,2048,8192,32768,131072,524288,2147483647};
    String[] tier_name = new String[]{"ULV","LV","MV","HV","EV","IV","LuV","ZPM","UV","MAX"};

    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {
        super.getWailaBody(itemStack, tooltip);
        tooltip.add(StringHelper.localize("info.rc.waila.names.dynamo.ratio")+": "+GeneralConfig.FE_converter_ratio+":1");
        if(ModUtils.isModLoaded("ic2")){
            tooltip.add("IC2 Tier:"+tier);
            tooltip.add("IC2 Voltage:"+tier_name[tier]+" "+tier_volt[tier]);
        }
    }

    @Override
    public NBTTagCompound writeGuiTagCompound(NBTTagCompound tag) {
        tag.setInteger("ic2tier",tier);
        return super.writeGuiTagCompound(tag);
    }

    @Override
    public void readGuiTagCompound(NBTTagCompound tag) {
        super.readGuiTagCompound(tag);
        if(tag.hasKey("ic2tier")){
            tier = tag.getInteger("ic2tier");
        }
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        if(compound.hasKey("ic2tier")){
            tier = compound.getInteger("ic2tier");
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound.setInteger("ic2tier",tier);
        return super.writeToNBT(compound);
    }

    @Override
    public boolean screw(EnumFacing facing, EntityLivingBase player) {
        if(ModUtils.isModLoaded("ic2")){
            loopIC2Tier();
            player.sendMessage(new TextComponentString("IC2 Tier set to: "+tier));
            return true;
        }
        return false;
    }
}
