package cn.tohsaka.factory.rotationcraft.tiles.reactor;

import cn.tohsaka.factory.librotary.api.power.CapabilityRotaryPower;
import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.librotary.api.power.base.RotaryPowerSource;
import cn.tohsaka.factory.librotary.api.power.interfaces.IPowerMachine;
import cn.tohsaka.factory.librotary.packet.PacketCustom;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.power.IModeProvider;
import cn.tohsaka.factory.rotationcraft.blocks.reactor.BlockTCU;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileRotation;
import cn.tohsaka.factory.rotationcraft.utils.AxlsUtils;
import cn.tohsaka.factory.rotationcraft.utils.FacingTool;
import cn.tohsaka.factory.rotationcraft.utils.WorldUtils;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.FluidTankPropertiesWrapper;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.common.registry.GameRegistry;

import javax.annotation.Nullable;

@GameInitializer(after = BlockTCU.class)
public class TileTCU extends TileRotation implements IModeProvider {
    public static void init(){
        GameRegistry.registerTileEntity(TileTCU.class,new ResourceLocation(RotationCraft.MOD_ID,"tiletcu"));
    }
    RotaryPowerSource powerSource;
    public TileTCU(){
        powerSource = new RotaryPowerSource(maxmumpower,this){



            @Nullable
            @Override
            public World getWorld() {
                return TileTCU.this.getWorld();
            }

            @Nullable
            @Override
            public BlockPos getSrc() {
                return null;
            }

            @Nullable
            @Override
            public BlockPos getDest() {
                return FacingTool.getRelativePos(pos,facing, FacingTool.Position.BACK,level);
            }

            @Nullable
            @Override
            public IPowerMachine getParent() {
                return null;
            }

            @Nullable
            @Override
            public IPowerMachine getChild() {
                return WorldUtils.getCapability(CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY,pos,world,facing.getOpposite());
            }
        };
    }

    public FluidTank lubricant = new FluidTank(16000);
    public FluidTank lpsteam = new FluidTank(64000);
    public int level = -1;
    public RotaryPower maxmumpower = new RotaryPower(12800,65536,0,0);
    public int step = 0;
    public static RotaryPower getPowerUsingLevel(int level){
        if(level<=0){
            return RotaryPower.ZERO.copy();
        }
        return new RotaryPower(800 << Math.min(Math.max(level-1,1),4),65536,0,0);
    }


    public RotaryPower getPowerGenerated(){
        RotaryPower power = getPowerUsingLevel(level);
        if(step == 0 || lubricant.getFluidAmount()==0){
            return RotaryPower.ZERO.copy();
        }
        if(step<2400){
            return new RotaryPower(power.getTorque(),Math.round(power.getSpeed()* (step/2400f)),0,0);
        }

        return power;
    }

    @Override
    public boolean canStart() {
        return (isValidatedTurbine() && level>0 && lubricant.getFluidAmount()>0 && lpsteam.getFluidAmount()>0);
    }

    public int getLevelByPos(BlockPos tpos){
        for(int i=1;i<6;i++){
            BlockPos pp = FacingTool.getRelativePos(pos,facing, FacingTool.Position.FRONT,i);
            if(pp.equals(tpos)){
                return i;
            }
        }
        return -1;
    }


    @Override
    public void update() {
        if(world.isRemote){
            angle = AxlsUtils.calcAngle(angle,getPowerGenerated().getSpeed());
        }else{
            if(canStart()){
                if(step<2400){
                    step++;
                }
                lubricant.drain(20*level,true);
                lpsteam.drain(20*level,true);
                powerSource.setPower(getPowerGenerated());
            }else{
                if(step>0){
                    step--;
                    if(lubricant.getFluidAmount()>0){
                        lubricant.drain(20*level,true);
                    }else {
                        step=0;
                    }

                }
                powerSource.setPower(getPowerGenerated());
            }
        }
        if(world.getTotalWorldTime() % 200 == 0){
            validateTurbine();
        }
    }

    @Override
    public RotaryPower getMinimumPower() {
        return RotaryPower.ZERO;
    }

    @Override
    public boolean onChangingMode(World world, EntityPlayer player, BlockPos pos) {
        validateTurbine();
        return true;
    }
    public boolean isValidatedTurbine(){
        return level>0;
    }

    public int validateTurbine(){
        level = -1;
        for(int i=1;i<6;i++){
            BlockPos turbo = FacingTool.getRelativePos(pos,facing, FacingTool.Position.FRONT,i);
            TileEntity teTurbo= world.getTileEntity(turbo);
            if(teTurbo==null || !(teTurbo instanceof TileMiniTurbine)){
                break;
            }
            level = i;
            ((TileMiniTurbine)teTurbo).setFacing2(facing.rotateY().rotateY());
            ((TileMiniTurbine) teTurbo).setCU(this);
        }
        return level;
    }
    public void invalidateTurbine(){
        for(int i=1;i<6;i++){
            BlockPos turbo = FacingTool.getRelativePos(pos,facing, FacingTool.Position.FRONT,i);
            TileEntity teTurbo= world.getTileEntity(turbo);
            if(teTurbo!=null && (teTurbo instanceof TileMiniTurbine)){
                ((TileMiniTurbine) teTurbo).setCU(null);
            }
        }
        level = -1;
        markDirty();
    }

    @Override
    public void invalidate() {
        invalidateTurbine();
        super.invalidate();
    }

    @Override
    public void setFacing(EnumFacing facing) {
        invalidateTurbine();
        super.setFacing(facing);
        validateTurbine();
    }


    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing side) {
        if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY){
            return true;
        }

        if(capability == CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY && (side==facing.getOpposite() || side ==null)){
            return true;
        }
        return super.hasCapability(capability,side);
    }

    public TileTCU getInstance(){
        return this;
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing side) {
        if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY){
            return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(new IFluidHandler(){

                @Override
                public IFluidTankProperties[] getTankProperties() {
                    return new IFluidTankProperties[]{new FluidTankPropertiesWrapper(lubricant),new FluidTankPropertiesWrapper(lpsteam)};
                }

                @Override
                public int fill(FluidStack resource, boolean doFill) {
                    int filled = 0;
                    if(resource.getFluid().equals(RotationCraft.fluids.get("lubricant"))){
                        filled = lubricant.fill(resource,doFill);
                    }
                    if(resource.getFluid().equals(RotationCraft.fluids.get("steamlp"))){
                        filled = lpsteam.fill(resource,doFill);
                    }
                    getInstance().markDirty();
                    return filled;
                }

                @Nullable
                @Override
                public FluidStack drain(FluidStack resource, boolean doDrain) {
                    return null;
                }

                @Nullable
                @Override
                public FluidStack drain(int maxDrain, boolean doDrain) {
                    return null;
                }
            });
        }

        if(capability == CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY && (side==facing.getOpposite() || side ==null)){
            return CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY.cast(powerSource);
        }
        return super.getCapability(capability, facing);
    }


    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound tag = super.getUpdateTag();
        tag.setInteger("level",level);
        tag.setInteger("step",step);
        tag.setTag("lubricant",lubricant.writeToNBT(new NBTTagCompound()));
        tag.setTag("lpsteam",lpsteam.writeToNBT(new NBTTagCompound()));
        return tag;
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tag) {
        super.handleUpdateTag(tag);
        if(tag.getInteger("level")>-1){
            validateTurbine();
        }else{
            invalidateTurbine();
        }
        step = tag.getInteger("step");
        lubricant.readFromNBT(tag.getCompoundTag("lubricant"));
        lpsteam.readFromNBT(tag.getCompoundTag("lpsteam"));
    }


    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        level = compound.getInteger("level");
        step = compound.getInteger("step");
        lubricant.readFromNBT(compound.getCompoundTag("lubricant"));
        lpsteam.readFromNBT(compound.getCompoundTag("lpsteam"));
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound = super.writeToNBT(compound);
        compound.setInteger("level",level);
        compound.setInteger("step",step);
        compound.setTag("lubricant",lubricant.writeToNBT(new NBTTagCompound()));
        compound.setTag("lpsteam",lpsteam.writeToNBT(new NBTTagCompound()));
        return compound;
    }




    @Override
    public void writeGuiData(PacketCustom packet, boolean isFullSync) {

    }

    @Override
    public void readGuiData(PacketCustom packet, boolean isFullSync) {

    }
}
