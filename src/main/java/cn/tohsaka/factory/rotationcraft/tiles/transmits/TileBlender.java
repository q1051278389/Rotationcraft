package cn.tohsaka.factory.rotationcraft.tiles.transmits;

import cn.tohsaka.factory.librotary.api.power.CapabilityRotaryPower;
import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.librotary.api.power.base.RotaryPowerBlender;
import cn.tohsaka.factory.librotary.api.power.interfaces.IMeterable;
import cn.tohsaka.factory.librotary.api.power.interfaces.IPowerBlender;
import cn.tohsaka.factory.librotary.api.power.interfaces.IPowerMachine;
import cn.tohsaka.factory.librotary.helper.LoopDetector;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.api.power.IModeProvider;
import cn.tohsaka.factory.rotationcraft.blocks.transmits.BlockAxls;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerBlender;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.GuiBlender;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileMachineBase;
import cn.tohsaka.factory.rotationcraft.utils.*;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Optional;

@GameInitializer(after = BlockAxls.class)
public class TileBlender extends TileMachineBase implements RenderUtils.IOutBoxDrawable,ITickable, IModeProvider, IGuiProvider, IMeterable, LoopDetector.LoopCatcher {
    IPowerBlender blender;
    public TileBlender(){
        blender = new RotaryPowerBlender(this) {


            @Nullable
            @Override
            public BlockPos[] getSrcs() {
                return TileBlender.this.getSrc();
            }

            @Nullable
            @Override
            public BlockPos[] getDests() {
                return TileBlender.this.getDest();
            }

            @Nullable
            @Override
            public IPowerMachine[] getParents() {
                if(type==0){//merge
                    IPowerMachine cap = getPowerMachine(FacingTool.Position.FRONT);
                    IPowerMachine cap2 = getPowerMachine(subaxlsposition);
                    return new IPowerMachine[]{cap,cap2};
                }else {
                    IPowerMachine cap = getPowerMachine(FacingTool.Position.FRONT);
                    return new IPowerMachine[]{cap};
                }
            }

            @Nullable
            @Override
            public IPowerMachine[] getChilds() {
                if(type==0){//merge
                    IPowerMachine cap = getPowerMachine(FacingTool.Position.BACK);
                    return new IPowerMachine[]{cap};
                }else {
                    IPowerMachine cap = getPowerMachine(FacingTool.Position.BACK);
                    IPowerMachine cap2 = getPowerMachine(subaxlsposition);
                    return new IPowerMachine[]{cap,cap2};
                }
            }

            @Nullable
            @Override
            public World getWorld() {
                return TileBlender.this.getWorld();
            }

            @Nullable
            @Override
            public BlockPos getSrc() {
                return null;
            }

            @Nullable
            @Override
            public BlockPos getDest() {
                return null;
            }

            @Nullable
            @Override
            public IPowerMachine getParent() {
                return null;
            }

            @Nullable
            @Override
            public IPowerMachine getChild() {
                return null;
            }
        };
    }
    public int type;//0 for merge, 1 for split
    public static void init(){
        GameRegistry.registerTileEntity(TileBlender.class,new ResourceLocation(RotationCraft.MOD_ID,"tileblender"));
    }
    public float angle0;
    public float angle1;
    public float angle2;
    public int[] workratio = new int[]{1,1};//inline outline
    public FacingTool.Position subaxlsposition = FacingTool.Position.LEFT;
    @Override
    public void update() {
        if(world.isRemote){
            if(type==0){//merge
                IPowerMachine cap = getPowerMachine(FacingTool.Position.FRONT);
                IPowerMachine cap2 = getPowerMachine(subaxlsposition);
                if(cap!=null){
                    angle0 = AxlsUtils.calcAngle(angle0,cap.getPower().getSpeed());
                }
                if(cap2!=null){
                    angle2 = AxlsUtils.calcAngle(angle2,cap2.getPower().getSpeed());
                }
                angle1 = AxlsUtils.calcAngle(angle1,blender.getPower().getSpeed());
            }else{
                IPowerMachine cap = getPowerMachine(FacingTool.Position.FRONT);
                if(cap!=null){
                    angle0 = angle1 = angle2 = AxlsUtils.calcAngle(angle0,cap.getPower().getSpeed());
                }
            }
        }else {
            if(type==0){//merge
                IPowerMachine cap = getPowerMachine(FacingTool.Position.FRONT);
                IPowerMachine cap2 = getPowerMachine(subaxlsposition);
                blender.setPower2(RotaryPower.ZERO);
                if(cap==null && cap2 == null){
                    blender.setPower(RotaryPower.ZERO);
                }else{
                    if(cap==null || cap2==null){
                        if(cap==null){
                            blender.setPower(cap2.getPower());
                        }else{
                            blender.setPower(cap.getPower());
                        }
                    }else{
                        RotaryPower power1 = cap.getPower();
                        RotaryPower power2 = cap2.getPower();
                        if(power1.getSpeed() == power2.getSpeed()){
                            blender.setPower(new RotaryPower(power1.getTorque()+power2.getTorque(),power1.getSpeed(),0,Math.max(power1.getBlend(),power2.getBlend())+1));
                        }else{
                            if(power1.getSpeed() >= power2.getSpeed()){
                                blender.setPower(power1);
                            }else {
                                blender.setPower(power2);
                            }
                        }
                    }
                }
            }else{
                IPowerMachine cap = getPowerMachine(FacingTool.Position.FRONT);
                if(cap!=null){
                    RotaryPower power = cap.getPower().copy();
                    blender.setPower(new RotaryPower((power.getTorque()/(workratio[0]+workratio[1])) * workratio[0],power.getSpeed(),angle0,power.getBlend()));
                    blender.setPower2(new RotaryPower((power.getTorque()/(workratio[0]+workratio[1])) * workratio[1],power.getSpeed(),angle0,power.getBlend()));
                }else {
                    blender.setPower(RotaryPower.ZERO);
                    blender.setPower2(RotaryPower.ZERO);
                }
            }
        }




    }

    @Override
    public void onLoopDetected(){
        Optional.of(world.getPlayerEntityByName(placer)).ifPresent(player -> {
            player.sendMessage(new TextComponentString("PowerNet looped on: "+pos.toString()));
        });
        blender.setPower(RotaryPower.ZERO);
        blender.setPower2(RotaryPower.ZERO);
        world.destroyBlock(pos,true);
    }

    @Override
    public void getOutBoxList(NonNullList<RenderUtils.RenderItem> drawables) {
        /*drawables.add(new RenderUtils.RenderItem(0x0000FF, FacingTool.getRelativePos(pos, facing, FacingTool.Position.FRONT,1),1F));
        drawables.add(new RenderUtils.RenderItem(0xFF0000,FacingTool.getRelativePos(pos,facing, FacingTool.Position.BACK,1),1F));
        drawables.add(new RenderUtils.RenderItem(type==0?0x0000FF:0xFF0000,FacingTool.getRelativePos(pos,facing, subaxlsposition,1),1F));
        */
        for (BlockPos pos:getDest()){
            drawables.add(new RenderUtils.RenderItem(0xFF0000, pos,1F));
        }
        for (BlockPos pos:getSrc()){
            drawables.add(new RenderUtils.RenderItem(0x0000ff, pos,1F));
        }
        super.getOutBoxList(drawables);
    }



    public BlockPos[] getSrc() {
        if(type==0){
            return new BlockPos[]{
                    FacingTool.getRelativePos(pos, facing, FacingTool.Position.FRONT,1),
                    FacingTool.getRelativePos(pos, facing, subaxlsposition,1)
            };
        }
        return new BlockPos[]{FacingTool.getRelativePos(pos, facing, FacingTool.Position.FRONT,1)};
    }


    public BlockPos[] getDest() {
        if(type==1){
            return new BlockPos[]{
                    FacingTool.getRelativePos(pos, facing, FacingTool.Position.BACK,1),
                    FacingTool.getRelativePos(pos, facing, subaxlsposition,1)
            };
        }
        return new BlockPos[]{FacingTool.getRelativePos(pos, facing, FacingTool.Position.BACK,1)};
    }

    @Override
    public boolean isActive() {
        return blender.getPower().isPowered();
    }


    public void setWorkratio(int[] workratio) {
        this.workratio = workratio;
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound tag = super.getUpdateTag();
        tag.setIntArray("workratio",workratio);
        tag.setInteger("type",type);
        tag.setInteger("subaxls",subaxlsposition.getIndex());

        return tag;
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tag) {
        super.handleUpdateTag(tag);
        workratio = tag.getIntArray("workratio");
        type = tag.getInteger("type");
        subaxlsposition = FacingTool.Position.getbyIndex(tag.getInteger("subaxls"));

    }

    @Override
    public boolean onChangingMode(World world, EntityPlayer player, BlockPos pos) {
        type=Math.abs(type-1);
        return true;
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        type = compound.getInteger("type");
        workratio = compound.getIntArray("workratio");
        subaxlsposition = FacingTool.Position.getbyIndex(compound.getInteger("subaxls"));
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound.setInteger("type",type);
        compound.setIntArray("workratio",workratio);
        compound.setInteger("subaxls",subaxlsposition.getIndex());
        return super.writeToNBT(compound);
    }

    @Override
    public boolean allowVerticalFacing() {
        return false;
    }

    @Override
    public void rotateBlock(EntityPlayer player) {
        if(subaxlsposition == FacingTool.Position.RIGHT){
            subaxlsposition = FacingTool.Position.LEFT;
            super.rotateBlock(player);
        }else{
            subaxlsposition = FacingTool.Position.RIGHT;
            setFacing(facing);
        }
    }


    @Override
    public Object getGuiClient(EntityPlayer player) {
        if(type==0){
            player.sendStatusMessage(StringHelper.createTranslationComponent("info.rc.blender.guideniedbytype"),true);
            return null;
        }
        return new GuiBlender(this,player.inventory);
    }

    @Override
    public Object getGuiServer(EntityPlayer player) {
        if(type==0){
            return null;
        }
        return new ContainerBlender(player.inventory);
    }

    @Override
    public NBTTagCompound getModePacket() {
        NBTTagCompound tag = super.getModePacket();
        tag.setIntArray("workratio",workratio);
        return tag;
    }

    @Override
    public void handleModePacket(NBTTagCompound compound) {
        super.handleModePacket(compound);
        setWorkratio(compound.getIntArray("workratio"));
    }

    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {
        tooltip.add(StringHelper.localize("info.rc.waila.names.mode")+":"+StringHelper.localize("info.rc.waila.names.blender.mode"+(type==1?"A":"B")));
        tooltip.add(StringHelper.localize("info.rc.waila.names.blender.ratio")+":  "+workratio[0]+":"+workratio[1]);

    }

    @SideOnly(Side.CLIENT)
    public void doSmoke(TileBlender te){
        for(int i=0;i<4;i++){
            ReikaParticleHelper.spawnAt(EnumParticleTypes.CRIT,te.getWorld(), te.getPos().getX()+te.getWorld().rand.nextDouble(), te.getPos().getY()+0.8*te.getWorld().rand.nextDouble(), te.getPos().getZ()+te.getWorld().rand.nextDouble());
        }
        if(world.getTotalWorldTime()%4==0){
            /*ISound sound = PositionedSoundRecord.getRecord(SoundEvents.ENTITY_BLAZE_HURT, 0.8F+world.rand.nextFloat(), 6.0F);
            SoundHandler.playSound(sound);*/
            world.playSound(pos.getX()+0.5,pos.getY()+0.5,pos.getZ()+0.5,SoundEvents.ENTITY_BLAZE_HURT, SoundCategory.AMBIENT,0.1F,1F,true);
        }
        //SoundHandler.startTileSound(SoundEvents.ENTITY_BLAZE_HURT,0.5F,te.getPos(),20);
    }







    protected IPowerMachine getPowerMachine(FacingTool.Position position){
        EnumFacing side;
        if(position.getIndex()<2){
            if(position == FacingTool.Position.LEFT){
                side = facing.rotateY().rotateY().rotateY();
            }else{
                side = facing.rotateY();
            }
        }else {
            side = facing.rotateY().rotateY();
        }
        IPowerMachine cap = CapabilityRotaryPower.getCapability(world, FacingTool.getRelativePos(pos,facing, position,1),side);
        return cap;
    }







    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing side) {
        if(capability == CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY){
            if(side == null){
                return true;
            }
            if(side==FacingTool.revert(this.facing)){
                return true;
            }
            EnumFacing facing1 = FacingTool.getRelativeSide(subaxlsposition,facing);
            if(side==facing1){
                return type==1?true:false;
            }
        }
        return super.hasCapability(capability, side);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing side) {
        if(capability == CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY){
            if(side == null){
                return CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY.cast(blender);
            }
            if(side==FacingTool.revert(this.facing)){
                return CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY.cast(blender);
            }
            if(side==FacingTool.getRelativeSide(subaxlsposition,facing)){
                if(type==1){
                    blender.setNext2(true);
                }
                return CapabilityRotaryPower.ROTARYPOWER_HANDLER_CAPABILITY.cast(blender);
            }
        }
        return super.getCapability(capability, facing);
    }


    @Override
    public String getMeterInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("TileEntity["+world.getBlockState(pos).getBlock().getLocalizedName()+"]\n");
        sb.append(this.pos.toString()+"  Facing: "+facing.getName()+"  Subline Position: "+subaxlsposition.name()+"\n");
        if(type==1){
            sb.append(String.format("Type: Split[%d,%d]\n",workratio[0],workratio[1]));
            IPowerMachine cap = getPowerMachine(FacingTool.Position.FRONT);
            if(cap!=null){
                RotaryPower power = cap.getPower().copy();
                sb.append("Input:"+power+"\n");
            }
            sb.append("Output[Inline]:"+blender.getPower()+"\n");
            sb.append("Output[Subline]:"+blender.getPower2()+"\n");
        }else{
            IPowerMachine cap = getPowerMachine(FacingTool.Position.FRONT);
            IPowerMachine cap2 = getPowerMachine(subaxlsposition);
            RotaryPower power1 = RotaryPower.ZERO;
            RotaryPower power2 = RotaryPower.ZERO;
            if(cap!=null){
                power1 = cap.getPower();
            }

            if(cap2!=null){
                power2 = cap2.getPower();
            }

            sb.append("Type: Merge\n");
            sb.append("Input[Inline]:"+power1+"\n");
            sb.append("Input[Subline]:"+power2+"\n");
            sb.append("Output:"+blender.getPower());
        }
        sb.append("\n");

        return sb.toString();
    }

    @Override
    public void onMeter() {
        addIO();
    }
}
