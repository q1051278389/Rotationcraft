package cn.tohsaka.factory.rotationcraft.proxy;

import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;

public class CommonEventHandler {
    public static final CommonEventHandler INSTANCE = new CommonEventHandler();

    @SubscribeEvent
    public void onPlayerJoin(EntityJoinWorldEvent event){
        if(FMLCommonHandler.instance().getSide()== Side.SERVER || !event.getWorld().isRemote){
            if(event.getEntity() instanceof EntityPlayerMP) {
                NetworkDispatcher.sendConfig((EntityPlayerMP)event.getEntity());
            }
        }

    }
}
