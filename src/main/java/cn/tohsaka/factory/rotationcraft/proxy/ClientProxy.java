package cn.tohsaka.factory.rotationcraft.proxy;

import cn.tohsaka.factory.librotary.packet.PacketCustom;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.blocks.transfer.storage.RenderTank;
import cn.tohsaka.factory.rotationcraft.blocks.transfer.storage.TileTank;
import cn.tohsaka.factory.rotationcraft.blocks.transmits.BlockMeter;
import cn.tohsaka.factory.rotationcraft.etc.RenderOrePiece;
import cn.tohsaka.factory.rotationcraft.etc.RenderResource;
import cn.tohsaka.factory.rotationcraft.etc.sounds.SoundHandler;
import cn.tohsaka.factory.rotationcraft.init.Sounds;
import cn.tohsaka.factory.rotationcraft.items.*;
import cn.tohsaka.factory.rotationcraft.network.ClientPacketHandler;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.CreativeTabCore;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileEngine;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileModelMachine;
import cn.tohsaka.factory.rotationcraft.renders.*;
import cn.tohsaka.factory.rotationcraft.renders.ReikaComptableRender.ReikaGearboxRender;
import cn.tohsaka.factory.rotationcraft.renders.ReikaComptableRender.ReikaShaftRender;
import cn.tohsaka.factory.rotationcraft.renders.ReikaComptableRender.RenderMiniTurbine;
import cn.tohsaka.factory.rotationcraft.renders.ReikaComptableRender.RenderTCU;
import cn.tohsaka.factory.rotationcraft.renders.machine.WorktableItemRender;
import cn.tohsaka.factory.rotationcraft.tiles.base.TileWorktable;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileBEMiner;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileFurnace;
import cn.tohsaka.factory.rotationcraft.tiles.power.TileCreativeEngine;
import cn.tohsaka.factory.rotationcraft.tiles.reactor.TileMiniTurbine;
import cn.tohsaka.factory.rotationcraft.tiles.reactor.TileTCU;
import cn.tohsaka.factory.rotationcraft.tiles.transmits.*;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.color.IItemColor;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ClientProxy extends CommonProxy{
    @Override
    public void preInit() {
        super.preInit();
        MinecraftForge.EVENT_BUS.register(RenderUtils.INSTANCE);
        MinecraftForge.EVENT_BUS.register(ClientEventHandler.INSTANCE);
        MinecraftForge.EVENT_BUS.register(SoundHandler.INSTANCE);
        PacketCustom.assignHandler(NetworkDispatcher.NET_CHANNEL, new ClientPacketHandler());


        ClientRegistry.bindTileEntitySpecialRenderer(TileMiniTurbine.class,new RenderMiniTurbine());
        ClientRegistry.bindTileEntitySpecialRenderer(TileCreativeEngine.class,new SimpleAxlsRender<TileCreativeEngine>(2,16));
        ClientRegistry.bindTileEntitySpecialRenderer(TileCorner.class,new CornerRender());
        ClientRegistry.bindTileEntitySpecialRenderer(TileBlender.class,new BlenderRender());
        ClientRegistry.bindTileEntitySpecialRenderer(TileGearbox.class,new ReikaGearboxRender());
        ClientRegistry.bindTileEntitySpecialRenderer(TileWorm.class,new ReikaGearboxRender());
        ClientRegistry.bindTileEntitySpecialRenderer(TileHighGear.class,new ReikaGearboxRender());
        ClientRegistry.bindTileEntitySpecialRenderer(TileAxls.class,new ReikaShaftRender());

        ClientRegistry.bindTileEntitySpecialRenderer(TileEngine.class,new UniversalEngineRender());
        ClientRegistry.bindTileEntitySpecialRenderer(TileBEMiner.class,new RotaryMachineRender());
        ClientRegistry.bindTileEntitySpecialRenderer(TileModelMachine.class,new RotaryMachineRender());

        ClientRegistry.bindTileEntitySpecialRenderer(TileWorktable.class, new WorktableItemRender());
        ClientRegistry.bindTileEntitySpecialRenderer(TileFurnace.class, new FurnaceRender());
        ClientRegistry.bindTileEntitySpecialRenderer(TileTank.class, RenderTank.INSTANCE());
        ClientRegistry.bindTileEntitySpecialRenderer(TileTCU.class,new RenderTCU());

        ClientRegistry.bindTileEntitySpecialRenderer(TileClutch.class,new ClutchRender());




        //Minecraft.getMinecraft().getItemColors().colorMultiplier()




        Sounds.init();

        for(Block block: RotationCraft.blocks.values()){
            if(block instanceof IModelRegister){
                RotationCraft.logger.info("register model:" + block.getClass().getName());
                ((IModelRegister)block).registerModel();
            }
            block.setCreativeTab(TAB_BLOCK);
        }
        for(Item item:RotationCraft.items.values()){
            if(item instanceof IModelRegister){
                ((IModelRegister)item).registerModel();
            }
            if(!(item instanceof ItemMaterial)){
                item.setCreativeTab(TAB_ITEMS);
            }
        }

    }

    @Override
    public void Init() {
        super.Init();
        Minecraft.getMinecraft().getItemColors().registerItemColorHandler(new RenderOrePiece(), RotationCraft.items.get(ItemCrushedOre.NAME));

        Minecraft.getMinecraft().getItemColors().registerItemColorHandler(new RenderResource(), ItemResource.resources.toArray(new ItemResource[]{}));


        Minecraft.getMinecraft().getItemColors().registerItemColorHandler(new IItemColor() {
            @Override
            public int colorMultiplier(ItemStack itemStack, int i) {
                return 0x3366CC;
            }
        }, RotationCraft.items.get(ItemPortalGem.NAME));
    }

    public static CreativeTabs TAB_BLOCK = new CreativeTabCore(RotationCraft.MOD_ID,"blocks")
    {
        @SideOnly(Side.CLIENT)
        @Override
        public ItemStack getIconItemStack()
        {
            return new ItemStack(RotationCraft.blocks.get(BlockMeter.NAME));
        }
    };

    public static CreativeTabs TAB_MATERIAL = new CreativeTabCore(RotationCraft.MOD_ID,"materials")
    {
        @SideOnly(Side.CLIENT)
        @Override
        public ItemStack getIconItemStack()
        {
            return ItemMaterial.getStackById(7);
        }
    };

    public static CreativeTabs TAB_ITEMS = new CreativeTabCore(RotationCraft.MOD_ID,"items")
    {
        @SideOnly(Side.CLIENT)
        @Override
        public ItemStack getIconItemStack()
        {
            return new ItemStack(RotationCraft.items.get(ItemScrewer.NAME));
        }
    };

    
}
