package cn.tohsaka.factory.rotationcraft.book.GuiRender;

import cn.tohsaka.factory.rotationcraft.book.GuiGuide;

public class RenderGui{
    public int x;
    public int y;
    public int yy;
    public int w;
    public int h;
    protected GuiGuide gui;
    public String key;
    public RenderGui(GuiGuide g,int x,int y,int width,int height){
        this.x=x;
        this.y = y;
        this.yy=y;
        this.w=width;
        this.h=height;
        gui = g;
    }
    public void drawForegroundLayer(int mouseX, int mouseY){

    }
    public void drawBackgroundLayer(float partialTicks, int mouseX, int mouseY){

    }
    public void mouseClicked(GuiGuide screen,String id,int mouseX, int mouseY, int mouseButton){
        screen.mouseClicked(id,mouseX,mouseY,mouseButton);
    }
    public void setOffset(int offset){
        y=yy+offset;
    }
}
