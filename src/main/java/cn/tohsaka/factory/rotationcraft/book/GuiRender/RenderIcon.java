package cn.tohsaka.factory.rotationcraft.book.GuiRender;

import cn.tohsaka.factory.rotationcraft.book.GuiGuide;
import net.minecraft.item.ItemStack;

public class RenderIcon extends RenderStack {

    public RenderIcon(GuiGuide gui, int xCrood, int yCrood) {
        super(gui, ItemStack.EMPTY, xCrood, yCrood, false);
    }
    public boolean isActive = false;
    public int uv[];
    public int mode;
    public RenderIcon setV(int m){
        mode = m;
        if(m==0){
            uv=new int[]{42,63};
        }else{
            uv=new int[]{84,105};
        }
        return this;
    }

    @Override
    public void drawBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        screen.mc.renderEngine.bindTexture(RenderInfoPage.bookb);
        screen.drawSizedTexturedModalRect(x-3,y-2,isActive?uv[1]:uv[0],221,20,20,256,241);
    }

    @Override
    public void mouseClicked(GuiGuide screen, String id, int mouseX, int mouseY, int mouseButton) {
        super.mouseClicked(screen, id, mouseX, mouseY, mouseButton);
    }

}
