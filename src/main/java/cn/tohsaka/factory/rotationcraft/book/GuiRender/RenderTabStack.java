package cn.tohsaka.factory.rotationcraft.book.GuiRender;

import cn.tohsaka.factory.rotationcraft.book.GuiGuide;
import net.minecraft.item.ItemStack;

import java.util.Arrays;

public class RenderTabStack extends RenderStack {
    boolean isActive = false;
    String tip;
    public RenderTabStack(GuiGuide gui, ItemStack item,String tiptitle, int xCrood, int yCrood, boolean enableTooptip,String k) {
        super(gui, item, xCrood, yCrood, enableTooptip);
        key = k;
        tip=tiptitle;
    }

    @Override
    public void mouseClicked(GuiGuide screen, String id, int mouseX, int mouseY, int mouseButton) {
        this.isActive = true;
        for(RenderGui render:screen.renders.values()){
            if(render instanceof RenderTabStack && !render.equals(this)){
                ((RenderTabStack) render).setActive(false);
            }
        }
        super.mouseClicked(screen, id, mouseX, mouseY, mouseButton);
    }

    public RenderTabStack setActive(boolean a){
        isActive = a;
        return this;
    }
    @Override
    public void drawBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        screen.mc.renderEngine.bindTexture(RenderInfoPage.bookb);
        screen.drawSizedTexturedModalRect(x-3,y-2,isActive?21:0,221,20,20,256,241);
        renderStack();
    }

    public void renderToolTip(ItemStack stack, int x, int y) {
        screen.drawHoveringText(Arrays.asList(new String[]{tip==null? itemStack.getDisplayName() :tip}),x,y);
    }
}