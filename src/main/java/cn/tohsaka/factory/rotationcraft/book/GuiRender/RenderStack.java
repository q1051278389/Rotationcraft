package cn.tohsaka.factory.rotationcraft.book.GuiRender;

import cn.tohsaka.factory.rotationcraft.book.GuiGuide;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.item.ItemStack;

public class RenderStack extends RenderGui {
    public ItemStack itemStack;
    public boolean tooltip;
    protected GuiGuide screen;
    public RenderStack(GuiGuide gui,ItemStack item, int xCrood, int yCrood,boolean enableTooptip){
        super(gui,xCrood,yCrood,16,16);
        itemStack = item;
        tooltip = enableTooptip;
        screen = gui;
    }

    @Override
    public void drawForegroundLayer(int mouseX, int mouseY) {
        if(tooltip){
            if(mouseX>=x && mouseX<=x+w && mouseY>=y && mouseY<= y+h){
                gui.renderToolTip(itemStack,mouseX,mouseY);
            }
        }
    }

    @Override
    public void drawBackgroundLayer(float partialTicks, int mouseX, int mouseY) {

        screen.mc.renderEngine.bindTexture(RenderInfoPage.bookb);
        screen.drawSizedTexturedModalRect(x-3,y-2,0,221,20,20,256,241);

        renderStack();

    }
    public void renderStack(){
        RenderHelper.enableGUIStandardItemLighting();
        RenderItem itemRenderer = Minecraft.getMinecraft().getRenderItem();
        itemRenderer.renderItemIntoGUI(itemStack,x,y);
        //http://www.glprogramming.com/red/chapter03.html
        RenderHelper.disableStandardItemLighting();
    }

}