package cn.tohsaka.factory.rotationcraft.blocks.engine;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemBlockModelBased;
import cn.tohsaka.factory.rotationcraft.prefab.block.BlockBase;
import cn.tohsaka.factory.rotationcraft.tiles.power.TileCoil;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.List;

@GameInitializer(after = BlockBase.class)
public class BlockCoil extends BlockBase implements IModelRegister, ITileEntityProvider {
    public static final ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"blockcoil");
    public ItemBlock itemBlock;

    public static void init(){
        RotationCraft.INSTANCE.blocks.put(NAME,new BlockCoil());
    }

    public BlockCoil() {
        super(Material.IRON);
        this.setLightOpacity(0);
        setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
        setRegistryName(NAME);
        //setCreativeTab(RotationCraft.TAB_BLOCK);
        ForgeRegistries.BLOCKS.register(this);
        itemBlock = new ItemBlockModelBased(this){
            @Override
            public String getUnlocalizedName(ItemStack stack) {
                return String.format("tile.rc.blockcoil.%d",stack.getMetadata());
            }
        };
        itemBlock.setHasSubtypes(true);
        itemBlock.setRegistryName(NAME);
        ForgeRegistries.ITEMS.register(itemBlock);
        setDefaultState();
    }

    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(itemBlock, 0, new ModelResourceLocation(getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(itemBlock, 1, new ModelResourceLocation(getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(itemBlock, 2, new ModelResourceLocation(getRegistryName(), "inventory"));
    }

    @Override
    public void getSubBlocks(CreativeTabs itemIn, NonNullList<ItemStack> items) {
        items.add(new ItemStack(this, 1, 0));
        items.add(new ItemStack(this, 1, 1));
        items.add(new ItemStack(this, 1, 2));
    }



    @Nullable
    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileCoil(meta);
    }

    @Override
    public int getMaxmeta() {
        return 2;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return EnumBlockRenderType.MODEL;
    }

    @Override
    public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos) {
        super.neighborChanged(state, worldIn, pos, blockIn, fromPos);
        TileEntity tile = worldIn.getTileEntity(pos);
        if(tile instanceof TileCoil){
            ((TileCoil) tile).onWorldChanged(worldIn.isBlockPowered(pos));
        }
    }

    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
        if(VARIANT!=null){
            TileEntity tile = world.getTileEntity(pos);
            ItemStack stack = ItemStack.EMPTY;
            if(tile!=null && tile instanceof TileCoil && ((TileCoil) tile).powerStorage!=null){
                //stack = new ItemStack(this,1,((TileCoil) tile).getMetadata());
                stack = new ItemStack(this,1,state.getValue(VARIANT));
                NBTTagCompound tag = new NBTTagCompound();
                tag.setTag("storage",((TileCoil) tile).powerStorage.writeToNBT(new NBTTagCompound()));
                stack.setTagCompound(tag);
            }
            return stack;
        }
        return ItemStack.EMPTY;
    }
    @Override
    public void onBlockPlacedBy(World world, BlockPos pos, IBlockState state, EntityLivingBase player, ItemStack stack) {
        super.onBlockPlacedBy(world, pos, state, player, stack);
        TileCoil te = (TileCoil) world.getTileEntity(pos);
        if(te!=null && stack.getTagCompound()!=null && stack.getTagCompound().hasKey("storage")){
            te.powerStorage.readFromNBT(stack.getTagCompound().getCompoundTag("storage"));
        }
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void addInformation(ItemStack stack, @Nullable World player, List<String> tooltip, ITooltipFlag advanced) {
        super.addInformation(stack, player, tooltip, advanced);
        if(stack.hasTagCompound() && stack.getTagCompound().hasKey("storage")){
            NBTTagCompound energy = stack.getTagCompound().getCompoundTag("storage");
            tooltip.add(StringHelper.localize("info.rc.coil.energy_stored")+": " + RotaryPower.formattedPower(energy.getLong("powerstored")));
        }
    }

    @Override
    public void getDrops(NonNullList<ItemStack> drops, IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {

    }

}