package cn.tohsaka.factory.rotationcraft.blocks.transfer.storage;// Made with Blockbench 4.1.3
// Exported for Minecraft version 1.7 - 1.12
// Paste this class into your mod and generate all required imports


import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.utils.FluidUtils;
import cn.tohsaka.factory.rotationcraft.utils.RenderHelper2;
import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import org.lwjgl.opengl.GL11;

import java.util.ArrayList;

import static net.minecraft.client.renderer.texture.TextureMap.LOCATION_BLOCKS_TEXTURE;

public class ModelTank extends RotaryModelBase {
	private final ModelRenderer bb_main;
	private final ModelRenderer cube_r1;
	private final ModelRenderer cube_r2;
	private final ModelRenderer cube_r3;
	private final ModelRenderer cube_r4;

	public ModelTank() {
		textureWidth = 128;
		textureHeight = 128;

		bb_main = new ModelRenderer(this);
		bb_main.setRotationPoint(0.0F, 24.0F, 0.0F);
		bb_main.cubeList.add(new ModelBox(bb_main, 0, 0, -8.0F, -1.0F, -8.0F, 16, 1, 16, 0.0F, false));

		cube_r1 = new ModelRenderer(this);
		cube_r1.setRotationPoint(0.0F, -8.0F, 0.0F);
		bb_main.addChild(cube_r1);
		setRotationAngle(cube_r1, -1.5708F, 1.5708F, 0.0F);
		cube_r1.cubeList.add(new ModelBox(cube_r1, 0, 0, -8.0F, 7.0F, -8.0F, 16, 1, 16, 0.0F, false));

		cube_r2 = new ModelRenderer(this);
		cube_r2.setRotationPoint(0.0F, -8.0F, 0.0F);
		bb_main.addChild(cube_r2);
		setRotationAngle(cube_r2, -1.5708F, 3.1416F, 0.0F);
		cube_r2.cubeList.add(new ModelBox(cube_r2, 0, 0, -8.0F, 7.0F, -8.0F, 16, 1, 16, 0.0F, false));

		cube_r3 = new ModelRenderer(this);
		cube_r3.setRotationPoint(0.0F, -8.0F, 0.0F);
		bb_main.addChild(cube_r3);
		setRotationAngle(cube_r3, -1.5708F, -1.5708F, 0.0F);
		cube_r3.cubeList.add(new ModelBox(cube_r3, 0, 0, -8.0F, 7.0F, -8.0F, 16, 1, 16, 0.0F, false));

		cube_r4 = new ModelRenderer(this);
		cube_r4.setRotationPoint(0.0F, -8.0F, 0.0F);
		bb_main.addChild(cube_r4);
		setRotationAngle(cube_r4, -1.5708F, 0.0F, 0.0F);
		cube_r4.cubeList.add(new ModelBox(cube_r4, 0, 0, -8.0F, 7.0F, -8.0F, 16, 1, 16, 0.0F, false));


	}

	@Override
	public void renderExtra(TileEntity te) {
		/*GlStateManager.pushMatrix();
		//GlStateManager.enableBlend();

		GlStateManager.blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GlStateManager.translate(-0.5, 1, -0.5);
		RenderHelper.bindTexture(LOCATION_BLOCKS_TEXTURE);
		renderFluid((TileTank) te);
		GlStateManager.popMatrix();*/

		GlStateManager.pushMatrix();
		GlStateManager.enableBlend();
		GlStateManager.blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GlStateManager.translate(-0.5, 1, -0.5);
		RenderHelper2.bindTexture(LOCATION_BLOCKS_TEXTURE);
		//renderFluid((TileTank) te);
		GlStateManager.rotate(180,1,0,0);
		GlStateManager.translate(0,-0.5,-1);
		renderFluid((TileTank) te);
		//GlStateManager.disableBlend();
		GlStateManager.popMatrix();
	}

	@Override
	public void renderAll(TileEntity te, ArrayList conditions, float phi, float theta) {
		bb_main.render(f5);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}






	public void renderFluid(TileTank te)
	{
		float scale = 0;
		FluidStack stack = te.tank.getFluid();
		if(stack==null || stack.amount==0){
			return;
		}
		Fluid fluid = stack.getFluid();
		scale = Math.min(Math.max(0.1F,((float)stack.amount / (float)te.tank.getCapacity())),0.95F);

		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder renderer = tessellator.getBuffer();
		TextureAtlasSprite sprite = RenderHelper2.getTexture(fluid.getStill());
		if(sprite == null) {
			return;
		}
		//RenderHelper.disableStandardItemLighting();


		renderer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX_COLOR);

		float u1 = sprite.getMinU();
		float v1 = sprite.getMinV();
		float u2 = sprite.getMaxU();
		float v2 = sprite.getMaxV();

		float margin = 1f;
		float offset = 0.0625f;

		int color = fluid.getColor();

		float r = FluidUtils.getRed(color);
		float g = FluidUtils.getGreen(color);
		float b = FluidUtils.getBlue(color);
		float a = 0.9F;
		int TANK_THICKNESS = 0;
		// Top
		renderer.pos(offset, scale, offset).tex(u1, v1).color(r, g, b, a).endVertex();
		renderer.pos(offset, scale, 0.9375).tex(u1, v2).color(r, g, b, a).endVertex();
		renderer.pos(0.9375, scale, 0.9375).tex(u2, v2).color(r, g, b, a).endVertex();
		renderer.pos(0.9375, scale, offset).tex(u2, v1).color(r, g, b, a).endVertex();
		tessellator.draw();

		//RenderHelper.enableStandardItemLighting();

	}
}