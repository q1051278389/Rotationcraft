package cn.tohsaka.factory.rotationcraft.blocks.transfer.shipping;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileMachineBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.IInventoryChangedListener;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.items.CapabilityItemHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

@GameInitializer
public class TileEntityShipping extends TileMachineBase implements IInventoryChangedListener, IGuiProvider
{
    private final InventoryShipping inventory;
    private final ShippingProxyWrapper proxy;

    public static void init(){
        GameRegistry.registerTileEntity(TileEntityShipping.class,new ResourceLocation(RotationCraft.MOD_ID,"tileshipping"));
    }

    public TileEntityShipping()
    {
        this.inventory = new InventoryShipping(this);
        this.proxy = new ShippingProxyWrapper(this, inventory);
    }


    @Nonnull
    @Override
    public NBTTagCompound getUpdateTag()
    {
        return this.writeToNBT(new NBTTagCompound());
    }


    @Override
    public void onInventoryChanged(@Nonnull IInventory invBasic)
    {
        this.markDirty();
    }

    @Override
    public boolean hasCapability(@Nonnull Capability<?> capability, @Nullable EnumFacing facing)
    {
        return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY || capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY || capability == CapabilityEnergy.ENERGY;
    }

    @Nullable
    @Override
    public <T> T getCapability(@Nonnull Capability<T> capability, @Nullable EnumFacing facing)
    {
        if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
        {
            return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.cast(proxy);
        } else if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY)
        {
            return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(proxy);
        } else if(capability == CapabilityEnergy.ENERGY)
        {
            return CapabilityEnergy.ENERGY.cast(proxy);
        }

        return null;
    }



    @Override
    public Object getGuiClient(EntityPlayer player) {
        return new GuiShipping(player.inventory,this.inventory);
    }

    @Override
    public Object getGuiServer(EntityPlayer player) {
        return new ContainerShipping(player.inventory,this.inventory);
    }

    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {

    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        readFromNBTSelf(compound);
        proxy.refreshProxyEntries();
    }


    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        proxy.refreshProxyEntries();
        writeToNBTSelf(compound);
        return super.writeToNBT(compound);
    }

    public void readFromNBTSelf(NBTTagCompound compound) {
        if(compound.hasKey("inventory")){
            NBTTagList list = (NBTTagList) compound.getTag("inventory");
            for(int i=0;i<inventory.getSizeInventory();i++){

                inventory.setSlotWithoutNotice(i,new ItemStack(list.getCompoundTagAt(i)));
            }
        }
    }
    public NBTTagCompound writeToNBTSelf(NBTTagCompound compound) {
        NBTTagList list = new NBTTagList();
        for(int i=0;i<inventory.getSizeInventory();i++){
            list.appendTag(inventory.getStackInSlot(i).serializeNBT());
        }
        compound.setTag("inventory",list);
        return compound;
    }

    @Override
    public void onBlockHarvested(EntityPlayer player) {
        if(!player.isCreative()){
            ItemStack stack = new ItemStack(BlockShipping.itemBlock);
            stack.setTagCompound(writeToNBTSelf(new NBTTagCompound()));
            world.spawnEntity(new EntityItem(world,pos.getX(),pos.getY(),pos.getZ(),stack));
        }
    }
}