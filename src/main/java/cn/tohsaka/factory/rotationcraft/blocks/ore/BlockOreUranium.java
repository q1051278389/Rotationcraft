package cn.tohsaka.factory.rotationcraft.blocks.ore;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemResource;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import cn.tohsaka.factory.rotationcraft.worldgen.OreGenDefinition;
import cn.tohsaka.factory.rotationcraft.worldgen.RcWorldGen;
import net.minecraft.block.BlockOre;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.OreDictionary;

@GameInitializer
public class BlockOreUranium extends BlockOre implements IModelRegister {
    public static ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"oreuranium");
    public static ItemBlock itemBlock;
    public BlockOreUranium() {
        super(MapColor.GREEN);
        this.setLightLevel(1F);
        setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
        setRegistryName(NAME);
        ForgeRegistries.BLOCKS.register(this);
        itemBlock = new ItemBlock(this);
        itemBlock.setRegistryName(NAME);
        ForgeRegistries.ITEMS.register(itemBlock);
        OreDictionary.registerOre("oreUranium",this);
    }
    public static void init(){
        BlockOreUranium block = new BlockOreUranium();
        RotationCraft.INSTANCE.blocks.put(NAME,block);
        ItemResource.registerOreExtended(block,"Uranium",0x80EA15);
        RcWorldGen.add(new OreGenDefinition(block,10,80,5,3));
    }

    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(itemBlock, 0, new ModelResourceLocation(getRegistryName(), "normal"));
    }

    public boolean isOpaqueCube(IBlockState state) {
        return true;
    }
}
