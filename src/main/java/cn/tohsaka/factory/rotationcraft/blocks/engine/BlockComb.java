package cn.tohsaka.factory.rotationcraft.blocks.engine;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.blocks.transmits.BlockAxls;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemBlockModelBased;
import cn.tohsaka.factory.rotationcraft.prefab.block.BlockBase;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileMachineBase;
import cn.tohsaka.factory.rotationcraft.tiles.power.TileCoil;
import cn.tohsaka.factory.rotationcraft.tiles.power.TileComb;
import cn.tohsaka.factory.rotationcraft.tiles.transmits.TileGearbox;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

@GameInitializer(after = BlockBase.class)
public class BlockComb extends BlockBase implements IModelRegister, ITileEntityProvider {

    public static final ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"blockcomb");
    public ItemBlock itemBlock;
    public Map<Integer, BlockAxls> blocks = new HashMap<>();

    public static void init(){
        RotationCraft.INSTANCE.blocks.put(NAME,new BlockComb());
    }

    public BlockComb() {
        super(Material.IRON);
        this.setLightOpacity(0);
        setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
        setRegistryName(NAME);
        //setCreativeTab(RotationCraft.TAB_BLOCK);
        ForgeRegistries.BLOCKS.register(this);
        itemBlock = new ItemBlockModelBased(this);
        itemBlock.setRegistryName(NAME);
        ForgeRegistries.ITEMS.register(itemBlock);
        setDefaultState();
    }

    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(itemBlock, 0, new ModelResourceLocation(getRegistryName(), "inventory"));
    }

    @Override
    public void getSubBlocks(CreativeTabs itemIn, NonNullList<ItemStack> items) {
        items.add(new ItemStack(this, 1, 0));
    }

    @Nullable
    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        //return new TileCreativeEngine();
        return new TileComb();
    }

    @Override
    public int getMaxmeta() {
        return 0;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return EnumBlockRenderType.MODEL;
    }


    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
        if(VARIANT!=null){
            TileEntity tile = world.getTileEntity(pos);
            ItemStack stack = new ItemStack(this,1,((TileMachineBase)tile).getMetadata());
            if(tile!=null && tile instanceof TileCoil){
                stack.setTagCompound(tile.writeToNBT(new NBTTagCompound()));
            }
            return stack;
        }
        return ItemStack.EMPTY;
    }
}
