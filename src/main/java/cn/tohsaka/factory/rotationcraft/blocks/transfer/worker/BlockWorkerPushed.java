package cn.tohsaka.factory.rotationcraft.blocks.transfer.worker;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.blocks.transfer.pipe.BlockPipe;
import cn.tohsaka.factory.rotationcraft.blocks.transfer.worker.tiles.TileWorker;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemMaterial;
import cn.tohsaka.factory.rotationcraft.prefab.block.BlockBase;
import cn.tohsaka.factory.rotationcraft.prefab.block.BlockWorkerBase;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import cn.tohsaka.factory.rotationcraft.utils.RecipeHelper;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

import javax.annotation.Nullable;

@GameInitializer(after = BlockBase.class)
public class BlockWorkerPushed extends BlockWorkerBase implements IModelRegister, ITileEntityProvider {
    private static final ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"blockworkerpushed");
    public static ItemBlock itemBlock;
    public BlockWorkerPushed(){
        setRegistryName(NAME);
        setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
        ForgeRegistries.BLOCKS.register(this);
        itemBlock = new ItemBlock(this);
        itemBlock.setRegistryName(NAME);
        ForgeRegistries.ITEMS.register(itemBlock);
    }
    public static void init(){
        RotationCraft.INSTANCE.blocks.put(NAME,new BlockWorkerPushed());
        RecipeHelper.addShapedRecipe(new ItemStack(itemBlock),"ABA","BCB","ABA",'A', ItemMaterial.getStackById(1),'B', BlockPipe.itemBlock,'C',new ItemBlock(Blocks.PISTON));

    }

    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(itemBlock, 0, new ModelResourceLocation(NAME, "normal"));

    }

    @Nullable
    @Override
    public TileEntity createNewTileEntity(World world, int i) {
        return new TileWorker();
    }
}
