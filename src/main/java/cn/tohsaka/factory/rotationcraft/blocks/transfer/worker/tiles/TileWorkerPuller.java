package cn.tohsaka.factory.rotationcraft.blocks.transfer.worker.tiles;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.blocks.transfer.pipe.BlockPipe;
import cn.tohsaka.factory.rotationcraft.enet.Network;
import cn.tohsaka.factory.rotationcraft.enet.NetworkConnection;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.utils.WorldUtils;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@GameInitializer
public class TileWorkerPuller extends TileWorker{
    public static void init(){
        GameRegistry.registerTileEntity(TileWorkerPuller.class,new ResourceLocation(RotationCraft.MOD_ID,"tileworker2"));
    }

    @Override
    protected void doEnergy() {
        Map<NetworkConnection, IEnergyStorage> capability = Network.getNetwork(node).getCapability(CapabilityEnergy.ENERGY, node);
        for (IEnergyStorage src : capability.values()) {
            for(int i=0;i<6;i++){
                if(src==null || !src.canExtract() || src.getEnergyStored()==0 || src.getMaxEnergyStored()==0){
                    continue;
                }
                if(rsmode[i]>0){
                    if(world.isBlockPowered(pos)!=(rsmode[i]==1?true:false)){
                        continue;
                    }
                }
                if(workingSides[i]==1) {
                    IEnergyStorage dst = WorldUtils.getCapability(CapabilityEnergy.ENERGY, pos, world, EnumFacing.VALUES[i]);
                    if (dst == null) {
                        continue;
                    }
                    if(!dst.canReceive()){
                        continue;
                    }
                    int e = Math.min(dst.getMaxEnergyStored() - dst.getEnergyStored(),src.getEnergyStored());
                    src.extractEnergy(e,false);
                    dst.receiveEnergy(e,false);
                }
            }
        }
    }
    @Override
    protected void doFluid() {
        Map<NetworkConnection, IFluidHandler> capability = Network.getNetwork(node).getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, node);
        for (IFluidHandler src : capability.values()) {
            for(int i=0;i<6;i++){
                if(rsmode[i]>0){
                    if(world.isBlockPowered(pos)!=(rsmode[i]==1?true:false)){
                        continue;
                    }
                }
                if(workingSides[i]==1){
                    IFluidHandler dst = WorldUtils.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY,pos,world, EnumFacing.VALUES[i]);
                    if(dst==null){
                        continue;
                    }
                    for (IFluidTankProperties tank : src.getTankProperties()) {
                        if(!tank.canDrain() || tank.getContents()==null){
                            continue;
                        }
                        FluidStack stack = tank.getContents().copy();
                        if(isFluidBlocked(stack,i)){
                            continue;
                        }


                        if(stack.amount==0){
                            continue;
                        }
                        int filled = dst.fill(stack.copy(),true);
                        if(filled==0){
                            continue;
                        }
                        stack = new FluidStack(stack.getFluid(),filled);
                        src.drain(stack,true);
                    }
                }
            }
        }
    }

    @Override
    protected void doItem() {
        Map<NetworkConnection, IItemHandler> capability = Network.getNetwork(node).getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, node);
        for (IItemHandler src : capability.values()) {
            for(int i=0;i<6;i++){
                if(rsmode[i]>0){
                    if(world.isBlockPowered(pos)!=(rsmode[i]==1?true:false)){
                        continue;
                    }
                }
                if(workingSides[i]==1){
                    IItemHandler dst = WorldUtils.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY,pos,world, EnumFacing.VALUES[i]);
                    if(dst==null){
                        continue;
                    }

                    for (int i1 = 0; i1 < src.getSlots(); i1++) {
                        ItemStack stackOrigin = src.extractItem(i1,64,true);
                        if(isItemBlocked(stackOrigin,i)){
                            continue;
                        }
                        if(stackOrigin.isEmpty()){
                            continue;
                        }
                        ItemStack stackResult = ItemHandlerHelper.insertItemStacked(dst,stackOrigin.copy(),false);
                        if(stackResult.isEmpty()){
                            src.extractItem(i1,stackOrigin.getCount(),false);
                        }else {
                            src.extractItem(i1,stackOrigin.getCount()-stackResult.getCount(),false);
                        }
                    }
                }
            }
        }
    }






    @Override
    public Collection<NetworkConnection> getConnections() {
        List<NetworkConnection> connections = new ArrayList<>();
        for(int i=0;i<6;i++){
            if(workingSides[i]==1 && !BlockPipe.isConnectedToPipe(world,pos,EnumFacing.VALUES[i])){
                connections.add(new NetworkConnection(node,world,pos,EnumFacing.VALUES[i],node,world.getBlockState(pos).getBlock().getLocalizedName()));
            }
        }
        return connections;
    }

}
