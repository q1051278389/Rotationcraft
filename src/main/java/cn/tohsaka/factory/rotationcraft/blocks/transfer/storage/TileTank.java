package cn.tohsaka.factory.rotationcraft.blocks.transfer.storage;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IModelProvider;
import cn.tohsaka.factory.rotationcraft.etc.RotaryModelBase;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileMachineBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fml.common.registry.GameRegistry;

import javax.annotation.Nullable;
import java.util.List;

@GameInitializer
public class TileTank extends TileMachineBase implements IModelProvider {
    public static void init(){
        GameRegistry.registerTileEntity(TileTank.class,new ResourceLocation(RotationCraft.MOD_ID,"tiletank"));
    }
    public FluidTank tank = new FluidTank(128 * 1000){
        @Override
        protected void onContentsChanged() {
            TileTank.this.markDirty();
        }
    };
    public TileTank(){

    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        return capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY;
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY){
            return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(tank);
        }
        return null;
    }

    @Override
    public RotaryModelBase getModel() {
        return RenderTank.INSTANCE().model;
    }

    @Override
    public String getTexName() {
        return "combtex";
    }

    @Override
    public void updateInformationForTEISR(ItemStack stack, TileEntity tileEntity, float partialTicks) {
        if(tileEntity instanceof TileTank && stack.hasTagCompound()){
            ((TileTank)tileEntity).readFromNBTSelf(stack.getTagCompound());
        }
    }

    @Override
    public void beginItemRender() {

    }

    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {

    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        tank.readFromNBT(compound);
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        tank.writeToNBT(compound);
        return compound;
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound tag = super.getUpdateTag();
        tank.readFromNBT(tag);
        return tag;
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tag) {
        super.handleUpdateTag(tag);
        tank.readFromNBT(tag);
    }

    public void readFromNBTSelf(NBTTagCompound compound) {
        if(compound.hasKey("Fluid")) {
            tank.readFromNBT(compound.getCompoundTag("Fluid"));
        }
    }
    public NBTTagCompound writeToNBTSelf(NBTTagCompound compound) {
        if(tank.getFluidAmount()==0){
            return null;
        }
        NBTTagCompound tag = new NBTTagCompound();
        tank.writeToNBT(tag);
        compound.setTag("Fluid",tag);
        return compound;
    }

    @Override
    public void onBlockHarvested(EntityPlayer player) {
        if(!player.isCreative()){
            ItemStack stack = new ItemStack(BlockTank.itemBlock);
            stack.setTagCompound(writeToNBTSelf(new NBTTagCompound()));
            world.spawnEntity(new EntityItem(world,pos.getX(),pos.getY(),pos.getZ(),stack));
        }
    }
}
