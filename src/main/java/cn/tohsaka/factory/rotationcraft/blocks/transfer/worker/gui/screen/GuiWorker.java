package cn.tohsaka.factory.rotationcraft.blocks.transfer.worker.gui.screen;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.blocks.transfer.worker.gui.container.ContainerWorker;
import cn.tohsaka.factory.rotationcraft.blocks.transfer.worker.tiles.TileWorker;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.gui.ElementBase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementButtonSide;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementSlot;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

import java.util.HashMap;
import java.util.Map;

public class GuiWorker  extends GuiMachinebase {
    public static String tex_path = "textures/gui/basic_gui.png";
    public static String btn_tex = RotationCraft.MOD_ID+":textures/gui/corner.png";
    public static ResourceLocation tex = new ResourceLocation(RotationCraft.MOD_ID,tex_path);

    public TileWorker tile;
    public Map<Integer,ElementButtonSide> buttons = new HashMap<>();
    @Override
    public void initGui() {
        super.initGui();
        for(int i=0;i<4;i++){
            addElement(new ElementSlot(this,152,9+i*18,new ResourceLocation(RotationCraft.MOD_ID,"textures/gui/worker/slotworker.png")));
        }
        for(int i=0;i<4;i++){
            addElement(new ElementSlot(this,134,9+i*18,new ResourceLocation(RotationCraft.MOD_ID,"textures/gui/worker/slotfilter.png")));
        }


        addElement(new ElementButtonSide(this,70,20,"D").setid(0));
        addElement(new ElementButtonSide(this,89,20,"U").setid(1));
        addElement(new ElementButtonSide(this,70,39,"N").setid(2));
        addElement(new ElementButtonSide(this,89,39,"S").setid(3));
        addElement(new ElementButtonSide(this,70,58,"W").setid(4));
        addElement(new ElementButtonSide(this,89,58,"E").setid(5));


        for(ElementBase element:elements){
            if(element instanceof ElementButtonSide){
                buttons.put(((ElementButtonSide) element).getid(),(ElementButtonSide) element);
            }
        }

        revalidate();
    }

    public void revalidate(){
        for(int i=0;i<6;i++){
            buttons.get(i).setEnabled(!tile.isSideConnectedToPipe(EnumFacing.VALUES[i]));
            buttons.get(i).setActive(tile.workingSides[i]==1?true:false);
        }
    }
    EntityPlayer player;
    public GuiWorker(TileWorker te, InventoryPlayer playerInv) {
        super(new ContainerWorker(te,playerInv), tex);
        NAME = te.getBlockType().getLocalizedName();
        tile = te;
        player = playerInv.player;
        showtab = false;
    }


    @Override
    public void handleIDButtonClick(int id, int mouseButton) {
        if(mouseButton == 0){
            tile.setWorkingSide(id,buttons.get(id).toggle());
            return;
        }
        if(mouseButton == 1){
            NBTTagCompound tagCompound = new NBTTagCompound();
            tagCompound.setInteger("mode",0);
            tagCompound.setInteger("side",id);
            tagCompound.setUniqueId("player",player.getUniqueID());
            NetworkDispatcher.uploadingClientSetting(player,tile,tagCompound);
            return;
        }

    }
}
