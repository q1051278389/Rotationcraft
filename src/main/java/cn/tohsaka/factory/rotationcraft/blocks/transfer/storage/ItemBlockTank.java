package cn.tohsaka.factory.rotationcraft.blocks.transfer.storage;

import cn.tohsaka.factory.rotationcraft.items.ItemBlockModelBased;
import net.minecraft.block.Block;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.templates.FluidHandlerItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

public class ItemBlockTank extends ItemBlockModelBased{
    public ItemBlockTank(Block block) {
        super(block);
        this.setMaxStackSize(1);
    }


    @Nullable
    @Override
    public ICapabilityProvider initCapabilities(ItemStack stack, @Nullable NBTTagCompound nbt) {
        if(stack.getItem() == this){
            return new ItemTankCapProvider(stack);
        }
        return super.initCapabilities(stack, nbt);
    }


    protected class ItemTankCapProvider implements ICapabilityProvider {
        ItemStack stack;

        public ItemTankCapProvider(ItemStack s){
            stack = s;
        }
        @Override
        public boolean hasCapability(@Nonnull Capability<?> capability, @Nullable EnumFacing facing) {
            return capability == CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY;
        }

        @Nullable
        @Override
        public <T> T getCapability(@Nonnull Capability<T> capability, @Nullable EnumFacing facing) {
            if(capability == CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY){
                return CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY.cast(new FluidHandlerItemStack(stack,128000){

                });
            }
            return null;
        }
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
        super.addInformation(stack, worldIn, tooltip, flagIn);
        if(stack.hasCapability(CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY,null)){
            FluidHandlerItemStack handlerItem = (FluidHandlerItemStack) stack.getCapability(CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY,null);
            if(handlerItem!=null && handlerItem.getFluid()!=null) {
                FluidStack contents = handlerItem.getFluid();
                tooltip.add(String.format("%s: %d / %d mB", contents.getLocalizedName(), contents.amount,128000));
            }
        }
    }
}
