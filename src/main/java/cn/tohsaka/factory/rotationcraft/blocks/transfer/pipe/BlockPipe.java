package cn.tohsaka.factory.rotationcraft.blocks.transfer.pipe;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemMaterial;
import cn.tohsaka.factory.rotationcraft.prefab.block.BlockBase;
import cn.tohsaka.factory.rotationcraft.utils.FluidHelper;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import cn.tohsaka.factory.rotationcraft.utils.MultipartUtils;
import cn.tohsaka.factory.rotationcraft.utils.RecipeHelper;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.items.CapabilityItemHandler;

import javax.annotation.Nullable;
import java.util.List;

@GameInitializer(after = BlockBase.class)
public class BlockPipe extends Block implements IPipeConnectable,IModelRegister, ITileEntityProvider {

	public static final IProperty<Boolean> DOWN = PropertyBool.create("down");
	public static final IProperty<Boolean> UP = PropertyBool.create("up");
	public static final IProperty<Boolean> NORTH = PropertyBool.create("north");
	public static final IProperty<Boolean> SOUTH = PropertyBool.create("south");
	public static final IProperty<Boolean> WEST = PropertyBool.create("west");
	public static final IProperty<Boolean> EAST = PropertyBool.create("east");

	public static AxisAlignedBB[] smallSides = new AxisAlignedBB[7];
	public static AxisAlignedBB[] largeSides = new AxisAlignedBB[7];

	public static AxisAlignedBB defaultBox;

	static {
		smallSides[0] = new AxisAlignedBB(0.3, 0.0, 0.3, 0.7, 0.3, 0.7);
		smallSides[1] = new AxisAlignedBB(0.3, 0.7, 0.3, 0.7, 1.0, 0.7);
		smallSides[2] = new AxisAlignedBB(0.3, 0.3, 0.0, 0.7, 0.7, 0.3);
		smallSides[3] = new AxisAlignedBB(0.3, 0.3, 0.7, 0.7, 0.7, 1.0);
		smallSides[4] = new AxisAlignedBB(0.0, 0.3, 0.3, 0.3, 0.7, 0.7);
		smallSides[5] = new AxisAlignedBB(0.7, 0.3, 0.3, 1.0, 0.7, 0.7);
		smallSides[6] = new AxisAlignedBB(0.3, 0.3, 0.3, 0.7, 0.7, 0.7);

		largeSides[0] = new AxisAlignedBB(0.25, 0.0, 0.25, 0.75, 0.25, 0.75);
		largeSides[1] = new AxisAlignedBB(0.25, 0.75, 0.25, 0.75, 1.0, 0.75);
		largeSides[2] = new AxisAlignedBB(0.25, 0.25, 0.0, 0.75, 0.75, 0.25);
		largeSides[3] = new AxisAlignedBB(0.25, 0.25, 0.75, 0.75, 0.75, 1.0);
		largeSides[4] = new AxisAlignedBB(0.0, 0.25, 0.25, 0.25, 0.75, 0.75);
		largeSides[5] = new AxisAlignedBB(0.75, 0.25, 0.25, 1.0, 0.75, 0.75);
		largeSides[6] = new AxisAlignedBB(0.25, 0.25, 0.25, 0.75, 0.75, 0.75);

		defaultBox = smallSides[6];
	}




	public static final ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"pipe");
	public static void init(){
		RotationCraft.INSTANCE.blocks.put(NAME,new BlockPipe());
		RecipeHelper.addShapedRecipe(new ItemStack(itemBlock,8),"ABA",'A', ItemMaterial.getStackById(1),'B', Items.REDSTONE);
	}
	public static ItemBlock itemBlock;

	public BlockPipe() {
		super(Material.ROCK);
		setRegistryName(NAME);
		setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
		ForgeRegistries.BLOCKS.register(this);
		itemBlock = new ItemBlock(this);
		itemBlock.setRegistryName(NAME);
		ForgeRegistries.ITEMS.register(itemBlock);


		setDefaultState(blockState.getBaseState()
				.withProperty(UP, false)
				.withProperty(DOWN, false)
				.withProperty(NORTH, false)
				.withProperty(SOUTH, false)
				.withProperty(EAST, false)
				.withProperty(WEST, false)
		);
	}



	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {

		float x1=0.375F;
		float y1=0.375F;
		float z1=0.375F;
		float x2=0.625F;
		float y2=0.625F;
		float z2=0.625F;


		if (isConnectedTo(source, pos, EnumFacing.UP)) {
			y2=1.0F;
		}

		if (isConnectedTo(source, pos, EnumFacing.DOWN)) {
			y1=0.0F;
		}

		if (isConnectedTo(source, pos, EnumFacing.SOUTH)) {
			z2=1.0F;
		}

		if (isConnectedTo(source, pos, EnumFacing.NORTH)) {
			z1=0.0F;
		}

		if (isConnectedTo(source, pos, EnumFacing.EAST)) {
			x2=1.0F;
		}

		if (isConnectedTo(source, pos, EnumFacing.WEST)) {
			x1=0.0F;
		}

		return new AxisAlignedBB(x1, y1, z1, x2, y2, z2);
	}

	public static boolean isConnectedTo(IBlockAccess world, BlockPos pos, EnumFacing facing) {
		IBlockState state=world.getBlockState(pos.offset(facing));

		if(state.getBlock() instanceof IPipeConnectable){
			return true;
		}
		TileEntity te = world.getTileEntity(pos.offset(facing,1));
		if(te==null){
			return false;
		}
		return FluidHelper.isFluidHandler(te,facing.getOpposite()) ||
				te.hasCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY,facing.getOpposite()) ||
				te.hasCapability(CapabilityEnergy.ENERGY,facing.getOpposite());
	}

	public static boolean isConnectedToPipe(IBlockAccess world, BlockPos pos, EnumFacing facing) {
		IBlockState state=world.getBlockState(pos.offset(facing));

		if(state.getBlock() instanceof IPipeConnectable){
			return true;
		}
		return false;
	}

	@Override
	public EnumBlockRenderType getRenderType(IBlockState state) {
		return EnumBlockRenderType.MODEL;
	}

	@Override
	public boolean doesSideBlockRendering(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing face) {
		return false;
	}

	@Override
	public boolean isNormalCube(IBlockState state, IBlockAccess world, BlockPos pos) {
		return false;
	}

	@Override
	public boolean isOpaqueCube(IBlockState state) {
		return false;
	}

	@Override
	public boolean isBlockNormalCube(IBlockState state) {
		return false;
	}

	@Override
	public boolean isFullCube(IBlockState state) {
		return false;
	}

	@Override
	public boolean isNormalCube(IBlockState state) {
		return false;
	}

	@Override
	public int getMetaFromState(IBlockState state) {
		return 0;
	}

	@Override
	protected BlockStateContainer createBlockState() {
		return new BlockStateContainer(this, UP, DOWN, EAST, WEST, NORTH, SOUTH);
	}

	@Override
	public IBlockState getActualState(IBlockState state, IBlockAccess world, BlockPos pos) {
		IBlockState actualState= getDefaultState();

		if (isConnectedTo(world, pos, EnumFacing.UP)) {
			actualState=actualState.withProperty(UP, true);
		}

		if (isConnectedTo(world, pos, EnumFacing.DOWN)) {
			actualState=actualState.withProperty(DOWN, true);
		}

		if (isConnectedTo(world, pos, EnumFacing.SOUTH)) {
			actualState=actualState.withProperty(SOUTH, true);
		}

		if (isConnectedTo(world, pos, EnumFacing.NORTH)) {
			actualState=actualState.withProperty(NORTH, true);
		}

		if (isConnectedTo(world, pos, EnumFacing.EAST)) {
			actualState=actualState.withProperty(EAST, true);
		}

		if (isConnectedTo(world, pos, EnumFacing.WEST)) {
			actualState=actualState.withProperty(WEST, true);
		}

		return actualState;
	}

	@Override
	public void registerModel() {
		ModelLoader.setCustomModelResourceLocation(itemBlock, 0, new ModelResourceLocation(NAME, "normal"));

	}

	@Nullable
	@Override
	public TileEntity createNewTileEntity(World world, int i) {
		return new TilePipeCore();
	}


	@Deprecated
	@Override
	public RayTraceResult collisionRayTrace(IBlockState blockState, World world, BlockPos pos, Vec3d start, Vec3d end) {
		TileEntity tile = world.getTileEntity(pos);

		if (tile == null || !(tile instanceof TilePipeCore)) {
			return null;
		}

		List<AxisAlignedBB> boxes = ((TilePipeCore) tile).getCollisionBoxes();
		MultipartUtils.AdvancedRayTraceResult result = MultipartUtils.collisionRayTrace(pos, start, end, boxes);

		if (result != null && result.valid()) {
			setDefaultForTile((TilePipeCore)tile, result.bounds);
		}

		return result != null ? result.hit : null;
	}

	@Deprecated
	@Override
	public AxisAlignedBB getSelectedBoundingBox(IBlockState state, World world, BlockPos pos) {
		TileEntity tile = world.getTileEntity(pos);

		if (tile == null || !(tile instanceof TilePipeCore)) {
			return null;
		}
		return getDefaultForTile((TilePipeCore)tile).offset(pos);
	}
	private static AxisAlignedBB getDefaultForTile(TilePipeCore tile) {
		return defaultBox;
	}

	private static void setDefaultForTile(TilePipeCore tile, AxisAlignedBB box) {
		defaultBox = box;
		return;
	}

	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
		TileEntity tile = worldIn.getTileEntity(pos);
		if(tile==null || !(tile instanceof TilePipeCore)){
			return false;
		}

		return ((TilePipeCore) tile).onBlockActivated(state,playerIn,hand,facing,defaultBox);
	}
}