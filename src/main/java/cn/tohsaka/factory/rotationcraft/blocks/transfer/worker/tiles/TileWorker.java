package cn.tohsaka.factory.rotationcraft.blocks.transfer.worker.tiles;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.ICustomGuiProvider;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.blocks.transfer.worker.gui.container.ContainerWorker;
import cn.tohsaka.factory.rotationcraft.blocks.transfer.worker.gui.container.ContainerWorkerSide;
import cn.tohsaka.factory.rotationcraft.blocks.transfer.worker.gui.screen.GuiWorker;
import cn.tohsaka.factory.rotationcraft.blocks.transfer.worker.gui.screen.GuiWorkerSide;
import cn.tohsaka.factory.rotationcraft.blocks.transfer.pipe.BlockPipe;
import cn.tohsaka.factory.rotationcraft.blocks.transfer.pipe.TilePipeCore;
import cn.tohsaka.factory.rotationcraft.blocks.transfer.worker.BlockWorkerPulled;
import cn.tohsaka.factory.rotationcraft.enet.INetTickable;
import cn.tohsaka.factory.rotationcraft.enet.Network;
import cn.tohsaka.factory.rotationcraft.enet.NetworkConnection;
import cn.tohsaka.factory.rotationcraft.enet.Node;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemFilter;
import cn.tohsaka.factory.rotationcraft.items.ItemWorker;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileInvMachine;
import cn.tohsaka.factory.rotationcraft.utils.FluidHelper;
import cn.tohsaka.factory.rotationcraft.utils.WorldUtils;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.*;

@GameInitializer
public class TileWorker extends TileInvMachine implements INetTickable, ITickable,IGuiProvider, ICustomGuiProvider {
    public TileWorker(){
        createSlots(20);
        setSlotMode(SlotMode.NONE,0,20);// worker 4 + global filter 4 + (side 6 * 2)
    }
    public static void init(){
        GameRegistry.registerTileEntity(TileWorker.class,new ResourceLocation(RotationCraft.MOD_ID,"tileworker"));
    }

    public int[] workingSides = new int[]{0,0,0,0,0,0};

    public void setWorkingSide(int side,boolean working){
        workingSides[side] = working?1:0;
        NetworkDispatcher.uploadingClientSetting(null,this);
    }

    public int[] rsmode = new int[]{0,0,0,0,0,0};
    public void setSideRSMode(int side,int mode){
        rsmode[side] = mode;
        NetworkDispatcher.uploadingClientSetting(null,this);
    }



    @Override
    public void tickNetwork() {
        if(checkHasWorker(0)){
            doItem();
        }
        if(checkHasWorker(1)){
            doFluid();
        }
        if(checkHasWorker(2)){
            doEnergy();
        }
    }

    @Override
    public boolean isNodeInvalid() {
        return this.isInvalid();
    }

    protected void doItem() {
        Map<NetworkConnection, IItemHandler> capability = Network.getNetwork(node).getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, node);
        for(int i=0;i<6;i++){
            if(rsmode[i]>0){
                if(world.isBlockPowered(pos)!=(rsmode[i]==1?true:false)){
                    continue;
                }
            }
            if(workingSides[i]==1){
                IItemHandler src = WorldUtils.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY,pos,world, EnumFacing.VALUES[i]);
                if(src==null){
                    continue;
                }

                for (IItemHandler dst : capability.values()) {
                    for (int i1 = 0; i1 < src.getSlots(); i1++) {
                        ItemStack stackOrigin = src.extractItem(i1,64,true);
                        if(isItemBlocked(stackOrigin,i)){
                            continue;
                        }
                        if(stackOrigin.isEmpty()){
                            continue;
                        }
                        ItemStack stackResult = ItemHandlerHelper.insertItemStacked(dst,stackOrigin.copy(),false);
                        if(stackResult.isEmpty()){
                            src.extractItem(i1,stackOrigin.getCount(),false);
                        }else {
                            src.extractItem(i1,stackOrigin.getCount()-stackResult.getCount(),false);
                        }
                    }
                }
            }
        }
    }
    protected void doFluid() {
        Map<NetworkConnection, IFluidHandler> capability = Network.getNetwork(node).getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, node);
        for(int i=0;i<6;i++){
            if(rsmode[i]>0){
                if(world.isBlockPowered(pos)!=(rsmode[i]==1?true:false)){
                    continue;
                }
            }
            if(workingSides[i]==1){
                IFluidHandler src = WorldUtils.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY,pos,world, EnumFacing.VALUES[i]);
                if(capability.values().size()==0){
                    continue;
                }
                if(src==null){
                    continue;
                }
                for (IFluidTankProperties tank : src.getTankProperties()) {
                    if(!tank.canDrain() || tank.getContents()==null){
                        continue;
                    }
                    FluidStack stack = tank.getContents().copy();
                    if(isFluidBlocked(stack,i)){
                        continue;
                    }
                    if(stack.amount==0){
                        continue;
                    }

                    for (IFluidHandler dst : capability.values()) {

                        int filled = dst.fill(stack.copy(),true);
                        if(filled==0){
                            continue;
                        }
                        stack = new FluidStack(stack.getFluid(),filled);
                        src.drain(stack,true);
                    }
                }
            }
        }
    }
    protected void doEnergy() {
        Map<NetworkConnection, IEnergyStorage> capability = Network.getNetwork(node).getCapability(CapabilityEnergy.ENERGY, node);
        for(int i=0;i<6;i++){
            if(rsmode[i]>0){
                if(world.isBlockPowered(pos)!=(rsmode[i]==1?true:false)){
                    continue;
                }
            }
            if(workingSides[i]==1) {
                IEnergyStorage src = WorldUtils.getCapability(CapabilityEnergy.ENERGY, pos, world, EnumFacing.VALUES[i]);
                if(src==null){
                    continue;
                }
                if(!src.canExtract() || src.getEnergyStored()==0 || src.getMaxEnergyStored()==0){
                    continue;
                }
                for (NetworkConnection conn : capability.keySet()) {
                    if(conn.getCapabilityPos().equals(pos.offset(EnumFacing.VALUES[i]))){
                        continue;//loop dectector
                    }
                    IEnergyStorage dst = capability.get(conn);
                    if (dst == null || !dst.canReceive()) {
                        continue;
                    }
                    int e = Math.min(dst.getMaxEnergyStored() - dst.getEnergyStored(),src.getEnergyStored());
                    src.extractEnergy(e,false);
                    dst.receiveEnergy(e,false);
                }
            }
        }

    }

    public boolean checkHasWorker(int meta){
        for(int i=0;i<4;i++){
            if(getStackInSlot(i).getItem() instanceof ItemWorker && getStackInSlot(i).getMetadata()==meta){
                return true;
            }
        }
        return false;
    }

    public boolean isItemBlocked(ItemStack stack,int side){
        HashSet<ItemStack> white = new HashSet<>();
        HashSet<ItemStack> black = new HashSet<>();
        for (int i = 4; i < 8; i++) {
            ItemStack stackFilter = getStackInSlot(i);
            boolean w = stackFilter.getMetadata()==1;
            (w?white:black).addAll(ItemFilter.getMarked(stackFilter));
        }
        for (int i = 0; i < 2; i++) {
            ItemStack stackFilter = getStackInSlot(8+i+(side*2));
            boolean w = stackFilter.getMetadata()==1;
            (w?white:black).addAll(ItemFilter.getMarked(stackFilter));
        }

        for (ItemStack itemStack : white) {
            if(ItemStack.areItemsEqual(itemStack,stack)){
                return false;
            }
        }
        for (ItemStack itemStack : black) {
            if(ItemStack.areItemsEqual(itemStack,stack)){
                return true;
            }
        }
        return white.size()>0?true:false;
    }


    public boolean isFluidBlocked(FluidStack stack, int side){
        HashSet<Fluid> white = new HashSet<>();
        HashSet<Fluid> black = new HashSet<>();
        for (int i = 4; i < 8; i++) {
            ItemStack stackFilter = getStackInSlot(i);
            boolean w = stackFilter.getMetadata()==1;
            (w?white:black).addAll(ItemFilter.getMarkedFluid(stackFilter));
        }
        for (int i = 0; i < 2; i++) {
            ItemStack stackFilter = getStackInSlot(8+i+(side*2));
            boolean w = stackFilter.getMetadata()==1;
            (w?white:black).addAll(ItemFilter.getMarkedFluid(stackFilter));
        }

        for (Fluid fluid : white) {
            if(FluidHelper.isFluidEqual(fluid,stack)){
                return false;
            }
        }
        for (Fluid fluid : black) {
            if(FluidHelper.isFluidEqual(fluid,stack)){
                return true;
            }
        }
        return white.size()>0?true:false;
    }

    @Override
    public void update() {
        if(!world.isRemote && world.getTotalWorldTime() % 40 == 0){
            updateNetwork();
        }
    }
    public Node node = new Node(this, UUID.randomUUID());
    public void updateNetwork(){
        for(int i=0;i<6;i++){
            if (isSideConnectedToPipe(EnumFacing.VALUES[i])) {
                TileEntity tile = world.getTileEntity(pos.offset(EnumFacing.VALUES[i]));
                if(tile!=null && !tile.isInvalid() && tile instanceof TilePipeCore && ((TilePipeCore)tile).node.getNetwork().isPresent()){
                    if(node.getNetwork().isPresent()){
                        Network.mergeNetwork(node.getNetwork().get(),((TilePipeCore)tile).node.getNetwork().get());
                    }else{
                        ((TilePipeCore)tile).node.getNetwork().get().addNode(node);
                    }
                }
            }
        }
        if(!this.node.getNetwork().isPresent()){
            Network.createByNode(node);
        }
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        if(compound.hasKey("sides")){
            workingSides = compound.getIntArray("sides");
        }
        if(compound.hasKey("rs")){
            rsmode = compound.getIntArray("rs");
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound.setIntArray("sides",workingSides);
        compound.setIntArray("rs",rsmode);
        return super.writeToNBT(compound);
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound tag = super.getUpdateTag();
        tag.setIntArray("sides",workingSides);
        tag.setIntArray("rs",rsmode);
        return tag;
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tag) {
        super.handleUpdateTag(tag);
        if(tag.hasKey("sides")){
            workingSides = tag.getIntArray("sides");
        }
        if(tag.hasKey("rs")){
            rsmode = tag.getIntArray("rs");
        }
    }


    @Override
    public NBTTagCompound getModePacket() {
        NBTTagCompound tag = super.getModePacket();
        tag.setIntArray("sides",workingSides);
        tag.setIntArray("rs",rsmode);
        return tag;
    }

    @Override
    public void handleModePacket(NBTTagCompound tag) {
        super.handleModePacket(tag);
        if(tag.hasKey("sides")){
            workingSides = tag.getIntArray("sides");
        }
        if(tag.hasKey("rs")){
            rsmode = tag.getIntArray("rs");
        }
    }

    @Override
    public void handleDataUpload(NBTTagCompound tag) {
        if(tag.getInteger("mode")==0){
            EntityPlayer player = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUUID(tag.getUniqueId("player"));
            if(player!=null && !player.isDead){
                player.openGui(RotationCraft.MOD_ID, 114514 + tag.getInteger("side"),world,pos.getX(),pos.getY(),pos.getZ());
            }
            return;
        }
    }

    @Override
    public Collection<NetworkConnection> getConnections() {
        List<NetworkConnection> connections = new ArrayList<>();
        connections.add(new NetworkConnection(node,world,pos,null,node, BlockWorkerPulled.itemBlock.getBlock().getLocalizedName()));
        return connections;
    }

    @Override
    public BlockPos getBlockPos() {
        return getPos();
    }

    @Override
    public Object getGuiClient(EntityPlayer player) {
        return new GuiWorker(this,player.inventory);
    }

    @Override
    public Object getGuiServer(EntityPlayer player) {
        return new ContainerWorker(this,player.inventory);
    }

    @Override
    public void getWailaBody(ItemStack itemStack, List<String> tooltip) {

    }


    public boolean isSideConnectedToPipe(EnumFacing side){
        IBlockState state = world.getBlockState(pos.offset(side,1));
        return (state.getBlock() instanceof BlockPipe);
    }

    @Override
    public Object getGuiClient(EntityPlayer player, int id) {
        return new GuiWorkerSide(this,player.inventory,id - 114514);
    }

    @Override
    public Object getGuiServer(EntityPlayer player, int id) {
        return new ContainerWorkerSide(this,player.inventory,id - 114514);
    }

    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        if(index<4){
            return stack.getItem() instanceof ItemWorker;
        }else{
            return stack.getItem() instanceof ItemFilter;
        }
    }
}
