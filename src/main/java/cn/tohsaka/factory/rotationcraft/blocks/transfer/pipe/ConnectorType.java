package cn.tohsaka.factory.rotationcraft.blocks.transfer.pipe;

public enum ConnectorType {
    NONE(-1),
    NORMAL(0),
    LARGE(1);

    public final int id;
    ConnectorType(int id){
        this.id = id;
    }
}
