package cn.tohsaka.factory.rotationcraft.gui.machine.screen;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerBlender;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.gui.ElementBase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementButton;
import cn.tohsaka.factory.rotationcraft.tiles.transmits.TileBlender;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.ResourceLocation;

import java.util.HashMap;
import java.util.Map;

public class GuiBlender extends GuiMachinebase {
    IInventory playerinventory;
    TileBlender tile;
    public static String tex_path = "textures/gui/blender.png";
    public static String btn_tex = RotationCraft.MOD_ID+":"+tex_path;
    public static ResourceLocation tex = new ResourceLocation(RotationCraft.MOD_ID,tex_path);
    protected int mouseX = 0;
    protected int mouseY = 0;

    int[][] data = new int[][]{{1,1},{3,1},{1,3},{7,1},{1,7},{15,1},{1,15},{31,1},{1,31}};
    public GuiBlender(TileBlender te, InventoryPlayer playerInv) {
        super(new ContainerBlender(playerInv),tex);
        tile = te;
        playerinventory = playerInv;
    }
    public Map<Integer,ElementButton> buttons = new HashMap<>();
    @Override
    public void initGui() {
        super.initGui();
        addElement(new ElementButton(this,7,20,162,17,7,167,7,185,7,185,btn_tex)
                .setText("1:1",getCenteredOffset("1:1")).setid(0));

        addElement(new ElementButton(this,7,38,71,17,177,37,177,55,177,55,btn_tex)
                .setText("3:1 inline",22).setid(1));
        addElement(new ElementButton(this,98,38,71,17,177,37,177,55,177,55,btn_tex)
                .setText("1:3 inline",114).setid(2));

        addElement(new ElementButton(this,7,56,71,17,177,37,177,55,177,55,btn_tex)
                .setText("7:1 inline",22).setid(3));
        addElement(new ElementButton(this,98,56,71,17,177,37,177,55,177,55,btn_tex)
                .setText("1:7 inline",114).setid(4));

        addElement(new ElementButton(this,7,74,71,17,177,37,177,55,177,55,btn_tex)
                .setText("15:1 inline",18).setid(5));
        addElement(new ElementButton(this,98,74,71,17,177,37,177,55,177,55,btn_tex)
                .setText("1:15 inline",111).setid(6));

        addElement(new ElementButton(this,7,92,71,17,177,37,177,55,177,55,btn_tex)
                .setText("31:1 inline",18).setid(7));
        addElement(new ElementButton(this,98,92,71,17,177,37,177,55,177,55,btn_tex)
                .setText("1:31 inline",111).setid(8));

        for(ElementBase element:elements){
            if(element instanceof ElementButton){
                ((ElementButton)element).setGuiManagedClicks(true);
                buttons.put(((ElementButton) element).getid(),(ElementButton) element);
            }
        }


        revalidate();
    }

    @Override
    public void handleIDButtonClick(int id, int mouseButton) {
        tile.setWorkratio(data[id]);
        NetworkDispatcher.uploadingClientSetting(mc.player,tile);
        revalidate();
    }

    public void revalidate(){
        for(ElementBase element:elements){
            if(element instanceof ElementButton){
                int[] d = data[((ElementButton) element).getid()];
                if(d[0] == tile.workratio[0] && d[1] == tile.workratio[1]){
                    element.setEnabled(false);
                }else{
                    element.setEnabled(true);
                }
            }
        }
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int x, int y) {
        boolean u = fontRenderer.getUnicodeFlag();
        fontRenderer.setUnicodeFlag(false);
        String text = StringHelper.localize(String.format("tile.rc.blockblender.%s.name",tile.getMetadata()));
        fontRenderer.drawString(text,getCenteredOffset(text),6,0,false);
        fontRenderer.setUnicodeFlag(u);
    }
}
