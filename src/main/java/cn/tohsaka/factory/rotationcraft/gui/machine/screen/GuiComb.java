package cn.tohsaka.factory.rotationcraft.gui.machine.screen;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerComb;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementFuelTank;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementSlot;
import cn.tohsaka.factory.rotationcraft.tiles.power.TileComb;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

import java.util.Arrays;
import java.util.List;

public class GuiComb extends GuiMachinebase {
    public static String tex_path = "textures/gui/basic_gui.png";
    public static String btn_tex = RotationCraft.MOD_ID+":"+tex_path;
    public static ResourceLocation tex = new ResourceLocation(RotationCraft.MOD_ID,tex_path);

    public TileComb tile;
    private ElementFuelTank fuel;
    @Override
    public void initGui() {
        super.initGui();
        addElement(new ElementSlot(this,79,35));
        fuel = (ElementFuelTank) addElement(new ElementFuelTank(this,51,12,tile.fuelTank));
    }

    public GuiComb(TileComb te, InventoryPlayer playerInv) {
        super(new ContainerComb(te,playerInv), tex);
        NAME = StringHelper.localize("tile.rc.blockcomb.name");
        tile = te;
    }

    @Override
    protected void updateElementInformation() {
        NetworkDispatcher.requireGuiUpdate(tile);
        super.updateElementInformation();
    }

    @Override
    protected void renderHoveredToolTip(int x, int y) {
        super.renderHoveredToolTip(x, y);
        if(x>50 && x<59 && y>11 && y<69){
            List<String> tips = Arrays.asList(
                    StringHelper.localize("info.rc.fueltitle")+":"+StringHelper.localize("item.material60.name"),
                    StringHelper.localize("info.rc.fuelcap")+":"+String.valueOf(tile.fuelTank.getFuel()),
                    String.format(StringHelper.localize("info.rc.fuelcaptime"),tile.fuelTank.getFuel()/20)
            );
            this.drawHoveringText(tips, x, y, fontRenderer);
        }
    }
}
