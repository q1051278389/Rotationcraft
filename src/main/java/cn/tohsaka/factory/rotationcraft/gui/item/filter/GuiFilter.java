package cn.tohsaka.factory.rotationcraft.gui.item.filter;

import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;

public class GuiFilter extends GuiMachinebase {
    public GuiFilter(EntityPlayer player) {
        super(new ContainerFilter(player), new ResourceLocation("minecraft:textures/gui/container/shulker_box.png"));
        NAME = player.getHeldItem(EnumHand.MAIN_HAND).getDisplayName();
    }

    @Override
    public void initGui() {
        super.initGui();
    }
}
