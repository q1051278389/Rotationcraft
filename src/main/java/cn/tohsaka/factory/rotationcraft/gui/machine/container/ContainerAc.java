package cn.tohsaka.factory.rotationcraft.gui.machine.container;

import cn.tohsaka.factory.rotationcraft.prefab.container.ContainerCore;
import cn.tohsaka.factory.rotationcraft.prefab.container.Slots.SlotValidated;
import cn.tohsaka.factory.rotationcraft.tiles.power.TileAc;
import net.minecraft.entity.player.InventoryPlayer;

public class ContainerAc extends ContainerCore {
    public ContainerAc(TileAc te, InventoryPlayer inv){
        super(te,inv);
        addSlotToContainer(new SlotValidated(item -> te.isItemValidForSlot(0,item),te,0,80,34));
    }
}
