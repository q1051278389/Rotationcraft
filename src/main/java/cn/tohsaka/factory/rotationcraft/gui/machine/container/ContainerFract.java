package cn.tohsaka.factory.rotationcraft.gui.machine.container;

import cn.tohsaka.factory.rotationcraft.prefab.container.ContainerCore;
import cn.tohsaka.factory.rotationcraft.prefab.container.Slots.SlotRemoveOnly;
import cn.tohsaka.factory.rotationcraft.prefab.container.Slots.SlotValidated;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileFerm;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileFract;
import net.minecraft.entity.player.InventoryPlayer;

public class ContainerFract extends ContainerCore {

    public ContainerFract(TileFract te, InventoryPlayer inv) {
        super(te, inv);
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 2; j++) {
                int index = i * 2 + j;
                addSlotToContainer(new SlotValidated(item -> te.isItemValidForSlot(index, item), te, index, j * 18 + 26, i * 18 + 18));
            }
        }
        addSlotToContainer(new SlotValidated(item -> te.isItemValidForSlot(6, item), te, 6, 98, 36));
        addSlotToContainer(new SlotValidated(item -> te.isItemValidForSlot(7, item), te, 7, 119, 52));
    }
}
