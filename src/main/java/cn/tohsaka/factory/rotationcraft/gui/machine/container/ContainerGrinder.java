package cn.tohsaka.factory.rotationcraft.gui.machine.container;

import cn.tohsaka.factory.rotationcraft.prefab.container.ContainerCore;
import cn.tohsaka.factory.rotationcraft.prefab.container.Slots.SlotValidated;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileFract;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileGrinder;
import net.minecraft.entity.player.InventoryPlayer;

public class ContainerGrinder extends ContainerCore {

    public ContainerGrinder(TileGrinder te, InventoryPlayer inv) {
        super(te, inv);
        addSlotToContainer(new SlotValidated(item -> te.isItemValidForSlot(0, item), te, 0, 76, 35));
        addSlotToContainer(new SlotValidated(item -> te.isItemValidForSlot(1, item), te, 1, 136, 35));
        addSlotToContainer(new SlotValidated(item -> te.isItemValidForSlot(2, item), te, 2, 35, 60));
    }
}
