package cn.tohsaka.factory.rotationcraft.gui.machine.container;

import cn.tohsaka.factory.rotationcraft.prefab.container.ContainerCore;
import cn.tohsaka.factory.rotationcraft.prefab.container.Slots.SlotValidated;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementSlot;
import cn.tohsaka.factory.rotationcraft.tiles.power.TileComb;
import cn.tohsaka.factory.rotationcraft.tiles.power.TilePfe;
import net.minecraft.entity.player.InventoryPlayer;

public class ContainerPfe extends ContainerCore {

    public ContainerPfe(TilePfe te, InventoryPlayer inv){
        super(te,inv);
        addSlotToContainer(new SlotValidated( item -> te.isItemValidForSlot(0,item),te,0,49,55));
        addSlotToContainer(new SlotValidated( item -> te.isItemValidForSlot(1,item),te,1,69,55));
        addSlotToContainer(new SlotValidated( item -> te.isItemValidForSlot(2,item),te,2,89,55));

    }



}
