package cn.tohsaka.factory.rotationcraft.gui.machine.container;

import cn.tohsaka.factory.rotationcraft.prefab.container.ContainerCore;
import cn.tohsaka.factory.rotationcraft.tiles.power.TileSteamEngine;
import net.minecraft.entity.player.InventoryPlayer;

public class ContainerSteam extends ContainerCore {
    public ContainerSteam(TileSteamEngine te, InventoryPlayer inv){
        super(te,inv);
    }
}

