package cn.tohsaka.factory.rotationcraft.gui.machine.screen;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerCVT;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.*;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.varable.GuiColor;
import cn.tohsaka.factory.rotationcraft.tiles.transmits.TileCVT;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

import java.util.Arrays;
import java.util.List;

public class GuiCVT extends GuiMachinebase {
    public static String tex_path = "textures/gui/basic_gui.png";
    public static String btn_tex = RotationCraft.MOD_ID+":"+tex_path;
    public static ResourceLocation tex = new ResourceLocation(RotationCraft.MOD_ID,tex_path);

    public TileCVT tile;
    private ElementFluidTank tank;
    private ElementTextField tf1;
    private ElementTextField tf2;
    private ElementButtonManaged save;
    private ElementButtonOption mode1;
    private ElementButtonOption mode2;
    private static List<Character> allowedChar = Arrays.asList('0','1','2','3','4','5','6','7','8','9');
    @Override
    public void initGui() {
        super.initGui();
        tank = (ElementFluidTank) addElement(new ElementFluidTank(this,152,15,tile.lubricantTank).setLarge().drawTank(true));
        addElement(new ElementSlot(this,8,59));
        tf1 = (ElementTextField) addElement(new ElementTextFieldNumber(this,68,21,30,12,2,String.valueOf(tile.getRatio(0))).setValueRange(0,32).setBackgroundColor(new GuiColor(0, 0, 0).getColor(),0,new GuiColor(115, 115, 115).getColor()));
        tf2 = (ElementTextField) addElement(new ElementTextFieldNumber(this,68,36,30,12,2,String.valueOf(tile.getRatio(1))).setValueRange(0,32).setBackgroundColor(new GuiColor(0, 0, 0).getColor(),0,new GuiColor(115, 115, 115).getColor()));
        save = (ElementButtonManaged) addElement(new ElementButtonManaged(this, 67, 58, 32, 18, StringHelper.localize("info.cvt.gui.save")) {
            @Override
            public void onClick() {
                try {
                    int off = Integer.parseInt(tf1.getText());
                    int on = Integer.parseInt(tf2.getText());
                    tile.setRsRatio(new int[]{off,on});
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        });

        mode1 = (ElementButtonOption) addElement(new ElementButtonOption(this, 104, 20, 30, 14) {
            @Override
            public void onValueChanged(int value, String label) {
                tile.setMode(0,value);
            }
        }.setValue(0,StringHelper.localize("info.rc.waila.names.gearbox.modeA"))
                .setValue(1,StringHelper.localize("info.rc.waila.names.gearbox.modeB")).setSelectedIndex2(tile.getMode(0)));

        mode2 = (ElementButtonOption) addElement(new ElementButtonOption(this, 104, 35, 30, 14) {
            @Override
            public void onValueChanged(int value, String label) {
                tile.setMode(1,value);
            }
        }.setValue(0,StringHelper.localize("info.rc.waila.names.gearbox.modeA"))
                .setValue(1,StringHelper.localize("info.rc.waila.names.gearbox.modeB")).setSelectedIndex2(tile.getMode(1)));

    }

    public GuiCVT(TileCVT te, InventoryPlayer playerInv) {
        super(new ContainerCVT(te,playerInv), tex);
        NAME = StringHelper.localize("tile.rc.cvt.name");
        tile = te;
        showtab = false;
    }

    @Override
    protected void updateElementInformation() {
        super.updateElementInformation();
        boolean disable = tile.getStackInSlot(0).isEmpty();
        save.setEnabled(!disable);
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int x, int y) {
        super.drawGuiContainerForegroundLayer(x, y);
        fontRenderer.drawString(StringHelper.localize("info.cvt.gui.rs_off.tips"), 8, 23, 0x404040);
        fontRenderer.drawString(StringHelper.localize("info.cvt.gui.rs_on.tips"), 8, 38, 0x404040);
    }
}
