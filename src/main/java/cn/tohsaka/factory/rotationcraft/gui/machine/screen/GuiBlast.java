package cn.tohsaka.factory.rotationcraft.gui.machine.screen;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerBlast;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.gui.ElementTex;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementDualScaled;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementDualScaled2;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileBlast;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import cn.tohsaka.factory.rotationcraft.utils.TempetureUtils;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

import java.util.Arrays;

public class GuiBlast extends GuiMachinebase {
    public static String tex_path = "textures/gui/blast.png";
    public static String btn_tex = RotationCraft.MOD_ID+":"+tex_path;
    public static ResourceLocation tex = new ResourceLocation(RotationCraft.MOD_ID,tex_path);

    public TileBlast tile;
    private ElementDualScaled temp;
    private ElementDualScaled2 progress;
    @Override
    public void initGui() {
        super.initGui();
        temp = (ElementDualScaled) addElement(new ElementDualScaled(this,11,15).setMode(0).setBackground(false).setSize(11,56).setTexture(ElementTex.TEMP,11,56));
        temp.setQuantity(0);
        progress = (ElementDualScaled2) addElement(new ElementDualScaled2(this,119,35,176,14).setMode(1).setBackground(false).setSize(24,16).setTexture(btn_tex,256,256));
        progress.setQuantity(0);

    }

    public GuiBlast(TileBlast te, InventoryPlayer playerInv) {
        super(new ContainerBlast(te,playerInv), tex);
        tile = te;
        NAME = StringHelper.localize("tile.rc.blockblast.name");
    }

    @Override
    protected void updateElementInformation() {
        NetworkDispatcher.requireGuiUpdate(tile);
        super.updateElementInformation();
        temp.setQuantity(TempetureUtils.getMeterQuantity(tile.getTemp()));
        if(tile.isActive() && tile.processMax>0){
            progress.setQuantity(tile.getScaledProgress(24));
        }
    }

    @Override
    protected void renderHoveredToolTip(int x, int y) {
        super.renderHoveredToolTip(x, y);
        if(x>12 && x<22 && y>14 && y<71){
            this.drawHoveringText(Arrays.asList(String.format("%.1f",tile.getTemp())+" "+ StringHelper.localize("info.rc.tempchar")), x, y, fontRenderer);
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int x, int y) {
        super.drawGuiContainerBackgroundLayer(partialTicks, x, y);
    }
}
