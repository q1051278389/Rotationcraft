package cn.tohsaka.factory.rotationcraft.gui.machine.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;

public class ContainerCorner extends Container {
    public ContainerCorner(InventoryPlayer inv){
        bindPlayerInventory(inv);
    }

    @Override
    public boolean canInteractWith(EntityPlayer playerIn) {
        return true;
    }


    protected int xOffset = 8;
    protected int yOffset = 84;
    protected void bindPlayerInventory(InventoryPlayer inventoryPlayer) {

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 9; j++) {
                addSlotToContainer(new Slot(inventoryPlayer, j + i * 9 + 9, xOffset + j * 18, yOffset + i * 18));
            }
        }
        for (int i = 0; i < 9; i++) {
            addSlotToContainer(new Slot(inventoryPlayer, i, xOffset + i * 18, yOffset + 58));
        }
    }
}
