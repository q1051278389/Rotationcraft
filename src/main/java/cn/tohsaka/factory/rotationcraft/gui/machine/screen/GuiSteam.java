package cn.tohsaka.factory.rotationcraft.gui.machine.screen;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerSteam;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.gui.ElementBase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.ElementTex;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementDualScaled;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementFuelTank;
import cn.tohsaka.factory.rotationcraft.tiles.power.TileSteamEngine;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import cn.tohsaka.factory.rotationcraft.utils.TempetureUtils;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;

import java.util.Arrays;

public class GuiSteam extends GuiMachinebase {
    public static String tex_path = "textures/gui/steam.png";
    public static String btn_tex = RotationCraft.MOD_ID+":"+tex_path;
    public static ResourceLocation tex = new ResourceLocation(RotationCraft.MOD_ID,tex_path);

    public TileSteamEngine tile;
    private ElementDualScaled temp;
    private ElementDualScaled progress;
    private ElementFuelTank tank1;
    @Override
    public void initGui() {
        super.initGui();
        temp = (ElementDualScaled) addElement(new ElementDualScaled(this,118,18).setMode(0).setBackground(false).setSize(11,56).setTexture(ElementTex.TEMP,11,56));
        temp.setQuantity(0);

        tank1 = (ElementFuelTank) addElement(new ElementFuelTank(this,48,18, tile.tank).setType(3).setSize(16,16));
    }

    public GuiSteam(TileSteamEngine te, InventoryPlayer playerInv) {
        super(new ContainerSteam(te,playerInv), tex);
        tile = te;
    }

    @Override
    protected void updateElementInformation() {
        NetworkDispatcher.requireGuiUpdate(tile);
        super.updateElementInformation();
        temp.setQuantity(TempetureUtils.getMeterQuantity(tile.getTemp()));
    }

    @Override
    protected void renderHoveredToolTip(int x, int y) {
        ElementBase e = getElementAtPosition(x,y);
        if(e == temp){
            this.drawHoveringText(Arrays.asList(String.format("%.1f",tile.getTemp())+" "+ StringHelper.localize("info.rc.tempchar")), x, y, fontRenderer);

        }
        if(x >=48 && x<56 && y >=18 && y<75){
            NonNullList tips = NonNullList.create();
            tank1.addTooltip(tips);
            this.drawHoveringText(tips, x, y, fontRenderer);
        }
        super.renderHoveredToolTip(x, y);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int x, int y) {
        super.drawGuiContainerBackgroundLayer(partialTicks, x, y);
    }
}
