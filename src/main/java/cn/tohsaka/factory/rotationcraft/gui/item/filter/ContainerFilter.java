package cn.tohsaka.factory.rotationcraft.gui.item.filter;

import cn.tohsaka.factory.rotationcraft.items.ItemFilter;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;

public class ContainerFilter extends Container {
    public ItemFilter.FilterInv inventory;
    public ContainerFilter(EntityPlayer player){
        ItemStack stack = player.getHeldItem(EnumHand.MAIN_HAND);
        if(stack.getItem() instanceof ItemFilter){
            inventory = ((ItemFilter)stack.getItem()).createNewContainer(stack);

            for (int k = 0; k < 3; ++k)
            {
                for (int l = 0; l < 9; ++l)
                {
                    this.addSlotToContainer(new Slot(inventory, l + k * 9, 8 + l * 18, 18 + k * 18));
                }
            }

            for (int i1 = 0; i1 < 3; ++i1)
            {
                for (int k1 = 0; k1 < 9; ++k1)
                {
                    this.addSlotToContainer(new Slot(player.inventory, k1 + i1 * 9 + 9, 8 + k1 * 18, 84 + i1 * 18));
                }
            }

            for (int j1 = 0; j1 < 9; ++j1)
            {
                this.addSlotToContainer(new Slot(player.inventory, j1, 8 + j1 * 18, 142));
            }

        }
    }
    @Override
    public boolean canInteractWith(EntityPlayer playerIn) {
        return true;
    }

    @Override
    public ItemStack slotClick(int slotId, int dragType, ClickType clickTypeIn, EntityPlayer player) {
        if(slotId>=inventory.getSizeInventory()){
            return super.slotClick(slotId,dragType,clickTypeIn,player);
        }
        this.inventory.setInventorySlotContents(slotId,player.inventory.getItemStack());
        return player.inventory.getItemStack();
    }

    @Override
    public void onContainerClosed(EntityPlayer playerIn) {
        super.onContainerClosed(playerIn);
        if(playerIn.isServerWorld()){
            inventory.closeInventory(playerIn);
        }
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
        return ItemStack.EMPTY;
    }
}