package cn.tohsaka.factory.rotationcraft.gui.machine.screen;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerBedReactor;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.gui.ElementBase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementDualScaled2;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementFuelTank;
import cn.tohsaka.factory.rotationcraft.tiles.reactor.TileBedCore;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;

public class GuiBedReactor extends GuiMachinebase {
    public static String tex_path = "textures/gui/bed.png";
    public static String btn_tex = RotationCraft.MOD_ID+":"+tex_path;
    public static ResourceLocation tex = new ResourceLocation(RotationCraft.MOD_ID,tex_path);

    public TileBedCore tile;
    public ElementFuelTank fuel;
    public ElementFuelTank garbage;
    public ElementFuelTank co2;
    public ElementFuelTank hotco2;
    public ElementDualScaled2 temp;
    @Override
    public void initGui() {
        super.initGui();
        fuel = (ElementFuelTank) addElement(new ElementFuelTank(this,23,19,tile.fuelTank).setType(10).setIdleType(11));
        garbage = (ElementFuelTank) addElement(new ElementFuelTank(this,31,19,tile.garbage).setType(7).setIdleType(11));


        co2 = (ElementFuelTank) addElement(new ElementFuelTank(this,135,19,tile.co2tank).setType(8).setIdleType(11));
        hotco2 = (ElementFuelTank) addElement(new ElementFuelTank(this,143,19,tile.hotco2tank).setType(9).setIdleType(11));


        temp = (ElementDualScaled2) addElement(new ElementDualScaled2(this,10,19,176,0).setMode(2).setBackground(false).setSize(11,56).setTexture(GuiPulse.btn_tex,256,256));
        temp.setQuantity(0);
    }

    public GuiBedReactor(TileBedCore te, InventoryPlayer playerInv) {
        super(new ContainerBedReactor(te,playerInv), tex);
        NAME = StringHelper.localize("tile.rc.bedreactor.name");
        tile = te;
        showtab = false;
    }

    @Override
    protected void updateElementInformation() {
        NetworkDispatcher.requireGuiUpdate(tile);
        temp.setQuantity((int) Math.max((Math.min(tile.temperture,600F) / 600F) * 50,14));

    }

    @Override
    protected void renderHoveredToolTip(int x, int y) {
        drawString(fontRenderer,
                StringHelper.localize("info.rc.bedreactor.tips.line0.title")
                        +
                        (tile.isActive()?StringHelper.localize("info.rc.bedreactor.tips.line0.value1"):StringHelper.localize("info.rc.bedreactor.tips.line0.value0"))
                ,42,22,0x999999);
        drawString(fontRenderer,StringHelper.localize("info.rc.bedreactor.tips.line1.title") + " "+ String.format("%.1f",tile.coretemp)+"'C",42,32,0x999999);
        drawString(fontRenderer,StringHelper.localize("info.rc.bedreactor.tips.line2.title") + " "+ String.format("%.1f",tile.effective),42,42,0x999999);
        drawString(fontRenderer,StringHelper.localize("info.rc.bedreactor.tips.line3.title") + " "+ String.format("%.1f",tile.radsize * 1.5f)+"'C",42,52,0x999999);
        ElementBase e = getElementAtPosition(x,y);


        if (e!=null){
            if (e == temp) {
                tooltip.add(String.format("%.1f", tile.coretemp) + " " + StringHelper.localize("info.rc.tempchar"));
            }

            if(e == fuel){
                NonNullList tips = NonNullList.create();
                fuel.addTooltip(tips);
                this.drawHoveringText(tips, x, y, fontRenderer);
            }

            if(e == garbage){
                NonNullList tips = NonNullList.create();
                garbage.addTooltip(tips);
                this.drawHoveringText(tips, x, y, fontRenderer);
            }
        }
        super.renderHoveredToolTip(x,y);
    }
}
