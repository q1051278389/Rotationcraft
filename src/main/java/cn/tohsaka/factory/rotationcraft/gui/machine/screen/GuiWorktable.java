package cn.tohsaka.factory.rotationcraft.gui.machine.screen;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerWorktable;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.tiles.base.TileWorktable;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

public class GuiWorktable extends GuiMachinebase {
    public static String tex_path = "textures/gui/worktable.png";
    public static String btn_tex = RotationCraft.MOD_ID+":"+tex_path;
    public static ResourceLocation tex = new ResourceLocation(RotationCraft.MOD_ID,tex_path);

    public TileWorktable tile;
    @Override
    public void initGui() {
        super.initGui();
        /*addElement(new ElementButtonManaged(this,124,64,27, 18,"save") {
            @Override
            public void onClick() {
                System.out.println("test");




                System.out.print("patternList.add(new WorktableRecipe(new String[]{");
                ItemStack[] stacks = tile.getCraftMatrixStack();
                for(int i=0;i<9;i++){
                    if(stacks[i].isEmpty()){
                        System.out.print("\"NULL\"");
                    }else {
                        String regname = stacks[i].getItem().getRegistryName().toString();
                        System.out.print(String.format("\"%s,%d,%d\"",regname,stacks[i].getCount(),stacks[i].getMetadata()));
                    }
                    if(i<8){
                        System.out.print(",");
                    }
                }

                System.out.print("},");

                if(tile.getStackInSlot(27).getItem() instanceof ItemMaterial){
                    System.out.print(String.format("ItemMaterial.getStackById(%d,%d)",((ItemMaterial)tile.getStackInSlot(27).getItem()).meta,tile.getStackInSlot(27).getCount()));
                }else{
                    String regname = tile.getStackInSlot(27).getItem().getRegistryName().toString();
                    System.out.print(String.format("GameRegistry.makeItemStack(\"%s\",%d,%d,null)",regname,tile.getStackInSlot(27).getMetadata(),tile.getStackInSlot(27).getCount()));
                }

                System.out.print("));\n");
                NetworkDispatcher.uploadingClientSetting(mc.player,tile);
            }
        });*/
    }

    public GuiWorktable(TileWorktable te, InventoryPlayer playerInv) {
        super(new ContainerWorktable(te,playerInv), tex);
        NAME = StringHelper.localize("tile.rc.worktable.name");
        tile = te;
    }

    @Override
    protected void updateElementInformation() {
        NetworkDispatcher.requireGuiUpdate(tile);
        super.updateElementInformation();
    }
}
