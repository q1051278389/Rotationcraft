package cn.tohsaka.factory.rotationcraft.gui.machine.container;

import cn.tohsaka.factory.rotationcraft.prefab.container.ContainerCore;
import cn.tohsaka.factory.rotationcraft.prefab.container.Slots.SlotCrafter;
import cn.tohsaka.factory.rotationcraft.tiles.base.TileWorktable;
import cn.tohsaka.factory.rotationcraft.utils.Utils;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerWorktable extends ContainerCore {
    TileWorktable tile;
    public ContainerWorktable(TileWorktable te, InventoryPlayer playerInventory) {
        super(te, playerInventory);
        tile = te;

        for(int l = 0; l < 3; ++l) {
            for(int i1 = 0; i1 < 3; ++i1) {
                this.addSlotToContainer(new Slot(te, i1 + l * 3, 30 + i1 * 18, 17 + l * 18));
            }
        }

        //addSlotToContainer(new SlotCrafter(te, 9,  124, 35){


        addSlotToContainer(new SlotCrafter(playerInventory.player, te,te,9, 124, 35));
    }

    @Override
    protected boolean supportsShiftClick(EntityPlayer player, int slotIndex) {
        return true;
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slotIndex) {
        if(slotIndex==45){
            int maxcraft = tile.getMaxCraft();
            if(maxcraft>0){
                Utils.distributeOutput(player.inventory,tile.doCraft(player,maxcraft).copy(),0,player.inventory.getSizeInventory(),true);
            }
            return ItemStack.EMPTY;
        }else {
            return super.transferStackInSlot(player, slotIndex);
        }
    }
}
