package cn.tohsaka.factory.rotationcraft.gui.machine.screen;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerAc;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementSlot;
import cn.tohsaka.factory.rotationcraft.tiles.power.TileAc;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

public class GuiAc extends GuiMachinebase {
    public static String tex_path = "textures/gui/basic_gui.png";
    public static String btn_tex = RotationCraft.MOD_ID+":"+tex_path;
    public static ResourceLocation tex = new ResourceLocation(RotationCraft.MOD_ID,tex_path);

    public TileAc tile;

    @Override
    public void initGui() {
        super.initGui();
        addElement(new ElementSlot(this,80,34));
    }

    public GuiAc(TileAc te, InventoryPlayer playerInv) {
        super(new ContainerAc(te,playerInv), tex);
        NAME = StringHelper.localize("tile.rc.blockac.name");
        tile = te;
        showtab = false;
    }
}
