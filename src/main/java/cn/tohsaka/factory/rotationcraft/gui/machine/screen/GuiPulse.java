package cn.tohsaka.factory.rotationcraft.gui.machine.screen;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerPulse;
import cn.tohsaka.factory.rotationcraft.items.ItemMaterial;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.gui.ElementBase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.GuiMachinebase;
import cn.tohsaka.factory.rotationcraft.prefab.gui.elements.ElementDualScaled2;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TilePulse;
import cn.tohsaka.factory.rotationcraft.utils.FluidHelper;
import cn.tohsaka.factory.rotationcraft.utils.RenderHelper2;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.FluidTank;

import java.util.List;

public class GuiPulse extends GuiMachinebase {
    public static String tex_path = "textures/gui/pulsejetgui.png";
    public static String btn_tex = RotationCraft.MOD_ID+":"+tex_path;
    public static ResourceLocation tex = new ResourceLocation(RotationCraft.MOD_ID,tex_path);

    public TilePulse tile;
    private ElementDualScaled2 temp;
    private ElementDualScaled2 progress;
    private ElementDualScaled2 progress2;
    private ElementDualScaled2 tankCata;
    private ElementDualScaled2 tankWater;
    @Override
    public void initGui() {
        super.initGui();
        temp = (ElementDualScaled2) addElement(new ElementDualScaled2(this,20,15,176,0).setMode(2).setBackground(false).setSize(11,56).setTexture(btn_tex,256,256));
        temp.setQuantity(0);
        progress = (ElementDualScaled2) addElement(new ElementDualScaled2(this,114,22,176,56).setMode(2).setBackground(false).setSize(38,40).setTexture(btn_tex,256,256));
        progress.setQuantity(0);

        progress2 = (ElementDualScaled2) addElement(new ElementDualScaled2(this,130,35,214,54).setMode(0).setBackground(false).setSize(6,12).setTexture(btn_tex,256,256));
        progress2.setQuantity(0);


        tankWater = (ElementDualScaled2) addElement(new ElementDualScaled2(this,58,15,198,0).setMode(2).setBackground(false).setSize(7,54).setTexture(btn_tex,256,256));
        tankCata = (ElementDualScaled2) addElement(new ElementDualScaled2(this,90,15,247,0).setMode(2).setBackground(false).setSize(7,54).setTexture(btn_tex,256,256));

    }

    public GuiPulse(TilePulse te, InventoryPlayer playerInv) {
        super(new ContainerPulse(te,playerInv), tex);
        tile = te;
        NAME = "tile.rc.blockpulse.name";
    }

    @Override
    protected void updateElementInformation() {
        NetworkDispatcher.requireGuiUpdate(tile);
        temp.setQuantity((int) Math.max((Math.min(tile.temp,800F) / 800F) * 50,14));

        tankWater.setQuantity(Math.round((((float)tile.waterTank.getFluidAmount()) / ((float)tile.waterTank.getCapacity())) * 54F));
        tankCata.setQuantity(Math.round((((float)tile.cataTank.getFluidAmount()) / ((float)tile.cataTank.getCapacity())) * 54F));

        if(!tile.isActive()){
            progress.setQuantity(0);
            progress2.setQuantity(0);
            return;
        }

        float process = Math.abs(tile.processMax-tile.processRem);
        float max2 = tile.processMax * 0.25F;
        if(process > (tile.processMax*0.75)){
            process -= (tile.processMax*0.75);
            progress.setQuantity(40);
            progress2.setQuantity(Math.round((process / max2)*12));//12
        }else{
            progress.setQuantity((int) Math.round(((float)Math.abs(tile.processMax-tile.processRem))/ ((float)tile.processMax*0.75)*40));
            progress2.setQuantity(0);
        }

    }

    @Override
    public void addTooltips(List<String> tooltip) {
        ElementBase element = getElementAtPosition(mouseX, mouseY);
        if(element != null) {
            if (element.equals(temp)) {
                tooltip.add(String.format("%.1f", tile.getTemp()) + " " + StringHelper.localize("info.rc.tempchar"));
            }
            if (element.equals(tankWater)) {
                getTankTip(tile.waterTank, tooltip);
            }
            if (element.equals(tankCata)) {
                getTankTip(tile.cataTank, tooltip);
            }
        }
        super.addTooltips(tooltip);
    }


    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int x, int y) {
        super.drawGuiContainerBackgroundLayer(partialTicks, x, y);
        if(tile.getWorld().isBlockPowered(tile.getPos())){
            RenderHelper2.enableGUIStandardItemLighting();
            this.itemRender.renderItemAndEffectIntoGUI(new ItemStack(ItemMaterial.materialMap.get(50),1),guiLeft+154,guiTop+6);
        }
    }


    public void getTankTip(FluidTank tank,List<String> list) {

        if (tank.getFluid() != null && tank.getFluidAmount() > 0) {
            list.add(StringHelper.getFluidName(tank.getFluid()));

            if (FluidHelper.isPotionFluid(tank.getFluid())) {
                FluidHelper.addPotionTooltip(tank.getFluid(), list, 1.0F);
            }
        }

        list.add(StringHelper.formatNumber(tank.getFluidAmount()) + " / " + StringHelper.formatNumber(tank.getCapacity()) + " mB");
    }
}
