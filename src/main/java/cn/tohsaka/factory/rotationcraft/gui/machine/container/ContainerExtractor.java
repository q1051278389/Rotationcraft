package cn.tohsaka.factory.rotationcraft.gui.machine.container;

import cn.tohsaka.factory.rotationcraft.prefab.container.ContainerCore;
import cn.tohsaka.factory.rotationcraft.prefab.container.Slots.SlotRemoveOnly;
import cn.tohsaka.factory.rotationcraft.prefab.container.Slots.SlotValidated;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileExtractor;
import net.minecraft.entity.player.InventoryPlayer;

public class ContainerExtractor extends ContainerCore {

    public ContainerExtractor(TileExtractor te, InventoryPlayer inv){
        super(te,inv);
        addSlotToContainer(new SlotValidated( item -> te.isItemValidForSlot(0,item),te,0,26,13));
        addSlotToContainer(new SlotValidated( item -> te.isItemValidForSlot(1,item),te,1,62,13));
        addSlotToContainer(new SlotValidated( item -> te.isItemValidForSlot(2,item),te,2,98,13));
        addSlotToContainer(new SlotValidated( item -> te.isItemValidForSlot(3,item),te,3,134,13));

        addSlotToContainer(new SlotRemoveOnly( te,4,26,55));
        addSlotToContainer(new SlotRemoveOnly( te,5,62,55));
        addSlotToContainer(new SlotRemoveOnly( te,6,98,55));
        addSlotToContainer(new SlotRemoveOnly( te,7,134,55));
        addSlotToContainer(new SlotRemoveOnly( te,8,152,55));


    }



}
