package cn.tohsaka.factory.rotationcraft.gui.machine.container;

import cn.tohsaka.factory.rotationcraft.prefab.container.ContainerCore;
import cn.tohsaka.factory.rotationcraft.prefab.container.Slots.SlotRemoveOnly;
import cn.tohsaka.factory.rotationcraft.prefab.container.Slots.SlotValidated;
import cn.tohsaka.factory.rotationcraft.tiles.reactor.TileBedCore;
import net.minecraft.entity.player.InventoryPlayer;

public class ContainerBedReactor extends ContainerCore {
    public ContainerBedReactor(TileBedCore te, InventoryPlayer inv){
        super(te,inv);
        addSlotToContainer(new SlotValidated(item -> te.isItemValidForSlot(0,item),te,0,152,40));
        addSlotToContainer(new SlotRemoveOnly(te,1,152,58));
    }
}
