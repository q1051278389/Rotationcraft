package cn.tohsaka.factory.rotationcraft.renders;


import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.util.ResourceLocation;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.models.ModelAxls;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileMachineBase;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;

public class SimpleAxlsRender<T extends TileMachineBase> extends TileEntitySpecialRenderer<T> {
    private ModelAxls model;
    private static ResourceLocation texture = new ResourceLocation(RotationCraft.MOD_ID,"textures/render/axls/hsla.png");
    public SimpleAxlsRender(int width,int offset){
        model = new ModelAxls(width,offset);
    }
    @Override
    public void render(T te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
        GlStateManager.pushMatrix();
        GlStateManager.translate(x+0.5, y+0.5, z+0.5);
        RenderUtils.rotate(te.getFacing(), 0, 180, 90, 270);
        bindTexture(texture);
        OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 240, 240);
        model.render(0.0625F, te.angle );
        GlStateManager.popMatrix();
    }
}
