package cn.tohsaka.factory.rotationcraft.renders;


import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.models.ModelAxls;
import cn.tohsaka.factory.rotationcraft.tiles.transmits.TileAxls;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

import java.util.ArrayList;

public class AxlsRender extends TileEntitySpecialRenderer<TileAxls> {
    private ModelAxls model = new ModelAxls();
    private ArrayList<ResourceLocation> textures = new ArrayList<>();
    public AxlsRender(){
        textures.add(new ResourceLocation(RotationCraft.MOD_ID,"textures/render/axls/hsla.png"));
        textures.add(new ResourceLocation(RotationCraft.MOD_ID,"textures/render/axls/diamond.png"));
        textures.add(new ResourceLocation(RotationCraft.MOD_ID,"textures/render/axls/bedrock.png"));
    }
    @Override
    public void render(TileAxls te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
        GlStateManager.pushMatrix();
        GlStateManager.translate(x+0.5, y+0.5, z+0.5);
        if(te.getFacing().ordinal()>1){
            RenderUtils.rotate(te.getFacing(), 0, 180, 90, 270);
        }else{
            if(te.getFacing() == EnumFacing.UP){
                GlStateManager.rotate(-90,1,0,0);
            }else{
                GlStateManager.rotate(90,1,0,0);
            }
        }
        bindTexture(textures.get(te.getMetadata()));
        model.render(0.0625F, te.angle);
        GlStateManager.popMatrix();
        renderMeter(te,x,y,z);
        renderMeter2(te,x,y,z);

    }
    private void renderMeter2(TileAxls te,double x, double y, double z) {
        GlStateManager.pushMatrix();
        OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 255, 255);
        GlStateManager.translate(x+0.5, y+1, z+0.5);
        GlStateManager.rotate(180,0,0,1);
        RenderUtils.rotate(te.getFacing(), -90, 90, 180, 0);
        GlStateManager.translate(-0.370, 0.125, -0.438);
        GlStateManager.scale(0.008F, 0.008F, 0.008F);
        //boolean unicodeflag = getFontRenderer().getUnicodeFlag();
        //getFontRenderer().setUnicodeFlag(false);
        drawInfo(te);
        //getFontRenderer().setUnicodeFlag(unicodeflag);
        GlStateManager.popMatrix();
    }


    private void renderMeter(TileAxls te,double x, double y, double z) {
        GlStateManager.pushMatrix();
        OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 255, 255);
        GlStateManager.translate(x+0.5, y+1, z+0.5);
        GlStateManager.rotate(180,1,0,0);
        RenderUtils.rotate(te.getFacing(), -90, 90, 180, 0);
        GlStateManager.translate(-0.370, 0.125, -0.438);
        GlStateManager.scale(0.008F, 0.008F, 0.008F);
        //GlStateManager.scale(0.1F,0.1F,0.1F);
        //drawString("test12345678_"+te.getFacing().toString());
        //boolean unicodeflag = getFontRenderer().getUnicodeFlag();
        //getFontRenderer().setUnicodeFlag(false);
        drawInfo(te);
        //getFontRenderer().setUnicodeFlag(unicodeflag);
        GlStateManager.popMatrix();
    }
    private void drawInfo(TileAxls te){
        if(te.getType()==1 || (te.metercounter && StringHelper.isAltKeyDown())){
            RotaryPower power = te.powerForwarder.getPower();
            drawString("Power:",6,8);
            drawString(power.getFormattedWatt(),12,20);
            drawString("Torque:",6,32);
            drawString(power.getFormattedNm(),12,44);
            drawString("Speed: ",6,56);
            drawString(power.getFormattedSpeed(),12,68);
            return;
        }
        if(te.getType()==2){
            drawString("Power:",6,8);
            drawString(RotaryPower.formattedPower(te.powercounter), 12,20);
            drawString("Ticks:",6,32);
            drawString(te.ticks +" ticks",12,44);
            drawString("Avg: ",6,56);
            drawString(RotaryPower.formattedPower(te.powercounter/te.ticks)+"/s",12,68);
        }
    }

    private void drawString(String str,int x,int y){
        FontRenderer fontRenderer = getFontRenderer();
        fontRenderer.drawString(str,x,y,0xFFFFFF);
    }
}
