package cn.tohsaka.factory.rotationcraft.renders.ReikaComptableRender;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.turbine.ModelTurbine;
import cn.tohsaka.factory.rotationcraft.tiles.reactor.TileTCU;
import cn.tohsaka.factory.rotationcraft.utils.RenderHelper2;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class RenderTCU extends TileEntitySpecialRenderer<TileTCU> {
    private ModelTurbine[] models = new ModelTurbine[5];

    public RenderTCU() {
        super();
        this.buildModels();
    }

    private void buildModels() {
        for (int i = 0; i < models.length; i++) {
            models[i] = new ModelTurbine(i);
        }
    }

    @Override
    public void render(TileTCU te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
        /*if(te.level<=0){
            return;
        }*/
        renderTileEntityTurbineCoreAt(te,x,y,z);
    }


    public final void renderTileEntityTurbineCoreAt(TileTCU tile,double x, double y, double z)
    {
        if(tile.level<=0 || tile.level>5){
            return;
        }
        RenderHelper2.bindTexture(new ResourceLocation(RotationCraft.MOD_ID,"textures/render/turbine.png"));
        for(int i=0;i<tile.level;i++){
            if (tile.getWorld()!=null) {
                GlStateManager.pushMatrix();
                GlStateManager.enableRescaleNormal();
                GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);

                GlStateManager.translate(x+0.5, y-0.4375, z+0.5);
                RenderUtils.rotate(tile.getFacing(), 180, 0, 270, 90);
                GlStateManager.translate(0, 0, i+1);
                GlStateManager.rotate(180,0,1,0);
                models[i].renderAll(tile, null, tile.angle / 16, 0);

                GL11.glDisable(GL12.GL_RESCALE_NORMAL);
                GL11.glPopMatrix();
                GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            }
        }
    }

    public final void renderTileEntityTurbineCoreAt2(TileTCU tile,double x, double y, double z)
    {
        /*GlStateManager.pushMatrix();
        GlStateManager.enableRescaleNormal();
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        GlStateManager.translate(x, y, z);
        GlStateManager.scale(1.0F, -1.0F, -1.0F);
        GlStateManager.translate(0.5F, 0.5F, 0.5F);
        GlStateManager.translate(0F,-2F,-1F);
        RenderUtils.rotate(tile.getFacing(), 0, 180, 270, 90);
        //RenderUtils.rotate(tile.getFacing(), 90, 270, -90, 90);
        RenderHelper.bindTexture(new ResourceLocation(RotationCraft.MOD_ID,"textures/render/turbine.png"));
        for(int i=1;i<tile.level;i++){
            if (tile.getWorld()!=null) {
                GlStateManager.translate(0,0,i*0.5);
                models[i].renderAll(tile, null, -tile.angle, 0);
            }
        }

        GL11.glDisable(GL12.GL_RESCALE_NORMAL);
        GL11.glPopMatrix();
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);*/
    }
}
