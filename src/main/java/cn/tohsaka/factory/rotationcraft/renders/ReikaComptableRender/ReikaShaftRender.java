package cn.tohsaka.factory.rotationcraft.renders.ReikaComptableRender;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.animated.ModelShaft;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.animated.ModelShaftV;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.helper.ReikaModHelper;
import cn.tohsaka.factory.rotationcraft.tiles.transmits.TileAxls;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

public class ReikaShaftRender extends TileEntitySpecialRenderer<TileAxls> {

    protected ModelShaft ShaftModel = new ModelShaft();
    protected ModelShaftV VShaftModel = new ModelShaftV();

    @Override
    public void render(TileAxls te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
        ReikaModHelper.setupGL(te,x,y,z);
        ReikaModHelper.bindTexture(new ResourceLocation(RotationCraft.MOD_ID,getTexName(te)));

        if(te.getFacing().ordinal()<2){
            if(te.getFacing() == EnumFacing.DOWN){
                GlStateManager.rotate(-180,0,0,1);
                GlStateManager.pushMatrix();
                GlStateManager.translate(0,-2,0);
                VShaftModel.renderShaft(te,-te.angle);
                GlStateManager.popMatrix();
            }else {
                VShaftModel.renderShaft(te, -te.angle);
            }
        }else{
            RenderUtils.rotate(te.getFacing(), 0, 0, 0, 0);
            ShaftModel.renderShaft(te,te.angle);
        }

        ReikaModHelper.closeGL(te,x,y,z);
        renderMeter(te,x,y,z);
        renderMeter2(te,x,y,z);
    }

    public String getTexName(TileAxls te){
        String s = "textures/render/transmission/shaft/";
        if(te.getFacing().ordinal()<2){
            s+="v";
        }
        s+="shafttex";
        switch (te.getMetadata()){
            case 1:
                s+="d";
                break;
            case 2:
                s+="b";
                break;
        }

        return s+".png";
    }






    private void renderMeter2(TileAxls te,double x, double y, double z) {
        GlStateManager.pushMatrix();
        GlStateManager.translate(x+0.5, y+1, z+0.5);
        GlStateManager.rotate(180,0,0,1);
        RenderUtils.rotate(te.getFacing(), -90, 90, 180, 0);
        GlStateManager.translate(-0.370, 0.125, -0.438);
        GlStateManager.scale(0.008F, 0.008F, 0.008F);
        boolean unicodeflag = getFontRenderer().getUnicodeFlag();
        getFontRenderer().setUnicodeFlag(false);
        drawInfo(te);
        getFontRenderer().setUnicodeFlag(unicodeflag);
        GlStateManager.popMatrix();
    }


    private void renderMeter(TileAxls te,double x, double y, double z) {
        GlStateManager.pushMatrix();
        GlStateManager.translate(x+0.5, y+1, z+0.5);
        GlStateManager.rotate(180,1,0,0);
        RenderUtils.rotate(te.getFacing(), -90, 90, 180, 0);
        GlStateManager.translate(-0.370, 0.125, -0.438);
        GlStateManager.scale(0.008F, 0.008F, 0.008F);
        //GlStateManager.scale(0.1F,0.1F,0.1F);
        //drawString("test12345678_"+te.getFacing().toString());
        boolean unicodeflag = getFontRenderer().getUnicodeFlag();
        getFontRenderer().setUnicodeFlag(false);
        drawInfo(te);
        getFontRenderer().setUnicodeFlag(unicodeflag);
        GlStateManager.popMatrix();
    }
    private void drawInfo(TileAxls te){
        OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 240, 240);
        if(te.getType()==1){
            if(!te.metercounter || StringHelper.isAltKeyDown()){
                RotaryPower power = te.powerForwarder.getPower();
                drawString("Power:",6,8);
                drawString(power.getFormattedWatt(),12,20);
                drawString("Torque:",6,32);
                drawString(power.getFormattedNm(),12,44);
                drawString("Speed: ",6,56);
                drawString(power.getFormattedSpeed(),12,68);
                return;
            }
            if(te.metercounter){
                drawString("Power:",6,8);
                drawString(RotaryPower.formattedPower(te.powercounter), 12,20);
                drawString("Ticks:",6,32);
                drawString(te.ticks +" ticks",12,44);
                drawString("Avg: ",6,56);
                drawString(RotaryPower.formattedPower(te.powercounter/te.ticks)+"/s",12,68);
            }
        }
    }

    private void drawString(String str,int x,int y){
        FontRenderer fontRenderer = getFontRenderer();
        fontRenderer.drawString(str,x,y,0xFFFFFF);
    }
}
