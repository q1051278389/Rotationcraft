package cn.tohsaka.factory.rotationcraft.renders;


import cn.tohsaka.factory.rotationcraft.prefab.render.GearboxModel;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.util.ResourceLocation;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.models.ModelGearBox2x;
import cn.tohsaka.factory.rotationcraft.tiles.transmits.TileGearbox;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;

import java.util.ArrayList;

public class GearboxRender extends TileEntitySpecialRenderer<TileGearbox>{
    private ModelGearBox2x model2x = new ModelGearBox2x();
    private GearboxModel model4816 = new GearboxModel();
    //private ModelGearBox4x model8x = new ModelGearBox4x();
    //private ModelGearBox4x model16x = new ModelGearBox4x();
    //private ModelGearBox256x model256x = new ModelGearBox256x();
    private ArrayList<ResourceLocation> textures = new ArrayList<>();
    public GearboxRender(){
        textures.add(new ResourceLocation(RotationCraft.MOD_ID,"textures/render/axls/hsla.png"));
        textures.add(new ResourceLocation(RotationCraft.MOD_ID,"textures/render/axls/diamond_model.png"));
        textures.add(new ResourceLocation(RotationCraft.MOD_ID,"textures/render/axls/bedrock_model.png"));
    }
    @Override
    public void render(TileGearbox te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
        GlStateManager.pushMatrix();
        GlStateManager.translate(x+0.5, y+0.5, z+0.5);
        RenderUtils.rotate(te.getFacing(), 0, 180, 90, 270);
        //bindTexture(textures.get(te.getMetadata()));
        OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 240, 240);
        bindTexture(textures.get(te.getMetadata()));
        switch (te.gearratio){
            case 2:
                model2x.render(0.0625F, te );
                break;
            case 256:
                model2x.render(0.0625F, te );
                break;
            default:
                model4816.render(0.0625F,te);
                break;
        }
        GlStateManager.popMatrix();
    }
}
