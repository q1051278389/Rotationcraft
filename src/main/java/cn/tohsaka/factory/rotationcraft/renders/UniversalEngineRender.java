package cn.tohsaka.factory.rotationcraft.renders;

import cn.tohsaka.factory.rotationcraft.prefab.tile.TileEngine;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;

public class UniversalEngineRender extends TileEntitySpecialRenderer<TileEngine> {
    @Override
    public void render(TileEngine te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
        te.renderTileEntityAt(x,y,z);
    }
}
