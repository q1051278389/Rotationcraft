package cn.tohsaka.factory.rotationcraft.renders;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.models.ModelAxls;
import cn.tohsaka.factory.rotationcraft.models.ModelCornerAxls;
import cn.tohsaka.factory.rotationcraft.tiles.transmits.TileCorner;
import cn.tohsaka.factory.rotationcraft.tiles.transmits.TileGearbox;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

public class CornerRender extends TileEntitySpecialRenderer<TileCorner> {
    public static ResourceLocation texture = new ResourceLocation(RotationCraft.MOD_ID,"textures/render/axls/hsla.png");
    private ModelCornerAxls inputAxls;
    private ModelCornerAxls outputAxls;
    public CornerRender(){
        inputAxls = new ModelCornerAxls(8,0,true);
        outputAxls = new ModelCornerAxls(8,0,false);
    }

    @Override
    public void render(TileCorner te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
        GlStateManager.pushMatrix();
        GlStateManager.translate(x+0.5, y+0.5, z+0.5);
        bindTexture(texture);
        //OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 240, 240);

        GlStateManager.pushMatrix();
        if(te.getFacing().ordinal()>1){
            RenderUtils.rotate(te.getFacing(), 0, 180, 90, 270);
        }else{
            if(te.getFacing() == EnumFacing.UP){
                GlStateManager.rotate(90,1,0,0);
            }else{
                GlStateManager.rotate(-90,1,0,0);
            }
        }
        outputAxls.render(0.0625F, -te.angle,this.rendererDispatcher.renderEngine);
        GlStateManager.popMatrix();
        if(te.getFacingIn().ordinal()>1){
            RenderUtils.rotate(te.getFacingIn(), 0, 180, 90, 270);
        }else{
            if(te.getFacingIn() == EnumFacing.UP){
                GlStateManager.rotate(90,1,0,0);
            }else{
                GlStateManager.rotate(-90,1,0,0);
            }
        }
        inputAxls.render(0.0625F,te.angle,this.rendererDispatcher.renderEngine);
        GlStateManager.popMatrix();
    }

    public void bind(ResourceLocation location) {
        super.bindTexture(location);
    }
}
