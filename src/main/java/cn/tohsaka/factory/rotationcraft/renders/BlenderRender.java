package cn.tohsaka.factory.rotationcraft.renders;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.models.ModelCornerAxls;
import cn.tohsaka.factory.rotationcraft.tiles.transmits.TileBlender;
import cn.tohsaka.factory.rotationcraft.utils.FacingTool;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.util.ResourceLocation;

public class BlenderRender extends TileEntitySpecialRenderer<TileBlender> {
    public static ResourceLocation texture = new ResourceLocation(RotationCraft.MOD_ID,"textures/render/axls/hsla.png");
    public static ResourceLocation texture2 = new ResourceLocation(RotationCraft.MOD_ID,"textures/render/axls/bedrock_model.png");
    private ModelCornerAxls inputAxls;
    private ModelCornerAxls outputAxls;
    private ModelCornerAxls subAxls;
    public BlenderRender(){
        inputAxls = new ModelCornerAxls(8,0,false);
        outputAxls = new ModelCornerAxls(8,0,false);
        subAxls = new ModelCornerAxls(8,0,true);
    }

    @Override
    public void render(TileBlender te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
        GlStateManager.pushMatrix();
        GlStateManager.translate(x+0.5, y+0.5, z+0.5);
        bindTexture(te.getMetadata()==0?texture:texture2);
        //OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 240, 240);

        GlStateManager.pushMatrix();
        RenderUtils.rotate(te.getFacing(), 180, 0, 270, 90);
        outputAxls.render(0.0625F, -te.angle1,this.rendererDispatcher.renderEngine);

        if(te.subaxlsposition == FacingTool.Position.LEFT){
            GlStateManager.rotate(90,0,1,0);
        }else{
            GlStateManager.rotate(-90,0,1,0);
        }
        subAxls.render(0.0625F, te.angle2,this.rendererDispatcher.renderEngine);
        GlStateManager.popMatrix();

        bindTexture(te.getMetadata()==0?texture:texture2);
        RenderUtils.rotate(te.getFacing(), 0, 180, 90, 270);
        inputAxls.render(0.0625F,te.angle0,this.rendererDispatcher.renderEngine);
        GlStateManager.popMatrix();
    }
}
