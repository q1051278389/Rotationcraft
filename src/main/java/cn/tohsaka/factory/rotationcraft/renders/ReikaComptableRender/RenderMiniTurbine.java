/*******************************************************************************
 * @author Reika Kalseki
 * 
 * Copyright 2017
 * 
 * All rights reserved.
 * Distribution of the software in any form is only allowed with
 * explicit, prior permission from the owner.
 ******************************************************************************/
package cn.tohsaka.factory.rotationcraft.renders.ReikaComptableRender;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.etc.reikamodel.turbine.ModelTurbine;
import cn.tohsaka.factory.rotationcraft.tiles.reactor.TileMiniTurbine;
import cn.tohsaka.factory.rotationcraft.utils.RenderHelper2;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class RenderMiniTurbine extends TileEntitySpecialRenderer<TileMiniTurbine>
{
	private ModelTurbine[] models = new ModelTurbine[7];

	public RenderMiniTurbine() {
		super();
		this.buildModels();
	}

	private void buildModels() {
		for (int i = 0; i < models.length; i++) {
			models[i] = new ModelTurbine(i);
		}
	}


	@Override
	public void render(TileMiniTurbine te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
		renderTileEntityTurbineCoreAt(te,x,y,z,0);
	}

	public final void renderTileEntityTurbineCoreAt(TileMiniTurbine tile, double x, double y, double z, int par8)
	{
		if(tile.cu().isPresent()){
			TileEntityRendererDispatcher.instance.render(tile.cu().get(), Minecraft.getMinecraft().getRenderPartialTicks(),0);
			return;
		}
		GlStateManager.pushMatrix();
		GlStateManager.enableRescaleNormal();
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		GlStateManager.translate(x, y, z);
		GlStateManager.scale(1.0F, -1.0F, -1.0F);
		GlStateManager.translate(0.5F, 0.5F, 0.5F);
		GlStateManager.translate(0F,-2F,-1F);
		RenderUtils.rotate(tile.getFacing(), 0, 180, 270, 90);
		//RenderUtils.rotate(tile.getFacing(), 90, 270, -90, 90);
		RenderHelper2.bindTexture(new ResourceLocation(RotationCraft.MOD_ID,"textures/render/"+tile.getTexName()+".png"));

		if (tile.getWorld()!=null) {
			models[0].renderAll(tile, null, -tile.angle, 0);
		}



		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		GL11.glPopMatrix();
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
	}

	@Override
	public boolean isGlobalRenderer(TileMiniTurbine te) {
		return true;
	}
}