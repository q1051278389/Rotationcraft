package cn.tohsaka.factory.rotationcraft.renders;


import cn.tohsaka.factory.rotationcraft.tiles.machines.TileFurnace;
import cn.tohsaka.factory.rotationcraft.utils.FluidUtils;
import cn.tohsaka.factory.rotationcraft.utils.RenderHelper2;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import org.lwjgl.opengl.GL11;

import static net.minecraft.client.renderer.texture.TextureMap.LOCATION_BLOCKS_TEXTURE;

public class FurnaceRender extends TileEntitySpecialRenderer<TileFurnace>{

    public FurnaceRender(){

    }
    @Override
    public void render(TileFurnace te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
        GlStateManager.pushMatrix();
        GL11.glEnable(GL11.GL_BLEND);
        GlStateManager.blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GlStateManager.translate(x, y, z);
        bindTexture(LOCATION_BLOCKS_TEXTURE);
        renderFluid();
        GL11.glDisable(GL11.GL_BLEND);
        GlStateManager.popMatrix();
    }

    private void renderFluid()
    {
        float scale = 0.0625F * 14F;
        Fluid fluid = FluidRegistry.LAVA;

        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder renderer = tessellator.getBuffer();
        TextureAtlasSprite sprite = RenderHelper2.getTexture(fluid.getStill());
        if(sprite == null) {
            return;
        }
        RenderHelper2.disableStandardItemLighting();


        renderer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX_COLOR);

        float u1 = sprite.getMinU();
        float v1 = sprite.getMinV();
        float u2 = sprite.getMaxU();
        float v2 = sprite.getMaxV();

        float margin = 1f;
        float offset = 0.0625f;

        int color = fluid.getColor();

        float r = FluidUtils.getRed(color);
        float g = FluidUtils.getGreen(color);
        float b = FluidUtils.getBlue(color);
        float a = 0.9F;
        int TANK_THICKNESS = 0;
        // Top
        renderer.pos(offset, scale, offset).tex(u1, v1).color(r, g, b, a).endVertex();
        renderer.pos(offset, scale, 0.9375).tex(u1, v2).color(r, g, b, a).endVertex();
        renderer.pos(0.9375, scale, 0.9375).tex(u2, v2).color(r, g, b, a).endVertex();
        renderer.pos(0.9375, scale, offset).tex(u2, v1).color(r, g, b, a).endVertex();
        tessellator.draw();

        RenderHelper2.enableStandardItemLighting();

    }
}
