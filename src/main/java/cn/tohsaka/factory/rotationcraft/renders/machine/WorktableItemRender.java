package cn.tohsaka.factory.rotationcraft.renders.machine;

import cn.tohsaka.factory.rotationcraft.tiles.base.TileWorktable;
import cn.tohsaka.factory.rotationcraft.utils.RenderUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;

public class WorktableItemRender extends TileEntitySpecialRenderer<TileWorktable> {
    @Override
    public void render(TileWorktable te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
        super.render(te, x, y, z, partialTicks, destroyStage, alpha);
        ItemRenderer itemRenderer = Minecraft.getMinecraft().getItemRenderer();
        RenderItem renderItem = Minecraft.getMinecraft().getRenderItem();
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                ItemStack stack = te.getStackInSlot(i*3+j);
                if(stack!=null && !stack.isEmpty()){
                    IBakedModel ibakedmodel = renderItem.getItemModelWithOverrides(stack, te.getWorld(), null);
                    if(ibakedmodel == null){
                        continue;
                    }
                    EntityItem entItem = new EntityItem(te.getWorld(), 0D, 0D, 0D, stack);
                    entItem.hoverStart = 0.0F;
                    GlStateManager.pushMatrix();
                    GlStateManager.translate(x+0.5, y+1, z+0.5);
                    RenderUtils.rotate(te.getFacing(), 180, 0, 270, 90);
                    GlStateManager.rotate(-90F, 1F, 0F, 0F);
                    GlStateManager.translate(-0.5, 0.5, 0);
                    GlStateManager.translate(j*0.1875,-(i*0.1875),0);
                    {
                        if(ibakedmodel.isGui3d()){
                            GlStateManager.translate(0.3125, -0.3125, 0.625);
                            GlStateManager.rotate(90F, 1F, 0F, 0F);
                            GlStateManager.translate(0, -0.5625, 0);


                            GlStateManager.scale(0.125,0.125,0.125);
                        }else{
                            GlStateManager.translate(0.3125, -0.3125, 0.004);
                            GlStateManager.scale(0.125,0.125,0.125);
                        }
                    }
                    //GlStateManager.disableLighting();
                    //RenderHelper.disableStandardItemLighting();
                    itemRenderer.renderItem(Minecraft.getMinecraft().player,stack, ItemCameraTransforms.TransformType.NONE);

                    GlStateManager.popMatrix();
                }
            }
        }
    }
}
