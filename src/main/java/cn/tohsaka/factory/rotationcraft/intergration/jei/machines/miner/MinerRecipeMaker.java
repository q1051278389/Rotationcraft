package cn.tohsaka.factory.rotationcraft.intergration.jei.machines.miner;
import cn.tohsaka.factory.rotationcraft.crafting.scripts.GlobalOre;
import mezz.jei.api.recipe.IStackHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class MinerRecipeMaker {
    private MinerRecipeMaker() {
    }

    public static List<MinerRecipeWrapper> getRecipes(IStackHelper stackHelper) {
        List<MinerRecipeWrapper> recipes = new ArrayList();
        for(Ingredient s : GlobalOre.oreWeights.keySet()) {
            if(s!=null && s.test(ItemStack.EMPTY)){
                addWrapperToList(stackHelper,new GlobalOre.StackRandomItem(GlobalOre.oreWeights.get(s), Ingredient.fromStacks(ItemStack.EMPTY)),recipes);
            }else{
                addWrapperToList(stackHelper,new GlobalOre.StackRandomItem(GlobalOre.oreWeights.get(s), s),recipes);
            }
        }
        recipes.sort(Comparator.comparingInt(value -> value.getRecipe().itemWeight));
        return recipes;
    }

    private static void addWrapperToList(IStackHelper stackHelper, GlobalOre.StackRandomItem recipe, List<MinerRecipeWrapper> recipes) {
        recipes.add(new MinerRecipeWrapper(recipe));
    }
}
