package cn.tohsaka.factory.rotationcraft.intergration.crafttweaker.handlers;

import cn.tohsaka.factory.rotationcraft.crafting.PulseRecipe;
import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.crafting.scripts.PulseRecipes;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TilePulse;
import crafttweaker.annotations.ModOnly;
import crafttweaker.annotations.ZenRegister;
import crafttweaker.api.item.IItemStack;
import net.minecraft.item.crafting.Ingredient;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

import static cn.tohsaka.factory.rotationcraft.intergration.crafttweaker.InputHelper.toStack;

@ZenClass("mods.rotationcraft.pulse")
@ModOnly("rc")
@ZenRegister
public class Pulse {
    @ZenMethod
    public static void addRecipe(IItemStack output,IItemStack input,int fuel,int time) {
        PulseRecipe recipe = new PulseRecipe(Ingredient.fromStacks(toStack(input)),toStack(output),fuel,time);
        Recipes.INSTANCE.addPattern(TilePulse.class,recipe);
    }

    @ZenMethod
    public static void delRecipe(IItemStack output) {
        PulseRecipes.stacksToRemove.add(toStack(output));
    }
}
