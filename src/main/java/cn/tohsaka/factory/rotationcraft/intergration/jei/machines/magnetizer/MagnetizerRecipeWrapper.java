//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.tohsaka.factory.rotationcraft.intergration.jei.machines.magnetizer;

import cn.tohsaka.factory.rotationcraft.crafting.MagnetizerRecipe;
import cn.tohsaka.factory.rotationcraft.intergration.jei.base.RCRecipeWrapper;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.ingredients.VanillaTypes;
import net.minecraft.client.Minecraft;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashSet;

public class MagnetizerRecipeWrapper extends RCRecipeWrapper<MagnetizerRecipe> {
    public static HashSet<MagnetizerRecipeWrapper> recipes = new HashSet<>();
    public MagnetizerRecipeWrapper(MagnetizerRecipe stack) {
        super(stack);
        recipes.add(this);
    }
    public void getIngredients(IIngredients ingredients) {
        ingredients.setInputs(VanillaTypes.ITEM,Arrays.asList(this.getRecipe().igd.getMatchingStacks()));
        ingredients.setOutputs(VanillaTypes.ITEM, Arrays.asList(this.getRecipe().getOutput()));
    }

    @Override
    public void drawInfo(Minecraft minecraft, int recipeWidth, int recipeHeight, int mouseX, int mouseY) {
        {
            DecimalFormat df = new DecimalFormat("#0.00000");
            String str = "2K Power to "+df.format(this.getRecipe().uTvia2K)+"uT";
            minecraft.fontRenderer.drawString(str,(recipeWidth/2)-(minecraft.fontRenderer.getStringWidth(str) / 2),46,0);
        }
    }
}
