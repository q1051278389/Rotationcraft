//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.tohsaka.factory.rotationcraft.intergration.jei.machines.ferm;

import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileFerm;
import mezz.jei.api.recipe.IStackHelper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FermRecipeMaker {
    private FermRecipeMaker() {
    }

    public static List<FermRecipeWrapper> getRecipes(IStackHelper stackHelper) {
        List<FermRecipeWrapper> recipes = new ArrayList();
        Iterator var2 = Recipes.INSTANCE.getList(TileFerm.class).iterator();

        while(var2.hasNext()) {
            CraftingPattern recipe = (CraftingPattern)var2.next();
            addWrapperToList(stackHelper, recipe, recipes);
        }

        return recipes;
    }

    private static void addWrapperToList(IStackHelper stackHelper, CraftingPattern recipe, List<FermRecipeWrapper> recipes) {
        recipes.add(new FermRecipeWrapper(recipe));
    }
}
