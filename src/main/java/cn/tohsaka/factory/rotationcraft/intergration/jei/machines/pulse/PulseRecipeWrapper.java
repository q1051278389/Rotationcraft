//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.tohsaka.factory.rotationcraft.intergration.jei.machines.pulse;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.PulseRecipe;
import cn.tohsaka.factory.rotationcraft.intergration.jei.base.RCRecipeWrapper;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.ingredients.VanillaTypes;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.FluidStack;

import java.util.Arrays;
import java.util.HashSet;

public class PulseRecipeWrapper extends RCRecipeWrapper<CraftingPattern> {
    public static HashSet<PulseRecipeWrapper> recipes = new HashSet<>();
    public PulseRecipeWrapper(CraftingPattern recipe) {
        super(recipe);
        recipes.add(this);
    }
    public void getIngredients(IIngredients ingredients) {
        if(this.getRecipe() instanceof PulseRecipe){
            PulseRecipe recipe = (PulseRecipe) getRecipe();
            /*List<List<ItemStack>> input = new ArrayList<>();
            for(ItemStack item : recipe.iinput.getMatchingStacks()){
                input.add(Arrays.asList(new ItemStack[]{item}));
            }*/
            ingredients.setInput(VanillaTypes.FLUID,new FluidStack(RotationCraft.fluids.get("jetfuel"),recipe.getData().getInteger("fluid")));

            ingredients.setInputs(VanillaTypes.ITEM, Arrays.asList(recipe.iinput.getMatchingStacks()));
            ingredients.setOutputs(VanillaTypes.ITEM, Arrays.asList(new ItemStack[]{this.getRecipe().getOutput()}));
        }
    }
}
