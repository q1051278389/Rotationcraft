//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.tohsaka.factory.rotationcraft.intergration.jei.machines.ferm;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.blocks.machine.BlockFerm;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.GuiFerm;
import cn.tohsaka.factory.rotationcraft.intergration.jei.base.RCRecipeCategory;
import cn.tohsaka.factory.rotationcraft.intergration.jei.base.RCRecipeCategoryUid;
import mezz.jei.api.IGuiHelper;
import mezz.jei.api.gui.*;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.ingredients.VanillaTypes;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.FluidStack;

import java.util.List;

public class FermRecipeCategory extends RCRecipeCategory<FermRecipeWrapper> implements ITooltipCallback<FluidStack> {
    private static final ResourceLocation guiTexture = GuiFerm.tex;
    private final IDrawableAnimated progressBar0;

    public FermRecipeCategory(IGuiHelper guiHelper) {
        super(guiHelper.createDrawable(guiTexture, 4, 4, 168, 72), RotationCraft.blocks.get(BlockFerm.NAME).getUnlocalizedName()+".name");

        IDrawableStatic progressBarDrawable0 = guiHelper.createDrawable(guiTexture, 176,31, 11, 56);
        this.progressBar0 = guiHelper.createAnimatedDrawable(progressBarDrawable0, 40, IDrawableAnimated.StartDirection.BOTTOM, false);
    }

    public String getUid() {
        return RCRecipeCategoryUid.FERM;
    }

    public void drawExtras(Minecraft minecraft) {
        progressBar0.draw(minecraft,19, 11);
    }
    public void setRecipe(IRecipeLayout recipeLayout, FermRecipeWrapper recipeWrapper, IIngredients ingredients) {
        IGuiItemStackGroup guiItemStacks = recipeLayout.getItemStacks();
        guiItemStacks.init(0, true, 50, 12);
        guiItemStacks.init(1, true, 50, 48);
        guiItemStacks.init(3, false, 111, 30);

        if(ingredients.getInputs(VanillaTypes.FLUID)!=null && ingredients.getInputs(VanillaTypes.FLUID).size()>0){
            try {
                IGuiFluidStackGroup guiFluidStacks = recipeLayout.getFluidStacks();
                guiFluidStacks.init(0,true,51,31);
                guiFluidStacks.addTooltipCallback(this::onTooltip);
                guiFluidStacks.set(ingredients);
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }



        guiItemStacks.set(ingredients);
    }

    public List<String> getTooltipStrings(int mouseX, int mouseY) {
        return super.getTooltipStrings(mouseX, mouseY);
    }


    @Override
    public void onTooltip(int slotIndex, boolean input, FluidStack ingredient, List<String> tooltip) {
        tooltip.add(1,ingredient.amount+" mB");
    }
}
