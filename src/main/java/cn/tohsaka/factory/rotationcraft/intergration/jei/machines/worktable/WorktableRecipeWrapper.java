//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.tohsaka.factory.rotationcraft.intergration.jei.machines.worktable;

import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.WorktableRecipe;
import cn.tohsaka.factory.rotationcraft.intergration.jei.base.RCRecipeWrapper;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.ingredients.VanillaTypes;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class WorktableRecipeWrapper extends RCRecipeWrapper<CraftingPattern> {
    public static HashSet<WorktableRecipeWrapper> recipes = new HashSet<>();
    public WorktableRecipeWrapper(WorktableRecipe recipe) {
        super(recipe);
        recipes.add(this);
    }
    public void getIngredients(IIngredients ingredients) {
        List<List<ItemStack>> input = new ArrayList<>();

        for(Ingredient igd : ((WorktableRecipe)this.getRecipe()).ingredients){
            input.add(Arrays.asList(igd.getMatchingStacks()));
        }
        ingredients.setInputLists(VanillaTypes.ITEM, input);
        ingredients.setOutputs(VanillaTypes.ITEM, Arrays.asList(new ItemStack[]{this.getRecipe().getOutput()}));
    }
}
