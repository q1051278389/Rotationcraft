//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.tohsaka.factory.rotationcraft.intergration.jei.machines.worktable;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.blocks.base.BlockWorktable;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.GuiWorktable;
import cn.tohsaka.factory.rotationcraft.intergration.jei.base.RCRecipeCategory;
import cn.tohsaka.factory.rotationcraft.intergration.jei.base.RCRecipeCategoryUid;
import mezz.jei.api.IGuiHelper;
import mezz.jei.api.gui.ICraftingGridHelper;
import mezz.jei.api.gui.IGuiItemStackGroup;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.ITooltipCallback;
import mezz.jei.api.ingredients.IIngredients;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

import java.util.List;

public class WorktableRecipeCategory extends RCRecipeCategory<WorktableRecipeWrapper> implements ITooltipCallback<ItemStack> {
    private static final ResourceLocation guiTexture = GuiWorktable.tex;
    private final ICraftingGridHelper craftingGridHelper;
    private static final int craftOutputSlot = 9;
    private static final int craftInputSlot1 = 0;
    public WorktableRecipeCategory(IGuiHelper guiHelper) {
        super(guiHelper.createDrawable(guiTexture, 27, 14, 130, 58), RotationCraft.blocks.get(BlockWorktable.NAME).getUnlocalizedName()+".name");
        craftingGridHelper = guiHelper.createCraftingGridHelper(craftInputSlot1, craftOutputSlot);
    }

    public String getUid() {
        return RCRecipeCategoryUid.WORKTABLE;
    }

    public void drawExtras(Minecraft minecraft) {

    }
    boolean requirers = false;
    public void setRecipe(IRecipeLayout recipeLayout, WorktableRecipeWrapper recipeWrapper, IIngredients ingredients) {
        IGuiItemStackGroup guiItemStacks = recipeLayout.getItemStacks();
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                guiItemStacks.init(i*3+j, true, 2+j*18, 2+i*18);
            }
        }
        //List<List<ItemStack>> inputs = ingredients.getInputs(VanillaTypes.ITEM);
        //craftingGridHelper.setInputs(guiItemStacks, inputs, 3, 3);

        guiItemStacks.init(9, false, 95, 19);
        guiItemStacks.set(ingredients);
    }

    public List<String> getTooltipStrings(int mouseX, int mouseY) {
        return super.getTooltipStrings(mouseX, mouseY);
    }

    @Override
    public void onTooltip(int slotIndex, boolean input, ItemStack ingredient, List<String> tooltip) {

    }
}
