package cn.tohsaka.factory.rotationcraft.intergration.crafttweaker.handlers;

import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.GrinderRecipe;
import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.crafting.scripts.GrinderRecipes;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileGrinder;
import crafttweaker.annotations.ModOnly;
import crafttweaker.annotations.ZenRegister;
import crafttweaker.api.item.IItemStack;
import crafttweaker.api.liquid.ILiquidStack;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.nbt.NBTTagCompound;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

import static cn.tohsaka.factory.rotationcraft.intergration.crafttweaker.InputHelper.toFluid;
import static cn.tohsaka.factory.rotationcraft.intergration.crafttweaker.InputHelper.toStack;

@ZenClass("mods.rotationcraft.grinder")
@ModOnly("rc")
@ZenRegister
public class Grinder {
    @ZenMethod
    public static void addRecipe(IItemStack output,IItemStack input) {
        GrinderRecipe recipe = new GrinderRecipe(toStack(input),toStack(output));
        Recipes.INSTANCE.addPattern(TileGrinder.class,recipe);
    }
    @ZenMethod
    public static void addFluidRecipe(ILiquidStack output, IItemStack input) {
        NBTTagCompound tag = CraftingPattern.setFluid(new NBTTagCompound(),"fluid",toFluid(output));
        GrinderRecipe recipe = new GrinderRecipe(Ingredient.fromStacks(toStack(input)), ItemStack.EMPTY,input.getAmount(),tag);
        Recipes.INSTANCE.addPattern(TileGrinder.class,recipe);
    }

    @ZenMethod
    public static void delRecipe(IItemStack output) {
        GrinderRecipes.stacksToRemove.add(toStack(output));
    }
    @ZenMethod
    public static void delFluidRecipe(ILiquidStack output) {
        GrinderRecipes.fluidToRemove.add(toFluid(output));
    }
}
