//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.tohsaka.factory.rotationcraft.intergration.jei.machines.extractor;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.blocks.machine.BlockExtractor;
import cn.tohsaka.factory.rotationcraft.intergration.jei.base.RCRecipeCategory;
import cn.tohsaka.factory.rotationcraft.intergration.jei.base.RCRecipeCategoryUid;
import mezz.jei.api.IGuiHelper;
import mezz.jei.api.gui.IGuiItemStackGroup;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;

public class ExtractorRecipeCategory extends RCRecipeCategory<ExtractorRecipeWrapper>{
    public static String tex_path = "textures/gui/minerjei.png";
    public static String gui_tex = RotationCraft.MOD_ID+":"+tex_path;
    private static final ResourceLocation guiTexture = new ResourceLocation(RotationCraft.MOD_ID,tex_path);
    public ExtractorRecipeCategory(IGuiHelper guiHelper) {
        super(guiHelper.createDrawable(guiTexture, 0, 0, 106, 64), RotationCraft.blocks.get(BlockExtractor.NAME).getUnlocalizedName()+".name");

    }

    public String getUid() {
        return RCRecipeCategoryUid.EXTRACTOR;
    }

    public void drawExtras(Minecraft minecraft) {

    }

    /**
     * Set the {@link IRecipeLayout} properties from the {@link IRecipeWrapper} and {@link IIngredients}.
     *
     * @param recipeLayout  the layout that needs its properties set.
     * @param recipeWrapper the recipeWrapper, for extra information.
     * @param ingredients   the ingredients, already set by the recipeWrapper
     * @since JEI 3.11.0
     */
    @Override
    public void setRecipe(IRecipeLayout recipeLayout, ExtractorRecipeWrapper recipeWrapper, IIngredients ingredients) {
        IGuiItemStackGroup guiItemStacks = recipeLayout.getItemStacks();
        guiItemStacks.init(0, false, 72, 23);
        guiItemStacks.init(1,true,14,23);
        guiItemStacks.set(ingredients);
    }
}
