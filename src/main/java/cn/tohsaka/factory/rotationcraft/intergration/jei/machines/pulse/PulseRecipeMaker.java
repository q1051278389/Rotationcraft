//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.tohsaka.factory.rotationcraft.intergration.jei.machines.pulse;

import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TilePulse;
import mezz.jei.api.recipe.IStackHelper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PulseRecipeMaker {
    private PulseRecipeMaker() {
    }

    public static List<PulseRecipeWrapper> getRecipes(IStackHelper stackHelper) {
        List<PulseRecipeWrapper> recipes = new ArrayList();
        Iterator var2 = Recipes.INSTANCE.getList(TilePulse.class).iterator();

        while(var2.hasNext()) {
            CraftingPattern recipe = (CraftingPattern)var2.next();
            addWrapperToList(stackHelper, recipe, recipes);
        }

        return recipes;
    }

    private static void addWrapperToList(IStackHelper stackHelper, CraftingPattern recipe, List<PulseRecipeWrapper> recipes) {
        recipes.add(new PulseRecipeWrapper(recipe));
    }
}
