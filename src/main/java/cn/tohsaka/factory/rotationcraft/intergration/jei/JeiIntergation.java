//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.tohsaka.factory.rotationcraft.intergration.jei;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.blocks.base.BlockBlast;
import cn.tohsaka.factory.rotationcraft.blocks.base.BlockWorktable;
import cn.tohsaka.factory.rotationcraft.blocks.machine.*;
import cn.tohsaka.factory.rotationcraft.gui.machine.container.ContainerWorktable;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.*;
import cn.tohsaka.factory.rotationcraft.intergration.jei.base.RCRecipeCategoryUid;
import cn.tohsaka.factory.rotationcraft.intergration.jei.machines.blast.BlastRecipeCategory;
import cn.tohsaka.factory.rotationcraft.intergration.jei.machines.blast.BlastRecipeMaker;
import cn.tohsaka.factory.rotationcraft.intergration.jei.machines.extractor.ExtractorRecipeCategory;
import cn.tohsaka.factory.rotationcraft.intergration.jei.machines.extractor.ExtractorRecipeMaker;
import cn.tohsaka.factory.rotationcraft.intergration.jei.machines.ferm.FermRecipeCategory;
import cn.tohsaka.factory.rotationcraft.intergration.jei.machines.ferm.FermRecipeMaker;
import cn.tohsaka.factory.rotationcraft.intergration.jei.machines.grinder.GrinderRecipeCategory;
import cn.tohsaka.factory.rotationcraft.intergration.jei.machines.grinder.GrinderRecipeMaker;
import cn.tohsaka.factory.rotationcraft.intergration.jei.machines.magnetizer.MagnetizerRecipeCategory;
import cn.tohsaka.factory.rotationcraft.intergration.jei.machines.magnetizer.MagnetizerRecipeMaker;
import cn.tohsaka.factory.rotationcraft.intergration.jei.machines.miner.MinerRecipeCategory;
import cn.tohsaka.factory.rotationcraft.intergration.jei.machines.miner.MinerRecipeMaker;
import cn.tohsaka.factory.rotationcraft.intergration.jei.machines.pulse.PulseRecipeCategory;
import cn.tohsaka.factory.rotationcraft.intergration.jei.machines.pulse.PulseRecipeMaker;
import cn.tohsaka.factory.rotationcraft.intergration.jei.machines.worktable.WorktableRecipeCategory;
import cn.tohsaka.factory.rotationcraft.intergration.jei.machines.worktable.WorktableRecipeMaker;
import mezz.jei.api.*;
import mezz.jei.api.recipe.IRecipeCategory;
import mezz.jei.api.recipe.IRecipeCategoryRegistration;
import mezz.jei.api.recipe.VanillaRecipeCategoryUid;
import mezz.jei.api.recipe.transfer.IRecipeTransferRegistry;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SideOnly(Side.CLIENT)
@JEIPlugin
public class JeiIntergation implements IModPlugin {
    @Nullable
    public static IJeiHelpers jeiHelpers;
    public static IModRegistry registry;
    public JeiIntergation() {

    }
    public static Map<String, List<ItemStack>> catalysts = new HashMap<>();

    public void registerCategories(IRecipeCategoryRegistration registry) {
        jeiHelpers = registry.getJeiHelpers();
        IGuiHelper guiHelper = jeiHelpers.getGuiHelper();
        registry.addRecipeCategories(new IRecipeCategory[]{
                new FermRecipeCategory(guiHelper),
                new WorktableRecipeCategory(guiHelper),
                new GrinderRecipeCategory(guiHelper),
                new BlastRecipeCategory(guiHelper),
                new PulseRecipeCategory(guiHelper),
                new MinerRecipeCategory(guiHelper),
                new ExtractorRecipeCategory(guiHelper),
                new MagnetizerRecipeCategory(guiHelper)
        });
    }

    public void register(IModRegistry registry) {
        JeiIntergation.registry = registry;
        IRecipeTransferRegistry recipeTransferRegistry = registry.getRecipeTransferRegistry();

        registry.addRecipes(FermRecipeMaker.getRecipes(jeiHelpers.getStackHelper()), RCRecipeCategoryUid.FERM);
        registry.addRecipeClickArea(GuiFerm.class, 80,35,22,15, new String[]{RCRecipeCategoryUid.FERM});
        JeiIntergation.addCatalyst(RCRecipeCategoryUid.FERM, new ItemStack(RotationCraft.blocks.get(BlockFerm.NAME)));

        registry.addRecipes(WorktableRecipeMaker.getRecipes(jeiHelpers.getStackHelper()), RCRecipeCategoryUid.WORKTABLE);
        registry.addRecipeClickArea(GuiWorktable.class, 89,34,24,17, new String[]{RCRecipeCategoryUid.WORKTABLE});
        JeiIntergation.addCatalyst(RCRecipeCategoryUid.WORKTABLE, new ItemStack(RotationCraft.blocks.get(BlockWorktable.NAME)));

        recipeTransferRegistry.addRecipeTransferHandler(ContainerWorktable.class, RCRecipeCategoryUid.WORKTABLE, 36, 9, 0, 36);

        registry.addRecipes(GrinderRecipeMaker.getRecipes(jeiHelpers.getStackHelper()), RCRecipeCategoryUid.GRINDER);
        registry.addRecipeClickArea(GuiGrinder.class, 99,35,24,16, new String[]{RCRecipeCategoryUid.GRINDER});
        JeiIntergation.addCatalyst(RCRecipeCategoryUid.GRINDER, new ItemStack(RotationCraft.blocks.get(BlockGrinder.NAME)));

        registry.addRecipes(BlastRecipeMaker.getRecipes(jeiHelpers.getStackHelper()), RCRecipeCategoryUid.BLAST);
        registry.addRecipeClickArea(GuiBlast.class, 119,35,24,16, new String[]{RCRecipeCategoryUid.BLAST});
        JeiIntergation.addCatalyst(RCRecipeCategoryUid.BLAST, new ItemStack(RotationCraft.blocks.get(BlockBlast.NAME)));


        registry.addRecipes(PulseRecipeMaker.getRecipes(jeiHelpers.getStackHelper()), RCRecipeCategoryUid.PULSE);
        registry.addRecipeClickArea(GuiPulse.class, 119,34,28,14, new String[]{RCRecipeCategoryUid.PULSE});
        JeiIntergation.addCatalyst(RCRecipeCategoryUid.PULSE, new ItemStack(RotationCraft.blocks.get(BlockPulse.NAME)));


        registry.addRecipes(MinerRecipeMaker.getRecipes(jeiHelpers.getStackHelper()), RCRecipeCategoryUid.MINER);
        registry.addRecipeClickArea(GuiVoidMiner.class, 7,74,161,5, new String[]{RCRecipeCategoryUid.MINER});
        JeiIntergation.addCatalyst(RCRecipeCategoryUid.MINER, new ItemStack(RotationCraft.blocks.get(BlockVoidMiner.NAME)));

        registry.addRecipes(ExtractorRecipeMaker.getRecipes(jeiHelpers.getStackHelper()), RCRecipeCategoryUid.EXTRACTOR);
        registry.addRecipeClickArea(GuiExtractor.class, 29,34,121,16, new String[]{RCRecipeCategoryUid.EXTRACTOR});
        JeiIntergation.addCatalyst(RCRecipeCategoryUid.EXTRACTOR, new ItemStack(RotationCraft.blocks.get(BlockExtractor.NAME)));


        registry.addRecipes(MagnetizerRecipeMaker.getRecipes(jeiHelpers.getStackHelper()), RCRecipeCategoryUid.MAGNETIZER);
        JeiIntergation.addCatalyst(RCRecipeCategoryUid.MAGNETIZER, new ItemStack(RotationCraft.blocks.get(BlockMagnetizer.NAME)));




        for(String key:catalysts.keySet()){
            for(ItemStack stack:catalysts.get(key)){
                registry.addRecipeCatalyst(stack,new String[]{key});
            }
        }

        registry.addRecipeCatalyst(new ItemStack(RotationCraft.blocks.get(BlockFurnace.NAME)),new String[]{VanillaRecipeCategoryUid.SMELTING});
    }

    public static void addCatalyst(String uid,ItemStack stack){
        if(!catalysts.containsKey(uid)){
            catalysts.put(uid,new ArrayList<>());
        }
        catalysts.get(uid).add(stack);
    }
}
