//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.tohsaka.factory.rotationcraft.intergration.jei.machines.blast;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.blocks.base.BlockBlast;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.GuiBlast;
import cn.tohsaka.factory.rotationcraft.intergration.jei.base.RCRecipeCategory;
import cn.tohsaka.factory.rotationcraft.intergration.jei.base.RCRecipeCategoryUid;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import mezz.jei.api.IGuiHelper;
import mezz.jei.api.gui.*;
import mezz.jei.api.ingredients.IIngredients;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

import java.util.Arrays;
import java.util.List;

public class BlastRecipeCategory extends RCRecipeCategory<BlastRecipeWrapper> implements ITooltipCallback<ItemStack>{
    private static final ResourceLocation guiTexture = GuiBlast.tex;
    private final IDrawableAnimated progressBar0;
    private final IDrawableAnimated temp;
    public BlastRecipeCategory(IGuiHelper guiHelper) {
        super(guiHelper.createDrawable(guiTexture, 4, 4, 168, 72), RotationCraft.blocks.get(BlockBlast.NAME).getUnlocalizedName()+".name");

        IDrawableStatic progressBarDrawable0 = guiHelper.createDrawable(guiTexture, 176,14, 24, 16);
        IDrawableStatic temp = guiHelper.createDrawable(guiTexture,176,31,11,55);//7,11
        this.temp = guiHelper.createAnimatedDrawable(temp,40,IDrawableAnimated.StartDirection.BOTTOM,false);
        this.progressBar0 = guiHelper.createAnimatedDrawable(progressBarDrawable0, 40, IDrawableAnimated.StartDirection.LEFT, false);
    }

    public String getUid() {
        return RCRecipeCategoryUid.BLAST;
    }

    public void drawExtras(Minecraft minecraft) {
        progressBar0.draw(minecraft,115, 31);
        temp.draw(minecraft,7,11);
    }
    IGuiItemStackGroup guiItemStacks;
    BlastRecipeWrapper recipeWrapper;
    public void setRecipe(IRecipeLayout recipeLayout, BlastRecipeWrapper recipeWrapper, IIngredients ingredients) {
        guiItemStacks = recipeLayout.getItemStacks();
        this.recipeWrapper = recipeWrapper;
        boolean isShapeless = recipeWrapper.getRecipe().isShapeless();
        if(isShapeless){
            guiItemStacks.init(0, true, 57, 12);
            guiItemStacks.init(1, true, 21, 11);
            guiItemStacks.init(2, true, 21, 30);
            guiItemStacks.init(3, true, 21, 49);
            guiItemStacks.init(4, false, 143, 12);
            guiItemStacks.addTooltipCallback(this::onTooltip);
        }else{
            int q=0;
            for(int i=0;i<3;i++){
                for(int j=0;j<3;j++){
                    guiItemStacks.init(i*3+j, true, 57+j*18, 12+i*18);
                }
            }
            guiItemStacks.init(9, true, 21, 11);
            guiItemStacks.init(10, true, 21, 30);
            guiItemStacks.init(11, true, 21, 49);
            guiItemStacks.init(12, false, 143, 12);
        }
        guiItemStacks.set(ingredients);
    }


    @Override
    public void onTooltip(int slotIndex, boolean input, ItemStack ingredient, List<String> tooltip) {
        if(slotIndex==4 && recipeWrapper.getRecipe().getBound()>0){
            tooltip.add(tooltip.size()-1,String.format(StringHelper.localize("info.rc.bounds"),Math.round(recipeWrapper.getRecipe().getBound()*100))+"%");
        }

        if(slotIndex==0){
            tooltip.add(tooltip.size()-1,StringHelper.localize("info.rc.blast.9slot"));
        }
    }


    @Override
    public List<String> getTooltipStrings(int mouseX, int mouseY) {
        if(mouseX>6 && mouseY>10 && mouseX<18 && mouseY<67){
            return Arrays.asList(StringHelper.localize("info.rc.needtemp")+": "+recipeWrapper.getRecipe().getTemp()+StringHelper.localize("info.rc.tempchar"));
        }
        return super.getTooltipStrings(mouseX, mouseY);
    }
}
