//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.tohsaka.factory.rotationcraft.intergration.jei.machines.blast;

import cn.tohsaka.factory.rotationcraft.crafting.BlastRecipe;
import cn.tohsaka.factory.rotationcraft.intergration.jei.base.RCRecipeWrapper;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.ingredients.VanillaTypes;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.nbt.NBTTagCompound;

import java.util.*;

public class BlastRecipeWrapper extends RCRecipeWrapper<BlastRecipe> {
    public static HashSet<BlastRecipeWrapper> recipes = new HashSet<>();
    public BlastRecipeWrapper(BlastRecipe recipe) {
        super(recipe);
        recipes.add(this);
    }
    public void getIngredients(IIngredients ingredients) {
        List<List<ItemStack>> input = new ArrayList<>();
        List<List<ItemStack>> in = new LinkedList<>();
        for(ItemStack[] ss :this.getRecipe().inputItem){
            input.add(Arrays.asList(ss));
        }
        for(Ingredient item : this.getRecipe().cata){
            input.add(Arrays.asList(item.getMatchingStacks()));
        }
        NBTTagCompound tag = this.getRecipe().getData();
        ingredients.setInputLists(VanillaTypes.ITEM, input);
        ingredients.setOutputs(VanillaTypes.ITEM, Arrays.asList(new ItemStack[]{this.getRecipe().getOutput()}));
    }
}
