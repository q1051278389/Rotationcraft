//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.tohsaka.factory.rotationcraft.intergration.jei.base;

import mezz.jei.api.recipe.IRecipeWrapper;

public abstract class RCRecipeWrapper<R> implements IRecipeWrapper {
    private final R recipe;

    public RCRecipeWrapper(R recipe) {
        this.recipe = recipe;
    }

    public R getRecipe() {
        return this.recipe;
    }
}
