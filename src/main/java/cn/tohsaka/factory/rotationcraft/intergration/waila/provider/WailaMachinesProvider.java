package cn.tohsaka.factory.rotationcraft.intergration.waila.provider;

import cn.tohsaka.factory.librotary.api.power.RotaryPower;
import cn.tohsaka.factory.rotationcraft.config.GeneralConfig;
import cn.tohsaka.factory.rotationcraft.intergration.waila.IWailaSupport;
import cn.tohsaka.factory.rotationcraft.network.NetworkDispatcher;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileMachineBase;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileRotation;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaDataAccessor;
import mcp.mobius.waila.api.IWailaDataProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;

import javax.annotation.Nonnull;
import java.util.List;

public class WailaMachinesProvider implements IWailaDataProvider {
    private Long time = 0L;
    @Nonnull
    @Override
    public List<String> getWailaBody(ItemStack itemStack, List<String> tooltip, IWailaDataAccessor accessor, IWailaConfigHandler config) {
        List<String> tips = IWailaDataProvider.super.getWailaBody(itemStack, tooltip, accessor, config);
        TileEntity te = accessor.getTileEntity();
        try {
            if(te instanceof TileMachineBase){
                Long t = accessor.getWorld().getTotalWorldTime();
                if(te instanceof IWailaSupport){
                    if(GeneralConfig.wailapacketrate>0){
                        if(t>time && t % GeneralConfig.wailapacketrate == 0){
                            NetworkDispatcher.requireGuiUpdate((TileMachineBase) te);
                            time=t;
                        }
                    }
                    ((IWailaSupport)te).getWailaBody(itemStack,tooltip);
                    if(te instanceof TileRotation){
                        RotaryPower power = ((TileRotation) te).getMinimumPower();
                        if(power.isPowered() && power!=RotaryPower.ZERO){
                            tooltip.add(StringHelper.localize("info.rc.waila.names.require_power")+" "+power.getFormattedWatt());
                            tooltip.add(StringHelper.localize("info.rc.waila.names.torque")+" "+power.getFormattedNm());
                            tooltip.add(StringHelper.localize("info.rc.waila.names.speed")+" "+power.getFormattedSpeed());
                        }
                    }
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return tips;
    }
}
