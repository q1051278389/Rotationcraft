package cn.tohsaka.factory.rotationcraft.intergration.waila;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.intergration.waila.provider.WailaMachinesProvider;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileMachineBase;
import mcp.mobius.waila.api.IWailaPlugin;
import mcp.mobius.waila.api.IWailaRegistrar;
import mcp.mobius.waila.api.WailaPlugin;

@WailaPlugin(RotationCraft.MOD_ID)
public class RCWailaPlugin implements IWailaPlugin {
    @Override
    public void register(IWailaRegistrar registrar) {
        registrar.registerBodyProvider(new WailaMachinesProvider(), TileMachineBase.class);
    }
}
