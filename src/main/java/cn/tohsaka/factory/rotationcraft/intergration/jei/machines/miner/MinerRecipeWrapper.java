//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.tohsaka.factory.rotationcraft.intergration.jei.machines.miner;

import cn.tohsaka.factory.rotationcraft.crafting.scripts.GlobalOre;
import cn.tohsaka.factory.rotationcraft.intergration.jei.base.RCRecipeWrapper;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.ingredients.VanillaTypes;
import net.minecraft.client.Minecraft;

import java.util.Arrays;
import java.util.HashSet;

public class MinerRecipeWrapper extends RCRecipeWrapper<GlobalOre.StackRandomItem> {
    public static HashSet<MinerRecipeWrapper> recipes = new HashSet<>();
    public MinerRecipeWrapper(GlobalOre.StackRandomItem stack) {
        super(stack);
        recipes.add(this);
    }
    public void getIngredients(IIngredients ingredients) {
        ingredients.setOutputs(VanillaTypes.ITEM, Arrays.asList(this.getRecipe().ingredient.getMatchingStacks()));
    }

    @Override
    public void drawInfo(Minecraft minecraft, int recipeWidth, int recipeHeight, int mouseX, int mouseY) {
        float percent = (((float) this.getRecipe().itemWeight) / GlobalOre.weight_count) * 100F;
        {
            String str = String.format("%.2f",percent)+"%";
            minecraft.fontRenderer.drawString(str,(recipeWidth/2)-(minecraft.fontRenderer.getStringWidth(str) / 2),46,0);
        }
        if(StringHelper.isShiftKeyDown()){
            String str = String.format("%d / %d",this.getRecipe().itemWeight , GlobalOre.weight_count);
            minecraft.fontRenderer.drawString(str,(recipeWidth/2)-(minecraft.fontRenderer.getStringWidth(str) / 2),55,0);
        }
    }
}
