//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.tohsaka.factory.rotationcraft.intergration.jei.machines.grinder;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.blocks.machine.BlockGrinder;
import cn.tohsaka.factory.rotationcraft.gui.machine.screen.GuiGrinder;
import cn.tohsaka.factory.rotationcraft.intergration.jei.base.RCRecipeCategory;
import cn.tohsaka.factory.rotationcraft.intergration.jei.base.RCRecipeCategoryUid;
import mezz.jei.api.IGuiHelper;
import mezz.jei.api.gui.*;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.ingredients.VanillaTypes;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.FluidStack;

import java.util.List;

public class GrinderRecipeCategory extends RCRecipeCategory<GrinderRecipeWrapper> implements ITooltipCallback<FluidStack> {
    private static final ResourceLocation guiTexture = GuiGrinder.tex;
    private final IDrawableAnimated progressBar0;

    public GrinderRecipeCategory(IGuiHelper guiHelper) {
        super(guiHelper.createDrawable(guiTexture, 21, 18, 138, 61), RotationCraft.blocks.get(BlockGrinder.NAME).getUnlocalizedName()+".name");

        IDrawableStatic progressBarDrawable0 = guiHelper.createDrawable(guiTexture, 176,14, 24, 16);
        this.progressBar0 = guiHelper.createAnimatedDrawable(progressBarDrawable0, 40, IDrawableAnimated.StartDirection.LEFT, false);
    }

    public String getUid() {
        return RCRecipeCategoryUid.GRINDER;
    }

    public void drawExtras(Minecraft minecraft) {
        progressBar0.draw(minecraft,78, 16);
    }
    boolean requirers = false;
    public void setRecipe(IRecipeLayout recipeLayout, GrinderRecipeWrapper recipeWrapper, IIngredients ingredients) {
        IGuiItemStackGroup guiItemStacks = recipeLayout.getItemStacks();
        guiItemStacks.init(0, true, 54, 16);
        guiItemStacks.init(1, false, 114, 16);

        if(ingredients.getOutputs(VanillaTypes.FLUID)!=null && ingredients.getOutputs(VanillaTypes.FLUID).size()>0){
            try {
                IGuiFluidStackGroup guiFluidStacks = recipeLayout.getFluidStacks();
                guiFluidStacks.init(0,false,14,42);//35 60
                guiFluidStacks.addTooltipCallback(this::onTooltip);
                guiFluidStacks.set(ingredients);
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }


        guiItemStacks.set(ingredients);
    }


    @Override
    public void onTooltip(int slotIndex, boolean input, FluidStack ingredient, List<String> tooltip) {
        tooltip.add(1,ingredient.amount+" mB");
    }
}
