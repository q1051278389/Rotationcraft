//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.tohsaka.factory.rotationcraft.intergration.jei.machines.blast;

import cn.tohsaka.factory.rotationcraft.crafting.BlastRecipe;
import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileBlast;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileFerm;
import mezz.jei.api.recipe.IStackHelper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BlastRecipeMaker {
    private BlastRecipeMaker() {
    }

    public static List<BlastRecipeWrapper> getRecipes(IStackHelper stackHelper) {
        List<BlastRecipeWrapper> recipes = new ArrayList();
        Iterator var2 = Recipes.INSTANCE.getList(TileBlast.class).iterator();

        while(var2.hasNext()) {
            CraftingPattern recipe = (CraftingPattern)var2.next();
            if(recipe instanceof BlastRecipe){
                addWrapperToList(stackHelper, ((BlastRecipe) recipe), recipes);
            }
        }

        return recipes;
    }

    private static void addWrapperToList(IStackHelper stackHelper, BlastRecipe recipe, List<BlastRecipeWrapper> recipes) {
        recipes.add(new BlastRecipeWrapper(recipe));
    }
}
