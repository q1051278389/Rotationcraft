package cn.tohsaka.factory.rotationcraft.intergration.waila;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

import java.util.List;

public interface IWailaSupport {
    abstract void getWailaBody(ItemStack itemStack,List<String> tooltip);
}
