package cn.tohsaka.factory.rotationcraft.intergration.crafttweaker.handlers;

import cn.tohsaka.factory.rotationcraft.crafting.scripts.GlobalOre;
import crafttweaker.annotations.ModOnly;
import crafttweaker.annotations.ZenRegister;
import crafttweaker.api.item.IItemStack;
import net.minecraft.item.crafting.Ingredient;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

import static cn.tohsaka.factory.rotationcraft.intergration.crafttweaker.InputHelper.toStack;

@ZenClass("mods.rotationcraft.voidminer")
@ModOnly("rc")
@ZenRegister
public class VoidMiner {
    @ZenMethod
    public static void addOre(IItemStack ore, int chance) {
        GlobalOre.addOreWeight(Ingredient.fromStacks(toStack(ore)),chance);
    }
    @ZenMethod
    public static void delOre(IItemStack ore) {
        for(Ingredient ingredient:GlobalOre.oreWeights.keySet()){
            if(ingredient.test(toStack(ore))){
                GlobalOre.oreWeights.remove(ingredient);
            }
        }
    }
    @ZenMethod
    public static void setWeight(IItemStack ore,int chance) {
        GlobalOre.setWeight(toStack(ore),chance);
    }
}
