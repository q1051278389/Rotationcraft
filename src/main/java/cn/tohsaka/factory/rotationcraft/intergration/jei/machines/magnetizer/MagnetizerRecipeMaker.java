package cn.tohsaka.factory.rotationcraft.intergration.jei.machines.magnetizer;

import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.MagnetizerRecipe;
import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileMagnetizer;
import mezz.jei.api.recipe.IStackHelper;

import java.util.ArrayList;
import java.util.List;

public class MagnetizerRecipeMaker {
    private MagnetizerRecipeMaker() {
    }

    public static List<MagnetizerRecipeWrapper> getRecipes(IStackHelper stackHelper) {
        List<MagnetizerRecipeWrapper> recipes = new ArrayList();
        for(CraftingPattern s : Recipes.INSTANCE.getList(TileMagnetizer.class)) {
            addWrapperToList(stackHelper,(MagnetizerRecipe) s,recipes);
        }
        return recipes;
    }

    private static void addWrapperToList(IStackHelper stackHelper, MagnetizerRecipe recipe, List<MagnetizerRecipeWrapper> recipes) {
        recipes.add(new MagnetizerRecipeWrapper(recipe));
    }
}
