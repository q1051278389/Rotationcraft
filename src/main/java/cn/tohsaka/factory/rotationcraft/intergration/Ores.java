package cn.tohsaka.factory.rotationcraft.intergration;

import cn.tohsaka.factory.rotationcraft.init.INeedPostInit;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.items.ItemCrushedOre;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

import java.util.LinkedList;
import java.util.List;

@GameInitializer
public class Ores implements INeedPostInit {
    public static String[] orelist;
    public static void postinit(){
        String [] names = OreDictionary.getOreNames();
        List<String> strs = new LinkedList<>();
        for(String name:names){
            if(!name.toLowerCase().contains("ore")){
                continue;
            }
            strs.add(name);
        }
        orelist = strs.stream().toArray(String[]::new);
    }
    public static int getOre(ItemStack stack){
        if(!(stack.getItem() instanceof ItemCrushedOre)){
            return -1;
        }
        return 0;
    }
}
