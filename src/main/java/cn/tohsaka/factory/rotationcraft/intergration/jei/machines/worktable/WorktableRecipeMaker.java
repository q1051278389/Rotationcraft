//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.tohsaka.factory.rotationcraft.intergration.jei.machines.worktable;

import cn.tohsaka.factory.rotationcraft.crafting.CraftingPattern;
import cn.tohsaka.factory.rotationcraft.crafting.Recipes;
import cn.tohsaka.factory.rotationcraft.crafting.WorktableRecipe;
import cn.tohsaka.factory.rotationcraft.tiles.base.TileWorktable;
import cn.tohsaka.factory.rotationcraft.tiles.machines.TileFerm;
import mezz.jei.api.recipe.IStackHelper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class WorktableRecipeMaker {
    private WorktableRecipeMaker() {
    }

    public static List<WorktableRecipeWrapper> getRecipes(IStackHelper stackHelper) {
        List<WorktableRecipeWrapper> recipes = new ArrayList();
        Iterator var2 = Recipes.INSTANCE.getList(TileWorktable.class).iterator();

        while(var2.hasNext()) {
            WorktableRecipe recipe = (WorktableRecipe)var2.next();
            addWrapperToList(stackHelper, recipe, recipes);
        }

        return recipes;
    }

    private static void addWrapperToList(IStackHelper stackHelper, WorktableRecipe recipe, List<WorktableRecipeWrapper> recipes) {
        recipes.add(new WorktableRecipeWrapper(recipe));
    }
}
