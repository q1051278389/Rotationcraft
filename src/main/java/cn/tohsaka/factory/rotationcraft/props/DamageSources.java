package cn.tohsaka.factory.rotationcraft.props;

import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import net.minecraft.util.DamageSource;

@GameInitializer
public class DamageSources {
    public static DamageSource GEAR_EXPLODE;
    public static DamageSource COIL_EXPLODE;
    public static DamageSource FERM_EXPLODE;
    public static DamageSource JET_EXPLODE;
    public static DamageSource JET_SUCK;
    public static DamageSource ANY;
    public static DamageSource REACTOR_EXPLODE;
    public static void init(){
        GEAR_EXPLODE = new DamageSource("gear_explode");
        COIL_EXPLODE = new DamageSource("coil_explode");
        FERM_EXPLODE = new DamageSource("ferm_explode");
        JET_EXPLODE = new DamageSource("jet_explode");
        JET_SUCK = new DamageSource("jet_suck");
        REACTOR_EXPLODE = new DamageSource("reactor_explode");
        ANY = new DamageSource("anysource");
    }
}
