package cn.tohsaka.factory.rotationcraft.props;

import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.ReflectionHelper;

import java.util.ArrayList;
import java.util.List;

public class Brandings {
    public static List<String> brandingTexts = new ArrayList<>();
    public static void compile () {

        final List<String> existingBrandings = FMLCommonHandler.instance().getBrandings(true);
        final List<String> newList = new ArrayList<>();

        for (final String branding : brandingTexts) {
            newList.add(branding);
        }

        newList.addAll(existingBrandings);

        ReflectionHelper.setPrivateValue(FMLCommonHandler.class, FMLCommonHandler.instance(), newList, "brandings");
    }
}
