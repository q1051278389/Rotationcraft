package cn.tohsaka.factory.rotationcraft.utils;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.fluids.*;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.util.ResourceLocation;

public class TextureManager{
    public static TextureMap textureMap;
    private static final String BLOCKS = RotationCraft.MOD_ID+":blocks/";
    public static TextureAtlasSprite turbinetex;
    public static TextureManager INSTANCE = new TextureManager();
    public static void registerTextures(TextureMap map)
    {
        textureMap = map;
        turbinetex = map.registerSprite(new ResourceLocation(RotationCraft.MOD_ID,"render/turbine"));


        textureMap.registerSprite(FluidJetfuel.still);
        textureMap.registerSprite(FluidJetfuel.flow);
        textureMap.registerSprite(FluidLubricant.still);
        textureMap.registerSprite(FluidLubricant.flow);

        textureMap.registerSprite(FluidSteamLP.still);
        textureMap.registerSprite(FluidCO2.still);
        textureMap.registerSprite(FluidCO2H.still);
    }
}
