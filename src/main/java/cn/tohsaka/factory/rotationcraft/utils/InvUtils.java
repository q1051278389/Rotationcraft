package cn.tohsaka.factory.rotationcraft.utils;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

public class InvUtils {
    public static ItemStack insertItem(IInventory inventory,int slot,ItemStack stack){
        ItemStack stackInSlot = inventory.getStackInSlot(slot).copy();
        if(stackInSlot.isEmpty() || stackInSlot.isItemEqual(stack)){
            if(stackInSlot.isEmpty()){
                if(stack.getCount() > inventory.getInventoryStackLimit()){
                    ItemStack ret = stack.copy();
                    ret.setCount(stack.getCount() - inventory.getInventoryStackLimit());
                    stackInSlot = stack.copy();
                    stackInSlot.setCount(stack.getCount() - inventory.getInventoryStackLimit());
                    inventory.setInventorySlotContents(slot,stackInSlot);
                    return ret;
                }else{
                    stackInSlot = stack.copy();
                    inventory.setInventorySlotContents(slot,stackInSlot);
                }
                return ItemStack.EMPTY;
            }else{
                if((stack.getCount()+stackInSlot.getCount()) > inventory.getInventoryStackLimit()){
                    int remean = (stack.getCount()+stackInSlot.getCount()) - inventory.getInventoryStackLimit();
                    stackInSlot.setCount(inventory.getInventoryStackLimit());
                    ItemStack ret = stack.copy();
                    ret.setCount(remean);
                    inventory.setInventorySlotContents(slot,stackInSlot);
                    return ret;
                }else{
                    stackInSlot.setCount(stackInSlot.getCount()+stack.getCount());
                    inventory.setInventorySlotContents(slot,stackInSlot);
                    return ItemStack.EMPTY;
                }
            }
        }
        return stack;
    }


    public static ItemStack extractItem(IInventory inventory,int slot,ItemStack stack){
        ItemStack stackInSlot = inventory.getStackInSlot(slot).copy();
        if(!stackInSlot.isItemEqual(stack)){
            return stack;
        }
        if(!stackInSlot.isEmpty()){
            if(stackInSlot.getCount()>=stack.getCount()){
                stackInSlot.setCount(stackInSlot.getCount()-stack.getCount());
                inventory.setInventorySlotContents(slot,stackInSlot);
                return ItemStack.EMPTY;
            }else{
                ItemStack ret = stack.copy();
                ret.setCount(stack.getCount()-stackInSlot.getCount());
                inventory.setInventorySlotContents(slot,ItemStack.EMPTY);
                return ret;
            }
        }
        return stack;
    }
}
