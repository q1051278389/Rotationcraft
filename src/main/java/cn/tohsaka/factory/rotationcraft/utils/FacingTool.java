package cn.tohsaka.factory.rotationcraft.utils;

import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;

public class FacingTool {
    public static BlockPos getRelativePos(BlockPos pos, EnumFacing facing,Position position,int offset){
        EnumFacing nf;
        switch (position){
            case LEFT:
                nf = facing.rotateY();
                return pos.offset(nf,offset);
            case RIGHT:
                nf = facing.rotateY().rotateY().rotateY();
                return pos.offset(nf,offset);
            case FRONT:
                nf = facing;
                return pos.offset(nf,offset);
            case BACK:
                if(facing.ordinal()<2){
                    if(facing==EnumFacing.UP){
                        return pos.offset(EnumFacing.DOWN,offset);
                    }else{
                        return pos.offset(EnumFacing.UP,offset);
                    }
                }else{
                    nf = facing.rotateY().rotateY();
                }
                return pos.offset(nf,offset);
            case UP:
                return pos.offset(EnumFacing.UP,offset);
            case DOWN:
                return pos.offset(EnumFacing.DOWN,offset);
        }
        return null;
    }

    public static EnumFacing getRelativeSide(Position subaxlsposition, EnumFacing facing) {
        if(subaxlsposition==Position.LEFT){
            return facing.rotateY();
        }
        if(subaxlsposition==Position.RIGHT){
            return facing.rotateY().rotateY().rotateY();
        }
        if(subaxlsposition==Position.FRONT){
            return facing;
        }
        if(subaxlsposition == Position.BACK){
            if(facing.ordinal()<2){
                return revert(facing);
            }
            return facing.rotateY().rotateY();
        }
        if(subaxlsposition == Position.UP){
            if(facing.ordinal()<2){
                return facing;
            }
            return EnumFacing.UP;
        }
        if(subaxlsposition == Position.DOWN){
            return EnumFacing.DOWN;
        }

        return null;
    }


    public static int posOffset(BlockPos pos1,BlockPos pos2){
        int o = 0;
        o+=Math.abs(pos1.getX()-pos2.getX());
        o+=Math.abs(pos1.getY()-pos2.getY());
        o+=Math.abs(pos1.getZ()-pos2.getZ());
        return o;
    }


    public static enum Position{
        LEFT(0),
        RIGHT(1),
        FRONT(2),
        BACK(3),
        UP(4),
        DOWN(5);
        int index;
        Position(int i){
            index=i;
        }

        public int getIndex() {
            return index;
        }
        public static Position getbyIndex(int i){
            for(Position p : Position.values()){
                if(p.getIndex() == i){
                    return p;
                }
            }
            return null;
        }
    }
    public static EnumFacing revert(EnumFacing facing){
        return EnumFacing.VALUES[facing.ordinal()^1];
    }




}
