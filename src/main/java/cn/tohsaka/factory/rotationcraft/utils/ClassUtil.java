package cn.tohsaka.factory.rotationcraft.utils;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class ClassUtil {
    public static Collection<String> scan(String pkg, Class<?> annotation) {
        Set<Class<?>> clazzs = getClasses(pkg);
        ArrayList<Map.Entry<String,String>> result = new ArrayList<>();
        if (clazzs == null) {
            return Collections.EMPTY_LIST;
        }

        Set<Class<?>> inInterface = getByInterface(Object.class, clazzs);

        for (Class<?> clazz : clazzs) {

            Annotation[] annos = clazz.getAnnotations();
            for (Annotation anno : annos) {
                if(!clazz.getName().equals(annotation.getName())){
                    if(anno instanceof GameInitializer){
                        result.add(new Map.Entry<String, String>() {
                            @Override
                            public String getKey() {
                                return ((GameInitializer)anno).after().getName();
                            }

                            @Override
                            public String getValue() {
                                return clazz.getName();
                            }

                            @Override
                            public String setValue(String value) {
                                return null;
                            }
                        });
                    }
                }

            }
        }
        List<String> rr = new ArrayList<>();
        List<String> used = new ArrayList<>();
        used.add(Void.class.getName());
        while (result.size()>0){
            for(Map.Entry<String,String> entry:(ArrayList<Map.Entry<String,String>>)result.clone()){
                if(used.contains(entry.getKey())){
                    rr.add(entry.getValue());
                    used.add(entry.getValue());
                    result.remove(entry);
                }
            }
        }

        return rr;
    }

    public static Set<Class<?>> getClasses(String pack) {
        Set<Class<?>> classes = new LinkedHashSet<>();
        boolean recursive = true;
        String packageName = pack;
        String packageDirName = packageName.replace('.', '/');
        Enumeration<URL> dirs;
        try {
            dirs = Thread.currentThread().getContextClassLoader().getResources(packageDirName);
            while (dirs.hasMoreElements()) {
                URL url = dirs.nextElement();
                String protocol = url.getProtocol();
                if ("file".equals(protocol)) {
                    String filePath = URLDecoder.decode(url.getFile(), "UTF-8");
                    findAndAddClassesInPackageByFile(packageName, filePath, recursive, classes);
                } else if ("jar".equals(protocol)) {
                    JarFile jar;
                    try {
                        jar = ((JarURLConnection) url.openConnection()).getJarFile();
                        Enumeration<JarEntry> entries = jar.entries();
                        while (entries.hasMoreElements()) {
                            JarEntry entry = entries.nextElement();
                            String name = entry.getName();
                            if (name.charAt(0) == '/') {
                                name = name.substring(1);
                            }
                            if (name.startsWith(packageDirName)) {
                                int idx = name.lastIndexOf('/');
                                if (idx != -1) {
                                    packageName = name.substring(0, idx).replace('/', '.');
                                }
                                if ((idx != -1) || recursive) {
                                    if (name.endsWith(".class") && !entry.isDirectory()) {
                                        String className = name.substring(packageName.length() + 1, name.length() - 6);
                                        try {
                                            classes.add(Class.forName(packageName + '.' + className));
                                        } catch (NoClassDefFoundError e) {
                                            //e.printStackTrace();
                                        }catch (RuntimeException e){
                                            //e.printStackTrace();
                                        }catch (ClassNotFoundException e){
                                            //e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return classes;
    }

    public static void findAndAddClassesInPackageByFile(String packageName, String packagePath, final boolean recursive,
                                                        Set<Class<?>> classes) {
        File dir = new File(packagePath);
        if (!dir.exists() || !dir.isDirectory()) {
            return;
        }
        File[] dirfiles = dir.listFiles(new FileFilter() {
            public boolean accept(File file) {
                return (recursive && file.isDirectory()) || (file.getName().endsWith(".class"));
            }
        });
        for (File file : dirfiles) {
            if (file.isDirectory()) {
                findAndAddClassesInPackageByFile(packageName + "." + file.getName(), file.getAbsolutePath(), recursive,
                        classes);
            } else {
                String className = file.getName().substring(0, file.getName().length() - 6);
                try {
                    classes.add(
                            Thread.currentThread().getContextClassLoader().loadClass(packageName + '.' + className));
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public static Set<Class<?>> getByInterface(Class clazz, Set<Class<?>> classesAll) {
        Set<Class<?>> classes = new LinkedHashSet<Class<?>>();
        if (!clazz.isInterface()) {
            try {
                Iterator<Class<?>> iterator = classesAll.iterator();
                while (iterator.hasNext()) {
                    Class<?> cls = iterator.next();
                    if (clazz.isAssignableFrom(cls)) {
                        if (!clazz.equals(cls)) {
                            classes.add(cls);
                        } else {

                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return classes;
    }

}