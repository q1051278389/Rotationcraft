package cn.tohsaka.factory.rotationcraft.utils;

import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ItemStackUtils {
    public static ItemStack parseItem(String name){
        if(name.equals("NULL")){
            return ItemStack.EMPTY;
        }


        //minecraft:diamond,1
        String[] splited = name.split(",");
        String iname = splited[0];
        int count = 1;
        int meta = 0;
        String nbt = "";
        if(splited.length>1){
            count=Integer.parseInt(splited[1]);
        }
        if(splited.length>2){
            meta=Integer.parseInt(splited[2]);
        }
        if(splited.length>3){
            nbt=name.substring(name.indexOf("{"));
        }

        ItemStack itemStack = GameRegistry.makeItemStack(iname,meta,count,null);

        if(nbt!=""){
            itemStack.setTagCompound(NBTUtils.toTagCompound(nbt));
        }

        return itemStack;
    }


    public static ItemStack simpleParse(String name){
        //minecraft:diamond,1
        String[] splited = name.split(",");
        String iname = splited[0];
        int meta = Integer.parseInt(splited[1]);
        ItemStack itemStack = GameRegistry.makeItemStack(iname,meta,1,null);
        return itemStack;
    }
}
