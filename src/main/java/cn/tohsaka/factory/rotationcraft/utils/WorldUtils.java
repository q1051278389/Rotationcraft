package cn.tohsaka.factory.rotationcraft.utils;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;

import java.util.List;

public class WorldUtils {
    public static void makeExplode(BlockPos pos,int range, World world,int damage,List<ItemStack> drops,DamageSource damageSource){
        AxisAlignedBB aabb = new AxisAlignedBB(pos);
        world.playSound((EntityPlayer)null, pos.getX(), pos.getY(), pos.getZ(), SoundEvents.ENTITY_GENERIC_EXPLODE, SoundCategory.BLOCKS, 10.0F, 1.0F);
        List<EntityLivingBase> entities = world.getEntitiesWithinAABB(EntityLivingBase.class,aabb.grow(range,range,range),EntityLivingBase::isEntityAlive);
        for (EntityLivingBase entity:entities){
            if(entity!=null && entity.isEntityAlive()){
                entity.attackEntityFrom(damageSource,damage);
            }
        }
        if(drops!=null){
            for (ItemStack stack:drops){
                EntityItem item = new EntityItem(world, pos.getX(), pos.getY()+3, pos.getZ(), stack );
                world.spawnEntity(item);
            }
        }

    }



    public static void makeDamaged(BlockPos pos, World world,List<ItemStack> drops){
        AxisAlignedBB aabb = new AxisAlignedBB(pos);
        world.playSound((EntityPlayer)null, pos.getX(), pos.getY(), pos.getZ(), SoundEvents.ENTITY_ITEM_BREAK, SoundCategory.PLAYERS, 10.0F, 1.0F);
        if(drops!=null){
            for (ItemStack stack:drops){
                EntityItem item = new EntityItem(world, pos.getX(), pos.getY()+3, pos.getZ(), stack );
                world.spawnEntity(item);
            }
        }
    }

    public static <T> T getCapability(Capability<T> capability, BlockPos pos, World world, EnumFacing side){
        if(world==null || pos==null){
            return null;
        }
        TileEntity tile = world.getTileEntity(pos.offset(side));
        if(tile!=null){
            return  tile.getCapability(capability,side.getOpposite());
        }
        return null;
    }
}
