package cn.tohsaka.factory.rotationcraft.utils;

import cn.tohsaka.factory.rotationcraft.api.thermal.IThermalMachine;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@GameInitializer
public class TempetureUtils {
    public static HashMap<Block,Float> tempMap = new HashMap<>();
    public static void init(){
        tempMap.put(Blocks.LAVA, 600F);
        tempMap.put(Blocks.FLOWING_LAVA, 600F);
        tempMap.put(Blocks.FIRE, 200F);
        tempMap.put(Blocks.WATER, 12F);
        tempMap.put(Blocks.FLOWING_WATER, 12F);
        tempMap.put(Blocks.ICE, -3F);
        tempMap.put(Blocks.PACKED_ICE, -10F);
        tempMap.put(Blocks.SNOW, -2F);
        tempMap.put(Blocks.SNOW_LAYER, 0F);
    }
    public static float getBiomeTemp(World world, BlockPos pos){
        Biome biome = world.getBiome(pos);
        return 25 + (biome.getDefaultTemperature()*8);
    }
    public static float getBlockRelativeTemp(World world,BlockPos pos){
        float biomeTemp = getBiomeTemp(world,pos);
        List<Float> side = new ArrayList<>();
        float sourcetemp = -999;
        float max = 0;
        for (int i=0;i<6;i++){
            BlockPos p = FacingTool.getRelativePos(pos, EnumFacing.NORTH, FacingTool.Position.values()[i],1);
            IBlockState bs = world.getBlockState(p);
            if(tempMap.containsKey(bs.getBlock())){
                float t = tempMap.get(bs.getBlock());
                if(t>max){
                    max=t;
                }
                if(!side.contains(t)){
                    side.add(t);
                }
            }else{
                TileEntity te = world.getTileEntity(p);
                if(te != null && te instanceof IThermalMachine && ((IThermalMachine) te).isTempSource()){
                    sourcetemp = Math.max(sourcetemp,((IThermalMachine) te).getTemp());
                }
            }
        }
        float current = biomeTemp;
        for (float t:side){
            current += t;
        }
        //return Math.max(current,biomeTemp);
        if(sourcetemp != -999){
            return Math.max(sourcetemp,current);
        }
        return current;
    }
    public static float diffBlockTemp(float curTemp,World world,BlockPos pos){
        float t = getBlockRelativeTemp(world,pos);
        if(t==curTemp){
            return 0;
        }
        if(Math.abs(t-curTemp) > 50){
            return (t-curTemp)/50;
        }else{
            return (t-curTemp)/10;
        }
    }
    public static int getMeterQuantity(float temp){
        int max = 54;
        int maxtemp = 2500;
        int cold = 19;
        int warm = 34;
        int hot = 43;
        int shot = 49;
        int[] s = {0,19,34,43,49,53};
        if(temp<=18){
            return ((int)temp+1);
        }
        if(temp<=35){
            return ((int)(19 + ((35-18)*0.88)));
        }
        if(temp<=63){
            return ((int)(34 + (temp/7)));
        }
        if(temp<=900){
            return ((int)(43+ (temp/150)));
        }
        return ((int)(49 + (temp / 300)));
    }
}
