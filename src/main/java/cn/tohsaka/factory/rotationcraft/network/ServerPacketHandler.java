package cn.tohsaka.factory.rotationcraft.network;

import cn.tohsaka.factory.librotary.packet.ICustomPacketHandler;
import cn.tohsaka.factory.librotary.packet.PacketCustom;
import cn.tohsaka.factory.rotationcraft.api.IGuiPacketHandler;
import cn.tohsaka.factory.rotationcraft.prefab.tile.TileMachineBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.play.INetHandlerPlayServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;

/**
 * Created by covers1624 on 20/05/2017.
 */
public class ServerPacketHandler implements ICustomPacketHandler.IServerPacketHandler {

    @Override
    public void handlePacket(PacketCustom packet, EntityPlayerMP sender, INetHandlerPlayServer handler) {
        switch (packet.getType()) {
            case 3: {
                BlockPos pos = packet.readPos();
                TileEntity tile = sender.world.getTileEntity(pos);
                if (tile instanceof TileMachineBase) {
                    ((TileMachineBase) tile).handleModePacket(packet.readNBTTagCompound());
                } else {
                    System.out.println("Received Machine mode packet for tile that is not a Machine... Pos: " + pos.toString());
                }
                break;
            }

            case 4: {
                BlockPos pos = packet.readPos();
                TileEntity tile = sender.world.getTileEntity(pos);
                if (tile instanceof TileMachineBase) {
                    ((TileMachineBase) tile).handleDataUpload(packet.readNBTTagCompound());
                } else {
                    System.out.println("Received Machine mode packet for tile that is not a Machine... Pos: " + pos.toString());
                }
                break;
            }

            case 99:
                BlockPos pos = packet.readPos();
                TileEntity tile = sender.world.getTileEntity(pos);
                if(tile instanceof IGuiPacketHandler){
                    NetworkDispatcher.dispatchGuiChanges(sender,(IGuiPacketHandler) tile,true);
                }
                return;
        }
    }
}