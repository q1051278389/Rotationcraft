package cn.tohsaka.factory.rotationcraft.items;

import cn.tohsaka.factory.librotary.api.power.interfaces.IMeterable;
import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.item.ItemBase;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import cn.tohsaka.factory.rotationcraft.utils.RecipeHelper;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@GameInitializer(after = ItemScrewer.class)
public class ItemMeter extends ItemBase implements IModelRegister {
    public static final ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"meter");
    public ItemMeter(){
        setRegistryName(NAME);
        setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
        ForgeRegistries.ITEMS.register(this);
    }
    public static void init(){
        ItemMeter item = new ItemMeter();
        RotationCraft.INSTANCE.items.put(NAME,item);
        RecipeHelper.addShapelessRecipe(new ItemStack(item),new ItemStack(RotationCraft.items.get(ItemScrewer.NAME)),new ItemStack(Items.REDSTONE));
    }
    @Override
    public EnumActionResult onItemUse(EntityPlayer player, World world, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        TileEntity te = world.getTileEntity(pos);
        if(te == null || !(te instanceof IMeterable)){
            return EnumActionResult.FAIL;
        }
        if(!world.isRemote){
            player.sendMessage(new TextComponentString(((IMeterable)te).getMeterInfo()));
        }else {
            ((IMeterable)te).onMeter();
        }
        return EnumActionResult.SUCCESS;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(RotationCraft.MOD_ID+":meter"));
    }

    public static void postinit(){

    }

}
