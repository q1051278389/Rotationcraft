package cn.tohsaka.factory.rotationcraft.items;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IGuiProvider;
import cn.tohsaka.factory.rotationcraft.gui.item.filter.ContainerFilter;
import cn.tohsaka.factory.rotationcraft.gui.item.filter.GuiFilter;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.item.ItemBase;
import cn.tohsaka.factory.rotationcraft.proxy.GuiHandler;
import cn.tohsaka.factory.rotationcraft.utils.FluidHelper;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import cn.tohsaka.factory.rotationcraft.utils.RecipeHelper;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.*;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@GameInitializer
public class ItemFilter extends ItemBase implements IModelRegister, IGuiProvider {
    public static final ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"filter");
    public static ItemStack normal;
    public static ItemStack inverted;
    public ItemFilter(){
        setRegistryName(NAME);
        setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
        setHasSubtypes(true);
        this.setMaxStackSize(1);
        ForgeRegistries.ITEMS.register(this);
    }
    public static void init(){
        ItemFilter item = new ItemFilter();
        RotationCraft.INSTANCE.items.put(NAME,item);
        normal = new ItemStack(item,1,0);
        inverted = new ItemStack(item,1,1);
        RecipeHelper.addShapedRecipe(normal,"ABA",'A', new ItemStack(Blocks.IRON_BARS),'B',new ItemStack(Blocks.TRAPDOOR));
        RecipeHelper.addShapelessRecipe(inverted,normal);
        RecipeHelper.addShapelessRecipe(normal,inverted);
    }

    @Override
    public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
        if(isInCreativeTab(tab)){
            items.add(normal);
            items.add(inverted);
        }
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
        super.addInformation(stack, worldIn, tooltip, flagIn);
        if(stack.hasTagCompound() && stack.getTagCompound().hasKey("items")){
            NBTTagList tag = stack.getTagCompound().getTagList("items", Constants.NBT.TAG_COMPOUND);
            if(tag!=null){
                for (int i = 0; i < tag.tagCount(); i++) {
                    ItemStack stack1 = new ItemStack(tag.getCompoundTagAt(i));
                    tooltip.add(" - " + stack1.getDisplayName());
                }

            }
        }
    }

    @Override
    public String getUnlocalizedName(ItemStack stack) {
        return super.getUnlocalizedName(stack)+"."+stack.getMetadata();
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(NAME,"normal"));
        ModelLoader.setCustomModelResourceLocation(this, 1, new ModelResourceLocation(NAME,"inverted"));
    }


    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand) {
        if (!world.isRemote && hand == EnumHand.MAIN_HAND) {
            player.openGui(RotationCraft.MOD_ID, GuiHandler.FILTER_GUI, world, (int) player.posX, (int) player.posY, (int) player.posZ);
        }
        return new ActionResult<>(EnumActionResult.PASS, player.getHeldItem(hand));
    }


    public IInventory inventory;

    @Override
    public Object getGuiClient(EntityPlayer player) {
        ItemStack stack = player.getHeldItem(EnumHand.MAIN_HAND);
        return new GuiFilter(player);
    }

    @Override
    public Object getGuiServer(EntityPlayer player) {
        ItemStack stack = player.getHeldItem(EnumHand.MAIN_HAND);
        return new ContainerFilter(player);
    }



    public static FilterInv createNewContainer(ItemStack stack){
        return new FilterInv(stack);
    }
    public static Set<ItemStack> getMarked(ItemStack filter){
        HashSet<ItemStack> result = new HashSet<>();
        if(!filter.hasTagCompound() || !filter.getTagCompound().hasKey("items")){
            return result;
        }
        NBTTagList tag = filter.getTagCompound().getTagList("items", Constants.NBT.TAG_COMPOUND);
        if(tag!=null){
            for (int i = 0; i < tag.tagCount(); i++) {
                result.add(new ItemStack(tag.getCompoundTagAt(i)));
            }
        }
        return result;
    }

    public static Set<Fluid> getMarkedFluid(ItemStack filter){
        HashSet<Fluid> result = new HashSet<>();
        if(!filter.hasTagCompound() || !filter.getTagCompound().hasKey("items")){
            return result;
        }
        NBTTagList tag = filter.getTagCompound().getTagList("items", Constants.NBT.TAG_COMPOUND);
        if(tag!=null){
            for (int i = 0; i < tag.tagCount(); i++) {
                ItemStack stack = new ItemStack(tag.getCompoundTagAt(i));
                FluidStack stack1 = FluidHelper.getFluidStackFromHandler(stack);
                if(stack1!=null){
                    result.add(stack1.getFluid());
                }
            }
        }
        return result;
    }

    public static class FilterInv implements IInventory{
        ItemStack stack;
        public FilterInv(ItemStack stack){
            slots = new ItemStack[getSizeInventory()];
            this.stack = stack;
            load();
        }
            public ItemStack[] slots;
            @Override
            public int getSizeInventory() {
            return 27;
        }

            @Override
            public boolean isEmpty() {

            return !stack.hasTagCompound() || !stack.getTagCompound().hasKey("items");
        }

        @Override
        public ItemStack getStackInSlot(int index) {
            return slots[index].copy();
        }

        @Override
        public ItemStack decrStackSize(int index, int count) {
            setInventorySlotContents(index,ItemStack.EMPTY);
            return ItemStack.EMPTY;
        }

        @Override
        public ItemStack removeStackFromSlot(int index) {
            setInventorySlotContents(index,ItemStack.EMPTY);
            return ItemStack.EMPTY;
        }

        @Override
        public void setInventorySlotContents(int index, ItemStack stack) {
            if(index>=0 && slots.length>index){
                slots[index] = stack.copy();
                slots[index].setCount(1);
            }
        }

        @Override
        public int getInventoryStackLimit() {
            return 0;
        }

        @Override
        public void markDirty() {
            closeInventory(null);
        }

        @Override
        public boolean isUsableByPlayer(EntityPlayer player) {
            return false;
        }

        @Override
        public void openInventory(EntityPlayer player) {
            load();
        }


        public void load(){
            Arrays.fill(slots,ItemStack.EMPTY);
            if(!isEmpty()){
                NBTTagList tag = stack.getTagCompound().getTagList("items", Constants.NBT.TAG_COMPOUND);
                if(tag!=null){
                    for (int i = 0; i < tag.tagCount(); i++) {
                        setInventorySlotContents(i,new ItemStack(tag.getCompoundTagAt(i)));
                    }
                }
            }
        }

        public void save(){
            if(Arrays.stream(slots).allMatch(ItemStack::isEmpty)){
                stack.setTagCompound(null);
                return;
            }
            NBTTagList tagList = new NBTTagList();
            for(int i=0;i<getSizeInventory();i++){
                if(!slots[i].isEmpty()){
                    tagList.appendTag(slots[i].serializeNBT());
                }
            }
            NBTTagCompound tagCompound = new NBTTagCompound();
            tagCompound.setTag("items",tagList);
            stack.setTagCompound(tagCompound);
        }

        @Override
        public void closeInventory(EntityPlayer player) {
            if(player==null || player.isServerWorld()){
                save();
            }
        }

        @Override
        public boolean isItemValidForSlot(int index, ItemStack stack) {
            return true;
        }

        @Override
        public int getField(int id) {
            return 0;
        }

        @Override
        public void setField(int id, int value) {

        }

        @Override
        public int getFieldCount() {
            return 0;
        }

        @Override
        public void clear() {
            stack.setTagCompound(null);
        }

        @Override
        public String getName() {
            return stack.getDisplayName();
        }

        @Override
        public boolean hasCustomName() {
            return true;
        }

        @Override
        public ITextComponent getDisplayName() {
            return stack.getTextComponent();
        }



    }
}
