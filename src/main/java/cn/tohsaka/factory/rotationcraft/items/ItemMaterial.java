package cn.tohsaka.factory.rotationcraft.items;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.item.ItemBase;
import cn.tohsaka.factory.rotationcraft.proxy.ClientProxy;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.oredict.OreDictionary;

import javax.annotation.Nullable;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@GameInitializer(after = Void.class)
public class ItemMaterial extends ItemBase implements IModelRegister {
    public static String NAME = "material";
    public static ResourceLocation NAMERL = new ResourceLocation(RotationCraft.MOD_ID,NAME);
    public static int itemcounts = 88;
    public static Map<Integer,ItemMaterial> materialMap = new HashMap<>();
    public static ItemMaterial itemMaterial;
    public int meta;
    public ItemMaterial(){
        this.setMaxStackSize(64);
        this.setNoRepair();
    }
    public static void init(){
        for(int i=0;i<itemcounts;i++){
            itemMaterial = new ItemMaterial();
            itemMaterial.setUnlocalizedName(NAME+i);
            itemMaterial.setRegistryName(NAME+i);
            materialMap.put(i,itemMaterial);
            itemMaterial.meta = i;
            ForgeRegistries.ITEMS.register(itemMaterial);
            setExtra(itemMaterial);
        }
        RotationCraft.INSTANCE.items.put(NAMERL,itemMaterial);
    }

    private static void setExtra(ItemMaterial itemMaterial) {
        if(FMLCommonHandler.instance().getSide() == Side.CLIENT){
            itemMaterial.setCreativeTab(ClientProxy.TAB_MATERIAL);
        }
        if(itemMaterial.meta==1){
            OreDictionary.registerOre("ingotHsla",itemMaterial);
        }
        if(itemMaterial.meta==34){
            OreDictionary.registerOre("ingotBedrock",itemMaterial);
        }
        if(itemMaterial.meta==35){
            OreDictionary.registerOre("ingotAluminum",itemMaterial);
        }
        if(itemMaterial.meta==36){
            OreDictionary.registerOre("ingotTungsten",itemMaterial);
        }
        if(itemMaterial.meta==41){
            OreDictionary.registerOre("ingotSteel",itemMaterial);
        }
        if(itemMaterial.meta==41){
            OreDictionary.registerOre("ingotPlatinum",itemMaterial);
        }
        if(itemMaterial.meta==47){
            OreDictionary.registerOre("dustCoal",itemMaterial);
        }
    }

    @Override
    public int getItemBurnTime(ItemStack itemStack) {
        ItemMaterial material = ((ItemMaterial)itemStack.getItem());
        if(material.meta==38){
            return 1800;
        }
        if(material.meta==60){
            return 1200;
        }

        return super.getItemBurnTime(itemStack);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerModel() {
        for(int i : materialMap.keySet()){
            ModelLoader.setCustomModelResourceLocation(materialMap.get(i),0,new ModelResourceLocation(RotationCraft.MOD_ID+":materials/material_"+i));
        }
    }

    public static void postinit(){

    }

    public static ItemMaterial getItemById(int id){
        return materialMap.get(id);
    }
    public static ItemStack getStackById(int id){
        return getStackById(id,1);
    }
    public static ItemStack getStackById(int id,int amount){
        return new ItemStack(materialMap.get(id),amount,0);
    }

    public String toRecipeStr(int count){
        return RotationCraft.MOD_ID+":"+"material"+meta+","+count;
    }

    public static int getStackId(ItemStack stack){
        if(!(stack.getItem() instanceof ItemMaterial)){
            return -1;
        }
        return ((ItemMaterial)stack.getItem()).meta;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
        super.addInformation(stack, worldIn, tooltip, flagIn);
        if(stack.hasTagCompound() && stack.getTagCompound().hasKey("magnet")){
            DecimalFormat df = new DecimalFormat("#0.00000");
            tooltip.add(df.format(stack.getTagCompound().getDouble("magnet"))+" Tesla");
        }
    }
}
