package cn.tohsaka.factory.rotationcraft.items.tools;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import cn.tohsaka.factory.rotationcraft.utils.IteratorAABB;
import net.minecraft.block.BlockFarmland;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class ItemBedrockHoe extends ItemHoe implements IModelRegister {
    public static ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"bedrock_hoe");

    protected ItemBedrockHoe() {
        super(ItemBedrockPickaxe.material);
        setRegistryName(NAME);
        setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
        ForgeRegistries.ITEMS.register(this);
    }

    public static void init(){
        ItemBedrockHoe item = new ItemBedrockHoe();
        RotationCraft.INSTANCE.items.put(NAME,item);
    }

    @Override
    public boolean isDamageable() {
        return false;
    }

    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(RotationCraft.MOD_ID+":bedrock_hoe"));
    }

    @Override
    public void setDamage(ItemStack stack, int damage) {
        super.setDamage(stack, 0);
    }

    @Override
    public EnumRarity getRarity(ItemStack stack) {
        return EnumRarity.EPIC;
    }

    @Override
    protected void setBlock(ItemStack stack, EntityPlayer player, World worldIn, BlockPos pos, IBlockState state) {
        IteratorAABB aabb = new IteratorAABB(pos.add(-2,0,-2),5,1,5);
        worldIn.playSound(player, pos, SoundEvents.ITEM_HOE_TILL, SoundCategory.BLOCKS, 1.0F, 1.0F);
        if (!worldIn.isRemote) {
            while (aabb.hasNext()){
                BlockPos p = aabb.next();
                IBlockState s = worldIn.getBlockState(p);
                if(s.getBlock() == Blocks.GRASS || s.getBlock() == Blocks.GRASS_PATH || s.getBlock() == Blocks.DIRT){
                    if(state.getBlock() instanceof BlockFarmland){
                        worldIn.setBlockState(p, state.withProperty(BlockFarmland.MOISTURE, 7), 2);
                    }else {
                        worldIn.setBlockState(p, state, 11);
                    }
                }
            }
        }
    }

    public static ItemStack getEnchantStack(){
        ItemStack stack = new ItemStack(RotationCraft.items.get(NAME));
        return stack;
    }
}
