package cn.tohsaka.factory.rotationcraft.items.tools;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import com.google.common.collect.ImmutableList;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

import static net.minecraft.item.ItemSpade.EFFECTIVE_ON;

public class ItemBedrockShovel extends ItemTool implements IModelRegister {
    public static ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"bedrock_shovel");

    public ItemBedrockShovel() {
        super(ItemBedrockPickaxe.material, EFFECTIVE_ON);
        setRegistryName(NAME);
        setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
        ForgeRegistries.ITEMS.register(this);
    }

    public static void init(){
        ItemBedrockShovel item = new ItemBedrockShovel();
        RotationCraft.INSTANCE.items.put(NAME,item);
    }

    @Override
    public boolean isDamageable() {
        return false;
    }

    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(RotationCraft.MOD_ID+":bedrock_shovel"));
    }

    @Override
    public void setDamage(ItemStack stack, int damage) {
        super.setDamage(stack, 0);
    }

    @Override
    public EnumRarity getRarity(ItemStack stack) {
        return EnumRarity.EPIC;
    }

    public static ItemStack getEnchantStack(){
        ItemStack stack = new ItemStack(RotationCraft.items.get(NAME));
        return stack;
    }

    /**
     * Called before a block is broken.  Return true to prevent default block harvesting.
     * <p>
     * Note: In SMP, this is called on both client and server sides!
     *
     * @param itemstack The current ItemStack
     * @param pos       Block's position in world
     * @param player    The Player that is wielding the item
     * @return True to prevent harvesting, false to continue as normal
     */
    @Override
    public boolean onBlockStartBreak(ItemStack itemstack, BlockPos pos, EntityPlayer player) {
        if(!player.isSneaking() && !player.world.isRemote){
            World world = player.getEntityWorld();
            for(BlockPos tpos:calcAOEBlocks(itemstack,player.world,player,pos,3,3,1)){
                player.world.destroyBlock(tpos,true);
            }
        }
        return super.onBlockStartBreak(itemstack,pos,player);
    }

    public static ImmutableList<BlockPos> calcAOEBlocks(ItemStack stack, World world, EntityPlayer player, BlockPos origin, int width, int height, int depth) {
        return calcAOEBlocks(stack, world, player, origin, width, height, depth, -1);
    }

    public static ImmutableList<BlockPos> calcAOEBlocks(ItemStack stack, World world, EntityPlayer player, BlockPos origin, int width, int height, int depth, int distance) {

        // find out where the player is hitting the block
        IBlockState state = world.getBlockState(origin);

        if(state.getMaterial() == Material.AIR) {
            // what are you DOING?
            return ImmutableList.of();
        }

        // raytrace to get the side, but has to result in the same block
        RayTraceResult mop = (stack.getItem()).rayTrace(world, player, true);
        if(mop == null || !origin.equals(mop.getBlockPos())) {
            mop = (stack.getItem()).rayTrace(world, player, false);
            if(mop == null || !origin.equals(mop.getBlockPos())) {
                return ImmutableList.of();
            }
        }

        // we know the block and we know which side of the block we're hitting. time to calculate the depth along the different axes
        int x, y, z;
        BlockPos start = origin;
        switch(mop.sideHit) {
            case DOWN:
            case UP:
                // x y depends on the angle we look?
                Vec3i vec = player.getHorizontalFacing().getDirectionVec();
                x = vec.getX() * height + vec.getZ() * width;
                y = mop.sideHit.getAxisDirection().getOffset() * -depth;
                z = vec.getX() * width + vec.getZ() * height;
                start = start.add(-x / 2, 0, -z / 2);
                if(x % 2 == 0) {
                    if(x > 0 && mop.hitVec.x - mop.getBlockPos().getX() > 0.5d) {
                        start = start.add(1, 0, 0);
                    }
                    else if(x < 0 && mop.hitVec.x - mop.getBlockPos().getX() < 0.5d) {
                        start = start.add(-1, 0, 0);
                    }
                }
                if(z % 2 == 0) {
                    if(z > 0 && mop.hitVec.z - mop.getBlockPos().getZ() > 0.5d) {
                        start = start.add(0, 0, 1);
                    }
                    else if(z < 0 && mop.hitVec.z - mop.getBlockPos().getZ() < 0.5d) {
                        start = start.add(0, 0, -1);
                    }
                }
                break;
            case NORTH:
            case SOUTH:
                x = width;
                y = height;
                z = mop.sideHit.getAxisDirection().getOffset() * -depth;
                start = start.add(-x / 2, -y / 2, 0);
                if(x % 2 == 0 && mop.hitVec.x - mop.getBlockPos().getX() > 0.5d) {
                    start = start.add(1, 0, 0);
                }
                if(y % 2 == 0 && mop.hitVec.y - mop.getBlockPos().getY() > 0.5d) {
                    start = start.add(0, 1, 0);
                }
                break;
            case WEST:
            case EAST:
                x = mop.sideHit.getAxisDirection().getOffset() * -depth;
                y = height;
                z = width;
                start = start.add(-0, -y / 2, -z / 2);
                if(y % 2 == 0 && mop.hitVec.y - mop.getBlockPos().getY() > 0.5d) {
                    start = start.add(0, 1, 0);
                }
                if(z % 2 == 0 && mop.hitVec.z - mop.getBlockPos().getZ() > 0.5d) {
                    start = start.add(0, 0, 1);
                }
                break;
            default:
                x = y = z = 0;
        }

        ImmutableList.Builder<BlockPos> builder = ImmutableList.builder();
        for(int xp = start.getX(); xp != start.getX() + x; xp += x / MathHelper.abs(x)) {
            for(int yp = start.getY(); yp != start.getY() + y; yp += y / MathHelper.abs(y)) {
                for(int zp = start.getZ(); zp != start.getZ() + z; zp += z / MathHelper.abs(z)) {
                    // don't add the origin block
                    if(xp == origin.getX() && yp == origin.getY() && zp == origin.getZ()) {
                        continue;
                    }
                    if(distance > 0 && MathHelper.abs(xp - origin.getX()) + MathHelper.abs(yp - origin.getY()) + MathHelper.abs(
                            zp - origin.getZ()) > distance) {
                        continue;
                    }
                    BlockPos pos = new BlockPos(xp, yp, zp);
                    builder.add(pos);
                }
            }
        }

        return builder.build();
    }

}
