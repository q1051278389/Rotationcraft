package cn.tohsaka.factory.rotationcraft.items.tools;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.etc.tree.TreeHandler;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import com.google.common.collect.Multimap;
import net.minecraft.block.BlockFalling;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Enchantments;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;


public class ItemBedrockAxe extends ItemAxe implements IModelRegister {
    public static ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"bedrock_axe");

    protected ItemBedrockAxe() {
        super(ItemBedrockPickaxe.material,30F,2F);
        setRegistryName(NAME);
        setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
        ForgeRegistries.ITEMS.register(this);
    }

    public static void init(){
        ItemBedrockAxe item = new ItemBedrockAxe();
        RotationCraft.INSTANCE.items.put(NAME,item);
    }

    @Override
    public boolean canHarvestBlock(IBlockState block) {
        if(block.getBlock() instanceof BlockFalling){
            return true;
        }
        return super.canHarvestBlock(block);
    }

    @Override
    public boolean isDamageable() {
        return false;
    }


    @Override
    public Multimap<String, AttributeModifier> getAttributeModifiers(EntityEquipmentSlot slot, ItemStack stack) {
        Multimap<String, AttributeModifier> map = super.getAttributeModifiers(slot, stack);
        if (slot == EntityEquipmentSlot.MAINHAND) {
            map.put(SharedMonsterAttributes.ATTACK_DAMAGE.getName(), new AttributeModifier(ATTACK_DAMAGE_MODIFIER, "Tool modifier", 20D, 0));
            map.put(SharedMonsterAttributes.ATTACK_SPEED.getName(), new AttributeModifier(ATTACK_SPEED_MODIFIER, "Tool modifier", 4D, 0));
        }

        return map;
    }

    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(RotationCraft.MOD_ID+":bedrock_axe"));
    }

    @Override
    public void setDamage(ItemStack stack, int damage) {
        super.setDamage(stack, 0);
    }

    @Override
    public EnumRarity getRarity(ItemStack stack) {
        return EnumRarity.EPIC;
    }


    public static ItemStack getEnchantStack(){
        ItemStack stack = new ItemStack(RotationCraft.items.get(NAME));
        stack.addEnchantment(Enchantments.SILK_TOUCH,1);
        return stack;
    }

    @Override
    public boolean onBlockStartBreak(ItemStack itemstack, BlockPos pos, EntityPlayer player) {
        if(!player.isSneaking() && !player.world.isRemote){
            TreeHandler.getTree(player.world, pos).ifPresent(tree -> {
                NonNullList<ItemStack> stacks = NonNullList.create();
                TreeHandler.destroyInstant(tree, player,null);
            });
            return false;
        }
        return super.onBlockStartBreak(itemstack, pos, player);
    }
}
