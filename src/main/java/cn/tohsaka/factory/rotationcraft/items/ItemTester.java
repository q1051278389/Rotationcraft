package cn.tohsaka.factory.rotationcraft.items;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.crafting.scripts.GlobalOre;
import cn.tohsaka.factory.rotationcraft.etc.ItemRender;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.item.ItemBase;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@GameInitializer
public class ItemTester extends ItemBase implements IModelRegister {
    public static final ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"tester");
    public ItemTester(){
        setRegistryName(NAME);
        setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
        ForgeRegistries.ITEMS.register(this);
    }
    public static void init(){
        ItemTester item = new ItemTester();
        RotationCraft.INSTANCE.items.put(NAME,item);

        //ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation("Minecraft:stick"));
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation("minecraft:armor_stand"));
        setTileEntityItemStackRenderer(new ItemRender());
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
        /*int id = new Random().nextInt(Ores.orelist.length);
        String name = Ores.orelist[id];

        if(!worldIn.isRemote){
            ItemStack stack = ItemCrushedOre.createByOre(OreDictionary.getOres(name).get(0),1,0);
            worldIn.spawnEntity(new EntityItem(worldIn,playerIn.posX,playerIn.posY,playerIn.posZ,stack));
        }*/
        if(!worldIn.isRemote){
            ItemStack stack = GlobalOre.next(worldIn.rand);
            playerIn.sendMessage(new TextComponentTranslation(stack.getUnlocalizedName()));
        }
        return new ActionResult(EnumActionResult.PASS, playerIn.getHeldItem(handIn));
    }

}
