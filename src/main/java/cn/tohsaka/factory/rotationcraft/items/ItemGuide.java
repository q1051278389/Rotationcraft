package cn.tohsaka.factory.rotationcraft.items;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.proxy.GuiHandler;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@GameInitializer
public class ItemGuide extends ItemSword implements IModelRegister {
    public static final ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"guide");
    public static ToolMaterial material = EnumHelper.addToolMaterial("book", 3, 0, 0, 7F, 0);
    public ItemGuide(){
        super(material);
        setRegistryName(NAME);
        setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
        ForgeRegistries.ITEMS.register(this);
        setMaxStackSize(1);
        setNoRepair();
    }

    public static void init(){
        ItemGuide item = new ItemGuide();
        RotationCraft.INSTANCE.items.put(NAME,item);
    }

    public static void postinit(){

    }

    public static void loadComplete(){

    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand handIn) {
        player.openGui(RotationCraft.MOD_ID, GuiHandler.GUIDE_BOOK,world,0,0,0);
        return ActionResult.newResult(EnumActionResult.SUCCESS,player.getHeldItem(handIn));
    }

    @Override
    public Multimap<String, AttributeModifier> getItemAttributeModifiers(EntityEquipmentSlot equipmentSlot)
    {
        Multimap<String, AttributeModifier> multimap = HashMultimap.<String, AttributeModifier>create();

        if (equipmentSlot == EntityEquipmentSlot.MAINHAND)
        {
            multimap.put(SharedMonsterAttributes.ATTACK_DAMAGE.getName(), new AttributeModifier(ATTACK_DAMAGE_MODIFIER, "Weapon modifier", (double)this.getAttackDamage(), 0));
            multimap.put(SharedMonsterAttributes.ATTACK_SPEED.getName(), new AttributeModifier(ATTACK_SPEED_MODIFIER, "Weapon modifier", 6D, 0));
        }

        return multimap;
    }
    @SideOnly(Side.CLIENT)
    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(RotationCraft.MOD_ID+":guide"));
    }

}
