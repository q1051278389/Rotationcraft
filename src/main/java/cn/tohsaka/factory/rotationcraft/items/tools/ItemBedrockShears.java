package cn.tohsaka.factory.rotationcraft.items.tools;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Enchantments;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemShears;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatList;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.IShearable;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

import java.util.Iterator;
import java.util.List;
import java.util.Random;


public class ItemBedrockShears extends ItemShears implements IModelRegister {
    public static ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"bedrock_shears");

    protected ItemBedrockShears() {
        setRegistryName(NAME);
        setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
        ForgeRegistries.ITEMS.register(this);
    }

    public static void init(){
        ItemBedrockShears item = new ItemBedrockShears();
        RotationCraft.INSTANCE.items.put(NAME,item);
    }

    @Override
    public boolean isDamageable() {
        return false;
    }

    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(RotationCraft.MOD_ID+":bedrock_shears"));
    }

    @Override
    public void setDamage(ItemStack stack, int damage) {
        super.setDamage(stack, 0);
    }

    @Override
    public EnumRarity getRarity(ItemStack stack) {
        return EnumRarity.EPIC;
    }


    public boolean itemInteractionForEntity(ItemStack itemstack, EntityPlayer player, EntityLivingBase entity, EnumHand hand) {
        if (entity.world.isRemote) {
            return false;
        } else if (!(entity instanceof IShearable)) {
            return false;
        } else {
            IShearable target = (IShearable)entity;
            BlockPos pos = new BlockPos(entity.posX, entity.posY, entity.posZ);
            if (target.isShearable(itemstack, entity.world, pos)) {
                List<ItemStack> drops = target.onSheared(itemstack, entity.world, pos, EnchantmentHelper.getEnchantmentLevel(Enchantments.FORTUNE, itemstack));
                Random rand = new Random();

                EntityItem ent;
                for(Iterator var9 = drops.iterator(); var9.hasNext(); ent.motionZ += (double)((rand.nextFloat() - rand.nextFloat()) * 0.1F)) {
                    ItemStack stack = (ItemStack)var9.next();
                    stack.setCount(stack.getCount()*2);
                    ent = entity.entityDropItem(stack, 1.0F);
                    ent.motionY += (double)(rand.nextFloat() * 0.05F);
                    ent.motionX += (double)((rand.nextFloat() - rand.nextFloat()) * 0.1F);
                }

            }

            return true;
        }
    }

    public boolean onBlockStartBreak(ItemStack itemstack, BlockPos pos, EntityPlayer player) {
        if (!player.world.isRemote && !player.capabilities.isCreativeMode) {
            Block block = player.world.getBlockState(pos).getBlock();
            if (block instanceof IShearable) {
                IShearable target = (IShearable)block;
                if (target.isShearable(itemstack, player.world, pos)) {
                    List<ItemStack> drops = target.onSheared(itemstack, player.world, pos, EnchantmentHelper.getEnchantmentLevel(Enchantments.FORTUNE, itemstack));
                    Random rand = new Random();
                    Iterator var8 = drops.iterator();

                    while(var8.hasNext()) {
                        ItemStack stack = (ItemStack)var8.next();
                        float f = 0.7F;
                        double d = (double)(rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
                        double d1 = (double)(rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
                        double d2 = (double)(rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
                        stack.setCount(stack.getCount()*2);
                        EntityItem entityitem = new EntityItem(player.world, (double)pos.getX() + d, (double)pos.getY() + d1, (double)pos.getZ() + d2, stack);
                        entityitem.setDefaultPickupDelay();
                        player.world.spawnEntity(entityitem);
                    }

                    player.addStat(StatList.getBlockStats(block));
                    player.world.setBlockState(pos, Blocks.AIR.getDefaultState(), 11);
                    return true;
                }
            }

            return false;
        } else {
            return false;
        }
    }
    public static ItemStack getEnchantStack(){
        ItemStack stack = new ItemStack(RotationCraft.items.get(NAME));
        return stack;
    }
}
