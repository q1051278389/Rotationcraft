package cn.tohsaka.factory.rotationcraft.items;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.api.IUpgrade;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.item.ItemBase;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import cn.tohsaka.factory.rotationcraft.utils.RecipeHelper;
import cn.tohsaka.factory.rotationcraft.utils.StringHelper;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.text.DecimalFormat;
import java.util.List;

@GameInitializer
public class ItemUpgrade extends ItemBase implements IUpgrade,IModelRegister {
    public static final ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"upgrade");
    public static final int count = 9;
    public ItemUpgrade(){
        setRegistryName(NAME);
        setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
        setHasSubtypes(true);
        this.setMaxStackSize(1);
        ForgeRegistries.ITEMS.register(this);
    }

    public static void init(){
        ItemUpgrade itemUpgrade = new ItemUpgrade();
        RotationCraft.INSTANCE.items.put(NAME,itemUpgrade);
        addRecipe();
    }

    public static void addRecipe(){
        RecipeHelper.addShapedRecipe(getByMeta(0),"ABA","CDC","ACA",
                'A',ItemMaterial.getStackById(39),
                'B',ItemMaterial.getStackById(63),
                'C',ItemMaterial.getStackById(58),
                'D',ItemMaterial.getStackById(59)
        );
        RecipeHelper.addShapedRecipe(getByMeta(1),"ABA","BCB","ABA",
                'A',ItemMaterial.getStackById(0),
                'B', Items.REDSTONE,
                'C',ItemMaterial.getStackById(5)
        );
        RecipeHelper.addShapedRecipe(getByMeta(2),"ABA","BCB","ABA",
                'A',ItemMaterial.getStackById(0),
                'B', ItemMaterial.getStackById(39),
                'C',ItemMaterial.getStackById(84)
        );
        RecipeHelper.addShapedRecipe(getByMeta(3),"ABA","BCB","ABA",
                'A',ItemMaterial.getStackById(36),
                'B', ItemMaterial.getStackById(37),
                'C',ItemMaterial.getStackById(13)
        );
        RecipeHelper.addShapedRecipe(getByMeta(4),"ABA","CDC","ABA",
                'A',ItemMaterial.getStackById(86),
                'B', ItemMaterial.getStackById(39),
                'C',ItemMaterial.getStackById(57),
                'D',ItemMaterial.getStackById(16)
        );
        RecipeHelper.addShapedRecipe(getByMeta(5),"ABA","BCB","ABA",
                'A',ItemMaterial.getStackById(45),
                'B', ItemMaterial.getStackById(0),
                'C',ItemMaterial.getStackById(69)
        );
        RecipeHelper.addShapedRecipe(getByMeta(6),"ABC","BDB","CBA",
                'A',ItemMaterial.getStackById(7),
                'B', ItemMaterial.getStackById(1),
                'C',ItemMaterial.getStackById(20),
                'D',ItemMaterial.getStackById(59)
        );
    }

    public static ItemStack getByMeta(int id){
        return new ItemStack(RotationCraft.INSTANCE.items.get(NAME),1,id);
    }

    @Override
    public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
        if(isInCreativeTab(tab)){
            for(int i=0;i<count;i++){
                items.add(i,new ItemStack(this,1,i));
                if(i==2){
                    ItemStack stack = new ItemStack(this,1,2);
                    NBTTagCompound tagCompound = new NBTTagCompound();
                    tagCompound.setFloat("magnet",320);
                    stack.setTagCompound(tagCompound);
                    items.add(stack);
                }
            }
        }
    }

    @Override
    public String getUnlocalizedName(ItemStack stack) {
        return super.getUnlocalizedName(stack)+"."+stack.getMetadata();
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerModel() {
        for(int i=0;i<count;i++){
            ModelLoader.setCustomModelResourceLocation(this, i, new ModelResourceLocation(NAME,"upgrade"+i));
        }
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
        super.addInformation(stack, worldIn, tooltip, flagIn);
        if(stack.getMetadata()==2){
            if(stack.hasTagCompound() && stack.getTagCompound().hasKey("magnet")){
                if(stack.getTagCompound().getFloat("magnet")==320){
                    tooltip.add(StringHelper.localize("info.rc.etc.upgrade_readyforuse"));
                }
                DecimalFormat df = new DecimalFormat("#0.00000");
                tooltip.add(df.format(stack.getTagCompound().getFloat("magnet"))+" Tesla");
            }else {
                tooltip.add(StringHelper.localize("info.rc.etc.upgrade_need_magnetize_to_use"));
            }
        }
    }


    @Override
    public boolean readyForUse(ItemStack stack) {
        if(stack.getMetadata()==2){
            return stack.hasTagCompound() && stack.getTagCompound().hasKey("magnet") && stack.getTagCompound().getFloat("magnet") == 320;
        }else {
            return true;
        }
    }


}
