package cn.tohsaka.factory.rotationcraft.items;

import cn.tohsaka.factory.rotationcraft.etc.ItemRender;
import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemBlockModelBased extends ItemBlock {
    public ItemBlockModelBased(Block block) {
        super(block);
        if(FMLCommonHandler.instance().getSide() == Side.CLIENT){
            setRender();
        }
    }
    @SideOnly(Side.CLIENT)
    public void setRender(){
        setTileEntityItemStackRenderer(ItemRender.INSTANCE());
    }
    
    @Override
    public int getMetadata(int damage) {
        return damage;
    }
}
