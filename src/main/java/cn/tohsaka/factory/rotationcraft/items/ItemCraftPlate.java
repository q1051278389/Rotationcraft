package cn.tohsaka.factory.rotationcraft.items;

import cn.tohsaka.factory.rotationcraft.RotationCraft;
import cn.tohsaka.factory.rotationcraft.init.annotations.GameInitializer;
import cn.tohsaka.factory.rotationcraft.prefab.item.ItemBase;
import cn.tohsaka.factory.rotationcraft.proxy.GuiHandler;
import cn.tohsaka.factory.rotationcraft.utils.IModelRegister;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import net.minecraft.block.BlockWorkbench;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ContainerWorkbench;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatList;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@GameInitializer
public class ItemCraftPlate extends ItemBase implements IModelRegister {
    public static final ResourceLocation NAME = new ResourceLocation(RotationCraft.MOD_ID,"craftplate");
    public ItemCraftPlate(){
        setRegistryName(NAME);
        setUnlocalizedName(RotationCraft.MOD_ID + "." + NAME.getResourcePath());
        ForgeRegistries.ITEMS.register(this);
        setMaxStackSize(1);
        setNoRepair();
    }

    public static void init(){
        ItemCraftPlate item = new ItemCraftPlate();
        RotationCraft.INSTANCE.items.put(NAME,item);
    }


    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand handIn) {
        if (!world.isRemote){
            player.displayGui(new BlockWorkbench.InterfaceCraftingTable(world, null){
                @Override
                public Container createContainer(InventoryPlayer p_createContainer_1_, EntityPlayer p_createContainer_2_) {
                    return new ContainerWorkbench(p_createContainer_1_, player.world, null){
                        @Override
                        public boolean canInteractWith(EntityPlayer p_canInteractWith_1_) {
                            return true;
                        }
                    };
                }
            });
            player.addStat(StatList.CRAFTING_TABLE_INTERACTION);
        }
        return ActionResult.newResult(EnumActionResult.SUCCESS,player.getHeldItem(handIn));
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerModel() {
        ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(RotationCraft.MOD_ID+":craftplate"));
    }
}
